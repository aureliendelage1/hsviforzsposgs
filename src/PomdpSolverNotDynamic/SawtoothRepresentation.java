package PomdpSolverNotDynamic;

import posg.History;
import posg.Pair;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.max;
import static java.lang.Math.min;
public class SawtoothRepresentation<State,Action,Observation> implements Serializable {


    private int player;
    public static class Tooth<State,Action,Observation> implements Serializable {
        private static final long serialVersionUID = 1L;


        public Belief<State,Action,Observation> vertex;
        public final double value;
        public Tooth(Belief<State,Action,Observation> vertex, double value) {
            if(vertex == null) {
                throw new NullPointerException("The vertex of a tooth cannot be null");
            }
            this.vertex = vertex;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Tooth)) return false;
            Tooth tooth = (Tooth) o;
            if (Double.compare(tooth.value, value) != 0) return false;
            return vertex.equals(tooth.vertex);
        }

        @Override
        public int hashCode() {
            int result = vertex.hashCode();
            long temp = Double.doubleToLongBits(value);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }
    private static final long serialVersionUID = 1L;
    private boolean representMax;
    private boolean pruning;
    private int depth;


    private HashMap<Integer, HashMap<State,Double>> cornerValues;
    private HashMap<Integer,Set<Tooth>> teeth;
    private HashMap<Integer,Set<Tooth>> newTeeth;
    private int nbOfTeethInLastPruning;

    public SawtoothRepresentation(boolean representMax, boolean pruning, int depth, HashMap<Integer, HashMap<State,Double>> cornerValues, int player) {


        this.pruning = pruning;
        this.depth = depth;
        teeth = new HashMap<>();
        newTeeth = new HashMap<>();
        nbOfTeethInLastPruning = 0;
        this.cornerValues = cornerValues;
        if (player%2==1){
            this.representMax = true;
        }
        else{
            this.representMax = false;
        }

    }

    public void addImprovingTooth(Tooth tooth, int timestep) {

        if(tooth == null) {
            throw new NullPointerException("The added point can't be null");
        }
        if (!teeth.containsKey(timestep)){
            HashSet<Tooth> hashSet = new HashSet<>();
            this.teeth.put(timestep,hashSet);
        }
        teeth.get(timestep).add(tooth);

    }

    public void addTooth(Tooth tooth, int timestep) {
        if(tooth == null) {
            throw new NullPointerException("The added point can't be null");
        }
        if(!isPointDominated(tooth,timestep)) {
            addImprovingTooth(tooth,timestep);
        }
    }

    public double getValue(Belief<State,Action,Observation> point) {

        double x0 = this.scalarProduct(cornerValues,point);
        double approximationValue = x0;
        if (teeth.containsKey(point.timestep)) {
            for (Tooth tooth : teeth.get(point.timestep)) {

                double temp = getValueForPointKnowingX0(point, tooth, x0);
                if (representMax) {
                    approximationValue = min(approximationValue, temp);
                }
                else{
                    approximationValue = max(approximationValue, temp);
                }
            }
        }

        return approximationValue;
    }

    private double scalarProduct(HashMap<Integer, HashMap<State, Double>> cornerValues, Belief<State,Action,Observation> point) {
        double sum = 0.0;

        for (State s : point.getBelief().getNonZeroElements()){
            if (point.getBelief().getProbability(s)>0.0) {
                sum += point.getBelief().getProbability(s) * cornerValues.get(point.timestep).get(s);
            }
        }
        return sum;
    }

    private double getValueForPoint(Belief<State,Action,Observation> point, Tooth tooth) {
        double x0 = this.scalarProduct(cornerValues,point);

        return getValueForPointKnowingX0(point, tooth, x0);
    }

    private double getValueForPointKnowingX0(Belief<State,Action,Observation> point, Tooth tooth, double x0) {


        double miniPhi = Double.POSITIVE_INFINITY;


        for (State s : cornerValues.get(point.timestep).keySet()){


            if ((double) tooth.vertex.getBelief().getWeight(s)>0.0){

                miniPhi = min(miniPhi, (point.getBelief().getWeight(s) / (double) tooth.vertex.getBelief().getWeight(s)));
            }
        }
        return x0 + miniPhi*(tooth.value - scalarProduct(cornerValues,tooth.vertex));
    }

    private void prune() {


    }
    public double getMaxSlope() {
        prune();

        double maxSlope = Double.NEGATIVE_INFINITY;

        return maxSlope;
    }
    public boolean isPointDominated(Tooth tooth, int timestep) {
        return isPointDominatedByPointCollection(tooth, teeth.get(timestep));
    }


    private boolean isPointDominatedByNewPoints(Tooth tooth, int timestep) {
        return isPointDominatedByPointCollection(tooth, newTeeth.get(timestep));
    }
    private boolean isPointDominatedByPointCollection(Tooth tooth, Collection<Tooth> toothCollection) {
        for(Tooth knownTooth : toothCollection) {
            if(knownTooth.equals(tooth)) {
                continue;
            }
            if(isPointDominatedByPoint(tooth,knownTooth)) {
                return true;
            }
        }
        if(representMax && (scalarProduct(cornerValues,tooth.vertex)< tooth.value)) {
            return true;
        } else if(!representMax && (scalarProduct(cornerValues,tooth.vertex) > tooth.value)) {
            return true;
        }
        return false;
    }

    private boolean isPointDominatedByPoint(Tooth tooth1, Tooth tooth2) {
        double value = getValueForPoint(tooth1.vertex,tooth2);
        if(representMax) {
            return (value >= tooth1.value);
        } else {
            return (value <= tooth1.value);
        }
    }

    private void writeObject(final ObjectOutputStream stream) throws IOException {
        prune();
        stream.writeObject(representMax);

        stream.writeObject(cornerValues);
        stream.writeObject(teeth);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SawtoothRepresentation)) return false;
        SawtoothRepresentation that = (SawtoothRepresentation) o;
        if (representMax != that.representMax) return false;

        if (cornerValues != null ? !cornerValues.equals(that.cornerValues) : that.cornerValues != null) return false;
        prune();
        that.prune();
        return teeth.equals(that.teeth);
    }
    public int getSize() {
        return teeth.size();
    }

}