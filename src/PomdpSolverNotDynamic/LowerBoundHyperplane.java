package PomdpSolverNotDynamic;

import posg.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LowerBoundHyperplane<State,Action,Observation> extends ValueFunctionBound<State,Action,Observation> {

    private Posg<State,Action,Observation> posg;
    private int player;
    private HashMap<Integer, ArrayList<Hyperplane<State,Action,Observation>>> setOfHyperplanes = new HashMap<>();

    public LowerBoundHyperplane(Posg<State,Action,Observation> posg, int player, double discount, boolean pruning, int depth) throws Exception {
        super(false,posg,discount,pruning,player, depth);
        this.posg = posg;
        this.player = player;
        this.Initialize(posg.profondeurMax);
    }

    public double ComputeMaxValue(Belief<State,Action,Observation> b){
        double res = -10000.0;
        for (Hyperplane<State,Action,Observation> hyperplane : this.setOfHyperplanes.get(b.timestep)){
            double tmp = hyperplane.computeValue(b);
            if (tmp>res){
                res = tmp;
            }
        }
        return res;
    }

    @Override
    public void update(Belief<State, Action, Observation> belief, double value) throws Exception {
        HashMap<Pair<Action,Observation>, Hyperplane<State,Action,Observation>> map = new HashMap<>();

        if (belief.timestep<posg.profondeurMax-1) {
            for (Action action : this.posg.getActions(player)) {
                for (Observation o : this.posg.getObservations(player)) {

                    if(posg.getConditionalProbabilityOfObservation(belief,action,o,player)>0.0) {

                        map.put(new Pair<>(action, o), this.getNextBestVector(belief.getNewBelief(action, o)));
                    }

                }
            }
        }

        HashMap<Action,HashMap<State, Double>> betaA = new HashMap<>();
        for (Action a : this.posg.getActions(player)) {
            betaA.put(a,this.computeBetaA(belief, map,a));
        }
        HashMap<State, Double> argmaxHashmap = new HashMap<>();
        if (player%2==1) {
            double max = -Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double sum = 0.0;
                for (Map.Entry mapEntry : betaA.get(a).entrySet()) {
                    State s = (State) mapEntry.getKey();
                    sum += (double) mapEntry.getValue() * belief.getBelief().getWeight(s);
                }
                if (sum > max) {
                    max = sum;
                    argmaxHashmap = betaA.get(a);
                }
            }
        }
        else{
            double max = Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double sum = 0.0;
                for (Map.Entry mapEntry : betaA.get(a).entrySet()) {
                    State s = (State) mapEntry.getKey();
                    sum += (double) mapEntry.getValue() * belief.getBelief().getWeight(s);
                }
                if (sum < max) {
                    max = sum;
                    argmaxHashmap = betaA.get(a);
                }
            }
        }
        Hyperplane<State,Action,Observation> argmaxHyperplane = new Hyperplane<>(posg,player, belief.timestep, argmaxHashmap);

        this.setOfHyperplanes.get(belief.timestep).add(argmaxHyperplane);
    }


    private boolean isObservable(Belief<State, Action, Observation> belief, Action action, Observation o) {
        System.out.println("LowerBoundHyperplane::isObservable : method not done");
        System.exit(1);


        return true;
    }

    private HashMap<State,Double> computeBetaA(
            Belief<State,Action,Observation> belief, HashMap<Pair<Action,Observation>, Hyperplane<State,Action,Observation>> map, Action action) throws Exception {
        HashMap<State,Double> hashMapRes = new HashMap<>();

        for (State s : belief.getBelief().getNonZeroElements()){
            double reward = this.posg.getRewardTangent(action,s,player);

            double valueNextStep = 0.0;
            if (belief.timestep < posg.profondeurMax-1) {
                for (State nextState : this.posg.getStates()) {
                    for (Observation o : this.posg.getObservations(player)){
                        if (posg.getConditionalProbabilityOfObservation(belief,action,o,player)>0.0) {

                                valueNextStep += this.posg.getObservationProbability(action, nextState, o, player)
                                      * this.posg.getTransitionProbability(s, action, nextState, player)

                                    * map.get(new Pair<>(action, o)).values.get(nextState);

                        }
                    }
                }
            }
            hashMapRes.put(s,reward+1.0*valueNextStep);
        }
        return hashMapRes;
    }

    @Override
    public double getValue(Belief<State, Action, Observation> belief) {
        if (belief.timestep == depth){
            return 0.0;
        }
        if (player%2==1) {
            double res = -Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> hyperplane : this.setOfHyperplanes.get(belief.timestep)) {
                double val = hyperplane.computeValue(belief);
                if (val > res) {
                    res = val;
                }
            }
            return res;
        }
        else{
            double res = Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> hyperplane : this.setOfHyperplanes.get(belief.timestep)) {
                double val = hyperplane.computeValue(belief);
                if (val < res) {
                    res = val;
                }
            }
            return res;
        }
    }

    @Override
    public int getSize() {

        return this.setOfHyperplanes.get(0).size();
    }

    @Override
    public double getMaxSlope() {

        return 0;
    }

    private Hyperplane<State,Action,Observation> getNextBestVector(Belief<State,Action,Observation> newBelief){
        Hyperplane<State,Action,Observation> hyperplane = new Hyperplane<>(posg,player, newBelief.timestep);
        if (player%2==1) {
            double res = -Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> h : this.setOfHyperplanes.get(newBelief.timestep)) {
                double tmp = h.computeValue(newBelief);
                if (tmp > res) {
                    res = tmp;
                    hyperplane = h;
                }
            }
            return hyperplane;
        }
        else{
            double res = Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> h : this.setOfHyperplanes.get(newBelief.timestep)) {
                double tmp = h.computeValue(newBelief);
                if (tmp < res) {
                    res = tmp;
                    hyperplane = h;
                }
            }
            return hyperplane;
        }
    }

    private void Initialize(int depth) throws Exception {
        OccupancyState<State,Action,Observation> initialOccupancyState = new OccupancyState<State,Action,Observation>(posg);
        initialOccupancyState.InitialBelief = posg.initialBelief;
        OccupancyState<State,Action,Observation> oprime = initialOccupancyState.copy();

        this.initTimestep0(depth);

        for (int timestep = 1;timestep<this.posg.profondeurMax;timestep++){
            this.init(timestep, depth, player);
        }
    }

    private void init(int timestep, int depth, int player) {
        ArrayList<Hyperplane<State,Action,Observation>> listHyperplanes = new ArrayList<>();
        double rMaxMin;
        if (player%2==1){

            rMaxMin = posg.computeMinReward();
        }
        else{
            rMaxMin = posg.computeMaxReward();
        }
        HashMap<State,Double> hashMap = new HashMap();
        for (State s : this.posg.getStates()){
            hashMap.put(s,rMaxMin*(depth-timestep));
        }
        Hyperplane<State,Action,Observation> hyperplaneInit = new Hyperplane<>(posg,player,timestep,hashMap);
        listHyperplanes.add(hyperplaneInit);
        this.setOfHyperplanes.put(timestep,listHyperplanes);

    }

    private void initTimestep0(int depth) {
        ArrayList<Hyperplane<State,Action,Observation>> arrayList = new ArrayList<>();
        HashMap<State, Double> hashMap = new HashMap<>();
        double rMaxMin;
        if (player%2==1){
            rMaxMin = this.posg.computeMinReward()*depth;
        }
        else{
            rMaxMin = this.posg.computeMaxReward()*depth;
        }
        for (State s : posg.getStates()){
            hashMap.put(s,rMaxMin);
        }
        Hyperplane<State,Action,Observation> h = new Hyperplane<>(posg,player,0,hashMap);
        arrayList.add(h);
        this.setOfHyperplanes.put(0,arrayList);
    }
}
