package PomdpSolverNotDynamic;

import posg.*;
import util.Distribution;
import util.MatrixX;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class Belief<State,Action,Observation> {
    private Distribution<State> belief = new Distribution<>();
    private Posg<State, Action, Observation> posg;
    private int player;
    public int timestep;

    public Belief(Posg<State,Action,Observation> posg, Distribution<State> distribution, int timestep, int player) {
        this.posg = posg;
        this.belief = distribution;
        this.timestep = timestep;
        this.player = player;
    }

    public Distribution<State> getBelief(){
        return belief;
    }
    public Belief(Posg<State,Action,Observation> posg, int player, int timestep) {
        this.posg = posg;
        History<Action,Observation> hPlayerI = new History<>();
        History<Action,Observation> hPlayerNegI = new History<>();
        Distribution<Pair<State, History<Action, Observation>>> distribution = new Distribution<>();
        for (State s : posg.getStates()){
            this.belief.addWeight(s,posg.initialBelief.getWeight(s));
        }
        this.timestep = timestep;
    }

    public MatrixX toMatrixX(Collection<State> collection){
        System.out.println("method not done : toMatrixX");
        System.exit(1);
        double[][] A = new double[0][];
        return new MatrixX(A);
    }

    public Belief<State,Action,Observation> getNewBelief(Action actionPlayerI, Observation zPlayerI) throws Exception {
        Distribution<State> distribution = this.getNewDistrib(this.belief,actionPlayerI,zPlayerI);
        try {
            distribution.sanityCheck();
        }
        catch(Exception e){

            e.printStackTrace();
        }

        return new Belief<>(posg,distribution,this.timestep+1,player);
    }

    private Distribution<State> getNewDistrib(Distribution<State> distrib, Action actionPlayerI, Observation zPlayerI) {
        if (distrib.getNonZeroElements().size() == 0){
            for (State s : this.posg.getStates()){
                distrib.addWeight(s,this.posg.initialBelief.getProbability(s));
            }
        }
        Distribution<State> newDistrib = new Distribution<>();
        for (State s : this.posg.getStates()){
            try {
                newDistrib.addWeight(s,this.computeValue(s,distrib,actionPlayerI,zPlayerI));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            newDistrib.sanityCheck();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDistrib;
    }

    private double computeValue(State nextState, Distribution<State> distrib, Action actionPlayerI, Observation zPlayerI) throws Exception {


        double numerator = 0.0;
        for (State s : this.posg.getStates()) {
            numerator += this.posg.getObservationProbability(actionPlayerI, nextState, zPlayerI, player)
                    * this.posg.getTransitionProbability(s, actionPlayerI, nextState, player)
                    * distrib.getProbability(s);
        }


        double denominator = 0.0;
        for (State sprime : this.posg.getStates()) {
            for (State startState : this.posg.getStates()){
                denominator +=  posg.getObservationProbability(actionPlayerI,sprime,zPlayerI,player)
                        * this.posg.getTransitionProbability(startState,actionPlayerI,sprime,player)
                        * distrib.getWeight(startState);
            }
        }
        if (denominator>0.0) {
            return numerator / denominator;
        }
        else{
            return 0.0;
        }
    }

    public ArrayList<History<Action,Observation>> groupByState(Set<Pair<State,History<Action,Observation>>> pairList){
        ArrayList<History<Action,Observation>> res = new ArrayList<>();

        for (Pair<State,History<Action,Observation>> pair : pairList){
            if (!res.contains((pair.getElement1()))){
                res.add(pair.getElement1());
            }
        }
        return res;
    }

}
