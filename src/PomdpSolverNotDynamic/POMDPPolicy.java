package PomdpSolverNotDynamic;


public interface POMDPPolicy<State,Action,Observation> {
    Action getBestAction(Belief<State,Action,Observation> belief) throws Exception;
}
