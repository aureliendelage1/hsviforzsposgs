package PomdpSolverNotDynamic;

import posg.*;
import util.Distribution;
import util.MatrixX;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class Belief2<State,Action,Observation> {
    public  Distribution<Pair<State,History<Action,Observation>>> belief;
    public SigmaOccupancyState<State,Action,Observation> s;
    public BehavioralStrategy<State,Action,Observation> strat;
    private Posg<State, Action, Observation> posg;
    private int player;
    public int timestep;
    private ArrayList<History<Action,Observation>> listHistories;

    public Belief2(Posg<State,Action,Observation> posg, int timestep, int player,
                   SigmaOccupancyState<State,Action,Observation> s, BehavioralStrategy<State,Action,Observation> strat,
                   Distribution<Pair<State,History<Action,Observation>>> distrib) {
        this.posg = posg;
        this.belief = distrib;
        this.timestep = timestep;
        this.player = player;
        this.s = s;
        this.strat = strat;
        this.listHistories = this.groupBy(player);
        this.belief = distrib;
    }

    private ArrayList<History<Action,Observation>> groupBy(int player) {
        ArrayList<History<Action, Observation>> list = new ArrayList<>();
        if (player%2==1) {
            for (Pair<State, JointHistory<Action, Observation>> pair : this.s.o.getDistrib().getNonZeroElements()) {
                if (!list.contains(pair.getElement1().getHistJ2())){
                    list.add(pair.getElement1().getHistJ2());
                }
            }
        }
        else{
            for (Pair<State, JointHistory<Action, Observation>> pair : this.s.o.getDistrib().getNonZeroElements()) {
                if (!list.contains(pair.getElement1().getHistJ1())){
                    list.add(pair.getElement1().getHistJ1());
                }
            }
        }
        if (list.size() == 0){
            list.add(new History<>());
        }
        return list;
    }

    public MatrixX toMatrixX(Collection<State> collection){
        System.out.println("method not done : toMatrixX");
        System.exit(1);
        double[][] A = new double[0][];
        return new MatrixX(A);
    }

    public Belief<State,Action,Observation> getNewBelief(Action actionPlayerI, Observation zPlayerI) throws Exception {
        Distribution<State> distribution = this.getNewDistrib(this.belief,actionPlayerI,zPlayerI);
        try {
            distribution.sanityCheck();
        }
        catch(Exception e){

            e.printStackTrace();
        }

        return new Belief<State,Action,Observation>(posg,distribution,this.timestep+1,player);
    }

    private Distribution<State> getNewDistrib(Distribution<Pair<State, History<Action, Observation>>> distrib, Action actionPlayerI, Observation zPlayerI) {
        if (distrib.getNonZeroElements().size() == 0){
            History<Action,Observation> hVoid = new History<>();
            for (State s : this.posg.getStates()){
                distrib.addWeight(new Pair<>(s,hVoid),this.posg.initialBelief.getProbability(s));
            }
        }
        Distribution<State> newDistrib = new Distribution<>();
        for (State s : this.posg.getStates()){
            try {
                newDistrib.addWeight(s,this.computeValue(s,distrib,actionPlayerI,zPlayerI));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            newDistrib.sanityCheck();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDistrib;
    }

    private double computeValue(State nextState, Distribution<Pair<State, History<Action, Observation>>> distrib, Action actionPlayerI, Observation zPlayerI) throws Exception {

        double numerator = 0.0;
        for (State s : this.posg.getStates()){
            for (History<Action,Observation> hNegI : this.listHistories) {
                for (Action a : this.posg.getActions(player + 1 % 2)) {
                    for (Observation z : this.posg.getObservations(player + 1 % 2)){

                        numerator += this.posg.transitions.getProbability(s, new JointAction(actionPlayerI, a, player), nextState)
                                * this.strat.getStrategy().get(hNegI).getProbability(a)
                                * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI, a, player), nextState,
                                new JointObservation(zPlayerI, z, player))
                                *this.belief.getProbability(new Pair<State,History<Action,Observation>>(s,hNegI));


                    }
                }
            }
        }

        double denominator = 0.0;
        for (State sprime : this.posg.getStates()) {
            for (State s : this.posg.getStates()){
                for (History<Action,Observation> hNegI : this.listHistories) {
                    for (Action a : this.posg.getActions(player + 1 % 2)) {
                        for (Observation z : this.posg.getObservations(player + 1 % 2)) {
                            denominator += posg.transitions.getProbability(s, new JointAction(actionPlayerI, a, player), sprime)
                                    * this.strat.getStrategy().get(hNegI).getProbability(a)
                                    * this.belief.getProbability(new Pair<>(s, hNegI))
                                    * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI, a, player), sprime,
                                    new JointObservation(zPlayerI, z, player));

                        }
                    }
                }
            }
        }

        if (denominator>0.0) {
            return numerator / denominator;
        }
        else{
            return 0.0;
        }
    }

    public ArrayList<History<Action,Observation>> groupByState(Set<Pair<State,History<Action,Observation>>> pairList){
        ArrayList<History<Action,Observation>> res = new ArrayList<>();

        for (Pair<State,History<Action,Observation>> pair : pairList){
            if (!res.contains((pair.getElement1()))){
                res.add(pair.getElement1());
            }
        }
        return res;
    }

}
