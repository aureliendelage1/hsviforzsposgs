package PomdpSolverNotDynamic;

import posg.Pair;
import posg.Posg;

public class HSVI<State,Action,Observation> {
    final public ValueFunctionBound<State,Action,Observation> upperBound;

    final public ValueFunctionBound<State,Action,Observation> lowerBound;

    private double gamma;

    private double errorMargin;

    private Boolean exLXU;

    private Boolean exNUI;

    private Posg<State,Action,Observation> pomdp;
    private int player;


    public HSVI(Posg<State,Action,Observation> pomdp,
                double discount,
                double errorMargin,
                ValueFunctionBound<State,Action,Observation> lowerBound,
                ValueFunctionBound<State,Action,Observation> upperBound,
                Boolean exLXU,
                Boolean exNUI, int player) {
        this.gamma = 1.0;
        this.errorMargin = errorMargin;
        this.pomdp = pomdp;

        this.upperBound = upperBound;
        this.lowerBound = lowerBound;

        this.exLXU = exLXU;
        this.exNUI = exNUI;

        this.player = player;
    }

    public double getOptimalValue(Belief<State,Action,Observation> belief, long time, long timelimit, int iterations) throws Exception {
        solve(belief, time, timelimit, iterations);
        return lowerBound.getValue(belief);
    }


    public ValueFunctionBound<State, Action, Observation> solve(Belief<State,Action,Observation> belief, long time, long timelimit, int iterations) {
        int i=0;
        try{

            while(getWidth(belief) >= 0.001
                    && i != iterations) {


                System.out.format("#- %d %.1f %f %f %d %d\n",
                        i, (System.currentTimeMillis()-time)/1000.,
                        lowerBound.getValue(belief),upperBound.getValue(belief),
                        lowerBound.getSize(), upperBound.getSize());


                explore(belief,0.001,0, timelimit);
                i++;
            }
            System.out.format("## %d %.1f %f %f %d %d %f\n", i, (System.currentTimeMillis()-time)/1000.,
                    lowerBound.getValue(belief),upperBound.getValue(belief),
                    lowerBound.getSize(), upperBound.getSize(), getMaxSlope());


            if (player%2==1) {
                return upperBound;
            }
            else{
                return upperBound;
            }

        } catch (IllegalStateException exception) {
            System.out.format("#! %d %.1f %f %f %d %d %f\n", i, (System.currentTimeMillis()-time)/1000.,
                    lowerBound.getValue(belief),upperBound.getValue(belief),
                    lowerBound.getSize(), upperBound.getSize(), getMaxSlope());
            throw exception;
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (player%2==1){
            return  upperBound;
        }
        else{
            return upperBound;
        }
    }

    private double getWidth(Belief<State,Action,Observation> belief) {


        return Math.abs(upperBound.getValue(belief) - lowerBound.getValue(belief));
    }

    private void explore(Belief<State,Action,Observation> belief, double currentErrorMargin, int depth, long timelimit) throws Exception {
        if ((getWidth(belief) < currentErrorMargin) || depth == pomdp.profondeurMax) {


            return;
        }

        updateBounds(belief);

        if (depth<pomdp.profondeurMax-1) {
            Pair<Action, Observation> path = getForwardExplorationHeuristic(belief, currentErrorMargin);

            Belief<State, Action, Observation> newBelief = belief.getNewBelief(path.getElement0(), path.getElement1());


            explore(newBelief, currentErrorMargin, depth + 1, timelimit);

        }

        if (timelimit > 0 && System.currentTimeMillis() >= timelimit) {
            return;
        }

        updateBounds(belief);
    }

    private void updateBounds(Belief<State,Action,Observation> belief) throws Exception {

        double maxiQUpperValue = upperBound.getBellman(belief).getElement1();
        double maxiQLowerValue = lowerBound.getBellman(belief).getElement1();


        double LB = lowerBound.getValue(belief);

        double UB = upperBound.getValue(belief);


        exNUI = false;
        if(exNUI)
            if(UB + 0.00001 < maxiQUpperValue
                    || LB - 0.00001 > maxiQLowerValue) {
                System.out.format("!! NUI: %f %f %f %f\n",
                        LB, maxiQLowerValue,
                        UB, maxiQUpperValue);

                throw new IllegalStateException("NUI: The bounds aren't upper/lower bounds");
            }

        upperBound.update(belief,maxiQUpperValue);
        lowerBound.update(belief,maxiQLowerValue);
    }

    private Pair<Action,Observation> getForwardExplorationHeuristic(Belief<State,Action,Observation> belief, double currentErrorMargin) throws Exception {

        Action maxiAction;
        maxiAction = upperBound.getBestAction(belief);
        double maxiObservationValue = Double.NEGATIVE_INFINITY;
        Observation maxiObservation = null;

        for (Observation observation : pomdp.getObservations(player)){

            double observationValue = pomdp.getConditionalProbabilityOfObservation(belief,maxiAction,observation,player);
            if (observationValue > 0.0) {

                observationValue *= getWidth(belief.getNewBelief(maxiAction, observation)) - (currentErrorMargin / gamma);


            if(maxiObservationValue < observationValue) {
                maxiObservation = observation;
                maxiObservationValue = observationValue;
            }
            }
        }

        return new Pair<>(maxiAction,maxiObservation);
    }

    public Posg<State,Action,Observation> getPOMDP() {
        return pomdp;
    }

    public double getMaxSlope() {
        double u = upperBound.getMaxSlope();
        double l = lowerBound.getMaxSlope();

        return (l>u)?l:u;
    }

}