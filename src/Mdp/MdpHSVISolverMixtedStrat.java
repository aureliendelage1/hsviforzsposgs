package Mdp;

import posg.*;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;

public class MdpHSVISolverMixtedStrat<State,Action,Observation> {

    public HashMap<DecisionRule<State,Action,Observation>, Double> strat;
    public int depth;
    public Posg<State,Action,Observation> posg;
    public double epsilon;
    HashMap<Pair<State, History<Action,Observation>>, Double> majorantValueFunction = new HashMap<>();
    HashMap<Pair<State, History<Action,Observation>>, Double> minorantValueFunction = new HashMap<>();
    public Pair<State,History<Action,Observation>> initialState;
    public int player;
    int it = 0;

    public int initialTimestep;

    public MdpHSVISolverMixtedStrat(HashMap<DecisionRule<State,Action,Observation>, Double> strat, int depth, Posg<State,Action,Observation> posg, double epsilon, int player,
                         Pair<State,History<Action,Observation>> initialState, int initialTimestep){
        this.strat = strat;
        this.depth = depth;
        this.posg = posg;
        this.epsilon = epsilon;
        this.player = player;
        this.initialState = initialState;
        this.initialTimestep = initialTimestep-1;
    }

    public double solve(){
        this.initialize();

        while (Math.abs(this.getMajorantValue(initialState) - this.getMinorantValue(initialState)) > epsilon && (it <10)){

            this.explore(initialState,1);
            it++;
        }


        return this.majorantValueFunction.get(initialState);
    }

    public void explore(Pair<State,History<Action,Observation>> State, int t){
        if (t==depth || Math.abs(this.getMajorantValue(State) - this.getMinorantValue(State)) < epsilon){
            return;
        }
        this.update(State,t);
        Action bestAction = this.getBestAction(State, t);
        Pair<State,History<Action,Observation>> nextState = this.getGreatedWidthState(State, bestAction, t);

        this.explore(nextState,t+1);

        this.update(State,t);
        return;
    }

    public Pair<State, History<Action,Observation>> getGreatedWidthState(Pair<State,History<Action,Observation>> state, Action bestAction, int timestep) {

        Pair<State,History<Action,Observation>> stateGreatestWidth = new Pair<>();
        double max = - Double.POSITIVE_INFINITY;
        for (State x : this.posg.getStates()){
            for (Action aNegI : this.posg.getActions(player+1%2)){
                for (Observation zNegI : this.posg.getObservations(player+1%2)){
                    History<Action,Observation> nextHistory = new History<>();
                    nextHistory.setListeActionObservation(new ArrayList(state.getElement1().getListeActionObservation()));
                    nextHistory.addActionObservation(aNegI,zNegI);
                    Pair<State,History<Action,Observation>> nextState = new Pair<>(x, nextHistory);
                    double val =  this.getProba(state,nextState,bestAction,timestep) * this.getWidth(nextState);

                    if (val>max){
                        max = val;
                        stateGreatestWidth = new Pair<>(x,new History<>(nextState.getElement1().getListeActionObservation()));
                    }
                }
            }
        }

        return stateGreatestWidth;

    }

    public double getWidth(Pair<State, History<Action,Observation>> nextState) {
        return ( Math.abs(this.getMajorantValue(nextState) - this.getMinorantValue(nextState)) - epsilon);
    }

    public Action getBestAction(Pair<State,History<Action,Observation>> State, int timestep) {
        if (player%2==1) {
            Action aStar = (Action) this.posg.getActions(player).toArray()[0];
            double max = -Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double val = this.computeValue(State, true, a,timestep);
                if (val > max) {
                    max = val;
                    aStar = a;
                }
            }
            return aStar;
        }
        else{
            Action aStar = (Action) this.posg.getActions(player).toArray()[0];
            double max = Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double val = this.computeValue(State, true, a,timestep);
                if (val < max) {
                    max = val;
                    aStar = a;
                }
            }
            return aStar;
        }
    }


    public void initialize(){
        if (player%2==1) {
            this.majorantValueFunction.put(initialState, posg.maxReward * (depth));
            this.minorantValueFunction.put(initialState, posg.minReward * (depth));
        }
        else{
            this.majorantValueFunction.put(initialState, posg.minReward * (depth));
            this.minorantValueFunction.put(initialState, posg.maxReward * (depth));
        }
    }


    public void update(Pair<State,History<Action,Observation>> State, int timestep){
        if (player%2==1) {

            Action aMax = (Action) this.posg.getActions(player).toArray()[0];
            double max = -Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double val = this.computeValue(State, true, a, timestep);
                if (val > max) {
                    aMax = a;
                    max = val;
                }
            }

            this.majorantValueFunction.put(State, max);


            double maxMinornat = -Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double val = this.computeValue(State, false, a, timestep);
                if (val > maxMinornat) {
                    aMax = a;
                    maxMinornat = val;
                }
            }

            this.minorantValueFunction.put(State, maxMinornat);


        }
        else{

            Action aMax = (Action) this.posg.getActions(player).toArray()[0];
            double max = Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double val = this.computeValue(State, true, a, timestep);
                if (val < max) {
                    aMax = a;
                    max = val;
                }
            }

            this.majorantValueFunction.put(State, max);


            double maxMinornat = Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double val = this.computeValue(State, false, a, timestep);
                if (val < maxMinornat) {
                    aMax = a;
                    maxMinornat = val;
                }
            }

            this.minorantValueFunction.put(State, maxMinornat);


        }
    }

    public double computeValue(Pair<State, History<Action,Observation>> state, boolean majorant, Action a, int timestep) {
        double reward = this.computeReward(state,a, timestep);
        double valueNextStep = 0.0;
        for (State s : this.posg.getStates()){
            for (Action aNegI : this.posg.getActions(player+1%2)){
                for (Observation zNegI : this.posg.getObservations(player+1%2)){
                    History<Action,Observation> nextHistory = new History<>();
                    nextHistory.setListeActionObservation(new ArrayList(state.getElement1().getListeActionObservation()));
                    nextHistory.addActionObservation(aNegI,zNegI);
                    Pair<State,History<Action,Observation>> nextState = new Pair<>(s,nextHistory);
                    if (majorant == true) {
                        valueNextStep += this.getProba(state, nextState,a, timestep) * this.getMajorantValue(nextState);
                    }
                    else{
                        valueNextStep += this.getProba(state, nextState,a, timestep) * this.getMinorantValue(nextState);
                    }
                }
            }
        }

        return reward + valueNextStep;
    }

    public double computeReward(Pair<State, History<Action,Observation>> state, Action a, int timestep) {
        double res = 0.0;


        for (Action aNegI : this.posg.getActions(player+1%2)){
            if (this.computeProbaMixtedStrat(strat,state.getElement1())>0.0) {
                res +=
                    this.computeProbaMixtedStrat(strat,state.getElement1(),aNegI)
                    * this.posg.rewards.getReward(state.getElement0(), new JointAction(a, aNegI, player), posg);
            }
            else{

                res += (float)(1.0/this.posg.getActions(player+1%2).size())
                        *this.posg.rewards.getReward(state.getElement0(),new JointAction(a,aNegI,player),posg);
            }


        }

        return res;
    }

    public double getProba(Pair<State, History<Action,Observation>> state, Pair<State, History<Action,Observation>> nextState, Action a, int timestep) {
        double res = 0.0;
        for (Observation z : this.posg.getObservations(player)){
            if (this.computeProbaMixtedStrat(strat,nextState.getElement1())>0.0) {
                res += this.posg.transitions.getProbability(state.getElement0(), new JointAction(a, nextState.getElement1().getLastAction(), player), nextState.getElement0())

                        * this.computeProbaMixtedStrat(strat,state.getElement1(),nextState.getElement1().getLastAction())
                        * this.posg.observationsProbabilities.getProba(new JointAction(a, nextState.getElement1().getLastAction(), player), nextState.getElement0(),
                        new JointObservation(z, nextState.getElement1().getLastObservation(), player));
            }
            else{


                res += this.posg.transitions.getProbability(state.getElement0(),new JointAction(a, nextState.getElement1().getLastAction(), player), nextState.getElement0())
                        * (float)(1.0/this.posg.getActions(player+1%2).size())
                        * this.posg.observationsProbabilities.getProba(new JointAction(a,nextState.getElement1().getLastAction(),player),nextState.getElement0(),
                        new JointObservation(z, nextState.getElement1().getLastObservation(),player));
            }


        }

        return res;
    }

    public double getMajorantValue(Pair<State,History<Action,Observation>> state){
        if (player%2==1) {
            double res = 0.0;

            int timestep = state.getElement1().getListeActionObservation().size();
            if (timestep >= depth) {
                return 0.0;
            }
            if (!this.majorantValueFunction.containsKey(state)) {
                this.majorantValueFunction.put(state, this.posg.maxReward * (depth - timestep));
            }
            return this.majorantValueFunction.get(state);
        }
        else{
            double res = 0.0;

            int timestep = state.getElement1().getListeActionObservation().size();
            if (timestep >= depth) {
                return 0.0;
            }
            if (!this.majorantValueFunction.containsKey(state)) {
                this.majorantValueFunction.put(state, this.posg.minReward * (depth - timestep));
            }
            return this.majorantValueFunction.get(state);
        }
    }

    private double getMinorantValue(Pair<State, History<Action,Observation>> state) {
        if (player%2==1) {
            double res = 0.0;
            int timestep = state.getElement1().getListeActionObservation().size();
            if (timestep >= depth) {
                return 0.0;
            }
            if (!this.minorantValueFunction.containsKey(state)) {
                this.minorantValueFunction.put(state, this.posg.minReward * (depth - timestep));
            }
            return this.minorantValueFunction.get(state);
        }
        else{
            double res = 0.0;
            int timestep = state.getElement1().getListeActionObservation().size();
            if (timestep >= depth) {
                return 0.0;
            }
            if (!this.minorantValueFunction.containsKey(state)) {
                this.minorantValueFunction.put(state, this.posg.maxReward * (depth - timestep));
            }
            return this.minorantValueFunction.get(state);
        }
    }

    private double computeProbaMixtedStrat(HashMap<DecisionRule<State,Action,Observation>, Double> strat, History<Action,Observation> element1) {
        double res = 0.0;
        for (Action a : this.posg.getActions(player+1%2)){
            res += this.computeProbaMixtedStrat(strat,element1,a);
        }
        return res;
    }

    private double computeProbaMixtedStrat(HashMap<DecisionRule<State,Action,Observation>, Double> strat, History<Action,Observation> element1, Action aNegI) {
        double res = 0.0;

        double denominator = 0.0;
        double numerator = 0.0;
        for (DecisionRule<State,Action,Observation> decisionRule : strat.keySet()){
            for (History<Action,Observation> h : decisionRule.decisionRule.keySet()){
                if (h.equals(element1)){
                    denominator += strat.get(decisionRule);
                    if (decisionRule.decisionRule.get(h).equals(aNegI)){
                        numerator+= strat.get(decisionRule);
                    }
                }
            }
        }
        if (denominator==0.0){
            return 0.0;
        }
        else{


            return numerator/denominator;
        }
    }

}
