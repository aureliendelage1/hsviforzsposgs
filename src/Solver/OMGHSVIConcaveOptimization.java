package Solver;

import Algorithm.*;
import Approximations.MajorantPointBased;
import Approximations.MinorantPointBased;
import posg.OccupancyState;
import posg.*;
import util.Distribution;
import util.RandomGenerator;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;

public class OMGHSVIConcaveOptimization<State,Action,Observation> implements OMGHSVI<State,Action,Observation> {

    private MajorantPointBased<State,Action,Observation> majorantPointBased;
    private MinorantPointBased<State,Action,Observation> minorantPointBased;
    private Posg<State,Action,Observation> posg;
    private OccupancyState<State,Action,Observation> initialOccupancyState;
    public double gamma;
    public double lambda;
    public int it = 0;
    public long time;

    public OMGHSVIConcaveOptimization(Posg<State,Action,Observation> POSG, OccupancyState<State,Action,Observation> o, long time){
        this.posg = POSG;
        this.gamma = POSG.discount;
        double valueMax = POSG.computeMaxReward();
        double valueMin = POSG.computeMinReward();

        this.lambda = 2*posg.computeMaxReward();
        majorantPointBased = new MajorantPointBased<State,Action,Observation>(this.lambda,this.gamma,valueMax);
        minorantPointBased = new MinorantPointBased<State,Action,Observation>(this.lambda,this.gamma,valueMin);
        this.initialOccupancyState = o;
	this.time = time;
    }

    public double getWidth(OccupancyState<State,Action,Observation> o, int timestep) throws Exception {
        return Math.abs(majorantPointBased.getValue(o, timestep) - minorantPointBased.getValue(o, timestep));

    }

    @Override
    public void solve(double epsilon, int depth) throws Exception {
        RandomGenerator r = new RandomGenerator();

        this.majorantPointBased.initializeB0(initialOccupancyState, depth);
        this.minorantPointBased.initializeB0(initialOccupancyState, depth);
        this.majorantPointBased.setDepth(depth);
        this.minorantPointBased.setDepth(depth);
        File file = new File("resRandomTmp.txt");
        FileWriter writerRes = new FileWriter(file, false);
        while (getWidth(initialOccupancyState,0)>epsilon){


            System.out.format("\n # %d %d %f %f %d",it,System.currentTimeMillis()-time,this.minorantPointBased.getExactValue(initialOccupancyState.copy(),0),
		this.majorantPointBased.getExactValue(initialOccupancyState.copy(),0),depth);
	    explore(epsilon, initialOccupancyState.copy(),0,depth,epsilon*gamma,r);
	    it++;
        }


        System.out.println("\nbounds at b_0 : " + this.majorantPointBased.getExactValue(initialOccupancyState.copy(),0) + " , "
                + this.minorantPointBased.getExactValue(initialOccupancyState.copy(),0));

        writerRes.close();
    }

    @Override
    public boolean explore(double epsilon, OccupancyState<State,Action,Observation> o, int t, int depth, double stopCriterion, RandomGenerator r) throws Exception {
        DOObackup<State,Action,Observation> doo = new DOObackup<>(epsilon);
        DOObackupMiniMax<State,Action,Observation> dooMiniMax = new DOObackupMiniMax<>(epsilon);


        if (t<depth) {

        }

        if (t==1){
            if (!(this.majorantPointBased.setOfValues.get(1)==null)&& !(this.minorantPointBased.setOfValues.get(1)==null)) {


            }

        }
        if (getWidth(o,t)>epsilon && t<depth){
            double valueMax;
            double valueMin;

            if (o.getTimestep()> 0) {
                boolean cplexLastStep = false;
                if ((o.getTimestep()+1)==depth) {
                    cplexLastStep = true;
                }
                if (!cplexLastStep) {

                    double lambda = posg.computeMaxReward();

                    doo = new DOObackup<>(epsilon);
                    dooMiniMax = new DOObackupMiniMax<>(epsilon);
                    doo.computeStartegy(this.majorantPointBased, this.posg, o.copy(),lambda);
                    HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayer1 = doo.strategyP1;
                    valueMax = doo.finalValue;
                    dooMiniMax.computeStartegy(this.minorantPointBased, this.posg, o.copy(),lambda);
                    HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayer2 = dooMiniMax.strategyP1;

                    OccupancyState<State, Action, Observation> oNext = o.copy();
                    oNext.update(distributionStrategyPlayer1, distributionStrategyPlayer2,true);
                    boolean useless = explore(epsilon, oNext.copy(), t + 1, depth, epsilon, r);


                    dooMiniMax.computeStartegy(this.minorantPointBased, this.posg, o.copy(),lambda);
                    valueMin = dooMiniMax.finalValue;
                    updateBounds(o, valueMax, valueMin, t);
                }
                else{

                    CplexSolverForPosgDirectV2<State,Action,Observation> cplexSolverForPosg = new CplexSolverForPosgDirectV2<>(posg,o.copy());

                    double[][] m = new double[posg.getActionsJ1().size()*o.getLocalHistoryJ1().size()][posg.getActionsJ2().size()*o.getLocalHistoryJ2().size()];

                    this.getUpperBoundGameMatrix(o.copy(),m);

                    for (int i = 0;i<m.length;i++){
                        for (int j = 0;j<m[i].length;j++){

                        }
                    }

                    Pair<Distribution<Action>,Double> pair = cplexSolverForPosg.getActionMax(m);
                    updateBounds(o, pair.getElement1(), pair.getElement1(), t);
                }
            }
            else{

                double lambda = posg.computeMaxReward();
                doo = new DOObackup<>(epsilon);
                dooMiniMax = new DOObackupMiniMax<>(epsilon);

                doo.computeStartegy(this.majorantPointBased,this.posg,o.copy(),lambda);

                HashMap<History<Action, Observation>, Distribution<Action>> strategyP1 = doo.strategyP1;
                dooMiniMax.computeStartegy(this.minorantPointBased,this.posg,o.copy(),lambda);
                HashMap<History<Action, Observation>, Distribution<Action>> strategyP2 = dooMiniMax.strategyP1;
                OccupancyState<State,Action,Observation> oNext = o.copy();
                oNext.initialupdate(strategyP1,strategyP2);
                boolean useless = explore(epsilon,oNext.copy(),t+1,depth,epsilon,r);


                valueMax = doo.finalValue;


                dooMiniMax.computeStartegy(this.minorantPointBased,this.posg,o.copy(),lambda);

                valueMin = dooMiniMax.finalValue;

                updateBounds(o,valueMax,valueMin,t);
            }
        }
        else{

            if (getWidth(o,t)<epsilon){


            }
        }
        return true;
    }

    private double getMaxWidth(int t, int depth) {

        return ((majorantPointBased.rMax*(depth - t) - minorantPointBased.rMin*
                (depth-t))/2);
    }

    private void getUpperBoundGameMatrix(OccupancyState<State,Action,Observation> o, double[][] m) throws Exception {
        for (int i = 0;i<o.getLocalHistoryJ1().size();i++){
            for (int j = 0;j<o.getLocalHistoryJ2().size();j++){


                double[][] Mij = new double[posg.getActionsJ1().size()][posg.getActionsJ2().size()];
                for (int k = 0;k<posg.getActionsJ1().size();k++){
                    for (int l = 0;l<posg.getActionsJ2().size();l++){
                        double sumOverStates = 0.0;
                        if (o.getTimestep()==0){
                            sumOverStates = posg.computeInitRewardFromPureStrategies((Action) posg.getActionsJ1().toArray()[k],(Action) posg.getActionsJ2().toArray()[l],o);

                            Mij[k][l] = sumOverStates;
                        }
                        else{
                            sumOverStates = 0.0;
                            for (State s :  posg.getStates()) {
                                JointHistory<Action,Observation> jointHistory = new JointHistory<Action,Observation>((History<Action,Observation>) o.getLocalHistoryJ1().toArray()[i]
                                        ,(History<Action,Observation>) o.getLocalHistoryJ2().toArray()[j]);
                                Pair<State,JointHistory<Action,Observation>> pair = new Pair<>(s,jointHistory);
                                sumOverStates += o.getDistribution().getWeight(pair)
                            *posg.computeRewardFromPureStrategies((Action) posg.getActionsJ1().toArray()[k],(Action) posg.getActionsJ2().toArray()[l],s);

                            }

                            Mij[k][l] = sumOverStates;
                        }
                        m[i*posg.getActionsJ1().size()+k][j*posg.getActionsJ2().size()+l] =  Mij[k][l];
                    }
                }
            }
        }
    }

    @Override
    public void updateBounds(OccupancyState<State,Action,Observation> o, double valueMax, double valueMin, int timestep) throws Exception {

        this.majorantPointBased.update(o,valueMax,timestep);
        this.minorantPointBased.update(o,valueMin,timestep);

    }

    public void setMajorantPointBased(MajorantPointBased<State,Action,Observation> majorantPointBased) {
        this.majorantPointBased = majorantPointBased;
    }

    public void setMinorantPointBased(MinorantPointBased<State,Action,Observation> minorantPointBased) {
        this.minorantPointBased = minorantPointBased;
    }

    public OccupancyState<State,Action,Observation> getInitialOccupancyState() {
        return initialOccupancyState;
    }

    public void setInitialOccupancyState(OccupancyState<State,Action,Observation> initialOccupancyState) {
        this.initialOccupancyState = initialOccupancyState;
    }

    public void initializeBounds() {


    }


    private void getLowerBoundGameMatrix(OccupancyState<State,Action,Observation> o, double[][] m) throws Exception {


    }
}

