package Solver;

import Algorithm.SolvePomdpWithStrat;
import AlgorithmForLpSolving.CplexSolverForPosgDirectV2ForLpSolver;
import AlgorithmForLpSolving.GameMatrix;
import Approximations.*;
import Pruning.PruningV;
import Pruning.PruningW;
import posg.*;
import util.Distribution;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class LPsolver<State,Action,Observation> {
    private double epsilon;
    private int depth;
    public HashMap<Integer,Double> lambda;
    private Posg<State,Action,Observation> posg;
    private OccupancyState<State,Action,Observation> initialOccupancyState;
    private VectorMajorantApproximation<State,Action, Observation> majorant;
    private VectorMinorantApproximation<State,Action, Observation> minorant;
    private ApproximatorWMajorant<State,Action,Observation> approximatorWMajorant;
    private ApproximatorWMinorant<State,Action,Observation> approximatorWMinorant;
    private int lengthTrajectories;
    long start;
    long end;

    double rho;

    public LPsolver(double epsilon, int depth, HashMap<Integer, Double> lambda, Posg posg, OccupancyState o, long start, long end, double rho){
        this.epsilon = epsilon;
        this.depth = depth;
        this.lambda = lambda;
        this.posg = posg;
        this.initialOccupancyState = o;

        approximatorWMajorant = new ApproximatorWMajorant<State,Action,Observation>(posg);
        approximatorWMinorant = new ApproximatorWMinorant<State,Action,Observation>(posg);

        this.start = start;
        this.end = end;

        this.rho = rho;
    }

    public Pair<BehavioralStrategy<State,Action,Observation>, BehavioralStrategy<State,Action,Observation>> hsviSolve() throws Exception {

        SigmaOccupancyState<State,Action,Observation> s = new SigmaOccupancyState<>();

        majorant = new VectorMajorantApproximation<State,Action,Observation>(depth,lambda, s, posg);
        minorant = new VectorMinorantApproximation<State,Action,Observation>(depth,lambda, s, posg);

        double rMax = this.posg.computeMaxReward();
        double rMin = this.posg.computeMinReward();

        majorant.setrMax(rMax);
        minorant.setrMin(rMin);

        SigmaOccupancyState<State,Action,Observation> sigmaOcc = new SigmaOccupancyState<>(this.initialOccupancyState);

        System.out.println("Bounds at b_0 : " +
			   " [" + majorant.getValue(sigmaOcc) +
			   ", " + minorant.getValue(sigmaOcc) + "]");
        int it = 0;

	System.out.println("\nformat : ## it time lowerBound upperBound");
        Pair<Vector<State,Action,Observation>, Vector<State,Action,Observation>> pairVec = new Pair<>();
        while (getWidth(new SigmaOccupancyState<>(initialOccupancyState))>epsilon && System.currentTimeMillis() < end) {
            it++;
            System.out.format("\n\n## it=%d -- time= %dms -- $\\underline{V}_0(\\sigma_0)$= %+.3f -- $\\overline{V}_0(\\sigma_0)$= %+.3f\n",
			      it, System.currentTimeMillis() - start,
			      minorant.getValue(sigmaOcc), majorant.getValue(sigmaOcc)
			      );
            PrintWriter writer = new PrintWriter((new FileOutputStream(new File("StatsConvergence.txt"),true)));
            writer.write("\nbounds at b_0 : " + majorant.getValue(sigmaOcc) + " , "+ minorant.getValue(sigmaOcc));
            writer.write("\n time : " + (System.currentTimeMillis() - start));
            writer.close();
             pairVec = this.explore(s,this.epsilon, new SigmaOccupancyState<>(initialOccupancyState.copy()), 0, this.depth, it, new Pair<>());

             if (it%1000==0) {
                 History<Action, Observation> voidHistory = new History<>();

                 Distribution<Pair<State, History<Action, Observation>>> d = new Distribution<>();

                 for (State ss : posg.getStates()) {
                     d.addWeight(new Pair<>(ss, voidHistory), posg.initialBelief.getWeight(ss));
                 }

                 Vector<State, Action, Observation> vectorMajorant = this.majorant.getSupportVector(this.initialOccupancyState);

                 Vector<State, Action, Observation> vectorMinorant = this.minorant.getSupportVector(this.initialOccupancyState);


                 Pair<MixtedStrategy<State, Action, Observation>, MixtedStrategy<State, Action, Observation>> pairMixtedStrategies;
                 pairMixtedStrategies = new Pair<>(new MixtedStrategy<State, Action, Observation>(posg, vectorMajorant.realizationWeights, 2),
                         new MixtedStrategy<State, Action, Observation>(posg, vectorMinorant.realizationWeights, 1));
                 //System.out.println("paire mixed strats RW : \n P1 :" + pairMixtedStrategies.getElement1() + " \n P2 :" + pairMixtedStrategies.getElement0());


                 Pair<BehavioralStrategy<State, Action, Observation>, BehavioralStrategy<State, Action, Observation>> pairBehavioralStrategies;
                 pairBehavioralStrategies = new Pair<>(new MixtedStrategy<State, Action, Observation>(posg, vectorMajorant.realizationWeights, 2).behavioralStrat,
                         new MixtedStrategy<State, Action, Observation>(posg, vectorMinorant.realizationWeights, 1).behavioralStrat);
                 pairBehavioralStrategies.getElement0().posg = posg;
                 pairBehavioralStrategies.getElement1().posg = posg;


                 SolvePomdpWithStrat<State, Action, Observation> solvePomdpWithStrat =
                         new SolvePomdpWithStrat<State, Action, Observation>(posg, 1, pairBehavioralStrategies.getElement0().toIntBehavioral());
                 SolvePomdpWithStrat<State, Action, Observation> solvePomdpWithStratP2 =
                         new SolvePomdpWithStrat<State, Action, Observation>(posg, 2, pairBehavioralStrategies.getElement1().toIntBehavioral());


                 PrintWriter writerPomdp = new PrintWriter((new FileOutputStream(new File("StatsConvergencePomdpBis.txt"), true)));
                 writerPomdp.write("\n[ " + solvePomdpWithStrat.solve(d, 0) + " , ");
                 writerPomdp.write(solvePomdpWithStratP2.solve(d, 0) + " ] ");
                 writerPomdp.close();
             }
        }
        System.out.format("\n\n## THE END [after it=%d] -- time= %dms -- $\\underline{V}_0(\\sigma_0)$= %+.3f -- $\\overline{V}_0(\\sigma_0)$= %+.3f\n",

			  it, System.currentTimeMillis() - start,
			  minorant.getValue(sigmaOcc), majorant.getValue(sigmaOcc)
			  );

        Vector<State,Action,Observation> vectorMajorant = this.majorant.getSupportVector(this.initialOccupancyState);

        Vector<State,Action,Observation> vectorMinorant = this.minorant.getSupportVector(this.initialOccupancyState);


        Pair<MixtedStrategy<State,Action,Observation>,MixtedStrategy<State,Action,Observation>> pairMixtedStrategies;
        DecimalFormat sf = new DecimalFormat("0.0000#E0");
        System.out.println("\n##### t=" + 0 +
                " -- $\\sigma$= " + s.o +
                " -- $[lower_bound(\\sigma_0),upper_bound(\\sigma_0)] : " + "["+sf.format(minorant.getValue(new SigmaOccupancyState<>(initialOccupancyState))) +
                ", " + sf.format(majorant.getValue(new SigmaOccupancyState<>(initialOccupancyState)))+"]" +
                " -- $thr(t)$= " + sf.format(this.computeThresHold(0)) +
                "\n"
        );

        Pair<MixtedStrategy<State,Action,Observation>, MixtedStrategy<State,Action,Observation>> pairMixtedStrategiesBruteForce =
                new Pair<>(new MixtedStrategy<State,Action,Observation>(posg,pairVec.getElement0().mixtedStrategy),
                new MixtedStrategy<State,Action,Observation>(posg,pairVec.getElement1().mixtedStrategy));

        //System.out.println("pair mixed strats brute force : \n P1 :" + pairMixtedStrategiesBruteForce.getElement1() + "\n P2 :" + pairMixtedStrategiesBruteForce.getElement0());
        pairMixtedStrategies = new Pair<>(new MixtedStrategy<State,Action,Observation>(posg,vectorMajorant.realizationWeights,2),
                new MixtedStrategy<State,Action,Observation>(posg,vectorMinorant.realizationWeights,1));
        //System.out.println("paire mixed strats RW : \n P1 :" + pairMixtedStrategies.getElement1() + " \n P2 :" + pairMixtedStrategies.getElement0());


        Pair<BehavioralStrategy<State,Action,Observation>,BehavioralStrategy<State,Action,Observation>> pairBehavioralStrategies;
        pairBehavioralStrategies = new Pair<>(new MixtedStrategy<State,Action,Observation>(posg,vectorMajorant.realizationWeights,2).behavioralStrat,
                new MixtedStrategy<State,Action,Observation>(posg,vectorMinorant.realizationWeights,1).behavioralStrat);

        //System.out.println("rw P2 : " + vectorMajorant.realizationWeights + " \n rw P1 : " + vectorMinorant.realizationWeights);
        //System.out.println("paire behavioral strats RW : \n P1 :" + pairBehavioralStrategies.getElement1() + " \n P2 :" + pairBehavioralStrategies.getElement0());

        PrintWriter writer = new PrintWriter((new FileOutputStream(new File("StatsConvergenceBis.txt"),true)));
        writer.write("\nbounds at b_0 : " + majorant.getValue(sigmaOcc) + " , "+ minorant.getValue(sigmaOcc));
        writer.write("\n time : " + (System.currentTimeMillis() - start));
        writer.close();

        History<Action, Observation> voidHistory = new History<>();

        Distribution<Pair<State, History<Action, Observation>>> d = new Distribution<>();

        for (State ss : posg.getStates()) {
            d.addWeight(new Pair<>(ss, voidHistory), posg.initialBelief.getWeight(ss));
        }

        pairBehavioralStrategies.getElement0().posg = posg;
        pairBehavioralStrategies.getElement1().posg = posg;

        SolvePomdpWithStrat<State, Action, Observation> solvePomdpWithStrat =
                new SolvePomdpWithStrat<State, Action, Observation>(posg, 1, pairBehavioralStrategies.getElement0().toIntBehavioral());
        SolvePomdpWithStrat<State, Action, Observation> solvePomdpWithStratP2 =
                new SolvePomdpWithStrat<State, Action, Observation>(posg, 2, pairBehavioralStrategies.getElement1().toIntBehavioral());


        PrintWriter writerPomdp = new PrintWriter((new FileOutputStream(new File("StatsConvergencePomdp.txt"), true)));
        writerPomdp.write("\n[ " + solvePomdpWithStrat.solve(d, 0) + " , ");
        writerPomdp.write(solvePomdpWithStratP2.solve(d, 0) + " ] ");
        writerPomdp.close();

        return pairBehavioralStrategies;

    }

    public Pair<Vector<State,Action,Observation>, Vector<State,Action,Observation>>
	explore( SigmaOccupancyState<State,Action,Observation> lastS,
		 double epsilon,
		 SigmaOccupancyState<State,Action,Observation> s,
		 int t,
		 int depth,
		 int iteration,
         Pair<BehavioralStrategy<State,Action,Observation>, BehavioralStrategy<State,Action,Observation>> pairStrat) throws Exception {

	DecimalFormat df = new DecimalFormat("0.0");

	DecimalFormat sf = new DecimalFormat("0.0000#E0");
	System.out.println("\n##### t=" + t +
			   "\n-- $\\sigma$= " + s.o +
			   "\n-- $width(\\sigma)$= " + sf.format(this.getWidth(s)) +
               "\n--$[lower_bound(\\sigma_0),upper_bound(\\sigma_0)] : [" + minorant.getValue(new SigmaOccupancyState<>(initialOccupancyState)) +
                    ", " + majorant.getValue(new SigmaOccupancyState<>(initialOccupancyState)) +
                    " ] \n-- $thr(t)$= " + sf.format(this.computeThresHold(t)) +
			   "\n"
			   );
        if (this.getWidth(s)>-this.computeThresHold(t) && t < depth){
            OccupancyState<State,Action,Observation> o = s.o;

            if (t == depth -1){
                CplexSolverForPosgDirectV2ForLpSolver<State,Action,Observation> cplexSolverForPosg = new CplexSolverForPosgDirectV2ForLpSolver<>(posg,o.copy());

                double[][] m;

		m = new double[ posg.getActionsJ1().size()*o.getLocalHistoryJ1().size() ]
		    [ posg.getActionsJ2().size()*o.getLocalHistoryJ2().size() ];
                this.getUpperBoundGameMatrix(o.copy(),m);
                Pair<HashMap<History<Action,Observation>,Distribution<Action>>,Double> pair = cplexSolverForPosg.getActionMax(m);

                m = new double[ posg.getActionsJ2().size()*o.getLocalHistoryJ2().size() ]
		    [ posg.getActionsJ1().size()*o.getLocalHistoryJ1().size() ];
                this.getLowerBoundGameMatrix(o.copy(),m);
                Pair<HashMap<History<Action,Observation>,Distribution<Action>>,Double> pair2 = cplexSolverForPosg.getActionMin(m);

                BehavioralStrategy<State,Action,Observation> stratP1 = new BehavioralStrategy<>(o,1,posg);
                stratP1.setStrategy(pair.getElement0());
                BehavioralStrategy<State,Action,Observation> stratP2 = new BehavioralStrategy<>(o,2,posg);
                stratP2.setStrategy(pair2.getElement0());
                stratP2.decompress(new SigmaOccupancyState<>(o),2);
                stratP1.decompress(new SigmaOccupancyState<>(o),1);
                Vector<State,Action,Observation> vP1 = majorant.updateLastStep(lastS, s, stratP2);
                Vector<State,Action,Observation> vP2 = minorant.updateLastStep(lastS, s, stratP1);

                this.lengthTrajectories = t;
                approximatorWMajorant.update(t-1, lastS, pairStrat.getElement1(), vP1);
                approximatorWMinorant.update(t-1, lastS, pairStrat.getElement0(), vP2);

                return new Pair<Vector<State,Action,Observation>, Vector<State,Action,Observation>>(vP1,vP2);
            }
            Pair<BehavioralStrategy<State, Action, Observation>, ArrayList<Double>> strategyP1 = this.approximatorWMajorant.ComputeOptimalStrategy(o);
            GameMatrix<State,Action,Observation> gP1 = this.approximatorWMajorant.getGameMatrix();

            Pair<BehavioralStrategy<State, Action, Observation>, ArrayList<Double>> strategyP2 = this.approximatorWMinorant.ComputeOptimalStrategy(o);
            GameMatrix<State,Action,Observation> gP2 = this.approximatorWMinorant.getGameMatrix();
        strategyP2.getElement0().decompress(s,2);
        strategyP1.getElement0().decompress(s,1);

            OccupancyState<State,Action,Observation> oprime = o.copy();
            if (o.getTimestep() == 0) {
                oprime.initialupdate(strategyP1.getElement0().getStrategy(), strategyP2.getElement0().getStrategy(),true);
            } else {
                oprime.update(strategyP1.getElement0().getStrategy(), strategyP2.getElement0().getStrategy(),true);
            }
            Pair<Vector<State,Action,Observation>, Vector<State,Action,Observation>> pairNextVector
		= explore(s, epsilon, new SigmaOccupancyState<>(oprime), t+1, depth, iteration, new Pair<>(strategyP1.getElement0(),strategyP2.getElement0()));


	    strategyP1 = this.approximatorWMajorant.ComputeOptimalStrategy(o);
	    gP1 = this.approximatorWMajorant.getGameMatrix();

	    strategyP2 = this.approximatorWMinorant.ComputeOptimalStrategy(o);
            gP2 = this.approximatorWMinorant.getGameMatrix();


            Vector<State, Action, Observation> vP1 = majorant.update(lastS, s, this.approximatorWMajorant, strategyP1.getElement1(),gP1);
            Vector<State, Action, Observation> vP2 = minorant.update(lastS, s, this.approximatorWMinorant, strategyP2.getElement1(),gP2);

            if (t>0) {
                approximatorWMajorant.update(t-1, lastS, pairStrat.getElement1(), vP1);
                approximatorWMinorant.update(t-1, lastS, pairStrat.getElement0(), vP2);
            }
            SolvePomdpWithStrat<State,Action,Observation> solvePomdpWithStrat = new SolvePomdpWithStrat<>(posg,1,vP1.strat);
            History<Action,Observation> voidHistory = new History<>();
            Distribution<Pair<posg.State,History<Action,Observation>>> d = new Distribution<>();
            SolvePomdpWithStrat<State,Action,Observation> solvePomdpWithStratP2 = new SolvePomdpWithStrat<>(posg,2,vP2.strat);


            Distribution<Pair<State,History<Action,Observation>>> d2 = new Distribution<>();
            for (State state : posg.getStates()){
                d2.addWeight(new Pair<>(state,voidHistory),posg.initialBelief.getWeight(state));
            }

            return new Pair<Vector<State,Action,Observation>, Vector<State,Action,Observation>>(vP1,vP2);
        } else {
            if (t>= depth) {
                throw new Exception("I shouldn't be there anyway");
            } else {
		System.out.println("[interrupting trajectory]");

		if (this.getWidth(s)<=this.computeThresHold(t)) {

		    System.out.println("( width(s) [= " + (this.getWidth(s))+ "]" +
				       " $\\leq$ thr(t) [=" + sf.format(this.computeThresHold(t)) + "] )");
		}
                if (t == depth-1) {
                    System.out.println("( t=H-1 )");
                    OccupancyState<State, Action, Observation> o = s.o;
                    this.lengthTrajectories = t;
                    Vector<State, Action, Observation> vMajorant = this.majorant.getSupportVector(s.o);
                    Vector<State, Action, Observation> vMinorant = this.minorant.getSupportVector(s.o);


                    Vector<State, Action, Observation> vP1 = majorant.updateLastStep(lastS, s, vMajorant.strat.get(s.o.timestep));
                    Vector<State, Action, Observation> vP2 = minorant.updateLastStep(lastS, s, vMinorant.strat.get(s.o.timestep));

                    this.lengthTrajectories = t;
                    return new Pair<Vector<State, Action, Observation>, Vector<State, Action, Observation>>(vP1, vP2);
                } else {
                    System.out.println("[interrupting trajectory]");


                    OccupancyState<State, Action, Observation> o = s.o;

                    this.lengthTrajectories = t;
                    Vector<State, Action, Observation> vMajorant = this.majorant.getSupportVector(s.o);
                    Vector<State, Action, Observation> vMinorant = this.minorant.getSupportVector(s.o);


                    System.out.println("sigma current : " + s.o);
                    Vector<State, Action, Observation> vP1 = majorant.update(lastS, s, vMajorant);
                    Vector<State, Action, Observation> vP2 = minorant.update(lastS, s, vMinorant);


                    return new Pair<>(vP1,vP2);
                }
            }
        }
    }

    private double computeThresHold(int depth) {
        double res = 0.0;


        return epsilon - 2*rho*(posg.maxReward-posg.minReward)*(posg.profondeurMax*depth);
    }

    private void getLowerBoundGameMatrix(OccupancyState<State,Action,Observation> o, double[][] m) throws Exception {
        for (int i = 0;i<o.getLocalHistoryJ2().size();i++){
            for (int j = 0;j<o.getLocalHistoryJ1().size();j++){


                double[][] Mij = new double[posg.getActionsJ2().size()][posg.getActionsJ1().size()];
                for (int k = 0;k<posg.getActionsJ2().size();k++){
                    for (int l = 0;l<posg.getActionsJ1().size();l++){
                        double sumOverStates = 0.0;
                        if (o.getTimestep()==0){
                            sumOverStates = posg.computeInitRewardFromPureStrategies((Action) posg.getActionsJ1().toArray()[l],(Action) posg.getActionsJ2().toArray()[k],o);

                            Mij[k][l] = sumOverStates;
                        }
                        else{
                            sumOverStates = 0.0;
                            for (State s :  posg.getStates()) {
                                JointHistory<Action,Observation> jointHistory = new JointHistory<Action,Observation>((History<Action,Observation>) o.getLocalHistoryJ1().toArray()[j]
                                        ,(History<Action,Observation>) o.getLocalHistoryJ2().toArray()[i]);
                                Pair<State,JointHistory<Action,Observation>> pair = new Pair<>(s,jointHistory);
                                sumOverStates += o.getDistribution().getWeight(pair)
                                        *posg.computeRewardFromPureStrategies((Action) posg.getActionsJ1().toArray()[l],(Action) posg.getActionsJ2().toArray()[k],s);
                            }
                            Mij[k][l] = sumOverStates;
                        }
                        m[i*posg.getActionsJ2().size()+k][j*posg.getActionsJ1().size()+l] =  - Mij[k][l];
                    }
                }
            }
        }
    }

    private void getUpperBoundGameMatrix(OccupancyState<State,Action,Observation> o, double[][] m) throws Exception {
        for (int i = 0;i<o.getLocalHistoryJ1().size();i++){
            for (int j = 0;j<o.getLocalHistoryJ2().size();j++){


                double[][] Mij = new double[posg.getActionsJ1().size()][posg.getActionsJ2().size()];
                for (int k = 0;k<posg.getActionsJ1().size();k++){
                    for (int l = 0;l<posg.getActionsJ2().size();l++){
                        double sumOverStates = 0.0;
                        if (o.getTimestep()==0){
                            sumOverStates = posg.computeInitRewardFromPureStrategies((Action) posg.getActionsJ1().toArray()[k],(Action) posg.getActionsJ2().toArray()[l],o);

                            Mij[k][l] = sumOverStates;
                        }
                        else{
                            sumOverStates = 0.0;
                            for (State s :  posg.getStates()) {
                                JointHistory<Action,Observation> jointHistory = new JointHistory<Action,Observation>((History<Action,Observation>) o.getLocalHistoryJ1().toArray()[i]
                                        ,(History<Action,Observation>) o.getLocalHistoryJ2().toArray()[j]);
                                Pair<State,JointHistory<Action,Observation>> pair = new Pair<>(s,jointHistory);
                                sumOverStates += o.getDistribution().getWeight(pair)
                                        *posg.computeRewardFromPureStrategies((Action) posg.getActionsJ1().toArray()[k],(Action) posg.getActionsJ2().toArray()[l],s);
                            }
                            Mij[k][l] = sumOverStates;
                        }
                        m[i*posg.getActionsJ1().size()+k][j*posg.getActionsJ2().size()+l] =  Mij[k][l];
                    }
                }
            }
        }
    }
    public double getWidth(SigmaOccupancyState<State,Action,Observation> sigma) throws Exception {
        return Math.abs(majorant.getValue(sigma) - minorant.getValue(sigma));
    }
}
