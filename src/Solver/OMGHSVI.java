package Solver;

import posg.OccupancyState;
import util.RandomGenerator;

public interface OMGHSVI<State,Action,Observation> {

    public double getWidth(OccupancyState<State,Action,Observation> o, int timestep) throws Exception;

    public void solve(double epsilon, int depth) throws Exception;

    public boolean explore(double epsilon, OccupancyState<State,Action,Observation> o, int t, int depth, double stopCriterion, RandomGenerator r) throws Exception;

    public void updateBounds(OccupancyState<State,Action,Observation> o, double valueMax, double valueMin, int timestep) throws Exception;
}
