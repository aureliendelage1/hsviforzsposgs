package Parser;

import posg.Posg;
import posg.State;
import posg.*;
import util.Distribution;
import util.RandomGenerator;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

public class ZsPOSGfromAmatoParser {
    String filename = "";
    PosgInputFileParser parserAmato;

    public ZsPOSGfromAmatoParser(String filename) throws FileNotFoundException {
        this.filename = filename;
        this.parserAmato = new PosgInputFileParser(filename);
    }

    public Posg initializePOSG(String filename) throws Exception {
        parserAmato.parse();

        Posg<State, LocalAction, LocalObservation> posg = new Posg();

        this.initializeStates(posg);
        this.initializeActions(posg);
        this.initializeObservations(posg);

        this.initializeObservationsProbabilities(posg);
        this.initializeTransitions(posg);
        this.InitializeRewardsMaybeBetter(posg);


        posg.cardStates = parserAmato.getStates();
        posg.cardObservations = parserAmato.getObs();
        posg.cardAction = parserAmato.getActions();

        posg.show();

        return posg;
    }

    private void InitializeRewards(Posg<State, LocalAction, LocalObservation> POSG) throws Exception {
        RandomGenerator r = new RandomGenerator();
        RewardSetGeneric<State, JointAction> rewardMap = new RewardSetGeneric<State, JointAction>(POSG.getStates(), POSG.getJointActions());
        double[][][][] tableRecompense = parserAmato.getRwdProbTable();
        int i = 0;
        int j = 0;
        int k = 0;
        for (LocalAction aJ1 : POSG.getActionsJ1()) {

            for (LocalAction aJ2 : POSG.getActionsJ2()) {

                j = 0;
                for (State stateStart : POSG.getStates()) {
                    k = 0;
                    for (State endState : POSG.getStates()) {
                        double S = 0.0;
                        for (int l = 0;l<tableRecompense[i][j][k].length;l++) {
                            S += tableRecompense[i][j][k][l];
                        }


                        rewardMap.setReward(stateStart, new JointAction(aJ1,aJ2), endState, S);
                        k++;
                    }
                    j++;
                }
                POSG.setRewards(rewardMap);
                i++;
            }
        }
    }

    private void printTab(double[][][][] tab){
        for (int i = 0; i< tab.length;i++) {
            for (int j = 0; j < tab[0].length; j++) {
                for (int k = 0; k < tab[0][0].length; k++) {
                    for (int l = 0; l < tab[0][0][0].length; l++) {

                    }
                }
            }
        }
    }

    private void InitializeRewardsMaybeBetter(Posg<State, LocalAction, LocalObservation> POSG) throws Exception {


        RandomGenerator r = new RandomGenerator();
        RewardSetGeneric<State, JointAction> rewardMap = new RewardSetGeneric<State, JointAction>(POSG.getStates(), POSG.getJointActions());
        double[][][][] tableRecompense = parserAmato.getRwdProbTable();

        this.printTab(tableRecompense);


        int i = 0;
        int j = 0;
        int k = 0;
        int l = 0;
        for (LocalAction aJ1 : POSG.getActionsJ1()) {
            j = 0;

            for (LocalAction aJ2 : POSG.getActionsJ2()) {
                k = 0;

                for (State stateStart : POSG.getStates()) {
                    l = 0;
                    for (State endState : POSG.getStates()) {
                        double S = 0.0;
                        for (int m = 0;m<tableRecompense[i*POSG.getActionsJ2().size()+j][k][l].length;m++) {
                            S += tableRecompense[i*POSG.getActionsJ2().size()+j][k][l][m];
                        }

                        rewardMap.setReward(stateStart, new JointAction(aJ1,aJ2), endState, S);
                    }
                    k++;
                }
                POSG.setRewards(rewardMap);
                j++;
            }
            i++;
        }
    }


    public void initializeTransitions(Posg<State, LocalAction, LocalObservation> POSG) throws Exception {
        HashMap<Pair<State, JointAction>, Distribution<State>> transitionMapProbabilities = new HashMap<Pair<State, JointAction>, Distribution<State>>();
        TransitionSetGeneric<State, JointAction> transition = new TransitionSetGeneric<State, JointAction>(POSG.getStates(), POSG.getJointActions());
        RandomGenerator r = new RandomGenerator();
        double[][][] transitionTable = parserAmato.getTrnProbTable();
        int i = 0;
        int j = 0;
        int k = 0;
        for (LocalAction aJ1 : POSG.getActionsJ1()){
            for (LocalAction aJ2 : POSG.getActionsJ2()){

                j = 0;
                JointAction a = new JointAction(aJ1,aJ2);
                for (State s : POSG.getStates()){
                    k = 0;
                    Distribution<State> distrib = new Distribution<>();

                    for (State endState : POSG.getStates()){

                        distrib.addWeight(endState,parserAmato.getTrnProbTable()[i][j][k]);
                        k++;
                    }


                    transitionMapProbabilities.put(new Pair<State, JointAction>(s, new JointAction(aJ1,aJ2)), distrib);
                    j++;
                }
                i++;
            }
        }
        transition.setTransitions(transitionMapProbabilities);
        POSG.setTransitions(transition);
        transition.sanityCheck();
    }

    public void initializeObservationsProbabilities(Posg<State, LocalAction, LocalObservation> POSG) throws Exception {
        ObservationSetGeneric<State, JointAction, JointObservation> observationSetGeneric =
                new ObservationSetGeneric<>(POSG.getStates(), POSG.getJointActions(), POSG.getJointObservations());
        HashMap<Pair<JointAction, State>, Distribution<JointObservation>> obsProbabilities
                = new HashMap<Pair<JointAction, State>, Distribution<JointObservation>>();
        RandomGenerator r = new RandomGenerator();
        double[][][] observationsTable = parserAmato.getObsProbTable();
        int i = 0;
        int j = 0;
        int k = 0;
        for (LocalAction aJ1 : POSG.getActionsJ1()){
            for (LocalAction aJ2 : POSG.getActionsJ2()){
                JointAction a = new JointAction(aJ1,aJ2);
                j = 0;
                for (State s : POSG.getStates()){
                    k = 0;
                    Distribution<JointObservation> distrib = new Distribution<>();
                    for (LocalObservation z1 : POSG.getObservationsJ1()){
                        for(LocalObservation z2 : POSG.getObservationsJ2()) {

                            distrib.addWeight(new JointObservation(z1,z2), observationsTable[i][j][k]);


                            k++;
                        }
                    }
                    obsProbabilities.put(new Pair<JointAction, State>(new JointAction(aJ1,aJ2),s),distrib);


                    j++;
                }
                i++;
            }
        }


        observationSetGeneric.setDistribProbabilities(obsProbabilities);
        observationSetGeneric.sanityCheck();
        POSG.setObservationsProbabilities(observationSetGeneric);
    }

    public void initializeStates(Posg POSG){

        ArrayList<State> setOfStates = new ArrayList<>();
        for (int i = 0; i < parserAmato.getStates(); i++){
            setOfStates.add(new State(parserAmato.getStateName(i)));

        }
        POSG.setStates(setOfStates);
    }

    public void initializeActions(Posg POSG){
        ArrayList<LocalAction> setOfActionsJ1 = new ArrayList<LocalAction>();

        for (int i = 0; i < parserAmato.action[0].length;i++){

            setOfActionsJ1.add(new LocalAction(parserAmato.action[0][i]));
        }

        ArrayList<LocalAction> setOfActionsJ2 = new ArrayList<LocalAction>();
        for (int i = 0; i < parserAmato.action[1].length;i++){
            setOfActionsJ2.add(new LocalAction(parserAmato.action[1][i]));
        }

        POSG.setActionsJ1(setOfActionsJ1);
        POSG.setActionsJ2(setOfActionsJ2);
    }

    public void initializeObservations(Posg POSG){
        ArrayList<LocalObservation> setOfObservationsJ1 = new ArrayList<LocalObservation>();
        for (int i = 0; i < parserAmato.observation[0].length;i++){
            setOfObservationsJ1.add(new LocalObservation(parserAmato.observation[0][i]));
        }

        ArrayList<LocalObservation> setOfObservationsJ2 = new ArrayList<LocalObservation>();
        for (int i = 0; i < parserAmato.observation[1].length;i++){
            setOfObservationsJ2.add(new LocalObservation(parserAmato.observation[1][i]));
        }

        POSG.setObservationsJ1(setOfObservationsJ1);
        POSG.setObservationsJ2(setOfObservationsJ2);
    }

    public void initiateOccupancyState(OccupancyState o, Posg<State, LocalAction, LocalObservation> POSG) throws Exception {
        double[] valuesOfStates = parserAmato.getStartValues();
        for (int i = 0;i<valuesOfStates.length;i++){

        }
        Distribution<State> distrib = new Distribution<>();
        int i = 0;
        for (State s : POSG.getStates()){

            distrib.addWeight(s,valuesOfStates[i]);
            i++;
        }
        distrib.sanityCheck();
        o.setInitialBelief(distrib);
    }

}
