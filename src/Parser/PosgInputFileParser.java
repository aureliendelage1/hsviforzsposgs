package Parser;

import java.io.*;
import java.util.StringTokenizer;
import java.util.NoSuchElementException;

public class PosgInputFileParser
{

    private double discount;
    private int agents;
    private int states;
    private boolean cost;


    private double[][][] O;
    private double[][][] T;
    private double[][][][] R;


    private double[] start;
    private BufferedReader br;
    private boolean t_set;
    private boolean r_o_set;
    private int tot_actions;
    private int tot_obs;


    public String[] state;
    public String[][] action;
    public String[][] observation;
    public int getAgents() {
        return agents;
    }
    public int getStates() {
        return states;
    }
    public boolean getCost() {
        return cost;
    }
    public double getDiscount() {
        return discount;
    }
    public int getActions() {
        return tot_actions;
    }
    public int getObs() {
        return tot_obs;
    }
    public double[][][] getTrnProbTable() {
        return T;
    }
    public double[][][] getObsProbTable() {
        return O;
    }
    public double[][][][] getRwdProbTable() {
        return R;
    }
    public int getStateValue(String s)
    {
        if (s.equals("*"))
            return -1;
        try {
            return (Integer.parseInt(s));
        }
        catch (Exception e) {
            for (int i=0; i<getStates(); i++)
                if (s.equals(state[i]))
                    return i;
            System.out.println("Inappropriate state format... please see the FORMAT.txt file for instructions");
            System.exit(1);
        }
        return -2;
    }
    public String getStateName(int v)
    {
        try {
            return (state[v]);
        }
        catch (Exception e) {
            return (new Integer(v)).toString();
        }
    }
    public int getParamValue(String params, String[][] values)
    {
        String[] p = params.split("\\s");
        if (values == action)
            return getParamValueHlp(p, values, tot_actions);
        else
            return getParamValueHlp(p, values, tot_obs);
    }
    public String getParamName(int num, boolean actn)
    {
        if (actn)
            return getParamNameHlp(num, action, 0, "");
        else
            return getParamNameHlp(num, observation, 0, "");
    }
    public double[] getStartValues()
    {
        return start;
    }
    public PosgInputFileParser(String filename) throws FileNotFoundException
    {
        agents = 0;
        states = 0;
        tot_actions = 0;
        tot_obs = 0;

        start = null;
        state = null;
        action = null;
        observation = null;

        O = null;
        T = null;
        R = null;

        t_set = false;
        r_o_set = false;

        br = new BufferedReader(new FileReader(filename));
    }
    private void initProbs(boolean actns)
    {
        if (actns)
        {
            tot_actions = 1;
            for (int i=0; i<action.length; i++)
                tot_actions *= action[i].length;

            if (!t_set)
            {
                T = new double[tot_actions][getStates()][getStates()];
                t_set = true;
            }

        }

        else
        {
            tot_obs = 1;
            for (int i=0; i<observation.length; i++)
                tot_obs *= observation[i].length;

            O = new double[tot_actions][getStates()][tot_obs];
            R = new double[tot_actions][getStates()][getStates()][tot_obs];
            r_o_set = true;
        }
    }
    private void setAgent(String line)
    {
        StringTokenizer st = new StringTokenizer(line, " :");

        st.nextToken();
        String agnt = st.nextToken();
        try
        {
            agents = Integer.parseInt(agnt);
            action = new String[agents][];
            observation = new String[agents][];
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("discount not formated correctly: " + nfe.getMessage());
            System.exit(1);
        }
    }
    private String setParam(String[][] val, String line) throws IOException
    {
        int action_line = 0;
        int actions;
        StringTokenizer actionTokenize;
        String current = "";

        do {
            if (action_line == 0) {
                actionTokenize = new StringTokenizer(line, " :");

                actionTokenize.nextToken();
            }
            else
                actionTokenize = new StringTokenizer(line);
            try {
                current = actionTokenize.nextToken();
                val[action_line] = new String[Integer.parseInt(current)];
                for (int i=0; i<val[action_line].length; i++)
                    val[action_line][i] = (new Integer(i)).toString();
            }


            catch (NoSuchElementException nsee) {
                line = br.readLine();
                StringTokenizer str = new StringTokenizer(line);
                String first = str.nextToken();
                String temp = "actions: ";

                do {
                    if (line.startsWith("#"))
                        continue;
                    str = new StringTokenizer(line);
                    for (int i=0; i<str.countTokens(); i++)
                        temp += str.nextToken() + " ";
                } while (!(line = br.readLine()).equals(""));

                str = new StringTokenizer(temp, " :");
                catchParamNFE(str, val, action_line, first);
            }


            catch (NumberFormatException nfe) {
                catchParamNFE(actionTokenize, val, action_line, current);
            }

            while((line = br.readLine()) != null && line.equals("")) { }
            action_line++;

        }
        while (action_line < getAgents());
        return line;
    }
    private void setValues(Object m, int a, int p, int p2, int o, double pr, boolean o_flag, boolean r_flag)
    {
        if (a == -1)
            for (int i=0; i<tot_actions; i++) {
                if (p == -1)
                    for (int j=0; j<getStates(); j++) {
                        if (o == -1) {
                            if (p2 == -1)
                                for (int k=0; k<getStates(); k++)
                                    for (int l=0; l<tot_obs; l++)
                                        ((double[][][][])m)[i][j][k][l] = pr;
                            else if (o_flag)
                                for (int k=0; k<tot_obs; k++) {
                                    if (!r_flag)
                                        ((double[][][])m)[i][j][k] = pr;
                                    else
                                        ((double[][][][])m)[i][j][p2][k] = pr;
                                }
                            else
                                for (int k=0; k<getStates(); k++)
                                    ((double[][][])m)[i][j][k] = pr;
                        }
                        else if (p2 == -1)
                            for (int k=0; k<getStates(); k++)
                                ((double[][][][])m)[i][j][k][o] = pr;
                        else if (!r_flag)
                            ((double[][][])m)[i][j][o] = pr;

                        else
                            ((double[][][][])m)[i][j][p2][o] = pr;
                    }
                else if (o == -1) {
                    if (p2 == -1)
                        for (int k=0; k<getStates(); k++)
                            for (int l=0; l<tot_obs; l++)
                                ((double[][][][])m)[i][p][k][l] = pr;
                    else if (o_flag)
                        for (int k=0; k<tot_obs; k++) {
                            if (!r_flag)
                                ((double[][][])m)[i][p][k] = pr;
                            else
                                ((double[][][][])m)[i][p][p2][k] = pr;
                        }
                    else
                        for (int k=0; k<getStates(); k++)
                            ((double[][][])m)[i][p][k] = pr;
                }
                else if (p2 == -1)
                    for (int k=0; k<getStates(); k++)
                        ((double[][][][])m)[i][p][k][o] = pr;
                else if (!r_flag)
                    ((double[][][])m)[i][p][o] = pr;
                else
                    ((double[][][][])m)[i][p][p2][o] = pr;
            }
        else if (p == -1)
            for (int j=0; j<getStates(); j++) {
                if (o == -1) {
                    if (p2 == -1)
                        for (int k=0; k<getStates(); k++)
                            for (int l=0; l<tot_obs; l++)
                                ((double[][][][])m)[a][j][k][l] = pr;
                    else if (o_flag)
                        for (int k=0; k<tot_obs; k++) {
                            if (!r_flag)
                                ((double[][][])m)[a][j][k] = pr;
                            else
                                ((double[][][][])m)[a][j][p2][k] = pr;
                        }
                    else
                        for (int k=0; k<getStates(); k++)
                            ((double[][][])m)[a][j][k] = pr;
                }
                else if (p2 == -1)
                    for (int k=0; k<getStates(); k++)
                        ((double[][][][])m)[a][j][k][o] = pr;
                else if (!r_flag)
                    ((double[][][])m)[a][j][o] = pr;
                else
                    ((double[][][][])m)[a][j][p2][o] = pr;
            }
        else if (o == -1) {
            if (p2 == -1)
                for (int k=0; k<getStates(); k++)
                    for (int l=0; l<tot_obs; l++)
                        ((double[][][][])m)[a][p][k][l] = pr;
            else if (o_flag)
                for (int k=0; k<tot_obs; k++) {
                    if (!r_flag)
                        ((double[][][])m)[a][p][k] = pr;
                    else
                        ((double[][][][])m)[a][p][p2][k] = pr;
                }
            else
                for (int k=0; k<getStates(); k++)
                    ((double[][][])m)[a][p][k] = pr;
        }
        else if (p2 == -1)
            for (int k=0; k<getStates(); k++)
                ((double[][][][])m)[a][p][k][o] = pr;
        else if (!r_flag)
            ((double[][][])m)[a][p][o] = pr;

        else
            ((double[][][][])m)[a][p][p2][o] = pr;
    }
    private String setProb(Object prob, String line, boolean obsr, boolean rwrd) throws IOException
    {
        StringTokenizer st;
        String[] acs = null;
        String[] obs = null;
        int params = getAgents();
        int a_value = 0;
        int o_value = 0;
        int cur_line = 0;
        boolean one_line = false;
        int start_s = 0;
        int end_s = 0;
        int r_end_s = 0;
        int tokens = 0;

        do {
            if (cur_line == 0) {
                StringTokenizer star_elim = new StringTokenizer(line, " :");
                tokens = getTokenCount(star_elim, obsr, rwrd);

                st = new StringTokenizer(line, " :");
                st.nextToken();

                String temp = st.nextToken();

                if (temp.equals("*")) {
                    a_value = -1;
                }
                else {
                    acs = new String[params];
                    acs[0] = temp;
                    for (int a=1; a<acs.length; a++)
                        acs[a] = st.nextToken();
                    a_value = getParamValueHlp(acs, action, tot_actions);
                }
            }

            else {
                st = new StringTokenizer(line);
                tokens = st.countTokens();
                params = getStates();
            }
            if ( (tokens >= (params*2 + 3) && tokens <= (params*2 + 4) && rwrd) ||
                    (tokens >= (params*2 + 2) && tokens <= (params*2 + 3) && obsr && !rwrd) ||
                    (tokens >= (params+3) && tokens <= (params+4) && !obsr && !rwrd)) {
                start_s = getStateValue(st.nextToken());
                if (rwrd)
                    r_end_s = getStateValue(st.nextToken());

                if ( (tokens >= (params*2 + 3) && tokens <= (params*2 + 4) && rwrd) ||
                        (tokens >= (params*2 + 2) && tokens <= (params*2 + 3) && obsr && !rwrd)) {
                    String temp2 = st.nextToken();
                    if (temp2.equals("*"))
                        end_s = -1;
                    else {
                        obs = new String[params];
                        obs[0] = temp2;
                        for (int o=1; o<obs.length; o++)
                            obs[o] = st.nextToken();
                        end_s = getParamValueHlp(obs, observation, tot_obs);
                    }
                }
                else
                    end_s = getStateValue(st.nextToken());

                double pr;

                if ((tokens == (params+4) && !rwrd) || tokens == (params*2+4) || (tokens == (params*2+3) && !rwrd))
                    pr = Double.parseDouble(st.nextToken());
                else {
                    line = br.readLine();
                    pr = Double.parseDouble(new StringTokenizer(line).nextToken());
                }

                if (rwrd)
                    setValues(prob,a_value,start_s,r_end_s,end_s,pr, true, true);
                else if (obsr)
                    setValues(prob,a_value,start_s,0,end_s,pr,true,false);
                else
                    setValues(prob,a_value,start_s,0,end_s,pr,false,false);
                return br.readLine();
            }
            else if (tokens == (params+3) || (tokens == (params+2) && !rwrd)) {
                start_s = getStateValue(st.nextToken());
                if (rwrd)
                    r_end_s = getStateValue(st.nextToken());
                one_line = true;
                cur_line = getStates()-1;
            }
            else if (tokens == (params+2) || (tokens == (params+1) && !rwrd)) {
                if (rwrd)
                    start_s = getStateValue(st.nextToken());
            }
            else if (tokens == getStates() || tokens == tot_obs || tokens == 1) {
                StringTokenizer tk = new StringTokenizer(line);
                String temp = tk.nextToken();

                if (temp.equalsIgnoreCase("IDENTITY") || temp.equalsIgnoreCase("UNIFORM")) {
                    cur_line = getStates();
                    if (!one_line)
                        start_s = -1;
                }

                if (!one_line && start_s != -1)
                    start_s = cur_line-1;
                if (temp.equalsIgnoreCase("IDENTITY") && !obsr && !rwrd)
                    for (int s=0; s<getStates(); s++)
                        for (int e=0; e<getStates(); e++) {
                            if (s == e)
                                setValues(prob,a_value,s,0,e,1.0,false,false);
                            else
                                setValues(prob,a_value,s,0,e,0.0,false,false);
                        }
                else if (temp.equalsIgnoreCase("UNIFORM")) {
                    if (!one_line)
                        for (int s=0; s<getStates(); s++)
                            uniformHlp(prob,a_value,start_s,s,1.0/getStates(),obsr,rwrd);
                    else
                        uniformHlp(prob,a_value,start_s,r_end_s,1.0/getStates(),obsr,rwrd);
                }


                else if (tokens == getStates())
                    for (int e=0; e<getStates(); e++)
                        setValues(prob,a_value,start_s,0,e,Double.parseDouble(st.nextToken()),false,false);
                else if (!rwrd)
                    for (int e=0; e<getObs(); e++)
                        setValues(prob,a_value,start_s,0,e,Double.parseDouble(st.nextToken()),true,false);
                else
                    for (int e=0; e<getObs(); e++)
                        setValues(prob,a_value,start_s,r_end_s,e,Double.parseDouble(st.nextToken()),true,true);
            }


            else {
                System.out.println("Incorrect Formatting in line " + line);
                System.out.println("Please see the INPUT FILE SPECIFICATIONS.txt file for instructions");
                System.exit(1);
            }

            line = br.readLine();
            cur_line++;
        }
        while (cur_line <= getStates());
        return line;
    }
    public void updateProbRow(Object prob, String line) throws IOException
    {
        if (prob == getTrnProbTable())
            setProb(prob, line, false, false);
        else if (prob == getObsProbTable())
            setProb(prob, line, true, false);
        else
            setProb(prob, line, true, true);
    }
    private String getParamNameHlp(int value, String[][] param, int param_col, String res)
    {
        int i;
        int num = 1;
        int temp;

        if (value == 0) {
            for (int f=param_col; f<param.length; f++)
                res += param[f][0] + " ";
            return res;
        }

        for (i=param.length-1; i>=0; num=temp, i--) {
            temp = num * param[i].length;
            if (value < temp)
                break;
        }

        if (i > param_col)
            for (int j=param_col; j<i; j++)
                res += param[j][0] + " ";

        int row;
        for (row=0; row<param[i].length; row++)
            if (value < row*num)
                break;

        return getParamNameHlp(value - num*(row-1), param, i+1, res + param[i][row-1] + " ");
    }
    private int getParamValueHlp(String[] params, String[][] values, int data)
    {
        int tot = data;
        int value = 0;
        for (int i=0; i<params.length; i++)
            for (int j=0; j<values[i].length; j++)
                if (params[i].equals(values[i][j]))
                {
                    value += j * (tot/values[i].length);
                    tot /= values[i].length;
                }
        return value;
    }
    private void catchParamNFE(StringTokenizer st, String[][] val, int action_line, String current)
    {
        int actions = st.countTokens();

        val[action_line] = new String[actions+1];
        val[action_line][0] = current;

        for (int count = 1; st.hasMoreTokens(); count++)
            val[action_line][count] = st.nextToken();
    }
    private void uniformHlp(Object m, int a, int p, int p2, double pr, boolean o_flag, boolean r_flag)
    {
        if (!o_flag && !r_flag)
            for (int e=0; e<getStates(); e++)
                setValues(m,a,p,0,e,1.0/getStates(),o_flag,r_flag);
        else if (!r_flag)
            for (int e=0; e<tot_obs; e++)
                setValues(m,a,p,0,e,1.0/getObs(),o_flag,r_flag);
        else
            for (int e=0; e<tot_obs; e++)
                setValues(m,a,p,p2,e,1.0/getObs(),o_flag,r_flag);
    }
    private void catchStatesNFE (String line)
    {
        StringTokenizer stateTokenize = new StringTokenizer(line);
        states = stateTokenize.countTokens() - 1;
        start = new double[states];

        state = new String[states];
        stateTokenize.nextToken();
        int count;
        for (count = 0; stateTokenize.hasMoreTokens(); count++)
            state[count] = stateTokenize.nextToken();
    }
    private int getTokenCount(StringTokenizer s, boolean obs, boolean rwd)
    {
        int r = s.countTokens();
        try {
            s.nextToken();

            String temp = s.nextToken();
            if (temp.equals("*"))
                r += getAgents() - 1;

            s.nextToken();
            if (rwd)
                s.nextToken();

            temp = s.nextToken();
            if (obs && temp.equals("*"))
                r += getAgents() - 1;
            return r;

        }
        catch (NoSuchElementException nsee) {
            return r;
        }
    }

    public void parse() throws IOException {
        String line = null;

        for (line = br.readLine(); line != null;) {
            if (line.startsWith("#")) {

                line = br.readLine();
            }


            else if (line.toLowerCase().startsWith("discount:"))
            {
                StringTokenizer st = new StringTokenizer(line, " :");
                st.nextToken();

                String discTemp = st.nextToken();
                try
                {
                    discount = Double.parseDouble(discTemp);
                }

                catch (NumberFormatException nfe)
                {
                    System.out.println("discount not formated correctly: " + nfe.getMessage());
                    System.exit(1);
                }
                line = br.readLine();
            }


            else if (line.toLowerCase().startsWith("agents:"))
            {
                setAgent(line);
                line = br.readLine();
            }


            else if (line.toLowerCase().startsWith("values:")) {

                StringTokenizer st = new StringTokenizer(line, " :");

                st.nextToken();

                if (st.countTokens() != 1) {
                    System.out.println("wrong number of params in values: def");
                    System.exit(1);
                }

                String values = st.nextToken();
                if (values.equals("reward")) {
                    cost = false;
                } else if (values.equals("cost")) {
                    cost = true;
                } else {
                    System.out.println(
                            "values: must be specified as 'cost' or 'reward'");
                    System.exit(1);
                }
                line = br.readLine();

            }


            else if (line.toLowerCase().startsWith("states:")) {

                StringTokenizer st = new StringTokenizer(line, " :");


                st.nextToken();


                try
                {
                    states = Integer.parseInt(st.nextToken());
                    start = new double[states];
                    line = "states:";
                    for (int i=0; i<states; i++)
                        line += " state" + (i+1);
                    catchStatesNFE(line);
                    line = br.readLine();
                }

                catch (NoSuchElementException nsee)
                {
                    String temp = "states: ";
                    while (!(line = br.readLine()).equals(""))  {
                        StringTokenizer str = new StringTokenizer(line);
                        for (int i=0; i<str.countTokens(); i++)
                            temp += str.nextToken() + " ";
                    }
                    catchStatesNFE(temp);
                }

                catch (NumberFormatException nfe)
                {
                    catchStatesNFE(line);
                    line = br.readLine();
                }
            }


            else if (line.toLowerCase().startsWith("actions:"))
            {
                if (getAgents() == 0)
                    setAgent("agents: 1");

                line = setParam(action, line);
                initProbs(true);
            }
            else if (line.toLowerCase().startsWith("observations:"))
            {
                if (getAgents() == 0)
                    setAgent("agents: 1");

                line = setParam(observation, line);
                initProbs(false);
            }


            else if (line.toLowerCase().startsWith("t"))
                line = setProb(T, line, false, false);

            else if (line.toLowerCase().startsWith("o"))
                line = setProb(O, line, true, false);
            else if (line.toLowerCase().startsWith("r"))
                line = setProb(R, line, true, true);
            else if (line.toLowerCase().startsWith("start:"))
            {

                StringTokenizer st = new StringTokenizer(line, " :");
                int tokens = st.countTokens() - 1;

                if (tokens == 0) {
                    line = br.readLine();
                    st = new StringTokenizer(line, " :");
                    tokens = st.countTokens();
                }

                if (tokens != start.length) {
                    System.out.println("wrong number of start probabilities");
                    System.out.println("given: " + tokens + ", expected: " + start.length);
                    System.exit(1);
                }

                if (st.countTokens() == start.length+1)
                    st.nextToken();

                String temp;

                try {
                    for (int i=0; i<tokens; i++) {
                        temp = st.nextToken();
                        start[i] = Double.parseDouble(temp);
                    }
                }
                catch (NumberFormatException nfe) {
                    System.out.println("start probs not formatted correctly: " + nfe.getMessage());
                    System.exit(1);
                }
                line = br.readLine();
            }

            if (line != null && line.equals(""))
                while((line = br.readLine()) != null && line.equals("")) { }
        }
        System.out.println("done parsing...");
    }
}
