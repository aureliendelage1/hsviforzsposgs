package posg;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class RewardSetGeneric<State,Action> extends RewardSet<State,Action> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<Transition<State,Action>,Double> rewards;
    private Map<Pair<State,Action>,Double> expectedReward;
    private Collection<State> states;
    private Collection<Action> actions;

    public RewardSetGeneric(Collection<State> states, Collection<Action> actions) {
        this.actions = actions;
        this.states = states;
        rewards = new HashMap<>();
        expectedReward = null;
    }

    @Override
    public void setReward(State startState, Action action, State endState, double reward) {

        expectedReward = null;

        if(startState == null) {
            for(State newStartState : states) {
                setReward(newStartState,action,endState,reward);
            }
        } else if(action == null) {
            for(Action newAction : actions) {
                setReward(startState,newAction,endState,reward);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setReward(startState,action,newEndState,reward);
            }
        } else {
            rewards.put(new Transition<>(startState,action,endState),reward);
        }
    }

    @Override
    public void initGetters(TransitionSet<State,Action> transitionSet) {
        expectedReward = new HashMap<>();
        for(State startState : states) {
            for(Action action : actions) {
                expectedReward.put(new Pair<>(startState,action),computeExpectedReward(transitionSet,startState,action));
            }
        }
    }

    @Override
    public double getReward(State startState, Action action, State endState) {
        if (!rewards.containsKey(new Transition<>(startState,action,endState))){
            System.out.println("HIGHLY WEIRD");
        }
        return rewards.computeIfAbsent(new Transition<>(startState,action,endState),k -> 0.0);
    }

    @Override
    public double getExpectedReward(State startState, Action action) {
        System.out.println("trying to get : r(" + startState + ", " + action + ")");
        System.out.println(this.rewards);
        return expectedReward.get(new Pair<>(startState,action));
    }

    public Map<Transition<State, Action>, Double> getRewards() {
        return this.rewards;
    }

    public double getReward(State s, Action jointAction, Posg posg) {
        double sum = 0.0;
        for (Object sprime : posg.getStates()){
            sum += posg.transitions.getProbability(s,jointAction,sprime)
                    *this.getReward(s,jointAction,(State) sprime);
        }
        return sum;
    }
}
