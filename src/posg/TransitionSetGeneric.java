package posg;

import util.Distribution;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TransitionSetGeneric<State,Action> extends TransitionSet<State,Action> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<Pair<State,Action>,Distribution<State>> transitions;

    private Collection<State> states;

    private Collection<Action> actions;

    public TransitionSetGeneric(Collection<State> states, Collection<Action> actions) {
        this.states = states;
        this.actions = actions;
        transitions = new HashMap<>();
    }

    public Map<Pair<State,Action>,Distribution<State>> getTransitions(){
        return this.transitions;
    }

    public void setTransitions(Map<Pair<State,Action>,Distribution<State>> transitions){
        this.transitions = transitions;
    }

    public void sanityCheck() throws Exception {
        double proba = 0.0;

        for (Pair<State,Action> elt : this.transitions.keySet()){

            Distribution<State> distrib = this.transitions.get(elt);
            distrib.sanityCheck();
        }

    }
    @Override
    public void setDistribution(State startState, Action action, Distribution<State> endStates) {
        if(endStates == null) {
            throw new IllegalArgumentException("The distribution over end states cannot be null");
        }
        if(startState == null) {
            for(State newStartState : states) {
                setDistribution(newStartState,action,endStates);
            }
        } else if(action == null) {
            for(Action newAction : actions) {
                setDistribution(startState,newAction,endStates);
            }
        } else {
            transitions.put(new Pair<>(startState, action), endStates);
        }
    }

    @Override
    public void setProbability(State startState, Action action, State endState, double probability) {
        if(startState == null) {
            for(State newStartState : states) {
                setProbability(newStartState,action,endState,probability);
            }
        } else if(action == null) {
            for(Action newAction : actions) {
                setProbability(startState,newAction,endState,probability);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setProbability(startState,action,newEndState,probability);
            }
        } else {
            Distribution<State> distribution = getOrCreateDistribution(startState, action);
            distribution.setWeight(endState, probability);
        }
    }

    @Override
    public void initGetters() {


        for(State startState : states) {
            for(Action action : actions) {
                Distribution<State> distribution = getOrCreateDistribution(startState,action);
                if(distribution.getNonZeroElements().size() == 0) {
                    throw new IllegalStateException("There is no transitions possible for the pair (" + startState + "," + action +").");
                }
            }
        }
    }

    @Override
    public double getProbability(State startState, Action action, State endState) {

        return transitions.get(new Pair<>(startState,action)).getWeight(endState);
    }

    @Override
    public Collection<State> getPossibleNextStates(State startState, Action action) {
        return transitions.get(new Pair<>(startState,action)).getNonZeroElements();
    }
    @Override
    public State sampleNextState(State startState, Action action) {
        return transitions.get(new Pair<>(startState,action)).sample();
    }

    private Distribution<State> getOrCreateDistribution(State startState, Action action) {
        return transitions.computeIfAbsent(new Pair<>(startState, action), k -> new Distribution<>());
    }
}
