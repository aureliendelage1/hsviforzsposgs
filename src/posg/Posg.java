package posg;

import AlgorithmForLpSolving.CplexLastStepBelief;
import PomdpSolver.Belief;
import PomdpSolver.SawtoothBound;
import PomdpSolver.ValueFunctionBound;
import util.Distribution;
import util.RandomGenerator;

import java.io.PrintWriter;
import java.util.*;

public class Posg<State,Action,Observation>{
    public double discount = 0.0;
    public int profondeurMax;
    public double epsilon;
    public double timestep;
    public int cardAction;
    public int cardObservations;
    public int cardStates;
    private Collection<State> states;
    private Collection<Action> actionsJ1;
    private Collection<Action> actionsJ2;
    private Collection<Observation> observationsJ1;
    private Collection<Observation> observationsJ2;

    public TransitionSetGeneric<State, JointAction> transitions;
    public RewardSetGeneric<State, JointAction> rewards;
    public ObservationSetGeneric<State, JointAction, JointObservation> observationsProbabilities;
    public double maxReward;
    public double minReward;
    private SawtoothBound<State,Action,Observation> sawtoothBound;
    private SawtoothBound<State,Action,Observation> lowerBoundHyperplane;
    private PomdpSolverNotDynamic.SawtoothBound<State,Action,Observation> sawtoothBoundNotDynamic;
    private PomdpSolverNotDynamic.SawtoothBound<State,Action,Observation> lowerBoundHyperplaneNotDynamic;

    public Distribution<State> getInitialBelief() {
        return initialBelief;
    }

    public void setInitialBelief(Distribution<State> initialBelief) {
        this.initialBelief = initialBelief;
    }

    public Distribution<State> initialBelief;

    public HashMap<Integer, Double> lambda;

    public boolean mdpHeuristic;

    public Posg(double discount, int profondeurMax, double epsilon){
        this.discount = discount;
        this.epsilon = epsilon;
        this.profondeurMax = profondeurMax;
    }

    public Posg() {

    }

    public void show() throws Exception {

        System.out.println("\nthe states of the POSG are the following: \n");
        for (State s : states){
            System.out.println(s.toString());
        }
        System.out.println("with initial belief : " + this.initialBelief);
        System.out.println("--------------------------------------");
        System.out.println("\nthe actions of the POSG for player 1 are the following: \n");
        for (Action a : actionsJ1){
            System.out.println(a.toString());
        }

        System.out.println("\nthe actions of the POSG for player 2 are the following: \n");
        for (Action a : actionsJ2){
            System.out.println(a.toString());
        }


        System.out.println("--------------------------------------");
        System.out.println("\nthe observations of the POSG for player 1 are the following: \n");
        for (Observation z : observationsJ1){
            System.out.println(z.toString());
        }

        System.out.println("\nthe observations of the POSG for player 2 are the following: \n");
        for (Observation z : observationsJ2){
            System.out.println(z.toString());
        }

        System.out.println("\nthe observations of the POSG are the following: \n");
        for (Observation z : observationsJ1){
            System.out.println(z.toString());
        }
/*
        PrintWriter writerTransitionsProbas = new PrintWriter("log/POSG-TransitionsProbas.txt", "UTF-8");
        System.out.println("--------------------------------------");
        System.out.println("\nLes probabilités de transition sont les suivantes: \n");
        Set<Pair<State, JointAction>> setOfStateAction = this.transitions.getTransitions().keySet();
        for (Pair<State, JointAction> pair : setOfStateAction){
            for (State s: this.transitions.getTransitions().get(pair).getNonZeroElements()){


                writerTransitionsProbas.println("T("+pair.getElement0().toString()+","+pair.getElement1().toString()+","+s.toString()+"="
                  +this.transitions.getTransitions().get(pair).getProbability(s));
            }
        }
        writerTransitionsProbas.close();

        PrintWriter writerRewards = new PrintWriter("log/POSG-Rewards.txt", "UTF-8");
        System.out.println("--------------------------------------");
        System.out.println("\nLes récompenses sont les suivantes de transition sont les suivantes: \n");
        for (State s : this.getStates()) {
            for (Action actionJ1 : this.getActions(1)) {
                for (Action actionJ2 : this.getActions(2)){


                writerRewards.println("La récompense : " + "r(" + s + "," + new JointAction<>(actionJ1,actionJ2) + "), = : " + this.rewards.getReward(s, new JointAction<>(actionJ1,actionJ2),this));
                }
            }
        }
        writerRewards.close();

        PrintWriter writerObservationsProbas = new PrintWriter("log/POSG-ObservationsProbas.txt", "UTF-8");
        System.out.println("--------------------------------------");
        System.out.println("\nLes probabilités observations sont les suivantes de transition sont les suivantes: \n");
        for (State s : states){
            for (JointAction actJ1J2 : this.getJointActions()){
                for (JointObservation z : this.getJointObservations()){


                 writerObservationsProbas.println("T(" + z.toString()+" | "+s.toString()+","+actJ1J2.toString()+ ") = "+
                           this.observationsProbabilities.getProba(actJ1J2,s,z));
                }
            }
        }

        writerObservationsProbas.close();
*/
        System.out.println("--------------------------------------");
        System.out.println("Begin Sanity check");

        this.sanityCheck();
        //System.out.println("\nDone, if no exception message was raised, this      is good");
        System.out.println("--------------------------------------");
    }

    private void sanityCheck() throws Exception {
        this.observationsProbabilities.sanityCheck();
        System.out.println("\n");
        this.transitions.sanityCheck();
    }

    public HashSet<JointObservation> getJointObservations() throws Exception {
        HashSet<JointObservation> jointObs = new HashSet<>();
        for (Observation zj1 : this.getObservationsJ1()) {
            for (Observation zj2 : this.getObservationsJ2()) {
                jointObs.add(new JointObservation(zj1,zj2));
            }
        }
        return jointObs;
    }

    public Collection<State> getStates() {
        return states;
    }

    public void setStates(Collection<State> states) {

        this.states = states;
    }

    public Collection<Action> getActionsJ1() {
        return actionsJ1;
    }


    public Collection<Action> getActionsJ2() {
        return actionsJ2;
    }

    public void setActionsJ1(Collection<Action> actions) {
        this.actionsJ1 = actions;
    }

    public void setActionsJ2(Collection<Action> actions) {
        this.actionsJ2 = actions;
    }

    public Collection<Observation> getObservationsJ1() {
        return observationsJ1;
    }

    public Collection<Observation> getObservationsJ2() {
        return observationsJ2;
    }

    public void setObservationsJ1(Collection<Observation> observations) {
        this.observationsJ1 = observations;
    }

    public void setObservationsJ2(Collection<Observation> observations) {
        this.observationsJ2 = observations;
    }

    public void setTransitions(TransitionSetGeneric<State, JointAction> transitions){

        this.transitions = transitions;
    }

    public void setRewards(RewardSetGeneric<State, JointAction> rewards){
        this.rewards = rewards;
    }

    public Map<Pair<State, JointAction>, Distribution<State>> getTransitions() {
        return transitions.getTransitions();
    }

    public HashSet<JointAction> getJointActions() throws Exception {
        HashSet<JointAction> jointActions = new HashSet<JointAction>();
        for (Action aj1 : this.getActionsJ1()) {
            for (Action aj2 : this.getActionsJ2()) {
                jointActions.add(new JointAction(aj1,aj2));
            }
        }
        return jointActions;
    }

    public void setObservationsProbabilities(ObservationSetGeneric obsProbabilities) {
        this.observationsProbabilities = obsProbabilities;
    }

    public ObservationSetGeneric getObservationProbabilities() {
        return this.observationsProbabilities;
    }

    public double computeReward(HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayerB1,
                                HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayerB2, OccupancyState<State,Action,Observation> o) throws Exception {
        double reward = 0.0;
        double rewardTemp = 0.0;

        if (o.getLocalHistoryJ1().isEmpty()){

            for (JointAction<Action> actionsJ1J2 : this.getJointActions()){
                for (State s : this.getStates()){

                    double tmp = 0.0;
                    for (State nextState : this.getStates()) {
                        double probaTransition = this.transitions.getProbability(s,actionsJ1J2,nextState);
                        reward += (distributionStrategyPlayerB1.get(distributionStrategyPlayerB1.keySet().iterator().next()).getWeight(actionsJ1J2.getActions().getElement0())
                                *distributionStrategyPlayerB2.get(distributionStrategyPlayerB2.keySet().iterator().next()).getWeight(actionsJ1J2.getActions().getElement1()))
                                *o.InitialBelief.getWeight(s)
                                *probaTransition
                                *this.rewards.getReward(s,actionsJ1J2,nextState);
                    }
                }
            }

        }
        else {
            for (Pair<State, JointHistory<Action, Observation>> pair : o.getDistrib().getNonZeroElements()) {

                for (JointAction<Action> a : this.getJointActions()) {
                    for (State nextState : this.getStates()) {
                        rewardTemp = o.getProba(pair);
                        rewardTemp *= distributionStrategyPlayerB1.get(pair.getElement1().getHistJ1()).getWeight(a.getActions().getElement0());
                        rewardTemp *= distributionStrategyPlayerB2.get(pair.getElement1().getHistJ2()).getWeight(a.getActions().getElement1());
                        rewardTemp *= transitions.getProbability(pair.getElement0(), a, nextState);
                        rewardTemp *= rewards.getReward(pair.getElement0(),a,nextState);

                        reward += rewardTemp;

                    }
                }

            }
        }
        return reward;
    }

    public double computeInitReward(Distribution<Action> distributionStrategyPlayerB1,
                                    Distribution<Action> distributionStrategyPlayerB2, OccupancyState o) throws Exception {
        double reward = 0.0;
        double rewardTemp = 0.0;
        for (State s : this.states){
            for(JointAction<Action> a : this.getJointActions()){
                for (State nextState : this.states) {
                    rewardTemp = o.InitialBelief.getWeight(s);

                    rewardTemp *= distributionStrategyPlayerB1.getWeight(a.getActions().getElement0());

                    rewardTemp *= distributionStrategyPlayerB2.getWeight(a.getActions().getElement1());

                    rewardTemp *= rewards.getReward(s,a,nextState);


                    rewardTemp *= transitions.getProbability(s,a,nextState);


                    reward += rewardTemp;
                }
            }
        }
        return reward;
    }

    public double computeMaxReward() {


        double max = -10000.0;
        double val = 0.0;
        for (Transition t : this.rewards.getRewards().keySet()){
            val = this.rewards.getRewards().get(t);
            if ( val>max ){
                max = val;
            }
        }

        return max;
    }

    public double computeMinReward() {


        double min = 100000;
        double val = 0.0;
        for (Transition t : this.rewards.getRewards().keySet()){
            val = this.rewards.getRewards().get(t);
            if ( val<min ){
                min = val;
            }
        }
        return min;
    }
    public Distribution<Action> generateInitialRandomDecisionRule(OccupancyState<State,Action,Observation> o, int player){
        RandomGenerator r = new RandomGenerator();
        Random random = new Random();
        Distribution<Action> distrib = new Distribution<>();
        if (player == 0) {
            Action a = (Action) this.actionsJ1.toArray()[random.nextInt(this.actionsJ1.size())];
            for (Action aa : this.actionsJ1) {
                if (aa.equals(a)) {
                    distrib.addWeight(aa, 1.0);
                }
                else{
                    distrib.addWeight(aa, 0.0);
                }
            }
        }
        if (player == 1) {
            Action a = (Action) this.actionsJ2.toArray()[random.nextInt(this.actionsJ2.size())];
            for (Action aa : this.actionsJ2) {
                if (aa.equals(a)) {
                    distrib.addWeight(aa, 1.0);
                }
                else{
                    distrib.addWeight(aa, 0.0);
                }
            }
        }
        return distrib;
    }
    public HashMap<History<Action, Observation>, Distribution<Action>> generateRandomDecisionRule(OccupancyState<State,Action,Observation> o, int player){
        RandomGenerator r = new RandomGenerator();
        Random random = new Random();
        HashMap<History<Action, Observation>, Distribution<Action>> distrib = new HashMap<>();
        if (player == 1) {
            for (History<Action, Observation> localHist : o.groupByPlayer1(o.groupByState(o.getDistribution().getNonZeroElements()))) {
                Distribution<Action> d = new Distribution<Action>();
                Action a = (Action) this.actionsJ1.toArray()[random.nextInt(this.actionsJ1.size())];
                for (Action aa : this.actionsJ1) {
                    if (aa.equals(a)) {
                        d.addWeight(aa, 1.0);
                    }
                    else{
                        d.addWeight(aa, 0.0);
                    }
                }
                d.normalize();
                distrib.put(localHist, d);
            }
        }
        else {
            for (History<Action, Observation> localHist : o.groupByPlayer2(o.groupByState(o.getDistribution().getNonZeroElements()))) {
                Distribution<Action> d = new Distribution<Action>();
                Action a = (Action) this.actionsJ2.toArray()[random.nextInt(this.actionsJ2.size())];
                for (Action aa : this.actionsJ2) {
                    if (aa.equals(a)) {
                        d.addWeight(aa, 1.0);
                    }
                    else{
                        d.addWeight(aa, 0.0);
                    }
                }
                d.normalize();
                distrib.put(localHist, d);
            }
        }
        return distrib;
    }
    public double computeInitRewardFromPureStrategies(Action aJ1, Action aj2, OccupancyState<State,Action,Observation> o) {
        double reward = 0.0;
        double rewardTemp = 0.0;
        JointAction<Action> a = new JointAction<>(aJ1,aj2);
        for (State s : this.states){
                for (State nextState : this.states) {
                    rewardTemp = o.InitialBelief.getWeight(s);
                    System.out.println("belief : " + s.toString() + " proba : " + o.InitialBelief.getWeight(s));
                    rewardTemp *= rewards.getReward(s,a,nextState);


                    rewardTemp *= transitions.getProbability(s,a,nextState);


                    reward += rewardTemp;
                }
            }
        return reward;
    }

    public double computeRewardFromPureStrategies(Action action, Action action1, State s) {
        double reward = 0.0;
        JointAction<Action> a = new JointAction<>(action,action1);

        for (State nextState : this.states) {
            double rewardTemp = 1.0;
            rewardTemp *= rewards.getReward(s,a,nextState);


            rewardTemp *= transitions.getProbability(s,a,nextState);


            reward += rewardTemp;
        }
        return reward;
    }

    public double getObsProba(Observation o, Action a, int player) throws Exception {
        double proba = 0.0;
        if (player == 1) {
            for (Observation oP2 : this.observationsJ2){
                for (Action aP2 : this.getActionsJ2()){
                    for (State s : this.getStates()) {
                        proba += this.getObservationProbabilities().getProba(new JointAction<Action>(a,aP2),s,new JointObservation<Observation>(o,oP2));
                    }
                }
            }
        }
        if (player == 2) {
            for (Observation oP1 : this.observationsJ1){
                for (Action aP1 : this.getActionsJ1()){
                    for (State s : this.getStates()) {
                        proba += this.getObservationProbabilities().getProba(new JointAction<Action>(aP1,a), s, new JointObservation<Observation>(oP1,o));
                    }
                }
            }
        }
        return proba;
    }

    public double getObsProba(OccupancyState<State,Action,Observation> occ,
                              JointHistory<Action,Observation> jointHistory, Observation o, JointAction<Action> a, int player) throws Exception {
        Double proba = 0.0;
        double tmp = 0.0;
        for (State s : this.getStates()){
            tmp+= occ.getProba(new Pair<State,JointHistory<Action,Observation>>(s,jointHistory));
        }
        if (occ.getTimestep()==0) {
            if (player % 2 == 1) {
                for (Observation oP2 : this.observationsJ2) {
                    for (State nextStep : this.getStates()) {
                        for (State s : this.getStates()) {
                            proba += this.getObservationProbabilities().getProba(a, nextStep, new JointObservation<Observation>(o, oP2))
                                    * occ.InitialBelief.getWeight(s) * transitions.getProbability(s, a, nextStep);
                        }
                    }
                }
            }
            if (player % 2 == 0) {
                for (Observation oP1 : this.observationsJ1) {
                    for (State nextStep : this.getStates()) {
                        for (State s : this.getStates()) {
                            proba += this.getObservationProbabilities().getProba(a, nextStep, new JointObservation<Observation>(oP1, o))
                                    * occ.InitialBelief.getWeight(s) * transitions.getProbability(s, a, nextStep);
                        }
                    }
                }
            }
            return proba;
        }
        else{
            if (player % 2 == 1) {
                for (Observation oP2 : this.observationsJ2) {
                    for (State nextStep : this.getStates()) {
                        for (State s : this.getStates()) {
                            proba += this.getObservationProbabilities().getProba(a, nextStep, new JointObservation<Observation>(o, oP2))
                                    * occ.getProba(new Pair<State,JointHistory<Action,Observation>>(s,jointHistory))/tmp * transitions.getProbability(s, a, nextStep);
                        }
                    }
                }
            }
            if (player % 2 == 0) {
                for (Observation oP1 : this.observationsJ1) {
                    for (State nextStep : this.getStates()) {
                        for (State s : this.getStates()) {
                            proba += this.getObservationProbabilities().getProba(a, nextStep, new JointObservation<Observation>(oP1, o))
                                    * occ.getProba(new Pair<State,JointHistory<Action,Observation>>(s,jointHistory))/tmp * transitions.getProbability(s, a, nextStep);
                        }
                    }
                }
            }
            if (proba.isNaN()){
                return 0.0;
            }
            return proba;
        }
    }

    public double getSigmaReward(OccupancyState<State, Action, Observation> o, JointHistory<Action,Observation> jointHistory, JointAction<Action> jointAction) throws Exception {


        Double value = 0.0;
        if (jointHistory.toString().equals("(,)")){
            return this.computeInitialReward(o,jointAction);

        }
        double tmp = 0.0;
        for (State temp : this.getStates()){
            tmp += o.getDistrib().getWeight(new Pair<State,JointHistory<Action,Observation>>(temp,jointHistory));
        }
        for (State s : this.getStates()){
            double probaOfState = o.getDistrib().getWeight(new Pair<State,JointHistory<Action,Observation>>(s,jointHistory));

            if (tmp != 0.0) {
                probaOfState /= tmp;
            }

            if (tmp==0.0){


                return 0.0;
            }
            for (State nextStep : this.getStates()){
                value += probaOfState * this.transitions.getProbability(s,jointAction,nextStep)*
                        this.rewards.getReward(s,jointAction,(State) nextStep);
            }
        }
        if (value.isNaN()){
            System.out.println("NAN REWARD !!!!!");

        }
        return value;
    }

    private double computeInitialReward(OccupancyState<State,Action,Observation> o, JointAction<Action> jointAction) throws Exception {
        double res = 0.0;
        for (State s : this.getStates()){
            for (State nextState : this.getStates()){
                res += o.InitialBelief.getWeight(s)
                        * this.rewards.getReward(s,jointAction,nextState)
                        * this.transitions.getProbability(s,jointAction,nextState);
            }
        }
        return res;
    }

    public Collection<Action> getActions(int i) {
        if (i%2==1) {
            return this.actionsJ1;
        }
        if (i%2==0){
            return this.actionsJ2;
        }
        System.out.println("?");
        return null;
    }

    public Collection<Observation> getObservations(int player) {
        if (player%2==1) {
            return this.observationsJ1;
        }
        if (player%2==0){
            return this.observationsJ2;
        }
        System.out.println("?");
        return null;
    }

    public double getProbaObs(Action aPlayerI, Action aNegI, State st, Observation zNextStepPlayerI, int player) throws Exception {
        double value = 0.0;
        JointAction<Action> jointAction;
        if (player%2==1) {
            jointAction = new JointAction<>(aPlayerI,aNegI);
        }
        else if (player%2==0){
            jointAction = new JointAction<>(aNegI,aPlayerI);
        }
        else{
            throw new Exception("cannot determine type");
        }
        JointObservation<Observation> jointObservation = null;
        for (State nextState : this.getStates()) {
            for (Observation zNegI : this.getObservations(player + 1 % 2)) {
                if (player % 2 == 1) {
                    jointObservation = new JointObservation<>(zNextStepPlayerI, zNegI);
                }
                if (player % 2 == 0) {
                    jointObservation = new JointObservation<>(zNegI, zNextStepPlayerI);
                }

                value += this.observationsProbabilities.getProba(jointAction, nextState, jointObservation) * transitions.getProbability(st,jointAction,nextState);

            }
        }

        return value;
    }

    public double getObsProbaPomdp(JointAction<Action> jointAction, State endState, Observation observation, int player){
        double res = 0.0;
            for (Observation zNegI : this.getObservations(player+1%2)){
                res += this.observationsProbabilities.getProba(jointAction,endState,new JointObservation(observation,zNegI,player));
        }
        return res;
    }

    public void checkSymetry() throws Exception {
        Set<Pair<State, JointAction>> setOfStateAction = this.transitions.getTransitions().keySet();
        for (Pair<State, JointAction> pair : setOfStateAction){
            for (State s: this.transitions.getTransitions().get(pair).getNonZeroElements()){
                JointAction jointAction = new JointAction<>(pair.getElement1().getActions().getElement1(),pair.getElement1().getActions().getElement0());
                Pair<State,JointAction> pairPrime = new Pair<>(pair.getElement0(),jointAction);
                if (this.transitions.getTransitions().get(pair).getProbability(s) != this.transitions.getTransitions().get(pairPrime).getProbability(s)){
                    System.out.println("transitions not symetrical!!!");
                    System.out.println("pair : " + pair + " pair prime : " + pairPrime);
                    System.exit(1);
                }
            }
        }


        for (State s : states){
            for (JointAction actJ1J2 : this.getJointActions()){
                for (JointObservation z : this.getJointObservations()){
                    JointAction jointAction = new JointAction<>(actJ1J2.getActions().getElement1(),actJ1J2.getActions().getElement0());
                    JointObservation jointObservation = new JointObservation<>(z.getObs().getElement1(),z.getObs().getElement0());
                   if(this.observationsProbabilities.getProba(actJ1J2,s,z) != this.observationsProbabilities.getProba(jointAction,s,jointObservation)){
                       System.out.println("observations not symetrical!!!");
                       System.out.println("pairAction : " + actJ1J2 + " pairprimeAction : " + jointAction);
                       System.out.println("pairObs : " + z + " pairprimeObs : " + jointObservation);
                       System.out.println("because values : " + (this.observationsProbabilities.getProba(actJ1J2,s,z)) + " and " + (this.observationsProbabilities.getProba(jointAction,s,jointObservation)));
                       System.exit(1);
                   }
                }
            }
        }


        for (Transition<State, JointAction> t : this.rewards.getRewards().keySet()){


            JointAction jointAction = new JointAction(t.action.getActions().getElement1(),t.action.getActions().getElement0());
            if (this.rewards.getReward(t.startState,t.action,t.endState) != - this.rewards.getReward(t.startState,jointAction,t.endState)){
                System.out.println("rewards not symetrical!!!");
                System.out.println("pair : " + t.action + " pairprime : " + jointAction);
                System.out.println("because values : " + (this.rewards.getReward(t.startState,t.action,t.endState) + " and " + this.rewards.getReward(t.startState,jointAction,t.endState)));
                System.exit(1);
            }
        }
    }

    public ArrayList<Observation> getPossibleObservations(Belief<State, Action, Observation> belief, Action maxiAction, int player) {
        ArrayList<Observation> list = new ArrayList<>();
        if (player%2==1) {
            for (Observation o : this.observationsJ1){
                if (this.getConditionalProbabilityOfObservation(belief,maxiAction,o,player)>0.0){
                    list.add(o);
                }
            }
            return list;
        }
        if (player%2==0){
            for (Observation o : this.observationsJ2){
                if (this.getConditionalProbabilityOfObservation(belief,maxiAction,o,player)>0.0){
                    list.add(o);
                }
            }
            return list;
        }
        return null;
    }

    public double computeReward(Belief<State, Action, Observation> belief, Action action, int player) throws Exception {
        double res = 0.0;
        for (Pair<State,History<Action,Observation>> pair : belief.getBelief().getNonZeroElements()){
            for (Action aNegI : this.getActions((player+1)%2)){
                res += belief.getBelief().getWeight(pair)
                        * this.rewards.getReward(pair.getElement0(),new JointAction<Action>(action,aNegI,player),this)
                        *(float)(1.0/this.getActions((player+1)%2).size());
            }
        }
        return res;
    }

    public double computeReward(PomdpSolverNotDynamic.Belief<State, Action, Observation> belief, Action action, int player) throws Exception {
        if (player%2==1){
            return this.maxReward;
        }
        if (player%2==0){
            return this.minReward;
        }


        double val = 0.0;
        if (player%2==1) {
            double[][] m = new double[this.getActions(player+1%2).size()][1];
            this.getGameMatrixP1(belief, action, m);
            CplexLastStepBelief<State, Action, Observation> cplex = new CplexLastStepBelief<>(this, new OccupancyState<>());
            val =  cplex.getActionMin(m).getElement1();
        }
        else{
            double[][] m = new double[this.getActions(player+1%2).size()][1];
            this.getGameMatrixP2(belief, action, m);
            CplexLastStepBelief<State, Action, Observation> cplex = new CplexLastStepBelief<>(this, new OccupancyState<>());
            val = cplex.getActionMax(m).getElement1();
        }

        return val;
    }

    public double getConditionalProbabilityOfObservation(Belief<State, Action, Observation> belief, Action maxiAction, Observation observation, int player) {


        double res = 0.0;

        for (Pair<State,History<Action,Observation>> pair : belief.getBelief().getNonZeroElements()){
            for (Action aNegI : this.getActions((player+1)%2)){
                for (Observation zNegI : this.getObservations((player+1)%2)){
                    for (State nextState : this.getStates()) {
                        JointAction<Action> jointAction = new JointAction<Action>(maxiAction, aNegI, player);
                        JointObservation<Observation> jointObservation = new JointObservation<Observation>(observation, zNegI, player);

                        History<Action, Observation> nextHistoryPlayerNegI = new History<>();
                        nextHistoryPlayerNegI.setListeActionObservation(pair.getElement1().getListeActionObservation());
                        nextHistoryPlayerNegI.addActionObservation(aNegI, zNegI);

                        res += belief.getBelief().getWeight(pair)
                                *this.getObservationProbability(observation,nextState,nextHistoryPlayerNegI,maxiAction,player)
                                *this.getTransitionProbability(nextState,nextHistoryPlayerNegI,maxiAction, pair.getElement0(),player,
                                    new BehavioralStrategy<>(),true);

                    }
                }
            }
        }
        return res;
    }
    public double getConditionalProbabilityOfObservation(PomdpSolverNotDynamic.Belief<State, Action, Observation> belief, Action maxiAction, Observation observation, int player) {

        double res = 0.0;

        for (State s : belief.getBelief().getNonZeroElements()){


                    for (State nextState : this.getStates()) {

                        res += belief.getBelief().getWeight(s)
                                * getObservationProbability(maxiAction,nextState,observation,player)
                                * getTransitionProbability(s,maxiAction,nextState,player);


            }
        }

        return res;
    }

    public Double computeRMaxMin(int player,History<Action,Observation> h, OccupancyState<State,Action,Observation> o) throws Exception {

        if (sawtoothBound!=null) {
            if (h.equals(new History<>())) {
                if (player % 2 == 1) {
                    Distribution<Pair<State, History<Action, Observation>>> distribution = new Distribution<>();
                    for (State s : this.states) {
                        distribution.addWeight(new Pair<>(s, h), initialBelief.getWeight(s));
                    }
                    return this.sawtoothBound.getValue(new Belief<>(this, distribution, 0, player));
                } else {
                    Distribution<Pair<State, History<Action, Observation>>> distribution = new Distribution<>();
                    for (State s : this.states) {
                        distribution.addWeight(new Pair<>(s, h), initialBelief.getWeight(s));
                    }
                    return this.lowerBoundHyperplane.getValue(new Belief<>(this, distribution, 0, player));
                }
            } else {
                SigmaOccupancyState<State, Action, Observation> s = new SigmaOccupancyState<>(o);
                s.ComputeSigmaMarginal(player);
                s.ComputeSigmaCondition(player);
                if (player % 2 == 1) {
                    return this.sawtoothBound.getValue(new Belief<>(this, s.computeConditionalStateHistoryDistrib(player).get(h), o.timestep, player));
                } else {
                    return this.lowerBoundHyperplane.getValue(new Belief<>(this, s.computeConditionalStateHistoryDistrib(player).get(h), o.timestep, player));

                }

            }
        }
        else{
            if (h.equals(new History<>())) {
                if (player % 2 == 1) {
                    Distribution<State> distribution = new Distribution<>();
                    for (State s : this.states) {
                        distribution.addWeight(s, initialBelief.getWeight(s));
                    }
                    return this.sawtoothBoundNotDynamic.getValue(new PomdpSolverNotDynamic.Belief<>(this, distribution, 0, player));
                } else {
                    Distribution<State> distribution = new Distribution<>();
                    for (State s : this.states) {
                        distribution.addWeight(s, initialBelief.getWeight(s));
                    }
                    return this.lowerBoundHyperplaneNotDynamic.getValue(new PomdpSolverNotDynamic.Belief<>(this, distribution, 0, player));
                }
            } else {
                SigmaOccupancyState<State, Action, Observation> s = new SigmaOccupancyState<>(o);


                if (player % 2 == 1) {
                    return this.sawtoothBoundNotDynamic.getValue(new PomdpSolverNotDynamic.Belief<>(this, s.computeStateDistribution(player).get(h), o.timestep, player));
                } else {
                    return this.lowerBoundHyperplaneNotDynamic.getValue(new PomdpSolverNotDynamic.Belief<>(this, s.computeStateDistribution(player).get(h), o.timestep, player));

                }

            }
        }
    }

    public double computeRMaxMin(int player, Belief<State, Action, Observation> newBelief) throws Exception {
        if (newBelief.timestep == this.profondeurMax -1){

            double MaxMin;
            if (player%2==1){
                MaxMin = -Double.POSITIVE_INFINITY;
            }
            else{
                MaxMin = Double.POSITIVE_INFINITY;
            }
            for (Action aPlayerI : this.getActions(player)){
                double valueAction = this.computeReward(newBelief,aPlayerI,player);
                if (player%2==1){
                    if (valueAction > MaxMin){
                        MaxMin = valueAction;
                    }
                }
                else{
                    if (valueAction < MaxMin){
                        MaxMin = valueAction;
                    }
                }
            }
            return MaxMin;
        }


        if (player%2==1){

            return this.sawtoothBound.getValue(newBelief);
        }

        else{

            return this.lowerBoundHyperplane.getValue(newBelief);
        }
    }

    public double computeRMaxMin(int player, PomdpSolverNotDynamic.Belief<State, Action, Observation> newBelief) throws Exception {

        if (player%2==1){
            return this.maxReward*(profondeurMax - newBelief.timestep);
        }
        if (player%2==0){
            return this.minReward*(profondeurMax - newBelief.timestep);
        }


        if (player%2==1){

            return this.sawtoothBoundNotDynamic.getValue(newBelief);
        }

        else{

            return this.lowerBoundHyperplaneNotDynamic.getValue(newBelief);
        }
    }

    public void setUpperBound(ValueFunctionBound<State, Action, Observation> upperBound) {
        this.sawtoothBound = (SawtoothBound<State, Action, Observation>) upperBound;
    }

    public void setLowerBound(ValueFunctionBound<State, Action, Observation> lowerBound) {
        this.lowerBoundHyperplane = (SawtoothBound<State, Action, Observation>) lowerBound;
    }

    public void setUpperBoundNotDynamic(PomdpSolverNotDynamic.ValueFunctionBound<State, Action, Observation> upperBound) {
        this.sawtoothBoundNotDynamic = (PomdpSolverNotDynamic.SawtoothBound<State, Action, Observation>) upperBound;
    }

    public void setLowerBoundNotDynamic(PomdpSolverNotDynamic.ValueFunctionBound<State, Action, Observation> lowerBound) {
        this.lowerBoundHyperplaneNotDynamic = (PomdpSolverNotDynamic.SawtoothBound<State, Action, Observation>) lowerBound;
    }

    public double getProbaObs(Action actionPlayerI, Observation zPlayerI, State nextState, int player) {
        double value = 0.0;
        JointObservation<Observation> jointObservation = null;
            for (Observation zNegI : this.getObservations(player + 1 % 2)) {
                for (Action aNegI : this.getActions(player+1%2)) {

                    value += this.observationsProbabilities.getProba(new JointAction(actionPlayerI,aNegI,player), nextState, new JointObservation(zPlayerI,zNegI,player))
                            * (float) (1.0/this.getActions(player+1%2).size());
                }

            }


        return value;
    }

    public double getProbaObs(PomdpSolverNotDynamic.Belief<State, Action, Observation> belief, Action maxiAction, Observation observation, int player) {
        double res = 0.0;
        for (State s : this.getStates()){
            res += belief.getBelief().getWeight(s)*getProbaObs(maxiAction,observation,s,player);
        }
        return res;
    }

    public double getObservationProbability(Action action, State endState, Observation observation, int player) {
        double res = 0.0;
        for (Action aNegI : this.getActions(player+1%2)){
            for (Observation zNegI : this.getObservations(player+1%2)){
                res += this.observationsProbabilities.getProba(new JointAction(action,aNegI,player),endState,new JointObservation(observation,zNegI,player))
                        * (float)(1.0/this.getActions(player+1%2).size());
            }
        }
        return res;
    }

    public double getTransitionProbability(State startState, Action action, State endState, int player) {
        double res = 0.0;
        for (Action aNegI : this.getActions(player+1%2)){
                res += this.transitions.getProbability(startState,new JointAction(action,aNegI,player),endState)
                        * (float)(1.0/this.getActions(player+1%2).size());
        }
        return res;
    }

    public double getRewardTangent(Action action, State startState, int player) throws Exception {
        double res = 0.0;
        for (Action aNegI : this.getActions(player+1%2)){
            res+=this.rewards.getReward(startState,new JointAction(action,aNegI,player),this)
                    *(float)(1.0/this.getActions(player+1%2).size());
        }
        return res;
    }

    public double getRewardTangent(PomdpSolverNotDynamic.Belief<State,Action,Observation> belief, Action action, int player) throws Exception {
        double res = 0.0;
        for (State s : this.getStates()){
            res+=belief.getBelief().getWeight(s)*this.getRewardTangent(action,s,player);
        }
        return res;
    }

    public double getProbaObs(JointAction jointAction, Observation zPlayerI, State sprime, int player) {
        double res = 0.0;
        for (Observation zNegI : this.getObservations(player+1%2)){
            res += this.observationsProbabilities.getProba(jointAction,sprime, new JointObservation<Observation>(zPlayerI,zNegI,player));
        }
        return res;
    }

    public double getObservationProbability(Observation zPlayerI, State nextState, History<Action, Observation> nextHistoryPlayerNegI, Action aPlayerI, int player) {
        double res = 0.0;
        double numerator = 0.0;
        double denominator = 0.0;
        Action aNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size()-1).getElement0();
        Observation zNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size()-1).getElement1();
        for (Observation zNegIDenominator : this.getObservations((player))){
            denominator += this.observationsProbabilities.getProba( new JointAction<Action> (aPlayerI,aNegI,player), nextState, new JointObservation<Observation>(zNegIDenominator,zNegI,player));
        }
        numerator = this.observationsProbabilities.getProba( new JointAction<Action> (aPlayerI,aNegI,player), nextState, new JointObservation<Observation>(zPlayerI,zNegI,player));
        if (denominator == 0.0){
            return 0.0;
        }
        res = numerator/denominator;
        return res;

    }

    public double getTransitionProbability(State nextState,History<Action,Observation> nextHistoryPlayerNegI,Action aPlayerI,State s, int player, BehavioralStrategy<State,Action,Observation> strat, boolean unif){
        if (!unif && strat.getStrategy().containsKey(nextHistoryPlayerNegI.getlastHistory())) {
            double res = 0.0;
            double sum = 0.0;
            Action aNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size()-1).getElement0();
            Observation zNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size()-1).getElement1();
            for (Observation zPlayerI : this.getObservations(player)) {
                sum += this.observationsProbabilities.getProba(new JointAction<Action>(aPlayerI, aNegI, player), nextState, new JointObservation<Observation>(zPlayerI, zNegI, player));
            }
            res = sum
                    * strat.getStrategy().get(nextHistoryPlayerNegI.getlastHistory()).getWeight(aNegI)
                    * transitions.getProbability(s, new JointAction(aPlayerI, aNegI, player), nextState);
            return res;
        }
        else{
            double res = 0.0;
            double sum = 0.0;
            Action aNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size() - 1).getElement0();
            Observation zNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size() - 1).getElement1();
            for (Observation zPlayerI : this.getObservations(player)) {
                sum += this.observationsProbabilities.getProba(new JointAction<Action>(aPlayerI, aNegI, player), nextState, new JointObservation<Observation>(zPlayerI, zNegI, player));
            }
            res = sum
                    * (float)(1.0/this.getActions(player+1%2).size())
                    * transitions.getProbability(s, new JointAction(aPlayerI, aNegI, player), nextState);
            return res;
        }
    }

    private void getGameMatrixP1(PomdpSolverNotDynamic.Belief<State,Action,Observation> b, Action a, double[][] m) throws Exception {
        for (int i = 0;i<1;i++){
            for (int j = 0;j<1;j++){


                double[][] Mij = new double[this.getActionsJ2().size()][this.getActionsJ1().size()];
                for (int k = 0;k<this.getActionsJ2().size();k++){
                    for (int l = 0;l<1;l++){
                        double sumOverStates = 0.0;
                            sumOverStates = 0.0;
                            for (State s :  this.getStates()) {
                                sumOverStates += b.getBelief().getWeight(s)
                                        *this.rewards.getReward(s,new JointAction(a,this.getActionsJ2().toArray()[k]),this);

                            }

                            Mij[k][l] = sumOverStates;

                        m[i*this.getActionsJ2().size()+k][0] =  - Mij[k][l];
                    }
                }
            }
        }
    }

    private void getGameMatrixP2(PomdpSolverNotDynamic.Belief<State,Action,Observation> b, Action a, double[][] m) throws Exception {
        for (int i = 0;i<1;i++){
            for (int j = 0;j<1;j++){


                double[][] Mij = new double[this.getActionsJ1().size()][this.getActionsJ2().size()];
                for (int k = 0;k<this.getActionsJ1().size();k++){
                    for (int l = 0;l<1;l++){
                        double sumOverStates = 0.0;
                        sumOverStates = 0.0;
                        for (State s :  this.getStates()) {
                                sumOverStates += b.getBelief().getWeight(s)
                                        *this.rewards.getReward(s,new JointAction(this.getActionsJ1().toArray()[k],a),this);
                            }
                            Mij[k][l] = sumOverStates;
                        m[i*this.getActionsJ1().size()+k][0] =  Mij[k][l];
                    }
                }
            }
        }
    }

}
