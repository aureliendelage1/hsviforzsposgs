package posg;

import java.util.ArrayList;

public class History<Action,Observation> {
    private int timestep;
    private ArrayList<Pair<Action,Observation>> listeActionObservation  = new ArrayList<Pair<Action,Observation>>();
    boolean voidHistory = false;

    public History(){
        voidHistory = true;
    }

    public History(Action a, Observation o){
        ArrayList<Pair<Action,Observation>> list = new ArrayList<>();
        list.add(new Pair<Action,Observation>(a,o));
        this.listeActionObservation = list;
    }

    public History(ArrayList<Pair<Action,Observation>> list){
        this.listeActionObservation = new ArrayList<>(list);
        voidHistory = false;
    }

    public History(String s) {

    }

    public int getTimestep() {
        return this.listeActionObservation.size();
    }

    public void setTimestep(int timestep) {
        this.timestep = timestep;
    }

    public ArrayList<Pair<Action, Observation>> getListeActionObservation() {
        return listeActionObservation;
    }

    public void setListeActionObservation(ArrayList<Pair<Action, Observation>> listeActionObservation) {
        this.listeActionObservation = new ArrayList<>(listeActionObservation);
    }

    public void addActionObservation(Action a, Observation z){
        this.listeActionObservation.add(new Pair<Action,Observation>(a,z));
        increaseTimestep();
    }

    public void increaseTimestep(){
        this.timestep = this.timestep+1;
    }

    public void append(Pair<Action,Observation> actionObservationPair) {
        this.listeActionObservation.add(actionObservationPair);
    }

    @Override
    public boolean equals(Object obj) {

        History h;
        try{
            h = (History) obj;
        }
        catch(Exception e){
            return false;
        }
        return h.getListeActionObservation().equals(this.listeActionObservation);
    }

    @Override
    public String toString() {
        String s = "";
        for (Pair<Action,Observation> pair : this.listeActionObservation){
            s+= pair.toString();
        }
        if (s.equals(""))
            return "none";
        return s;
    }

    @Override
    public int hashCode() {

        return this.listeActionObservation.hashCode();
    }

    public History<Action,Observation> getlastHistory() {

        if (this.listeActionObservation.size()==0){
            return new History<>();
        }
        History<Action,Observation> h = new History<>();
        ArrayList<Pair<Action,Observation>> list = new ArrayList<>(this.listeActionObservation);
        list.remove(list.size()-1);
        h.setListeActionObservation(list);

        return h;
    }

    public Action getLastAction() {
        if (this.listeActionObservation.size()>0) {
            return this.listeActionObservation.get(this.listeActionObservation.size() - 1).getElement0();
        }
        return (Action) new LocalAction();
    }

    public Observation getLastObservation() {
        if (this.listeActionObservation.size()>0) {
            return this.listeActionObservation.get(this.listeActionObservation.size() - 1).getElement1();
        }
        return (Observation) new LocalObservation("-");
    }

    public boolean extendsHist(History<Action, Observation> h, Action a) {


        if (this.listeActionObservation.size()==0 || this.listeActionObservation.size() <= h.listeActionObservation.size()){

            return false;
        }
        for (int i = 0;i<h.listeActionObservation.size();i++){
            if (!(h.listeActionObservation.get(i).equals(this.listeActionObservation.get((i))))){

                return false;
            }
        }
        if (this.listeActionObservation.get(h.listeActionObservation.size()).getElement0().equals(a)) {

            return true;
        }

        return false;
    }

    public void addActionObservationFirst(Action a, Observation lastObservation) {
        ArrayList<Pair<Action,Observation>> newList = new ArrayList<>();
        newList.add(new Pair<>(a,lastObservation));
        newList.addAll(new ArrayList<>(this.listeActionObservation));
        this.listeActionObservation = new ArrayList<>(newList);
    }

    public Action getFirstAction() {
        return this.listeActionObservation.get(0).getElement0();
    }

    public boolean extendsHist(History<Action, Observation> h, Action a, Observation o) {


        if (this.listeActionObservation.size()==0 || this.listeActionObservation.size() <= h.listeActionObservation.size()){

            return false;
        }
        for (int i = 0;i<h.listeActionObservation.size();i++){
            if (!(h.listeActionObservation.get(i).equals(this.listeActionObservation.get((i))))){

                return false;
            }
        }
        if (this.listeActionObservation.get(h.listeActionObservation.size()).getElement0().equals(a)
                &&  this.listeActionObservation.get(h.listeActionObservation.size()).getElement1().equals(o)  ) {

            return true;
        }

        return false;
    }

    public Observation getFirstObservation() {
        return this.listeActionObservation.get(0).getElement1();
    }

    public Action getSecondAction() {
        return this.listeActionObservation.get(1).getElement0();
    }
}
