package posg;

import java.util.ArrayList;
import java.util.HashMap;

public class DecisionRuleGenerator<State,Action,Observation> {

    public HashMap<HashMap<History<Action,Observation>, Action>, Double> decisionRule = new HashMap<>();
    public SigmaOccupancyState<State,Action,Observation> sigmaOcc;
    private Posg<State,Action,Observation> posg;

    public DecisionRuleGenerator(Posg<State,Action,Observation> posg){
        this.posg = posg;
    }

    public DecisionRuleGenerator(SigmaOccupancyState<State,Action,Observation> s){
        this.sigmaOcc = s;
        this.posg = s.o.POSG;
    }

    public DecisionRuleGenerator(History<Action, Observation> historyStrategy, Action a, DecisionRuleGenerator<State,Action, Observation> decisionRuleGenerator) {
    }

    public ArrayList<DecisionRule<State,Action,Observation>> generateDecisionRule(int player, ArrayList<History<Action,Observation>> list) {
        ArrayList<DecisionRule<State,Action,Observation>> res = new ArrayList<>();
        ArrayList<Pair<HashMap<History<Action,Observation>,Action>,Double>> hashmapList = new ArrayList();
        try {

            for (History<Action, Observation> h : list) {
                hashmapList = this.helperMakingDecisionRules(hashmapList, h, player, list);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        for (Pair<HashMap<History<Action,Observation>,Action>,Double> h : hashmapList){
            res.add(new DecisionRule<>(h.getElement0(),h.getElement1()));
        }
        return res;
    }

    private ArrayList<Pair<HashMap<History<Action,Observation>,Action>,Double>> helperMakingDecisionRules(
            ArrayList<Pair<HashMap<History<Action, Observation>, Action>, Double>> hashmapList,
            History<Action, Observation> history,
            int player, ArrayList<History<Action,Observation>> list)
    {
        ArrayList<Pair<HashMap<History<Action,Observation>,Action>,Double>> res = new ArrayList<>();
        if (hashmapList.size()==0){
            for (Action a : this.posg.getActions(player)){
                HashMap<History<Action,Observation>,Action> h = new HashMap<>();
                h.put(history,a);
                res.add(new Pair<>(new HashMap<>(h),0.0));

            }
        }
        else{
            for (Pair<HashMap<History<Action,Observation>,Action>,Double> hashmap : hashmapList){
                for (Action a : this.posg.getActions(player)){
                    hashmap.getElement0().put(history,a);
                    res.add(new Pair<>(new HashMap<History<Action,Observation>,Action>(hashmap.getElement0()),0.0));

                }
            }
        }
        return res;
    }

    private HashMap<Integer, ArrayList<Integer>> renameHashMapKeys(HashMap<Integer, ArrayList<Integer>> hashmap) {

        int indice = 0;
        HashMap<Integer, ArrayList<Integer>> hashMapCopie = new HashMap<>();
        for (Integer i : hashmap.keySet()){
            hashMapCopie.put(indice,hashmap.get(i));
            indice++;
        }
        hashmap = new HashMap<>(hashMapCopie);

        return hashmap;
    }


}
