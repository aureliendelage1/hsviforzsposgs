package posg;

import java.io.Serializable;
public class Transition<State,Action> implements Serializable {


    private static final long serialVersionUID = 1L;
    final public  State startState;
    final public Action action;
    final public State endState;

    public Transition(State startState, Action action, State endState) {
        if(startState == null || action == null || endState == null) {
            throw new NullPointerException("The starting posg.state, the posg.action and the ending posg.state shouldn't be null in a transition");
        }
        this.startState = startState;
        this.action = action;
        this.endState = endState;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transition)) return false;
        Transition<?, ?> that = (Transition<?, ?>) o;
        return startState.equals(that.startState) && action.equals(that.action) && endState.equals(that.endState);
    }
    @Override
    public int hashCode() {
        int result = startState.hashCode();
        result = 31 * result + action.hashCode();
        result = 31 * result + endState.hashCode();
        return result;
    }
}
