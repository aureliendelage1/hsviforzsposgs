package posg;

import util.Distribution;

import java.util.Collection;

abstract public class TransitionSet<State,Action> {

    public abstract void setDistribution(State startState, Action action, Distribution<State> endStates);

    public abstract void setProbability(State startState, Action action, State endState, double probability);

    public abstract void initGetters();

    public abstract State sampleNextState(State startState, Action action);

    public abstract double getProbability(State startState, Action action, State endState);

    public abstract Collection<State> getPossibleNextStates(State startState, Action action);

}
