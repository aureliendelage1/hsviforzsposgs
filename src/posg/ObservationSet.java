package posg;

import util.Distribution;

import java.util.Collection;

public abstract class ObservationSet<State,Action,Observation> {
    public abstract void setProbability(Action action, State endState, Observation observation, double probability);

    public abstract void setDistribution(Action action, State endState, Distribution<Observation> distribution);

    public abstract void initGetters();

    public abstract double getProbability(Action action, State endState, Observation observation);

    public abstract Collection<Observation> getPossibleObservations(Action action, State endState);

    public abstract Observation sampleObservation(Action action, State endState);
}
