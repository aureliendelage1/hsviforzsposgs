package posg;

import java.util.ArrayList;
import java.util.Objects;

public class Pair<K, V> {

    private  K element0;
    private  V element1;

    public Pair() {

    }

    public static <K, V> Pair<K, V> createPair(K element0, V element1) {
        return new Pair<K, V>(element0, element1);
    }

    public Pair(K element0, V element1) {
        this.element0 = element0;
        this.element1 = element1;
    }


    public K getElement0() {
        return element0;
    }

    public V getElement1() {
        return element1;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) {
            return false;
        }
        Pair<?, ?> p = (Pair<?, ?>) o;
        if (p.getElement0().equals(element0) && p.getElement1().equals(element1)){
        }
        return (p.getElement0().equals(element0) && p.getElement1().equals(element1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(element0,element1);
    }

    @Override
    public String toString() {
        return "("+this.element0.toString() + "," + this.element1.toString() + ")";
    }

}