package posg;

import Approximations.Vector;
import PomdpSolver.Belief;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SigmaOccupancyState<State,Action,Observation> {
    public OccupancyState<State,Action,Observation> o;

    public Distribution<JointHistory<Action,Observation>> sigmaDistrib = new Distribution<>();

    public Distribution<History<Action,Observation>> distribMarginaleP1;
    public Distribution<History<Action,Observation>> distribMarginaleP2;

    public HashMap<History<Action,Observation>,Distribution<History<Action,Observation>>> distribConditionnelleP1;
    public HashMap<History<Action,Observation>,Distribution<History<Action,Observation>>> distribConditionnelleP2;

    public HashMap<History<Action,Observation>,Distribution<Pair<State,History<Action,Observation>>>> distribCondWithStateP1;
    public HashMap<History<Action,Observation>,Distribution<Pair<State,History<Action,Observation>>>> distribCondWithStateP2;

    boolean initialTimestep = false;

    public HashMap<History<Action,Observation>,Double> listNewBeliefsP1 = new HashMap<>();
    public HashMap<History<Action,Observation>,Double> listNewBeliefsP2 = new HashMap<>();
    private HashMap<History<Action,Observation>,Distribution<State>> distribStateP1;
    private HashMap<History<Action,Observation>,Distribution<State>> distribStateP2;

    public HashMap<History<Action,Observation>,Distribution<History<Action,Observation>>> tc1;
    public HashMap<History<Action,Observation>,Distribution<History<Action,Observation>>> tc2;

    public SigmaOccupancyState(OccupancyState<State,Action,Observation> o){
        this.o = o;
        this.setSigmaDistrib();
    }

    public SigmaOccupancyState(SigmaOccupancyState<State,Action,Observation> s){
        this.o = s.o.copy();
        this.setSigmaDistrib(s);
        this.setSigmaDistrib();
        this.listNewBeliefsP1 = s.listNewBeliefsP1;
        this.listNewBeliefsP2 = s.listNewBeliefsP2;
    }

    public HashMap<History<Action,Observation>,Double> getListNewBeliefs(int player){
        if (player%2==1){
            return this.listNewBeliefsP1;
        }
        else{
            return this.listNewBeliefsP2;
        }
    }

    private void setSigmaDistrib(SigmaOccupancyState<State,Action,Observation> s) {
        if (s.distribMarginaleP1!=null){
            this.distribMarginaleP1 = s.distribMarginaleP1;
        }
        if (s.distribConditionnelleP1!=null){
            this.distribConditionnelleP1 = s.distribConditionnelleP1;
        }
        if (s.distribCondWithStateP1!=null){
            this.distribCondWithStateP1 = s.distribCondWithStateP1;
        }
        if (s.distribMarginaleP2!=null){
            this.distribMarginaleP2 = s.distribMarginaleP2;
        }
        if (s.distribConditionnelleP2!=null){
            this.distribConditionnelleP2 = s.distribConditionnelleP2;
        }
        if (s.distribCondWithStateP2!=null){
            this.distribCondWithStateP2 = s.distribCondWithStateP2;
        }
    }

    public SigmaOccupancyState(){
        initialTimestep = true;
    }

    public void setSigmaDistrib(){
        Distribution<JointHistory<Action,Observation>> distrib = new Distribution<>();
        for (Pair<State,JointHistory<Action,Observation>> key : o.getDistrib().getNonZeroElements()) {
            if (distrib.getWeights().containsKey(key.getElement1())){
                double weight = o.getProba(key);
                double tmp = distrib.getWeight(key.getElement1());
                distrib.setWeight(key.getElement1(),weight + tmp);
            }
            else{
                distrib.addWeight(key.getElement1(),o.getProba(key));
            }
        }
        this.sigmaDistrib = distrib;
    }

    public Distribution<History<Action,Observation>> ComputeSigmaMarginal(int player) throws Exception {
        Distribution<History<Action, Observation>> distrib = new Distribution<>();
        if (player%2==1) {
            if (o.getLocalHistoryJ1().isEmpty()){
                History<Action,Observation> voidHistory = new History<>();
                distrib.addWeight(voidHistory,1);
            }
            else {
                for (History<Action, Observation> historyP1 : o.getLocalHistoryJ1()) {
                    double proba = 0.0;
                    for (History<Action, Observation> historyP2 : o.getLocalHistoryJ2()) {
                        proba += this.sigmaDistrib.getWeight(new JointHistory<>(historyP1, historyP2));
                    }
                    distrib.addWeight(historyP1, proba);
                }
            }
            this.distribMarginaleP1 = distrib;
            return distrib;
        }
        else{
            if (o.getLocalHistoryJ2().isEmpty()){
                History<Action,Observation> voidHistory = new History<>();
                distrib.addWeight(voidHistory,1);
            }
            else {
                for (History<Action, Observation> historyP2 : o.getLocalHistoryJ2()) {
                    double proba = 0.0;
                    for (History<Action, Observation> historyP1 : o.getLocalHistoryJ1()) {
                        proba += this.sigmaDistrib.getWeight(new JointHistory<>(historyP1, historyP2));
                    }
                    distrib.addWeight(historyP2, proba);
                }
            }
            this.distribMarginaleP2 = distrib;
            return distrib;
        }
    }

    public HashMap<History<Action,Observation>,Distribution<History<Action,Observation>>> ComputeSigmaCondition(int player) throws Exception {
        HashMap<History<Action,Observation>,Distribution<History<Action,Observation>>> distribCondition = new HashMap<>();

        if (player%2==1){
            if (!(this.distribConditionnelleP1 == null)){
                return this.distribConditionnelleP1;
            }
            if (this.distribMarginaleP1==null){
                this.ComputeSigmaMarginal(1);
            }
            for (History<Action, Observation> historyP1 : o.getLocalHistoryJ1()) {
                Distribution<History<Action,Observation>> distrib = new Distribution<>();
                for (History<Action, Observation> historyP2 : o.getLocalHistoryJ2()) {
                    distrib.addWeight(historyP2,this.sigmaDistrib.getWeight(
                            new JointHistory<Action,Observation>(historyP1,historyP2))/this.distribMarginaleP1.getWeight(historyP1));
                }
                distribCondition.put(historyP1,distrib);
            }
            this.distribConditionnelleP1 = distribCondition;
            return distribCondition;
        }
        else{
            if (!(this.distribConditionnelleP2 == null)){
                return this.distribConditionnelleP2;
            }
            if (this.distribMarginaleP2 == null){
                this.ComputeSigmaMarginal(2);
            }
            for (History<Action, Observation> historyP2 : o.getLocalHistoryJ2()) {
                Distribution<History<Action,Observation>> distrib = new Distribution<>();
                for (History<Action, Observation> historyP1 : o.getLocalHistoryJ1()) {
                    distrib.addWeight(historyP1,this.sigmaDistrib.getWeight(
                            new JointHistory<Action,Observation>(historyP1,historyP2))/this.distribMarginaleP2.getWeight(historyP2));
                }
                distribCondition.put(historyP2,distrib);
            }
            this.distribConditionnelleP2 = distribCondition;
        }
        return distribCondition;
    }

    public double getNorme1(SigmaOccupancyState<State, Action, Observation> sprime) {
        double normeValue = 0.0;
        ArrayList<JointHistory<Action,Observation>> listJointHistory = new ArrayList<>();
        for (JointHistory<Action,Observation> jh : this.sigmaDistrib.getNonZeroElements()){
            listJointHistory.add(jh);
            normeValue += Math.abs(this.sigmaDistrib.getWeight(jh) - sprime.sigmaDistrib.getWeight(jh));
        }
        for (JointHistory<Action,Observation> jh : sprime.sigmaDistrib.getNonZeroElements()){
            if (!(listJointHistory.contains(jh))) {
                normeValue += Math.abs(this.sigmaDistrib.getWeight(jh) - sprime.sigmaDistrib.getWeight(jh));
            }
        }
        return normeValue;
    }

    public double getConditionalValue(History<Action, Observation> histPlayerI, History<Action, Observation> histPlayerNegI, int player) throws Exception {
        if (this.o.getTimestep() == 0){
                return 1.0;
        }
        if (player%2 == 1){
            if (this.distribConditionnelleP1.containsKey(histPlayerI)) {
                return this.distribConditionnelleP1.get(histPlayerI).getWeight(histPlayerNegI);
            }
            else{
                return 0.0;
            }
        }
        if (player%2 == 0){
            if (this.distribConditionnelleP2.containsKey(histPlayerI)) {
                return this.distribConditionnelleP2.get(histPlayerI).getWeight(histPlayerNegI);
            }
            else{
                return 0.0;
            }
        }
        System.exit(1);
        return -100000.0;
    }

    public double getMarginalValue(History<Action, Observation> histPlayerI, int player) throws Exception {
        if (player%2==1){
            if (this.distribMarginaleP1==null){
                this.ComputeSigmaMarginal(player);
            }
            return this.distribMarginaleP1.getWeight(histPlayerI);
        }
        else {
            if (this.distribMarginaleP2==null){
                this.ComputeSigmaMarginal(player);
            }
            return this.distribMarginaleP2.getWeight(histPlayerI);
        }
    }

    public Distribution<History<Action,Observation>> getDistribMarginale(int player) throws Exception {
        if (player%2==1){
            if (distribMarginaleP1 == null){
                this.ComputeSigmaMarginal(player);
            }
            return distribMarginaleP1;
        }
        else{
            if (distribMarginaleP2 == null){
                this.ComputeSigmaMarginal(player);
            }
            return distribMarginaleP2;
        }
    }

    public HashMap<History<Action,Observation>,Distribution<Pair<State,History<Action,Observation>>>> computeConditionalStateHistoryDistrib(int player) {
        HashMap<History<Action,Observation>,Distribution<Pair<State,History<Action,Observation>>>> hashMap = new HashMap<>();
        if (o.timestep==0){
            History<Action,Observation> h = new History<>();
            Distribution<Pair<State,History<Action,Observation>>> distribution = new Distribution<>();
            for (State s : o.POSG.getStates()){
                distribution.addWeight(new Pair<>(s,h),o.InitialBelief.getWeight(s));
            }
            hashMap.put(new History<>(),distribution);
            if (player%2==1){
                this.distribCondWithStateP1 = hashMap;
            }
            else{
                this.distribCondWithStateP2 = hashMap;
            }
            return hashMap;
        }
        if (player%2==1) {
            for (Pair<State, JointHistory<Action, Observation>> pair : o.getDistrib().getNonZeroElements()) {
                double tmp = this.distribMarginaleP1.getWeight(pair.getElement1().getHistJ1());
                double proba = o.getProba(pair) / tmp;
                if (!hashMap.containsKey(pair.getElement1().getHistJ1())) {
                    Distribution<Pair<State, History<Action, Observation>>> distrib = new Distribution<>();
                    distrib.addWeight(new Pair<>(pair.getElement0(),pair.getElement1().getHistJ2()),proba);
                    hashMap.put(pair.getElement1().getHistJ1(),distrib);
                }
                else{
                    hashMap.get(pair.getElement1().getHistJ1()).addWeight(new Pair<>(pair.getElement0(),pair.getElement1().getHistJ2()),proba);
                }
            }
        }
        if (player%2==0) {
            for (Pair<State, JointHistory<Action, Observation>> pair : o.getDistrib().getNonZeroElements()) {
                double tmp = this.distribMarginaleP2.getWeight(pair.getElement1().getHistJ2());
                double proba = o.getProba(pair) / tmp;
                if (!hashMap.containsKey(pair.getElement1().getHistJ2())) {
                    Distribution<Pair<State, History<Action, Observation>>> distrib = new Distribution<>();
                    distrib.addWeight(new Pair<>(pair.getElement0(),pair.getElement1().getHistJ1()),proba);
                    hashMap.put(pair.getElement1().getHistJ2(),distrib);
                }
                else{
                    hashMap.get(pair.getElement1().getHistJ2()).addWeight(new Pair<>(pair.getElement0(),pair.getElement1().getHistJ1()),proba);
                }
            }
        }
        if (player%2==1) {
            this.distribCondWithStateP1 = hashMap;
        }
        else{
            this.distribCondWithStateP2 = hashMap;
        }
        return hashMap;
    }

    public void makeSureDistributionsAreCalculated(int player) throws Exception {
        if (this.sigmaDistrib == null){
            this.setSigmaDistrib();
        }
        if (player%2==1){
            if (this.distribMarginaleP1==null){
                this.ComputeSigmaMarginal(player);
            }
            if (this.distribConditionnelleP1==null){
                this.ComputeSigmaCondition(player);
            }
            if (this.distribCondWithStateP1==null){
                this.distribCondWithStateP1 = this.computeConditionalStateHistoryDistrib(player);
            }
        }
        if (player%2==0){
            if (this.distribMarginaleP2==null){
                this.ComputeSigmaMarginal(player);
            }
            if (this.distribConditionnelleP2==null){
                this.ComputeSigmaCondition(player);
            }
            if (this.distribCondWithStateP2==null){
                this.distribCondWithStateP2 = this.computeConditionalStateHistoryDistrib(player);
            }
        }
    }

    public Belief<State,Action,Observation> getDistribCondWithState(Posg<State,Action,Observation> posg, int player,History<Action,Observation> h, int timestep) {
        if (player%2==1){
            if (distribCondWithStateP1 == null){
                this.computeConditionalStateHistoryDistrib(player);
            }
            return new Belief<State,Action,Observation>(posg, this.distribCondWithStateP1.get(h),timestep,player);
        }
        else{
            if (distribCondWithStateP2 == null){
                this.computeConditionalStateHistoryDistrib(player);
            }
            return new Belief<State,Action,Observation>(posg, this.distribCondWithStateP2.get(h),timestep,player);
        }
    }

    public HashMap<History<Action,Observation>, Distribution<State>> computeStateDistribution(int player) throws Exception {

        if (o.timestep==0){
            HashMap<History<Action,Observation>,Distribution<State>> hashMap = new HashMap<>();
            History<Action,Observation> h = new History<>();
            Distribution<State> distribution = new Distribution<>();
            for (State s : o.POSG.getStates()){
                distribution.addWeight(s,o.InitialBelief.getWeight(s));
            }
            hashMap.put(new History<>(),distribution);
            if (player%2==1){
                this.distribStateP1 = hashMap;
            }
            else{
                this.distribStateP2 = hashMap;
            }
            return hashMap;
        }
        HashMap<History<Action,Observation>, Distribution<State>> hashMap = new HashMap<>();
        for (History<Action,Observation> h : this.getDistribMarginale(player).getNonZeroElements()) {
            Distribution<State> d = new Distribution<>();
            for (State s : this.o.POSG.getStates()) {
                double val = 0.0;
                for (History<Action, Observation> hNegI : this.getDistribMarginale(player + 1 % 2).getNonZeroElements()) {
                    val += this.o.getProba(new Pair<>(s,new JointHistory<Action,Observation>(h,hNegI,player)));
                }
                d.addWeight(s,val/this.getDistribMarginale(player).getWeight(h));
            }
            hashMap.put(h,d);
        }
        if (player%2==1){
            this.distribStateP1 = hashMap;
        }
        else{
            this.distribStateP2 = hashMap;
        }
        return hashMap;
    }

    public PomdpSolverNotDynamic.Belief<State,Action,Observation> getStateDistrib(Posg<State, Action, Observation> posg, int player, History<Action, Observation> history, int timestep) {
        if (player%2==1){
            if (this.distribStateP1==null){
                try {
                    this.computeStateDistribution(player);
                    return new PomdpSolverNotDynamic.Belief<>(posg,this.distribStateP1.get(history),timestep,player) ;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            else{
                return new PomdpSolverNotDynamic.Belief<>(posg,this.distribStateP1.get(history),timestep,player) ;
            }
        }
        else{
            if (this.distribStateP2==null){
                try {
                    this.computeStateDistribution(player);
                    return new PomdpSolverNotDynamic.Belief<>(posg,this.distribStateP2.get(history),timestep,player) ;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            else{
                return new PomdpSolverNotDynamic.Belief<>(posg,this.distribStateP2.get(history),timestep,player) ;
            }
        }
        return null;
    }

    public Distribution<State> getNewDistrib(History<Action, Observation> nextHistory, int player, BehavioralStrategy<State,Action,Observation> strat) {
        Distribution<State> distrib = this.getBeliefDistrib();

        Distribution<State> newDistribution = new Distribution<>();

        Action aPlayerI = nextHistory.getLastAction();
        Observation zPlayerI = nextHistory.getLastObservation();

        double res = 0.0;

        double denominator = 0.0;
        for (State lastState : this.o.POSG.getStates()){
            for (State sprime : this.o.POSG.getStates()){
                for (History<Action,Observation> hNegI : this.getDistribCond(player, nextHistory.getlastHistory()).getNonZeroElements()) {
                    for (Action aNegI : this.o.POSG.getActions(player+1%2)){
                        for (Observation zNegI : this.o.POSG.getObservations(player+1%2)){
                            denominator += this.o.POSG.observationsProbabilities.getProba(new JointAction(aPlayerI,aNegI,player),sprime,new JointObservation(zPlayerI,zNegI,player))
                                    * this.o.POSG.transitions.getProbability(lastState,new JointAction(aPlayerI,aNegI,player),sprime)
                                    * strat.getStrategy().get(hNegI).getWeight(aNegI)
                                    * distrib.getWeight(lastState)
                                    * this.getDistribCond(player,nextHistory.getlastHistory()).getWeight(hNegI);

                        }
                    }
                }
            }
        }

        for (State nextState : this.o.POSG.getStates()){
            double numerator = 0.0;

            for (State lastState : this.o.POSG.getStates()){
                for (History<Action,Observation> hNegI : this.getDistribCond(player, nextHistory.getlastHistory()).getNonZeroElements()) {
                    for (Action aNegI : this.o.POSG.getActions(player+1%2)){
                        for (Observation zNegI : this.o.POSG.getObservations(player+1%2)){
                            numerator += this.o.POSG.observationsProbabilities.getProba(new JointAction(aPlayerI,aNegI,player),nextState,new JointObservation(zPlayerI,zNegI,player))
                                    * this.o.POSG.transitions.getProbability(lastState,new JointAction(aPlayerI,aNegI,player),nextState)
                                    * strat.getStrategy().get(hNegI).getWeight(aNegI)
                                    * distrib.getWeight(lastState)
                                    * this.getDistribCond(player,nextHistory.getlastHistory()).getWeight(hNegI);

                        }
                    }
                }
            }

            if (denominator > 0.0){
                res = numerator/denominator;
            }
            newDistribution.addWeight(nextState,res);
        }

        try {
            newDistribution.sanityCheck();

        }
        catch (Exception e){
            e.printStackTrace();
        }

        return newDistribution;
    }

    public Distribution<History<Action,Observation>> getNewDistribCond(History<Action, Observation> nextHistory, int player, BehavioralStrategy<State,Action,Observation> strat) {
        Distribution<State> distrib = this.getBeliefDistrib();

        Distribution<History<Action,Observation>> newDistribution = new Distribution<>();

        Action aPlayerI = nextHistory.getLastAction();
        Observation zPlayerI = nextHistory.getLastObservation();

        double res = 0.0;

        double denominator = 0.0;
        for (State lastState : this.o.POSG.getStates()){
            for (State sprime : this.o.POSG.getStates()){
                for (History<Action,Observation> hNegI : this.getDistribCond(player, nextHistory.getlastHistory()).getNonZeroElements()) {
                    for (Action aNegI : this.o.POSG.getActions(player+1%2)){
                        for (Observation zNegI : this.o.POSG.getObservations(player+1%2)){
                            if (strat.getStrategy().containsKey(hNegI)) {
                                denominator += this.o.POSG.observationsProbabilities.getProba(new JointAction(aPlayerI, aNegI, player), sprime, new JointObservation(zPlayerI, zNegI, player))
                                        * this.o.POSG.transitions.getProbability(lastState, new JointAction(aPlayerI, aNegI, player), sprime)
                                        * strat.getStrategy().get(hNegI).getWeight(aNegI)
                                        * distrib.getWeight(lastState)
                                        * this.getDistribCond(player, nextHistory.getlastHistory()).getWeight(hNegI);
                            }
                            else{
                                denominator += this.o.POSG.observationsProbabilities.getProba(new JointAction(aPlayerI, aNegI, player), sprime, new JointObservation(zPlayerI, zNegI, player))
                                        * this.o.POSG.transitions.getProbability(lastState, new JointAction(aPlayerI, aNegI, player), sprime)
                                        * (float) (1.0/this.o.POSG.getActions(player).size())
                                        * distrib.getWeight(lastState)
                                        * this.getDistribCond(player, nextHistory.getlastHistory()).getWeight(hNegI);
                            }

                        }
                    }
                }
            }
        }
        for (History<Action,Observation> hNegI : this.getDistribCond(player, nextHistory.getlastHistory()).getNonZeroElements()) {
            for (Action aNegI : this.o.POSG.getActions(player+1%2)){
                for (Observation zNegI : this.o.POSG.getObservations(player+1%2)){
                    History<Action,Observation> nextHistoryNegI = new History<>(hNegI.getListeActionObservation());
                    nextHistoryNegI.addActionObservation(aNegI,zNegI);
                    double numerator = 0.0;
                    for (State nextState : this.o.POSG.getStates()){
                        for (State lastState : this.o.POSG.getStates()){
                            if (strat.getStrategy().containsKey(hNegI)) {
                                numerator += this.o.POSG.observationsProbabilities.getProba(new JointAction(aPlayerI,aNegI,player),nextState,new JointObservation(zPlayerI,zNegI,player))
                                        * this.o.POSG.transitions.getProbability(lastState,new JointAction(aPlayerI,aNegI,player),nextState)
                                        * strat.getStrategy().get(hNegI).getWeight(aNegI)
                                        * distrib.getWeight(lastState)
                                        * this.getDistribCond(player,nextHistory.getlastHistory()).getWeight(hNegI);
                            }
                            else{
                                numerator += this.o.POSG.observationsProbabilities.getProba(new JointAction(aPlayerI,aNegI,player),nextState,new JointObservation(zPlayerI,zNegI,player))
                                        * this.o.POSG.transitions.getProbability(lastState,new JointAction(aPlayerI,aNegI,player),nextState)
                                        * (float)(1.0/this.o.POSG.getActions(player).size())
                                        * distrib.getWeight(lastState)
                                        * this.getDistribCond(player,nextHistory.getlastHistory()).getWeight(hNegI);
                            }

                        }
                    }
                    if (denominator > 0.0){

                        res = numerator/denominator;
                    }
                    else{

                    }
                    newDistribution.addWeight(nextHistoryNegI,res);
                }
            }
        }

        try {
            newDistribution.sanityCheck();

        }
        catch (Exception e){
            e.printStackTrace();
        }

        return newDistribution;
    }

    public Distribution<History<Action,Observation>> getDistribCond(int player, History<Action,Observation> histPlayerI){
        if (this.o.timestep >0) {
            if (player % 2 == 1) {
                return this.distribConditionnelleP1.get(histPlayerI);
            } else {
                return this.distribConditionnelleP2.get(histPlayerI);
            }
        }
        else{
            Distribution<History<Action,Observation>> d = new Distribution<>();
            d.addWeight(new History<>(),1.0);
            return d;
        }
    }

    private Distribution<State> getBeliefDistrib() {
        Distribution<State> distrib = new Distribution<>();
        if (this.o.timestep > 0) {
            for (Pair<State, JointHistory<Action, Observation>> pair : this.o.getDistrib().getNonZeroElements()) {
                if (distrib.getNonZeroElements().contains(pair.getElement0())) {
                    double val = distrib.getWeight(pair.getElement0());
                    distrib.setWeight(pair.getElement0(), val + o.getProba(pair));
                } else {
                    distrib.addWeight(pair.getElement0(), o.getProba(pair));
                }
            }

            try {
                distrib.sanityCheck();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return distrib;
        }
        else{
            return this.o.InitialBelief;
        }
    }

    @Override
    public String toString(){
        return this.o.toString();
    }

    @Override
    public boolean equals(Object s){
        SigmaOccupancyState<State,Action,Observation> sprime = new SigmaOccupancyState<>();
        try{
            sprime = (SigmaOccupancyState<State, Action, Observation>) s;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        if (!this.o.equals(sprime.o)){

            return false;
        }
        return true;
    }

    public Distribution<State> getStateDistrib(Posg<State, Action, Observation> posg, JointHistory<Action,Observation> jh) {
        if (this.o.timestep == 0){
            return this.o.InitialBelief;
        }

        Distribution<State> d = new Distribution<>();

        for (State s : posg.getStates()){
                if (d.getNonZeroElements().contains(s)) {
                    d.setWeight(s, d.getWeight(s) + this.o.getProba(new Pair<>(s,jh)));
                } else {
                    d.addWeight(s, this.o.getProba(new Pair<>(s,jh)));
                }
            }
        for (State s : this.o.POSG.getStates()){

            if (o.getProba(new Pair<>(s,jh)) > 0.0) {
                double carapuce = d.getWeight(s);
                d.setWeight(s, carapuce / o.getProba(new Pair<>(s, jh)));
            }
            else{
                d.setWeight(s, 0);
            }
        }
        return d;
    }

    public HashMap<History<Action,Observation>, Distribution<History<Action,Observation>>> computeNewConditionalBelief(SigmaOccupancyState<State,Action,Observation> s, BehavioralStrategy<State,Action,Observation> strat, int player) throws Exception {
        HashMap<History<Action,Observation>, Distribution<History<Action,Observation>>> hashMap = new HashMap<>();
        for (History<Action,Observation> h : s.getDistribMarginale(player).getNonZeroElements()){
            for (Action a : this.o.POSG.getActions(player)){
                for (Observation z : this.o.POSG.getObservations(player)){
                    History<Action,Observation> newHistory = new History<>(h.getListeActionObservation());
                    newHistory.addActionObservation(a,z);
                    hashMap.put(newHistory, s.getNewDistribCond(newHistory,player,strat));
                }
            }
        }

        return hashMap;
    }

    private void setDistribCond(HashMap<History<Action,Observation>, Distribution<History<Action,Observation>>> hashMap, int player) {
        if (player%2==1){
            this.distribConditionnelleP1 = hashMap;
        }
        else{
            this.distribConditionnelleP2 = hashMap;
        }
    }

    public Double getConditionalNorm(SigmaOccupancyState<State, Action, Observation> sigmaPrime,BehavioralStrategy<State,Action,Observation> strat, int player) throws Exception {

        if (player%2==1) {
            if (this.tc1==null) {
                this.tc1 = this.computeNewConditionalBelief(this, strat, player);
            }

            if (sigmaPrime.tc1==null) {
                sigmaPrime.tc1 = this.computeNewConditionalBelief(sigmaPrime, strat, player);
            }
        }
        else{
            if (this.tc2==null) {
                this.tc2 = this.computeNewConditionalBelief(this, strat, player);
            }

            if (sigmaPrime.tc2==null) {
                sigmaPrime.tc2 = this.computeNewConditionalBelief(sigmaPrime, strat, player);
            }
        }

        double res = 0.0;

        double normeValue = 0.0;
        ArrayList<Pair<History<Action,Observation>,History<Action,Observation>>> listJointHistory = new ArrayList<>();
        for (History<Action,Observation> h : this.getDistribCond(player).keySet()) {
            for (History<Action, Observation> hNegI : this.getDistribCond(player, h).getNonZeroElements()) {
                listJointHistory.add(new Pair<>(h,hNegI));
                normeValue += Math.abs(this.getConditionalValue(h,hNegI,player) - sigmaPrime.getConditionalValue(h,hNegI,player));
            }
        }for (History<Action,Observation> h : sigmaPrime.getDistribCond(player).keySet()) {
            for (History<Action, Observation> hNegI : sigmaPrime.getDistribCond(player, h).getNonZeroElements()) {
                if (!listJointHistory.contains(new Pair<>(h, hNegI))) {
                    normeValue += Math.abs(this.getConditionalValue(h,hNegI,player) - sigmaPrime.getConditionalValue(h,hNegI,player));
                }
            }
        }
        return normeValue;
    }
    public HashMap<History<Action,Observation>, Distribution<History<Action,Observation>>> getDistribCond(int player){
        if (player%2==1){
            return this.distribConditionnelleP1;
        }
        else{
            return this.distribConditionnelleP2;
        }
    }
}
