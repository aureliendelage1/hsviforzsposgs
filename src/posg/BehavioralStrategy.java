package posg;

import Approximations.ApproximatorW;
import Approximations.Vector;
import util.Distribution;
import util.RandomGenerator;

import java.util.ArrayList;
import java.util.HashMap;

public class BehavioralStrategy<State,Action,Observation> {
    public OccupancyState<State,Action,Observation> o;
    public int player;
    public Posg<State,Action,Observation> posg;

    public HashMap<History<Action,Observation>, Distribution<Action>> strategy;
    public int timestep = 0;
    public boolean alreadyDecompressed = false;

    public BehavioralStrategy() {

    }

    public BehavioralStrategy(HashMap<History<Action, Observation>, Distribution<Action>> strategyHashMap) {
        HashMap<History<Action,Observation>, Distribution<Action>> hashMap = new HashMap<>();
        for (History<Action,Observation> history : strategyHashMap.keySet()){
            hashMap.put(new History<>(new ArrayList<>(history.getListeActionObservation())), new Distribution<Action>(strategyHashMap.get(history)) );
        }
        this.setStrategy(strategyHashMap);
    }

    public HashMap<History<Action, Observation>, Distribution<Action>> getStrategy() {
        return strategy;
    }

    public void setStrategy(HashMap<History<Action, Observation>, Distribution<Action>> strategy) {
        this.strategy = strategy;
    }

    public BehavioralStrategy(OccupancyState<State,Action,Observation> o, int player, Posg<State,Action,Observation> posg){
        this.o = o;
        this.timestep = o.getTimestep();
        this.player = player;
        this.posg = posg;
    }

    public void generateRandomDistribution(OccupancyState<State,Action,Observation> o){
        HashMap<History<Action, Observation>, Distribution<Action>> strat = new HashMap<>();
        RandomGenerator r = new RandomGenerator();
        timestep = o.getTimestep();
        if (timestep == 0){

            History<Action,Observation> voidHistory = new History<>();
            Distribution<Action> distrib = new Distribution<>();
            if ((player%2)==1) {
                for (Action a : posg.getActionsJ1()) {
                    distrib.remove(a);
                    distrib.addWeight(a, r.getDouble());

                }
                distrib.normalize();
            }
            if ((player%2)==0){
                for (Action a : posg.getActionsJ2()) {
                    distrib.remove(a);
                    distrib.addWeight(a, r.getDouble());

                }
                distrib.normalize();
            }
            strat.put(voidHistory,distrib);
        }
        else{

            if (player%2==1) {
                for (Pair<State, JointHistory<Action,Observation>> pair : o.getDistrib().getNonZeroElements()){
                    if (!strat.containsKey(pair.getElement1().getHistJ1())) {
                        Distribution<Action> d = new Distribution<Action>();
                        for (Action a : posg.getActionsJ1()) {
                            d.addWeight(a, r.getDouble());
                        }
                        d.normalize();
                        strat.put(pair.getElement1().getHistJ1(), d);
                    }
                }
            }
            else{
                for (Pair<State, JointHistory<Action,Observation>> pair : o.getDistrib().getNonZeroElements()){
                    if (!strat.containsKey(pair.getElement1().getHistJ2())) {
                        Distribution<Action> d = new Distribution<Action>();
                        for (Action a : posg.getActionsJ2()) {
                            d.addWeight(a, r.getDouble());
                        }
                        d.normalize();
                        strat.put(pair.getElement1().getHistJ2(), d);
                    }
                }
            }
        }
        this.strategy = strat;
    }

    @Override
    public String toString() {
        return this.strategy.toString();
    }

    public BehavioralStrategy<State,Action,Observation> copy() {
        HashMap<History<Action,Observation>, Distribution<Action>> newHashMap = new HashMap<>();
        for (History<Action,Observation> key : this.getStrategy().keySet()){
            newHashMap.put(new History<Action,Observation>(new ArrayList(key.getListeActionObservation())), this.getStrategy().get(key));
        }
        return new BehavioralStrategy<State,Action,Observation>(newHashMap);
    }

    public HashMap<DecisionRule<State,Action,Observation>,Double> toMixedStrategyOneStep(){
        HashMap<DecisionRule<State,Action,Observation>,Double> mixedStrategy = new HashMap<>();
        DecisionRuleGenerator<State, Action, Observation> decisionRuleGenerator = new DecisionRuleGenerator<>(this.posg);
        ArrayList<History<Action,Observation>> listHistories = new ArrayList<>();
        listHistories.addAll(this.strategy.keySet());

        for (DecisionRule<State, Action, Observation> decisionRule : decisionRuleGenerator.generateDecisionRule(player, listHistories)) {
            double proba = 1.0;
            for (History<Action,Observation> h : decisionRule.decisionRule.keySet()){
                proba*=this.strategy.get(h).getProbability(decisionRule.decisionRule.get(h));
            }
            if (proba>0.0) {
                mixedStrategy.put(decisionRule, proba);
            }
        }

        return mixedStrategy;
    }

    public double getProbaOfDecisionRule(DecisionRule<State, Action, Observation> decisionRule, int player) {
        double proba = 1.0;
        for (History<Action,Observation> h : decisionRule.decisionRule.keySet()){
            if (this.strategy.containsKey(h)) {
                proba *= this.strategy.get(h).getProbability(decisionRule.decisionRule.get(h));
            }
            else{
                proba *= (float) (1.0/posg.getActions(player).size());
            }
        }
        return proba;
    }

    public BehavioralStrategy<State, Action, Observation> computeDeltaBeta(
            ApproximatorW<State,Action,Observation> bagW,
            ArrayList<Double> weights,
            int player, int timestep){
        HashMap<History<Action,Observation>,Distribution<Action>> mapRes = new HashMap<>();
        int index = 0;
        for (Pair<SigmaOccupancyState<State, Action, Observation>,
                Pair<BehavioralStrategy<State, Action, Observation>, Vector<State, Action, Observation>>
                >
                objet : bagW.bagW.get(timestep)) {
            double weight = 0.0;
            if (weights.get(index) != 0.0) {
                weight = Math.abs(weights.get(index));
            }

            for (History<Action,Observation> h : objet.getElement1().getElement0().strategy.keySet()){
                if (mapRes.containsKey(h)){
                    mapRes.put(h,mapRes.get(h).addDistribution(objet.getElement1().getElement0().strategy.get(h).scalarMultiply(weight)));
                }
                else{
                    mapRes.put(h,objet.getElement1().getElement0().strategy.get(h).scalarMultiply(weight));
                }
            }

        }
        return new BehavioralStrategy<State,Action,Observation>(mapRes);
    }


    public HashMap<Integer, BehavioralStrategy<State,Action,Observation>> toIntBehavioral(){
        HashMap<Integer, BehavioralStrategy<State,Action,Observation>> res = new HashMap<>();
        for (int i = timestep; i<this.posg.profondeurMax;i++){
            res.put(i, new BehavioralStrategy<>());
            res.get(i).strategy = new HashMap<>();
            for (History<Action,Observation> h : this.getStrategy().keySet()){
                if (h.getListeActionObservation().size()==i){
                    res.get(i).getStrategy().put(h,this.getStrategy().get(h));
                }
            }
        }
        return res;
    }

    public void decompress(SigmaOccupancyState<State, Action, Observation> lastSigmaOccupancyState, int player) {

        for (History<Action,Observation> h : lastSigmaOccupancyState.o.getListCompressedHistories(player).keySet()){
            //System.out.println("before, this.strategy.size : " + this.strategy.size() + "adding h :" + h + " ifdo : " + lastSigmaOccupancyState.o.getListCompressedHistories(player).get(h));
            this.strategy.put(h,this.strategy.get(lastSigmaOccupancyState.o.getListCompressedHistories(player).get(h)));
            //System.out.println("after, this.strategy.size : " + this.strategy.size());
        }

        this.alreadyDecompressed = true;
    }

    public void sanityCheck() {
        for (History<Action,Observation> h : strategy.keySet()){
            this.strategy.get(h).sanityCheckForBehavioralStrat();
        }
    }
}
