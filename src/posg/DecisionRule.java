package posg;

import java.util.ArrayList;
import java.util.HashMap;

public class DecisionRule<State,Action,Observation> {
    public HashMap<History<Action,Observation>, Action> decisionRule = new HashMap<>();
    public double proba = 0.0;

    public DecisionRule(){

    }


    public DecisionRule(HashMap<History<Action,Observation>,Action> decisionRule, double proba) {
        this.decisionRule = decisionRule;
        this.proba = proba;
    }

    public DecisionRule(History<Action,Observation> history){
        HashMap<History<Action,Observation>, Action> res = new HashMap<>();
        for (int i = 0; i<history.getListeActionObservation().size();i++){
            res.put(new History<>(new ArrayList<>(history.getListeActionObservation().subList(0,i))),history.getListeActionObservation().get(i).getElement0());
        }
        this.decisionRule = res;
    }

    public boolean isAnExtensionOf(DecisionRule<State, Action, Observation> decisionRule) {
        for (History<Action,Observation> history : this.decisionRule.keySet()){
            if ((decisionRule.decisionRule.containsKey(history.getlastHistory())) && (history.getLastAction().equals(decisionRule.decisionRule.get(history.getlastHistory())))){
                return true;
            }
        }
        return false;
    }

    public DecisionRule<State,Action,Observation> append(DecisionRule<State, Action, Observation> nextDecisionRule) {
        DecisionRule<State,Action,Observation> res = new DecisionRule<>();
        for (History<Action,Observation> h : nextDecisionRule.decisionRule.keySet()){
            res.decisionRule.put(h, nextDecisionRule.decisionRule.get(h));
        }
        for (History<Action,Observation> h : this.decisionRule.keySet()){
            res.decisionRule.put(h, this.decisionRule.get(h));
        }


        return res;
    }

    public String toString(){
        return this.decisionRule.toString();
    }

    @Override
    public boolean equals(Object o){
        DecisionRule h;
        try{
            h = (DecisionRule) o;
        }
        catch(Exception e){
            return false;
        }
        return this.decisionRule.equals(((DecisionRule<?, ?, ?>) o).decisionRule);
    }

    @Override
    public int hashCode(){
        return this.decisionRule.hashCode();
    }


    public Observation getNextObs(History<Action, Observation> h, Action action) {
        for (History<Action,Observation> hKeyset : this.decisionRule.keySet()){
            if (hKeyset.getListeActionObservation().size()>0 && hKeyset.getlastHistory().equals(h)){
                return hKeyset.getLastObservation();
            }
        }

        return (Observation) new LocalObservation("fail");
    }
}
