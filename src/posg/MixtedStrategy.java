package posg;

import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;

public class MixtedStrategy<State,Action,Observation> {
    public HashMap<DecisionRule<State,Action,Observation>, Double> strat;
    Posg<State,Action,Observation> posg;
    public BehavioralStrategy<State,Action,Observation> behavioralStrat = new BehavioralStrategy<>();

    public int initialTimestep = 0;

    public MixtedStrategy(Posg<State,Action,Observation> posg, HashMap<DecisionRule<State,Action,Observation>, Double> strat){
        this.posg = posg;
        this.strat = strat;
    }

    public MixtedStrategy(
            Posg<State,Action,Observation> posg,
            HashMap<Pair<History<Action, Observation>, History<Action,Observation>>, Double> strat,
            int player){
        this.posg = posg;

        this.strat = this.computeStratWithRW(strat);
        this.behavioralStrat = this.computeBehavioralStrat(strat, player);
    }

    public MixtedStrategy(
            Posg<State,Action,Observation> posg,
            HashMap<Pair<History<Action, Observation>, History<Action,Observation>>, Double> strat,
            History<Action,Observation> history,
            int player){
        this.posg = posg;

        this.strat = this.computeStratWithRW(strat,history);
        this.behavioralStrat = this.computeBehavioralStrat(strat, player,history);
        this.initialTimestep = history.getTimestep();
    }

    private BehavioralStrategy<State,Action,Observation> computeBehavioralStrat(
            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat,
            int player,
            History<Action,Observation> history) {

        BehavioralStrategy<State,Action,Observation> behavioralStrategy = new BehavioralStrategy<>();
        HashMap<History<Action,Observation>, Distribution<Action>> res = new HashMap<>();
        HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat2 = new HashMap<>();
        //System.out.println("history for mdp solve : " + history);
        //System.out.println("strat, rws : " + strat);

        for (Pair<History<Action,Observation>, History<Action,Observation>> rw : strat.keySet()) {
            if (rw.getElement0().getTimestep() == history.getTimestep()){
                //System.out.println("i'm here");

                ArrayList<Pair<Action,Observation>> listForConcatPrefix = new ArrayList<>();
                listForConcatPrefix.addAll(rw.getElement0().getListeActionObservation());
                listForConcatPrefix.addAll(rw.getElement1().getlastHistory().getListeActionObservation());

                History<Action,Observation> newHistPrefix = new History<>(listForConcatPrefix);
                ArrayList<Pair<Action,Observation>> list = new ArrayList<>();
                list.add(new Pair<>(rw.getElement1().getLastAction(),(Observation) new LocalObservation("-")));
                History<Action,Observation> newHistSuffix = new History<>(list);
                strat2.put(new Pair<>(newHistPrefix, newHistSuffix),strat.get(rw));
            }
        }

        for (int i = 0; i<posg.profondeurMax; i++){


            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> newRealizationWeights = new HashMap<>();
            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat2Copie = new HashMap<>(strat2);
            for (Pair<History<Action,Observation>, History<Action,Observation>> rw : strat2Copie.keySet()){

                if (rw.getElement0().getListeActionObservation().size() == posg.profondeurMax-(i+1)      ) {

                    Action aRw = rw.getElement0().getLastAction();

                    History<Action,Observation> prefix = rw.getElement0().getlastHistory();

                    for (Observation o : this.posg.getObservations(player)){
                        double valRw = 0.0;
                        for (Pair<History<Action, Observation>, History<Action, Observation>> rwNext : strat2.keySet()) {

                            History<Action,Observation> prefixRwNext = new History<>(rwNext.getElement0().getListeActionObservation());
                            if (prefixRwNext.extendsHist(prefix,aRw)
                                    && prefixRwNext.getLastObservation().equals(o)
                                    && (prefixRwNext.getListeActionObservation().size() == prefix.getListeActionObservation().size()+1) ){

                                valRw += strat2.get(rwNext);
                            }
                        }
                        if (!(newRealizationWeights.containsKey(new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, (Observation) new LocalObservation("-")))))) {
                            if (valRw>0.0) {


                                newRealizationWeights.put(new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, (Observation) new LocalObservation("-"))), valRw);
                                strat2.put(new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, (Observation) new LocalObservation("-"))), valRw);
                            }
                        }
                    }
                }
            }

        }

        //System.out.println("player : " + player);
        //System.out.println("final modified strat for mdp : " + this.stratToString(strat2,posg.profondeurMax-1));

        for (int i = 0; i < posg.profondeurMax; i++) {
            for (Pair<History<Action, Observation>, History<Action, Observation>> rw : strat2.keySet()) {
                if (rw.getElement0().getListeActionObservation().size() == i){
                    Distribution<Action> distribRes = new Distribution<>();


                    if (i==0){
                        for (Action a : this.posg.getActions(player)) {
                            History<Action,Observation> prefix = new History<>();
                            ArrayList<Pair<Action,Observation>> list = new ArrayList<>();
                            list.add(new Pair<>(a,(Observation) new LocalObservation("-")));
                            History<Action,Observation> suffix = new History<>(list);
                            if (strat2.containsKey(new Pair<>(prefix,suffix))) {
                                distribRes.addWeight(a, strat2.get(new Pair<>(prefix, suffix)));
                            }
                        }
                    }
                    else {
                        for (Action a : this.posg.getActions(player)) {
                            History<Action, Observation> prefix = rw.getElement0().getlastHistory();
                            ArrayList<Pair<Action, Observation>> list = new ArrayList<>();
                            list.add(new Pair<>(rw.getElement0().getLastAction(), (Observation) new LocalObservation("-")));
                            History<Action, Observation> suffixDenom = new History<>(list);

                            ArrayList<Pair<Action, Observation>> list2 = new ArrayList<>();
                            list2.add(new Pair<>(a, (Observation) new LocalObservation("-")));
                            History<Action,Observation> suffixNum = new History<>(list2);


                            if (strat2.containsKey(new Pair<>(rw.getElement0(),suffixNum)) && strat2.containsKey(new Pair<>(prefix, suffixDenom)))
                            {
                                if (strat2.get(new Pair<>(prefix, suffixDenom)) >= 0.0) {
                                    distribRes.addWeight(a, strat2.get(new Pair<>(rw.getElement0(), suffixNum)) / strat2.get(new Pair<>(prefix, suffixDenom)));
                                }
                                else{

                                }
                            }
                        }
                    }


                    res.put(new History<>(new ArrayList<>(rw.getElement0().getListeActionObservation())),distribRes);
                }
            }
        }
        behavioralStrategy.setStrategy(res);


        return behavioralStrategy;
    }

    private BehavioralStrategy<State,Action,Observation> computeBehavioralStrat(
            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat,
            int player) {
        BehavioralStrategy<State,Action,Observation> behavioralStrategy = new BehavioralStrategy<>();
        HashMap<History<Action,Observation>, Distribution<Action>> res = new HashMap<>();

        HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat2 = new HashMap<>();
        HashMap<Pair<History<Action,Observation>, History<Action,Observation>>,Double> mapStore = new HashMap<>();

        for (Pair<History<Action,Observation>, History<Action,Observation>> rw : strat.keySet()) {
            if (rw.getElement0().equals(new History<>())){
                History<Action,Observation> newHistPrefix = rw.getElement1().getlastHistory();
                ArrayList<Pair<Action,Observation>> list = new ArrayList<>();
                list.add(new Pair<>(rw.getElement1().getLastAction(),(Observation) new LocalObservation("-")));
                History<Action,Observation> newHistSuffix = new History<>(list);

                ArrayList<Pair<Action,Observation>> listModified = new ArrayList<>();
                listModified.add(new Pair<>(rw.getElement1().getLastAction(),(Observation) new LocalObservation("-")));
                History<Action,Observation> newHistSuffixModified = new History<>(listModified);
                mapStore.put(new Pair<>(newHistPrefix, newHistSuffixModified),strat.get(rw));


                strat2.put(new Pair<>(newHistPrefix, newHistSuffix),strat.get(rw));
            }
        }

        for (int i = 0; i<posg.profondeurMax-1; i++){


            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> newRealizationWeights = new HashMap<>();
            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat2Copie = new HashMap<>(strat2);
            for (Pair<History<Action,Observation>, History<Action,Observation>> rw : strat2Copie.keySet()){

                if (rw.getElement0().getListeActionObservation().size() == posg.profondeurMax-(i+1)      ) {

                    Action aRw = rw.getElement0().getLastAction();

                    History<Action,Observation> prefix = rw.getElement0().getlastHistory();

                    HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat2Temp = new HashMap<>();
                    for (Observation o : this.posg.getObservations(player)){
                        double valRw = 0.0;
                            for (Pair<History<Action, Observation>, History<Action, Observation>> rwNext : strat2.keySet()) {

                                History<Action,Observation> prefixRwNext = new History<>(rwNext.getElement0().getListeActionObservation());
                                if (prefixRwNext.extendsHist(prefix,aRw)
                                        && prefixRwNext.getLastObservation().equals(o)
                                        && (prefixRwNext.getListeActionObservation().size() == prefix.getListeActionObservation().size()+1) ){

                                    valRw += strat2.get(rwNext);
                                }
                            }

                        if (!(newRealizationWeights.containsKey(
                                new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, (Observation) new LocalObservation("-")))))
                        || true) {
                            if (valRw>0.0) {


                                newRealizationWeights.put(new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, (Observation) new LocalObservation("-"))), valRw);
                                strat2Temp.put(new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, (Observation) new LocalObservation("-"))), valRw);
                                mapStore.put(new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, o)), valRw);
                            }
                        }
                    }
                    double valTmp = -Double.POSITIVE_INFINITY;
                    Pair<History<Action,Observation>, History<Action,Observation>> pairArgMax;
                    for (Pair<History<Action,Observation>, History<Action,Observation>> pair : strat2Temp.keySet()){
                        if (strat2Temp.get(pair)>valTmp){
                            valTmp = strat2Temp.get(pair);
                            pairArgMax = pair;
                        }
                    }
                    strat2.put(new Pair<>(rw.getElement0().getlastHistory(), new History(aRw, (Observation) new LocalObservation("-"))), valTmp);
                }
            }

        }


        for (int i = 0; i < posg.profondeurMax; i++) {
            for (Pair<History<Action, Observation>, History<Action, Observation>> rw : strat2.keySet()) {
                if (rw.getElement0().getListeActionObservation().size() == i){
                    Distribution<Action> distribRes = new Distribution<>();


                    if (i==0){
                        for (Action a : this.posg.getActions(player)) {
                            History<Action,Observation> prefix = new History<>();
                            ArrayList<Pair<Action,Observation>> list = new ArrayList<>();
                            list.add(new Pair<>(a,(Observation) new LocalObservation("-")));
                            History<Action,Observation> suffix = new History<>(list);
                            if (strat2.containsKey(new Pair<>(prefix,suffix))) {
                                distribRes.addWeight(a, strat2.get(new Pair<>(prefix, suffix)));
                            }
                        }
                    }
                    else {
                        for (Action a : this.posg.getActions(player)) {
                            History<Action, Observation> prefix = rw.getElement0().getlastHistory();
                            ArrayList<Pair<Action, Observation>> list = new ArrayList<>();
                            list.add(new Pair<>(rw.getElement0().getLastAction(), (Observation) new LocalObservation("-")));
                            History<Action, Observation> suffixDenom = new History<>(list);

                            ArrayList<Pair<Action, Observation>> list2 = new ArrayList<>();
                            list2.add(new Pair<>(a, (Observation) new LocalObservation("-")));
                            History<Action,Observation> suffixNum = new History<>(list2);


                            if (strat2.containsKey(new Pair<>(rw.getElement0(),suffixNum)) && strat2.containsKey(new Pair<>(prefix, suffixDenom)))
                            {
                                if (strat2.get(new Pair<>(prefix, suffixDenom)) >= 0.0) {
                                    distribRes.addWeight(a, strat2.get(new Pair<>(rw.getElement0(), suffixNum)) / strat2.get(new Pair<>(prefix, suffixDenom)));
                                }
                                else{

                                }
                            }
                            //System.out.println("mapstore : " + mapStore);
                            ArrayList<Pair<Action, Observation>> listModified = new ArrayList<>();
                            listModified.add(new Pair<>(rw.getElement0().getLastAction(), rw.getElement0().getLastObservation()));
                            History<Action, Observation> suffixDenomModified = new History<>(listModified);
                            if (strat2.containsKey(new Pair<>(rw.getElement0(),suffixNum)) && mapStore.containsKey(new Pair<>(prefix, suffixDenomModified))){
                                distribRes.setWeight(a, strat2.get(new Pair<>(rw.getElement0(), suffixNum)) / mapStore.get(new Pair<>(prefix, suffixDenomModified)));
                            }
                        }
                    }


                    res.put(new History<>(new ArrayList<>(rw.getElement0().getListeActionObservation())),distribRes);
                }
            }
        }
        behavioralStrategy.setStrategy(res);


        return behavioralStrategy;
    }

    private String stratToString(HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat2, int i) {
        String res = "";
        for (Pair<History<Action,Observation>, History<Action,Observation>> p : strat2.keySet()){

                res += p.toString() + " = " + strat2.get(p) + " \n ";

        }
        return res;
    }

    private HashMap<DecisionRule<State,Action,Observation>, Double> computeStratWithRW(
            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>,
                    Double> strat) {

        HashMap<DecisionRule<State,Action,Observation>, Double> res = new HashMap<>();

        for (Pair<History<Action,Observation>, History<Action,Observation>> pair : strat.keySet()){
            if (pair.getElement0().getListeActionObservation().size() == 0){
                DecisionRule<State,Action,Observation> decisionRule = this.constructDecisionRule(pair);
                res.put(decisionRule,strat.get(pair));
            }
        }

        return res;
    }

    private HashMap<DecisionRule<State,Action,Observation>, Double> computeStratWithRW(
            HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat,
            History<Action,Observation> history) {

        HashMap<DecisionRule<State,Action,Observation>, Double> res = new HashMap<>();

        for (Pair<History<Action,Observation>, History<Action,Observation>> pair : strat.keySet()){
            if (pair.getElement0().equals(history)){
                DecisionRule<State,Action,Observation> decisionRule = this.constructDecisionRule(pair);
                res.put(decisionRule,strat.get(pair));
            }
        }

        return res;
    }

    private DecisionRule<State,Action,Observation> constructDecisionRule(Pair<History<Action,Observation>, History<Action,Observation>> pair) {
        DecisionRule<State,Action,Observation> d = new DecisionRule<>();
        for (int i = 0; i<pair.getElement1().getListeActionObservation().size();i++){
            d.decisionRule.put(new History<>(new ArrayList<>(pair.getElement1().getListeActionObservation().subList(0,i))),pair.getElement1().getListeActionObservation().get(i).getElement0());
        }
        return d;
    }

    @Override
    public String toString(){
        return this.strat.toString();
    }

}
