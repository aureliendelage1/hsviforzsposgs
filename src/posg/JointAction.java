package posg;

public class JointAction<Action> {

    private Pair<Action,Action> actions;

    public JointAction(){

    }
    public JointAction(Action actionJ1, Action actionJ2){
        this.actions = new Pair<Action,Action>(actionJ1,actionJ2);
    }

    public JointAction(Action a, Action aNegI, int player) {
        if (player%2==1){
            this.actions = new Pair<Action,Action>(a,aNegI);
        }
        else{
            this.actions = new Pair<Action,Action>(aNegI,a);
        }
    }

    @Override
    public String toString(){
        return "("+this.actions.getElement0().toString()+","+this.actions.getElement1().toString()+")";
    }

    public Pair<Action,Action> getActions(){
        return this.actions;
    }
    @Override
    public boolean equals(Object obj) {
        JointAction o;
        try{
            o = (JointAction) obj;
        }
        catch(Exception e){
            return false;
        }
        if (o.actions.getElement0().equals(this.actions.getElement0()) && o.actions.getElement1().equals(this.actions.getElement1())){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.actions.hashCode();
    }
}
