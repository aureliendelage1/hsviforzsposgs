package posg;

public class State {

    private String name ="";

    public State(String name){
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    @Override
    public String toString(){
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        State o;
        try{
            o = (State) obj;
        }
        catch(Exception e){
            return false;
        }
        return o.getName().equals(this.name);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
