package posg;

import util.Distribution;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ObservationSetGeneric<State,Action,Observation> extends ObservationSet<State,Action,Observation>
                                                             implements Serializable {
    private static final long serialVersionUID = 1L;

    public Map<Pair<Action, State>, Distribution<Observation>> getDistributions() {
        return distributions;
    }
    private Map<Pair<Action,State>, Distribution<Observation>> distributions;
    private Collection<State> states;
    private Collection<Action> actions;
    private Collection<Observation> observations;

    public ObservationSetGeneric(Collection<State> states, Collection<Action> actions, Collection<Observation> observations) {
        this.states = states;
        this.actions = actions;
        this.observations = observations;
        distributions = new HashMap<>();
    }

    @Override
    public void setProbability(Action action, State endState, Observation observation, double probability) {
        if(action == null) {
            for(Action newAction : actions) {
                setProbability(newAction, endState, observation, probability);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setProbability(action, newEndState, observation, probability);
            }
        } else if(observation == null) {
            for(Observation newObservation : observations) {
                setProbability(action, endState, newObservation, probability);
            }
        } else {
            getOrCreateDistribution(action,endState).setWeight(observation,probability);
        }
    }

    @Override
    public void setDistribution(Action action, State endState, Distribution<Observation> distribution) {
        if(action == null) {
            for(Action newAction : actions) {
                setDistribution(newAction, endState, distribution);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setDistribution(action, newEndState, distribution);
            }
        } else {
            distributions.put(new Pair<>(action,endState), distribution);
        }
    }

    @Override
    public void initGetters() {

        for(Action action : actions) {
            for(State endState : states) {
                Distribution<Observation> distribution = getOrCreateDistribution(action,endState);
                if(distribution.getNonZeroElements().size() == 0) {
                    throw new IllegalStateException("There is no observation possible for the couple (" + action + "," + endState + ").");
                }
            }
        }
    }

    @Override
    public double getProbability(Action action, State endState, Observation observation) {
        return distributions.get(new Pair<>(action,endState)).getWeight(observation);
    }

    public void sanityCheck() throws Exception {

        for (Pair<Action,State> elt : this.distributions.keySet()){


            this.distributions.get(elt).sanityCheck();
        }

    }
    public double getProba(Action a,State s,Observation z){
        for (Pair<Action,State> paire : this.distributions.keySet()){
            if (paire.equals(new Pair<Action,State>(a,s))){
                Distribution<Observation> distrib = this.distributions.get(paire);
                for (Observation zList : distrib.getNonZeroElements()){
                    JointObservation zListCast = (JointObservation) zList;
                    JointObservation zCast = (JointObservation) z;
                    if (zListCast.equals(zCast)){


                        return distrib.getWeight(zList);
                    }
                }
            }

        }
        return 0.0;
    }


    @Override
    public Collection<Observation> getPossibleObservations(Action action, State endState) {
        return distributions.get(new Pair<>(action,endState)).getNonZeroElements();
    }

    @Override
    public Observation sampleObservation(Action action, State endState) {
        return distributions.get(new Pair<>(action,endState)).sample();
    }

    private Distribution<Observation> getOrCreateDistribution(Action action, State endState) {
        return distributions.computeIfAbsent(new Pair<>(action, endState), k -> new Distribution<>());
    }

    public void setDistribProbabilities (HashMap<Pair<Action,State>, Distribution<Observation>> obsProbabilities) {
        this.distributions = obsProbabilities;
    }
}
