package posg;

public class LocalAction {
    private String name = "";

    public LocalAction(String name){
        this.name = name;
    }

    public LocalAction(){

    }

    public String getNom() {
        return name;
    }

    public void setNom(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return this.name;
    }

    @Override
    public boolean equals(Object a) {
        LocalAction aCast = new LocalAction("ksdfj");
        try{
            aCast = (LocalAction) a;
        }
        catch (Exception e){
            System.out.println("couldn't cast a : " + a.toString() + " into a localAction.. :(");
        }
        return this.name.equals(aCast.name);
    }

    @Override
    public int hashCode() {

        return this.name.hashCode();
    }
}
