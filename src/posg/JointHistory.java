package posg;

public class JointHistory<Action,Observation> {
    private Pair<History<Action,Observation>, History<Action,Observation>> hist;

    public JointHistory(History<Action,Observation> historyJ1, History<Action,Observation> historyJ2) {
        this.hist = new Pair<History<Action, Observation>, History<Action, Observation>>(historyJ1,historyJ2);
    }

    public JointHistory() {

    }

    public JointHistory(History<Action, Observation> histPlayerI, History<Action, Observation> histPlayerNegI, int player) {
        if (player%2==1){
            this.hist = new Pair<History<Action, Observation>, History<Action, Observation>>(histPlayerI,histPlayerNegI);
        }
        else{
            this.hist = new Pair<History<Action, Observation>, History<Action, Observation>>(histPlayerNegI,histPlayerI);
        }
    }

    public History<Action,Observation> getHistJ1(){
        return this.hist.getElement0();
    }

    public History<Action,Observation> getHistJ2(){
        return this.hist.getElement1();
    }

    @Override
    public boolean equals(Object obj) {
        JointHistory o;
        try{
            o = (JointHistory) obj;
        }
        catch(Exception e){
            return false;
        }
        if (o.getHistJ1().equals(this.getHistJ1()) && o.getHistJ2().equals(this.getHistJ2())){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        if (!(this.hist ==null)) {
            return this.hist.toString();
        }
        else{
            return "voidHistory";
        }
    }

    @Override
    public int hashCode() {
        if (this.hist==null){
            return super.hashCode();
        }
        return this.hist.hashCode();
    }

    public JointAction getLastJointAction() {
        return new JointAction(this.hist.getElement0().getListeActionObservation().get(this.hist.getElement0().getListeActionObservation().size()-1).getElement0(),
                this.hist.getElement1().getListeActionObservation().get(this.hist.getElement1().getListeActionObservation().size()-1).getElement0());
    }

    public JointObservation getLastJointObservation() {
        return new JointObservation(this.hist.getElement0().getListeActionObservation().get(this.hist.getElement0().getListeActionObservation().size()-1).getElement1(),
                this.hist.getElement1().getListeActionObservation().get(this.hist.getElement1().getListeActionObservation().size()-1).getElement1());
    }

    public History<Action,Observation> getHist(int player) {
        if (player%2==1){
            return this.getHistJ1();
        }
        else if (player%2==0){
            return this.getHistJ2();
        }
        else{
            System.out.println("cannot recognize what you want");
            System.exit(1);
        }
        return new History<>();
    }
}
