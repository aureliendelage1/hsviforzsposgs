package posg;

import util.Distribution;
import util.RandomGenerator;

import java.util.*;

public class RockPaperScissors {

    private ArrayList<State> listStates;

    public RockPaperScissors() {

    }

    public void temp() {

    }

    public void initiate(Posg POSG) throws Exception {
        this.initiateStates(POSG);
        this.iniateAction(POSG);
        this.initiateObservations(POSG);

        try {
            this.initiateTransitions(POSG);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.initiateRewards(POSG);
        this.initiateObservationProbabilities(POSG);

    }

    private void initiateObservations(Posg POSG) {
        ArrayList<LocalObservation> listLocalObservations = new ArrayList<LocalObservation>();
        LocalObservation localObservation1 = new LocalObservation("z1");
        LocalObservation localObservation2 = new LocalObservation("z2");
        List<LocalObservation> l = Arrays.asList(localObservation1,localObservation2);
        POSG.cardObservations = l.size();
        listLocalObservations.addAll(l);
        POSG.setObservationsJ1(listLocalObservations);
        POSG.setObservationsJ2(listLocalObservations);

    }

    private void iniateAction(Posg POSG) {
        ArrayList<LocalAction> listLocalActions = new ArrayList<>();
        ArrayList<LocalAction> listLocalActionsP2 = new ArrayList<>();
        LocalAction localAction1 = new LocalAction("Rock");
        LocalAction localAction2 = new LocalAction("Paper");
        LocalAction localAction1P2 = new LocalAction("RockP2");
        LocalAction localAction2P2 = new LocalAction("PaperP2");

        List<LocalAction> l = Arrays.asList(localAction1, localAction2);
        List<LocalAction> lP2 = Arrays.asList(localAction1P2, localAction2P2);
        POSG.cardAction = l.size();

        listLocalActions.addAll(l);
        listLocalActionsP2.addAll(lP2);
        POSG.setActionsJ1(listLocalActions);
        POSG.setActionsJ2(listLocalActionsP2);
    }

    private void initiateStates(Posg POSG) {
        ArrayList<State> listState = new ArrayList<State>();
        State state1 = new State("firstState");
        State state2 = new State("secondState");
        POSG.cardStates = 2;
        List<State> l = Arrays.asList(state1,state2);
        listState.addAll(l);
        this.listStates = listState;
        POSG.setStates(listState);
    }

    public void initiateTransitions(Posg<State, LocalAction, LocalObservation> POSG) throws Exception {
        HashMap<Pair<State, JointAction>, Distribution<State>> transitionMapProbabilities = new HashMap<Pair<State, JointAction>, Distribution<State>>();
        RandomGenerator r = new RandomGenerator();
        for (int i = 0;i<700;i++){
            r.getDouble();
        }
        TransitionSetGeneric<State, JointAction> transition = new TransitionSetGeneric<State, JointAction>(POSG.getStates(), POSG.getJointActions());
        for (State stateStart : POSG.getStates()) {
            for (LocalAction aj1 : POSG.getActionsJ1()) {
                for (LocalAction aj2 : POSG.getActionsJ2()) {
                    Distribution<State> distrib = new Distribution<State>();
                    for (State nextState : POSG.getStates()) {

                        double transitionProbability = r.getDouble();
                        distrib.setWeight(nextState, transitionProbability);
                    }
                    distrib.normalize();
                    transitionMapProbabilities.put(new Pair<State, JointAction>(stateStart, new JointAction(aj1,aj2)), distrib);
                }
            }
        }
        for (Pair<State, JointAction> pair : transitionMapProbabilities.keySet()){
            System.out.println("Sanity checking transitions \n ---------------");
            transitionMapProbabilities.get(pair).sanityCheck();
            System.out.println("Done. \n -------------------");
        }
        transition.setTransitions(transitionMapProbabilities);
        POSG.setTransitions(transition);
    }

    public void initiateRewards(Posg<State, LocalAction, LocalObservation> POSG) throws Exception {
        RandomGenerator r = new RandomGenerator();
        for (int i = 0;i<2050;i++){
            r.getDouble();
            r.getDouble();
            r.getDouble();
        }
        RewardSetGeneric<State, JointAction> rewardMap = new RewardSetGeneric<State, JointAction>(POSG.getStates(), POSG.getJointActions());
        for (JointAction<LocalAction> a : POSG.getJointActions()) {
            for (State stateStart : POSG.getStates()) {
                for (State endStart : POSG.getStates()) {
                    rewardMap.setReward(stateStart, a, endStart, r.getDouble());
                }
            }
            POSG.setRewards(rewardMap);
        }
    }

    public void initiateObservationProbabilities(Posg<State, LocalAction, LocalObservation> POSG) throws Exception {
        RandomGenerator r = new RandomGenerator();
        for (int i = 0;i<10;i++){
           r.getDouble();
        }
        ObservationSetGeneric<State, JointAction, JointObservation> observationSetGeneric
                = new ObservationSetGeneric(POSG.getStates(),POSG.getJointActions(),POSG.getJointObservations());
        HashMap<Pair<JointAction, State>, Distribution<JointObservation>> obsProbabilities
                = new HashMap<Pair<JointAction, State>, Distribution<JointObservation>>();
        for (JointAction<LocalAction> a : POSG.getJointActions()) {
            for (State stateStart : POSG.getStates()) {
                Distribution<JointObservation> distrib = new Distribution<>();
                for (JointObservation<LocalObservation> obs : POSG.getJointObservations()) {
                    double transitionProbability = r.getDouble();
                    distrib.setWeight(obs, transitionProbability);
                }
                distrib.normalize();
                obsProbabilities.put(new Pair<JointAction, State>(a,stateStart),distrib);
            }
        }
        observationSetGeneric.setDistribProbabilities(obsProbabilities);
        POSG.setObservationsProbabilities(observationSetGeneric);
    }

    public void initiateOccupancyState(OccupancyState o) {
        RandomGenerator r = new RandomGenerator();
        Distribution<State> InitialBelief = new Distribution<State>();
        for (State s : this.listStates){
            InitialBelief.setWeight(s,r.getDouble());
        }
        InitialBelief.normalize();
        o.setInitialBelief(InitialBelief);
    }
}
