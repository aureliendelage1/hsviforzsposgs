package posg;
public abstract class RewardSet<State,Action> {

    public abstract void setReward(State startState, Action action, State endState, double reward);

    public abstract void initGetters(TransitionSet<State,Action> transitionSet);

    protected double computeExpectedReward(TransitionSet<State,Action> transitionSet, State startState, Action action) {
        double value = 0;
        for(State endState : transitionSet.getPossibleNextStates(startState,action)) {
            value += transitionSet.getProbability(startState,action,endState) * getReward(startState,action,endState);
        }
        return value;
    }

    public abstract double getReward(State startState, Action action, State endState);

    public abstract double getExpectedReward(State startState, Action action);

}
