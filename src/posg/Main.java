package posg;

import Algorithm.SolvePomdp;
import Algorithm.SolvePomdpWithMixtedStrat;
import Algorithm.SolvePomdpWithMixtedStratStore;
import Algorithm.SolvePomdpWithStrat;
import Parser.ZsPOSGfromAmatoParser;
import PomdpSolverNotDynamic.Belief;
import PomdpSolverNotDynamic.HSVI;
import PomdpSolverNotDynamic.LowerBoundHyperplane;
import PomdpSolverNotDynamic.SawtoothBound;
import Solver.LPsolver;
import Solver.OMGHSVIConcaveOptimization;
import util.Distribution;

import java.text.DecimalFormat;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws Exception {

        long start = System.currentTimeMillis();
        long end = start + 3600000;

        boolean LPsolve = false;
        boolean competitiveTiger = false;
        boolean adversarialTiger = false;
        boolean recycling = false;
        boolean mabc = false;
	    boolean MP = false;
        boolean mdpHeuristic = true;

        System.out.print("parsing args: ");
        String methodSolve;
        String problem;
        int horizon = 2;
	    double epsilon = 0.0001;
	    double errorDoo = 0.2;

	    //System.out.println("WARNING : I added an option for the heurstic. Please add : [-mdp=y/n], y for mdpHeuristic and n for rMAx heuristic");
	    //System.out.println("I added a system.exit so that you see this change. Comment line 42 of Main.java once you have seen it.");


        for(int i = 0; i<args.length; i++) {

            if (args[i].startsWith("-m=")){
                methodSolve=args[i].substring(3);
                if (!(methodSolve.equals("lp") || methodSolve.equals("lc"))){
                    System.out.println("wrong option format");
                    System.exit(1);
                }
                if (methodSolve.equals("lp")){
                    LPsolve = true;
                }
                else{
                    LPsolve = false;
                }
                System.out.println("method chosen : " + methodSolve);
            }
            else if (args[i].startsWith("-p=")){
                problem=args[i].substring(3);
                if (!(problem.equals("compTiger") || problem.equals("advTiger") || problem.equals("recycling") || problem.equals("mabc") || problem.equals("MP"))){
                    System.out.println("wrong option format");
                    System.exit(1);
                }
                System.out.println("problem chosen : " + problem);
                if (problem.equals("compTiger")){
                    competitiveTiger = true;
		    errorDoo = 1.0;
                }
                if (problem.equals("advTiger")){
                    adversarialTiger = true;
		    errorDoo = 0.1;
                }
                if (problem.equals("recycling")){
                    recycling = true;
		    errorDoo = 0.2;
                }
                if (problem.equals("mabc")){
                    mabc = true;
		    errorDoo = 0.05;
                }
		if (problem.equals("MP")){
		   MP = true;
		}
            }
            else if (args[i].startsWith("-h=")){
                try{
                    horizon = Integer.parseInt(args[i].substring(3));
                }
                catch (Exception e){

                    System.out.println("cannot cast the horizon as an int");
                    System.exit(1);
                }
                if (!(horizon>=2)){
                    System.out.println("please, enter an horizon superior to 1");
                    System.exit(1);
                }
	    }
	    else if (args[i].startsWith("-e=")){
	    	try{
                    epsilon = Float.parseFloat(args[i].substring(3));
                }
                catch (Exception e){

                    System.out.println("cannot cast epsilon as a double	");
                    System.exit(1);
                }
	    }
            else if (args[i].startsWith("-mdp=")){
                String heuristic = args[i].substring(5);

                if (!(heuristic.equals("y"))){
                    mdpHeuristic = false;
                }
            }
            else{
                System.out.println("unrecognized argument : " + args[i] + ", exiting");
                System.exit(1);
            }
        }
        System.out.println("");


        boolean debug = false;


        Posg<State, LocalAction, LocalObservation> posg = new Posg();
        OccupancyState<State, LocalAction, LocalObservation> o = new OccupancyState<State, LocalAction, LocalObservation>();

        ZsPOSGfromAmatoParser a = new ZsPOSGfromAmatoParser("../resources/Recycling.dpomdp");

        if (!debug) {
            if (competitiveTiger) {
                a = new ZsPOSGfromAmatoParser("../resources/CompetitiveTiger.posg");
                posg = a.initializePOSG("../resources/CompetitiveTiger.posg");
            }
            if (adversarialTiger){
                a = new ZsPOSGfromAmatoParser("../resources/AdversarialTiger.posg");
                posg = a.initializePOSG("../resources/AdvrsarialTiger.posg");
            }
            if (recycling){
                a = new ZsPOSGfromAmatoParser("../resources/Recycling.dpomdp");
                posg = a.initializePOSG("../resources/Recycling.dpomdp");
            }
            if (mabc){
                a = new ZsPOSGfromAmatoParser("../resources/mabc.dpomdp");
                posg = a.initializePOSG("../resources/mabc.dpomdp");
            }
	    if (MP){
	        a = new ZsPOSGfromAmatoParser("../resources/MatchingPennies.posg");
                posg = a.initializePOSG("../resources/MatchingPennies.posg");
	    }
            o = new OccupancyState(posg);
            a.initiateOccupancyState(o, posg);

        }

        if (!LPsolve) {


            OMGHSVIConcaveOptimization omgHSVIConcaveOptimization = new OMGHSVIConcaveOptimization(posg, o,start);
            omgHSVIConcaveOptimization.setInitialOccupancyState(o);
            omgHSVIConcaveOptimization.initializeBounds();

            boolean debugCplex = false;
            omgHSVIConcaveOptimization.solve(errorDoo, horizon);
            long endAlg = System.currentTimeMillis();
            System.out.println("time : " + (endAlg - start)/1000);
        }
        else{

            posg.profondeurMax = horizon;
            posg.maxReward = posg.computeMaxReward();
            posg.minReward = posg.computeMinReward();


            posg.setInitialBelief(o.InitialBelief);
            posg.mdpHeuristic = mdpHeuristic;

            SolvePomdp<State,LocalAction,LocalObservation> solvePomdp = new SolvePomdp<>(posg,1);
            solvePomdp = new SolvePomdp<>(posg,2);
            posg.setInitialBelief(o.InitialBelief);


            Distribution<State> distrib = new Distribution<State>();

            for (State s : posg.getStates()){
                distrib.addWeight(s,posg.initialBelief.getWeight(s));
            }

            LowerBoundHyperplane lowerBoundP1 = new LowerBoundHyperplane(posg,1,1.0,true,posg.profondeurMax);
            SawtoothBound<State,LocalAction,LocalObservation> upperBoundP1 = new SawtoothBound<>(true,posg,1.0,epsilon,true,1,posg.profondeurMax);
            HSVI<State,LocalAction,LocalObservation> hsviP1 = new HSVI<>(posg, 1.0, epsilon, lowerBoundP1, upperBoundP1, true, true, 1);
            Belief<State,LocalAction,LocalObservation> bP1 = new Belief<State, LocalAction, LocalObservation>(posg,distrib,0,1);

            //posg.setUpperBoundNotDynamic(hsviP1.solve(bP1,0,600,100));
            //System.out.println(" majorant  b_0 : " + upperBoundP1.getValue(bP1));


            Distribution<Pair<State,History<LocalAction,LocalObservation>>> distribution = new Distribution<>();
            for (State s : posg.getStates()){
                distribution.addWeight(new Pair<>(s,new History<>()),posg.initialBelief.getWeight(s));
            }

            LowerBoundHyperplane lowerBoundP2 = new LowerBoundHyperplane(posg,2,1.0,true,posg.profondeurMax);
            SawtoothBound<State,LocalAction,LocalObservation> upperBoundP2 = new SawtoothBound<>(true,posg,1.0,epsilon,true,2,posg.profondeurMax);
            HSVI<State,LocalAction,LocalObservation> hsviP2 = new HSVI<>(posg, 1.0, epsilon, lowerBoundP2, upperBoundP2, true, true, 2);
            Belief<State,LocalAction,LocalObservation> bP2 = new Belief<State, LocalAction, LocalObservation>(posg,distrib,0,2);

            //posg.setLowerBoundNotDynamic(hsviP2.solve(bP2,0,600,100));
            //System.out.println(" minorant  b_0 : " + lowerBoundP2.getValue(bP2));

            System.out.println("initial belief : " + o);


            System.out.println("max lambda : " + (posg.maxReward*posg.profondeurMax - posg.minReward*posg.profondeurMax)/2);
            HashMap<Integer,Double> lambda = new HashMap<>();
            for (int t = 0;t<=posg.profondeurMax;t++){
                lambda.put(t,(posg.profondeurMax-t) * (posg.maxReward - posg.minReward));
            }
            System.out.println("lambda : " + lambda);

            posg.lambda = lambda;

            double rho = epsilon/(10000*(posg.maxReward - posg.minReward)*posg.profondeurMax*posg.profondeurMax);
	    

            LPsolver lPsolver = new LPsolver(epsilon,horizon,lambda,posg,o, start, end,rho);


	    System.out.println("");

            History<LocalAction,LocalObservation> voidHistory = new History<>();
            Distribution<Pair<State,History<LocalAction,LocalObservation>>> d = new Distribution<>();
            for (State s : posg.getStates()){
                d.addWeight(new Pair<>(s,voidHistory),posg.initialBelief.getWeight(s));
            }

 	System.out.println("\n Starting the solving process\n");
    System.out.println("The output of the execution prints (focusing on the most important information) one line per iteration (trajectory), with:");
    System.out.println("    1. iteration number,");
    System.out.println("    2. CPU time since beginning,");
    System.out.println("    3. lower- and upper-bounding values at t=0;");
    System.out.println("* within each iteration, one line per time step t, with:");
    System.out.println("    1. time step t,");
    System.out.println("    2. current compressed occupancy state (OS) $\\sigma_t$,");
    System.out.println("    3. list of compressed action-observation histories (\"aoh1=aoh2\" means that aoh1 and aoh2 are equivalent, and only aoh2 appears.),");
    System.out.println("    4. width (/gap) between bounds at current OS,");
    System.out.println("    5. value of lower and upper bounds at current OS, and");
    System.out.println("    6. value of the threshold required for stopping the trajectory;");
    System.out.println("* at the end of the execution:");
    System.out.println("    - the line \"pair sol\" provides the solution Nash equilibrium strategy profile as a pair of behavioral strategies, and");
    System.out.println("    - the lines \"value POMDP P1\" and \"value POMDP P2\" give, for validation purposes, give the optimal values an solution policies for the best-response POMDP problems obtained by fixing the strategies of one of the two players.");
            Pair<BehavioralStrategy<State,LocalAction,LocalObservation>,BehavioralStrategy<State,LocalAction,LocalObservation>> pair = lPsolver.hsviSolve();
 	    System.out.println("\n End of the solving process\n");

            //System.out.println("pair sol : " + pair);
            pair.getElement0().posg = posg;
            pair.getElement1().posg = posg;
            SolvePomdpWithStrat<State,LocalAction,LocalObservation> solvePomdpWithStrat =
                    new SolvePomdpWithStrat<State,LocalAction,LocalObservation>(posg,1,pair.getElement0().toIntBehavioral());
            SolvePomdpWithStrat<State,LocalAction,LocalObservation> solvePomdpWithStratP2 =
                    new SolvePomdpWithStrat<State,LocalAction,LocalObservation>(posg,2,pair.getElement1().toIntBehavioral());
            System.out.println("- value POMDP P1 : " + solvePomdpWithStrat.solve(d,0)  +
			       " (given solution $\\beta^2_{0:}$) : " +  solvePomdpWithStrat.strat);
            System.out.println("- value POMDP P2 : " + solvePomdpWithStratP2.solve(d,0)  +
			       " (given solution $\\beta^1_{0:}$)" + solvePomdpWithStratP2.strat);
	    System.out.println("");
        }
    }
}
