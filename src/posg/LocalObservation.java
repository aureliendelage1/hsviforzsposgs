package posg;

public class LocalObservation {
    private String name = "";

    public LocalObservation(String name){
        this.name = name;
    }

    public LocalObservation() {

    }

    public String getNom() {
        return name;
    }

    public void setNom(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return this.name;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public boolean equals(Object a) {
        LocalObservation aCast = new LocalObservation("ksdfj");
        try{
            aCast = (LocalObservation) a;
        }
        catch (Exception e){
            System.out.println("localObservation.java::Couldn't cast a : " + a.toString() + " into a localObservation. :(");
        }
        return this.name.equals(aCast.name);
    }
}
