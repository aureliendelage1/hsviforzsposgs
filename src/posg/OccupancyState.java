package posg;

import util.Distribution;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;


public class OccupancyState<State,Action,Observation> {
    public Posg<State, Action, Observation> POSG;
    private Distribution<Pair<State, JointHistory<Action, Observation>>> distribution;
    public Distribution<State> InitialBelief;
    public int timestep;
    public HashMap<History<Action,Observation>,History<Action,Observation>> listCompressedHistoriesP1 = new HashMap<>();
    public HashMap<History<Action,Observation>,History<Action,Observation>> listCompressedHistoriesP2 = new HashMap<>();

    public Distribution<Pair<State,JointHistory<Action,Observation>>> uncompressedDistrib;

    public OccupancyState(Posg POSG) {
        this.distribution = new Distribution<Pair<State, JointHistory<Action, Observation>>>();
        this.POSG = POSG;


    }

    public OccupancyState(){

    }

    public OccupancyState copy () {
        OccupancyState o = new OccupancyState(this.POSG);
        Distribution<State> initialBeliefCopy = new Distribution<>();
        for (State s: this.InitialBelief.getNonZeroElements()){

            initialBeliefCopy.setWeight(s,InitialBelief.getWeight(s));
        }
        HashMap<Double,Double> h = new HashMap<>();
        HashMap<Double,Double> hprime = new HashMap<>(h);
        o.setInitialBelief(initialBeliefCopy);
        initialBeliefCopy.setWeights(new HashMap<> (this.InitialBelief.getWeights()));


        Distribution<Pair<State, JointHistory<Action, Observation>>> distribCopy = new Distribution<>();
        for (Pair<State, JointHistory<Action, Observation>> key : this.distribution.getNonZeroElements()){
            distribCopy.addWeight(key,this.distribution.getWeights().get(key));
        }
        o.setDistribution(distribCopy);

        o.timestep = this.timestep;

        o.listCompressedHistoriesP1 = new HashMap(this.listCompressedHistoriesP1);
        o.listCompressedHistoriesP2 = new HashMap(this.listCompressedHistoriesP2);
        o.uncompressedDistrib = this.uncompressedDistrib;
        return o;
    }

    public ArrayList<Pair<State,History<Action,Observation>>> groupByForPomdp(int player){
        ArrayList<Pair<State,History<Action,Observation>>> listStatePomdp = new ArrayList<>();
        if (player%2==1){
            for (Pair<State,JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()) {
                if (!listStatePomdp.contains(new Pair<State,History<Action,Observation>>(pair.getElement0(),pair.getElement1().getHistJ2()))){
                    listStatePomdp.add(new Pair<State,History<Action,Observation>>(pair.getElement0(),pair.getElement1().getHistJ2()));
                }
            }
        }
        if (player%2==0){
            for (Pair<State,JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()) {
                if (!listStatePomdp.contains(new Pair<State,History<Action,Observation>>(pair.getElement0(),pair.getElement1().getHistJ1()))){
                    listStatePomdp.add(new Pair<State,History<Action,Observation>>(pair.getElement0(),pair.getElement1().getHistJ1()));
                }
            }
        }
        return listStatePomdp;
    }

    public ArrayList<History<Action,Observation>> groupByPlayer1(ArrayList<JointHistory<Action,Observation>> groupByState) {
        ArrayList<History<Action,Observation>> listLocalHist = new ArrayList<>();
        for (JointHistory<Action,Observation> h : groupByState){
            if (!listLocalHist.contains(h.getHistJ1())) {
                listLocalHist.add(h.getHistJ1());
            }
        }
        return listLocalHist;
    }

    public ArrayList<History<Action,Observation>> groupByPlayer2(ArrayList<JointHistory<Action,Observation>> groupByState) {
        ArrayList<History<Action,Observation>> listLocalHist = new ArrayList<>();
        for (JointHistory<Action,Observation> h : groupByState){
            if (!listLocalHist.contains(h.getHistJ2())) {
                listLocalHist.add(h.getHistJ2());
            }
        }
        return listLocalHist;
    }

    public void setWeight(Pair<State, JointHistory<Action, Observation>> pair, double value) throws Exception {
        this.distribution.setWeight(pair, value);

        if (this.POSG.timestep != pair.getElement1().getHistJ1().getListeActionObservation().size()) {
            throw new Exception("BUG : timestep != length(history)");
        }
        if (pair.getElement1().getHistJ1().getListeActionObservation().size() != pair.getElement1().getHistJ2().getListeActionObservation().size()) {
            throw new Exception("BUG : histories do not have the same length!");
        }
    }

    public void initialupdate(HashMap<History<Action,Observation>, Distribution<Action>> HashMapDistributionStrategyPlayer1,
			      HashMap<History<Action,Observation>, Distribution<Action>> HashMapDistributionStrategyPlayer2,
			      boolean compress)  throws Exception {

        Distribution<Action> distributionStrategyPlayer1 = HashMapDistributionStrategyPlayer1.get(HashMapDistributionStrategyPlayer1.keySet().iterator().next());
        Distribution<Action> distributionStrategyPlayer2 = HashMapDistributionStrategyPlayer2.get(HashMapDistributionStrategyPlayer2.keySet().iterator().next());

        distributionStrategyPlayer1.sanityCheck();
        distributionStrategyPlayer2.sanityCheck();


        for (State nextState : this.POSG.getStates()) {
            for (JointAction<Action> a : this.POSG.getJointActions()) {
                for (JointObservation<Observation> z : this.POSG.getJointObservations()) {
                    double probability = 0.0;
                    ArrayList<Pair<Action, Observation>> list = new ArrayList<>();
                    list.add(new Pair<Action, Observation>(a.getActions().getElement0(), z.getObs().getElement0()));
                    History histJ1 = new History();
                    histJ1.setListeActionObservation(list);

                    ArrayList<Pair<Action, Observation>> list2 = new ArrayList<>();
                    list2.add(new Pair<Action, Observation>(a.getActions().getElement1(), z.getObs().getElement1()));
                    History histJ2 = new History();
                    histJ2.setListeActionObservation(list2);
                    for (State lastState : this.POSG.getStates()) {
                        double conditionalProbability = this.InitialBelief.getWeight(lastState);
                        conditionalProbability *= distributionStrategyPlayer1.getWeight(a.getActions().getElement0());
                        conditionalProbability *= distributionStrategyPlayer2.getWeight(a.getActions().getElement1());
                        conditionalProbability *= this.POSG.getTransitions().get(new Pair<State, JointAction<Action>>(lastState, a)).getWeight(nextState);
                        conditionalProbability *= this.POSG.getObservationProbabilities().getProba(a, nextState, z);
                        probability = probability + conditionalProbability;


                    }
                    if (probability>0 || probability > 0) {
                        this.distribution.addWeight(new Pair<State, JointHistory<Action, Observation>>
                                (nextState, new JointHistory<Action, Observation>(histJ1, histJ2)), probability);
                    }
                    else{
                    }
                }
            }
        }
        timestep ++;

        this.uncompressedDistrib = this.distribution;
         if (compress) {

             this.distribution = makeCompression();

         }


        try {
            this.sanityCheck();
        }
        catch(Exception e){
            e.printStackTrace();
            this.distribution = uncompressedDistrib;
        }
    }

    public void initialupdate(HashMap<History<Action,Observation>, Distribution<Action>> HashMapDistributionStrategyPlayer1
            , HashMap<History<Action,Observation>, Distribution<Action>> HashMapDistributionStrategyPlayer2)  throws Exception {

        Distribution<Action> distributionStrategyPlayer1 = HashMapDistributionStrategyPlayer1.get(HashMapDistributionStrategyPlayer1.keySet().iterator().next());
        Distribution<Action> distributionStrategyPlayer2 = HashMapDistributionStrategyPlayer2.get(HashMapDistributionStrategyPlayer2.keySet().iterator().next());

        distributionStrategyPlayer1.sanityCheck();
        distributionStrategyPlayer2.sanityCheck();


        for (State nextState : this.POSG.getStates()) {
            for (JointAction<Action> a : this.POSG.getJointActions()) {
                for (JointObservation<Observation> z : this.POSG.getJointObservations()) {
                    double probability = 0.0;
                    ArrayList<Pair<Action, Observation>> list = new ArrayList<>();
                    list.add(new Pair<Action, Observation>(a.getActions().getElement0(), z.getObs().getElement0()));
                    History histJ1 = new History();
                    histJ1.setListeActionObservation(list);

                    ArrayList<Pair<Action, Observation>> list2 = new ArrayList<>();
                    list2.add(new Pair<Action, Observation>(a.getActions().getElement1(), z.getObs().getElement1()));
                    History histJ2 = new History();
                    histJ2.setListeActionObservation(list2);
                    for (State lastState : this.POSG.getStates()) {
                        double conditionalProbability = this.InitialBelief.getWeight(lastState);
                        conditionalProbability = conditionalProbability *
                                distributionStrategyPlayer1.getWeight(a.getActions().getElement0());
                        conditionalProbability = conditionalProbability *
                                distributionStrategyPlayer2.getWeight(a.getActions().getElement1());
                        conditionalProbability *= this.POSG.getTransitions().get(new Pair<State, JointAction<Action>>(lastState,a)).getWeight(nextState);
                        conditionalProbability *= this.POSG.getObservationProbabilities().getProba(a,nextState,z);
                        probability = probability + conditionalProbability;


                    }
                    if (probability>0 || probability > 0) {
                        this.distribution.addWeight(new Pair<State, JointHistory<Action, Observation>>
                                (nextState, new JointHistory<Action, Observation>(histJ1, histJ2)), probability);
                    }
                    else{


                    }
                }
            }
        }
        timestep ++;

        this.sanityCheck();

        this.sanityCheck();
    }


    private ArrayList<Pair<State,History<Action,Observation>>> getLocalHistoryWithState(int player) {
        if (player == 0){
            ArrayList<Pair<State,History<Action,Observation>>> list = new ArrayList<>();
            for (Pair<State, JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()){
                Pair<State,History<Action,Observation>> pairToAdd = new Pair<State,History<Action,Observation>>(pair.getElement0(),pair.getElement1().getHistJ1());
                if (!(list.contains(pairToAdd))){
                    list.add(pairToAdd);
                }
            }
            return list;
        }

        else{
            ArrayList<Pair<State,History<Action,Observation>>> list = new ArrayList<>();
            for (Pair<State, JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()){
                Pair<State,History<Action,Observation>> pairToAdd = new Pair<State,History<Action,Observation>>(pair.getElement0(),pair.getElement1().getHistJ2());
                if (!(list.contains(pairToAdd))){
                    list.add(pairToAdd);
                }
            }
            return list;
        }
    }


    private Distribution<Pair<State,JointHistory<Action,Observation>>> makeCompression() throws Exception {
        this.listCompressedHistoriesP1 = new HashMap<>();
        this.listCompressedHistoriesP2 = new HashMap<>();


        Distribution<Pair<State,JointHistory<Action,Observation>>> compressedDistrib = new Distribution<>();
        ArrayList<History<Action,Observation>> listToIgnore = new ArrayList<>();
        for (History<Action,Observation> histJ1 : this.getLocalHistoryJ1()){

            if (!(listToIgnore.contains(histJ1))) {
                for (History<Action, Observation> histJ2 : this.getLocalHistoryJ2()) {
                    for (State s : this.POSG.getStates()) {
                        Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, histJ2));
                        compressedDistrib.setWeight(pair, this.distribution.getWeight(pair));
                    }
                }

                for (History<Action, Observation> histJ1ToCheckLPE : this.getLocalHistoryJ1()) {
                    if (!histJ1ToCheckLPE.equals(histJ1)) {
                        if (areLPE(histJ1, histJ1ToCheckLPE, 0)) {
                            for (History<Action, Observation> histJ2 : this.getLocalHistoryJ2()) {
                                for (State s : this.POSG.getStates()) {
                                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, histJ2));
                                    Pair<State, JointHistory<Action, Observation>> pairLPE = new Pair<>(s, new JointHistory<Action, Observation>(histJ1ToCheckLPE, histJ2));
                                    compressedDistrib.setWeight(pair, compressedDistrib.getWeight(pair) + this.distribution.getWeight(pairLPE));
                                }
                            }
                            listToIgnore.add(histJ1ToCheckLPE);
                            this.listCompressedHistoriesP1.put(histJ1ToCheckLPE,histJ1);

                        }
                    }
                }
            }
            else{

            }
        }
        this.distribution = compressedDistrib;


        try {
            this.distribution.sanityCheck();
        }
        catch (Exception e){
            e.printStackTrace();
        }


        Distribution<Pair<State,JointHistory<Action,Observation>>> compressedDistribP2 = new Distribution<>();
        ArrayList<History<Action,Observation>> listToIgnoreP2 = new ArrayList<>();
        for (History<Action,Observation> histJ2 : this.getLocalHistoryJ2()){
            if (!(listToIgnoreP2.contains(histJ2))) {
                for (History<Action, Observation> histJ1 : this.getLocalHistoryJ1()) {
                    for (State s : this.POSG.getStates()) {
                        Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, histJ2));
                        compressedDistribP2.setWeight(pair, this.distribution.getWeight(pair));
                    }
                }
                for (History<Action, Observation> histJ2ToCheckLPE : this.getLocalHistoryJ2()) {
                    if (!histJ2ToCheckLPE.equals(histJ2)) {

                        if (areLPE(histJ2, histJ2ToCheckLPE, 1)) {

                            for (History<Action, Observation> histJ1 : this.getLocalHistoryJ1()) {
                                for (State s : this.POSG.getStates()) {
                                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, histJ2));
                                    Pair<State, JointHistory<Action, Observation>> pairLPE = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, histJ2ToCheckLPE));
                                    compressedDistribP2.setWeight(pair, compressedDistribP2.getWeight(pair) + this.distribution.getWeight(pairLPE));

                                }
                            }

                            listToIgnoreP2.add(histJ2ToCheckLPE);
                            listCompressedHistoriesP2.put(histJ2ToCheckLPE,histJ2);
                        }
                    }
                }
            }
            else{

            }
        }


        try {
            this.distribution.sanityCheck();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return compressedDistribP2;
    }

    private boolean areLPE(History<Action,Observation> hist, History<Action,Observation> histToCheckLPE, int player) throws Exception {
        if (player==0) {

            double sum = this.computeSum(hist, 0);
            for (State s : this.POSG.getStates()) {
                for (History<Action, Observation> histJ2 : this.getLocalHistoryJ2()) {
                    double sumTestLPE = this.computeSum(histToCheckLPE, 0);
                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(hist, histJ2));
                    Pair<State, JointHistory<Action, Observation>> pairToTest = new Pair<>(s, new JointHistory<Action, Observation>(histToCheckLPE, histJ2));


                    if (Math.abs((this.distribution.getWeight(pair) / sum) - (this.distribution.getWeight(pairToTest) / sumTestLPE))>0.0000001) {


                        return false;
                    }
                }
            }

            return true;
        }
        else{

            double sum = this.computeSum(hist, 1);
            double sumTestLPE = this.computeSum(histToCheckLPE, 1);
            for (State s : this.POSG.getStates()) {
                for (History<Action, Observation> histJ1 : this.getLocalHistoryJ1()) {
                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, hist));
                    Pair<State, JointHistory<Action, Observation>> pairToTest = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, histToCheckLPE));


                    if (Math.abs((this.distribution.getWeight(pair) / sum) - (this.distribution.getWeight(pairToTest) / sumTestLPE))>0.00000001) {
                        return false;
                    }
                }
            }

            return true;
        }
    }


    private boolean arePrivatehistoriesTPE(History<Action,Observation> hist, History<Action,Observation> histToCheckLPE, int suffixeSize, int player){

        for (int i = 0; i< suffixeSize; i++){
            if (!(hist.getListeActionObservation().get(hist.getListeActionObservation().size()-1-i)).equals(
                    histToCheckLPE.getListeActionObservation().get(histToCheckLPE.getListeActionObservation().size()-1-i))) {
                return false;
            }
        }

        return true;
    }
    private Distribution<Pair<State,JointHistory<Action,Observation>>> makeCompresionTPE() throws Exception {
        int m = this.getTruncationParam();
        Distribution<Pair<State,JointHistory<Action,Observation>>> compressedDistrib = new Distribution<>();
        ArrayList<Pair<State,JointHistory<Action,Observation>>> listToIgnore = new ArrayList<>();

        for (Pair<State,JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()) {
            if (!(listToIgnore.contains(pair))) {
                listToIgnore.add(pair);
                compressedDistrib.addWeight(pair, this.distribution.getWeight(pair));
                for (Pair<State, JointHistory<Action, Observation>> pairToCheckLPE : this.distribution.getNonZeroElements()) {
                    if (!(listToIgnore.contains(pairToCheckLPE))) {
                        if (this.suffixTPE(pair.getElement1().getHistJ1(),pairToCheckLPE.getElement1().getHistJ1(),m,0) &&
                                this.suffixTPE(pair.getElement1().getHistJ2(),pairToCheckLPE.getElement1().getHistJ2(),m,1)) {

                            listToIgnore.add(pairToCheckLPE);
                            compressedDistrib.setWeight(pair, compressedDistrib.getWeight(pair) + distribution.getWeight(pairToCheckLPE));
                        }
                    }
                }
            }
        }

        return compressedDistrib;

    }
    private int getTruncationParam() throws Exception {
        int m = 0;
        for (int i = 0;i<2;i++) {
            if (i==0) {
                for (History<Action, Observation> histJ1 : this.getLocalHistoryJ1()) {
                    for (History<Action, Observation> histJ1ToCheckTPE : this.getLocalHistoryJ1()) {
                        if (!(histJ1.equals(histJ1ToCheckTPE))) {

                            if (suffixTPE(histJ1,histJ1ToCheckTPE,m,0)) {

                                if (!(areLPEInidividual(histJ1, histJ1ToCheckTPE, i))) {

                                    m = m + 1;
                                }
                            }
                        }
                    }
                }
            }
            if (i==1) {
                for (History<Action, Observation> histJ2 : this.getLocalHistoryJ2()) {
                    for (History<Action, Observation> histJ2ToCheckTPE : this.getLocalHistoryJ2()) {
                        if (!(histJ2.equals(histJ2ToCheckTPE))) {
                            if (suffixTPE(histJ2,histJ2ToCheckTPE,m,1)) {
                                if (!(areLPEInidividual(histJ2, histJ2ToCheckTPE, i))) {
                                    m = m + 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        return m;
    }

    private boolean areTPEequivalent(Pair<State,JointHistory<Action,Observation>> pair, Pair<State,JointHistory<Action,Observation>> pairToCheck, int m){
        if (!(pair.getElement0().equals(pairToCheck.getElement0()))){
            return false;
        }
        if (!(this.suffixTPE(pair.getElement1().getHistJ1(),pairToCheck.getElement1().getHistJ1(),m,0))){
            return false;
        }
        if (!(this.suffixTPE(pair.getElement1().getHistJ2(),pairToCheck.getElement1().getHistJ2(),m,1))){
            return false;
        }
        return true;
    }

    private boolean suffixTPE(History<Action,Observation> hist, History<Action,Observation> histToCheckLPE, int suffixeSize, int player){

        for (int i = 0; i<= suffixeSize; i++){
            if (!(hist.getListeActionObservation().get(hist.getListeActionObservation().size()-1-i)).equals(
                    histToCheckLPE.getListeActionObservation().get(histToCheckLPE.getListeActionObservation().size()-1-i))) {
                return false;
            }
        }

        return true;
    }

    private Distribution<Pair<State,JointHistory<Action,Observation>>> makeCompressionV2() throws Exception {
        Distribution<Pair<State,JointHistory<Action,Observation>>> compressedDistrib = new Distribution<>();
        ArrayList<Pair<State,JointHistory<Action,Observation>>> listToIgnore = new ArrayList<>();

        for (Pair<State,JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()) {
            if (!(listToIgnore.contains(pair))) {
                listToIgnore.add(pair);
                compressedDistrib.addWeight(pair, this.distribution.getWeight(pair));
                for (Pair<State, JointHistory<Action, Observation>> pairToCheckLPE : this.distribution.getNonZeroElements()) {
                    if (!(listToIgnore.contains(pairToCheckLPE))) {
                        if (areLPE(pair, pairToCheckLPE)) {

                            listToIgnore.add(pairToCheckLPE);
                            compressedDistrib.setWeight(pair, compressedDistrib.getWeight(pair) + distribution.getWeight(pairToCheckLPE));
                        }
                    }
                }
            }
        }

        return compressedDistrib;

    }

    private boolean areLPEInidividual(History<Action, Observation> hist, History<Action, Observation> histToCheckLPE, int player) throws Exception {
        if (player == 0){
            double sum = this.computeSum(hist, 0);
            double sumTestLPE = this.computeSum(histToCheckLPE, 0);
            for (State s : this.POSG.getStates()) {
                for (History<Action, Observation> histJ2 : this.getLocalHistoryJ2()) {
                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(hist, histJ2));
                    Pair<State, JointHistory<Action, Observation>> pairToTest = new Pair<>(s, new JointHistory<Action, Observation>(histToCheckLPE, histJ2));


                    if (Math.abs((this.distribution.getWeight(pair) / sum) - (this.distribution.getWeight(pairToTest) / sumTestLPE))>0.001) {


                        return false;
                    }
                }
            }
            return true;
        }
        else{
            double sum = this.computeSum(hist, 1);
            double sumTestLPE = this.computeSum(histToCheckLPE, 1);

            for (State s : this.POSG.getStates()) {
                for (History<Action, Observation> histJ1 : this.getLocalHistoryJ1()) {
                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, hist));
                    Pair<State, JointHistory<Action, Observation>> pairToTest = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, histToCheckLPE));


                    if (Math.abs((this.distribution.getWeight(pair) / sum) - (this.distribution.getWeight(pairToTest) / sumTestLPE))>0.001) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    private boolean areLPE(Pair<State,JointHistory<Action,Observation>> hist, Pair<State,JointHistory<Action,Observation>> histToCheckLPE) throws Exception {

        if (!(hist.getElement0().equals(histToCheckLPE.getElement0()))){
            return false;
        }
        else{
            if (!(areLPEInidividual(hist.getElement1().getHistJ1(),histToCheckLPE.getElement1().getHistJ1(),0))){
                return false;
            }
            if (!(areLPEInidividual(hist.getElement1().getHistJ2(),histToCheckLPE.getElement1().getHistJ2(),1))){
                return false;
            }
        }

        return true;
    }

    private double computeSum(History<Action,Observation> hist, int player) throws Exception {
        if (player==0) {
            double sum = 0.0;
            for (State s : this.POSG.getStates()) {
                for (History<Action, Observation> histJ2 : this.getLocalHistoryJ2()) {
                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(hist, histJ2));
                    sum += this.distribution.getWeight(pair);
                }
            }
            return sum;
        }
        else{
            double sum = 0.0;
            for (State s : this.POSG.getStates()) {
                for (History<Action, Observation> histJ1 : this.getLocalHistoryJ1()) {
                    Pair<State, JointHistory<Action, Observation>> pair = new Pair<>(s, new JointHistory<Action, Observation>(histJ1, hist));
                    sum += this.distribution.getWeight(pair);
                }
            }
            return sum;
        }
    }

    public HashSet<History<Action,Observation>> getLocalHistoryJ1() throws Exception {
        HashSet<History<Action,Observation>> hashSet = new HashSet<>();
        for (Pair<State, JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()){
            if (!(hashSet.contains(pair.getElement1().getHistJ1()))){
                hashSet.add(pair.getElement1().getHistJ1());
            }
        }
        return hashSet;
    }

    public HashSet<History<Action,Observation>> getLocalHistoryJ2() throws Exception {
        HashSet<History<Action,Observation>> hashSet = new HashSet<>();
        for (Pair<State, JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()){
            if (!(hashSet.contains(pair.getElement1().getHistJ2()))){
                hashSet.add(pair.getElement1().getHistJ2());
            }
        }
        return hashSet;
    }

    public void update(HashMap<History<Action,Observation>, Distribution<Action>> distributionStrategyPlayer1
            , HashMap<History<Action,Observation>, Distribution<Action>> distributionStrategyPlayer2) throws Exception {
        System.out.println("in the second update");

        this.distribution.sanityCheck();

        Distribution<Pair<State, JointHistory<Action,Observation>>> newDistrib = new Distribution<>();
        int N = 0;
        double sum = 0;
        for (Pair<State, JointHistory<Action, Observation>> newPair : this.getListNextStateHistory()) {

            N++;
            double probability = 0.0;
            for (Pair<State, JointHistory<Action, Observation>> pair : this.uncompressedDistrib.getNonZeroElements()) {
                boolean hmmm = false;
                if (this.extendsHistory(newPair,pair)){
                    if (hmmm){
                        System.out.println("WTF, two histories ");
                    }
                    hmmm = true;

                    double probaConditionelle = distribution.getWeight(pair);

                    JointAction<Action> a = newPair.getElement1().getLastJointAction();
                    JointObservation<Observation> z = newPair.getElement1().getLastJointObservation();
                    probaConditionelle *= distributionStrategyPlayer1.get(pair.getElement1().getHistJ1()).getWeight(a.getActions().getElement0());


                    probaConditionelle *= distributionStrategyPlayer2.get(pair.getElement1().getHistJ2()).getWeight(a.getActions().getElement1());


                    probaConditionelle *= this.POSG.getTransitions().get(new Pair<State, JointAction>(pair.getElement0(),a)).getWeight(newPair.getElement0());

                    probaConditionelle *= this.POSG.getObservationProbabilities().getProba(a,newPair.getElement0(),z);


                    sum+= probaConditionelle;
                    probability += probaConditionelle;
                }
            }

            if (probability>0 || probability > 0) {

                newDistrib.addWeight(newPair, probability);
            }


        }

        timestep++;
        this.distribution = newDistrib;
    }

    public void update(HashMap<History<Action,Observation>, Distribution<Action>> distributionStrategyPlayer1
            , HashMap<History<Action,Observation>, Distribution<Action>> distributionStrategyPlayer2, boolean compress) throws Exception {
        Distribution<Pair<State, JointHistory<Action,Observation>>> newDistrib = new Distribution<>();
        int N = 0;
        double sum = 0;
        for (Pair<State, JointHistory<Action, Observation>> newPair : this.getListNextStateHistory()) {

            N++;
            double probability = 0.0;
            for (Pair<State, JointHistory<Action, Observation>> pair : this.uncompressedDistrib.getNonZeroElements()) {
                boolean hmmm = false;
                if (this.extendsHistory(newPair,pair)){
                    if (hmmm){
                        System.out.println("WTF, two histories ");
                    }
                    hmmm = true;

                    double probaConditionelle = distribution.getWeight(pair);

                    JointAction<Action> a = newPair.getElement1().getLastJointAction();
                    JointObservation<Observation> z = newPair.getElement1().getLastJointObservation();
                    probaConditionelle *= distributionStrategyPlayer1.get(pair.getElement1().getHistJ1()).getWeight(a.getActions().getElement0());


                    probaConditionelle *= distributionStrategyPlayer2.get(pair.getElement1().getHistJ2()).getWeight(a.getActions().getElement1());


                    probaConditionelle *= this.POSG.getTransitions().get(new Pair<State, JointAction>(pair.getElement0(),a)).getWeight(newPair.getElement0());

                    probaConditionelle *= this.POSG.getObservationProbabilities().getProba(a,newPair.getElement0(),z);


                    sum+= probaConditionelle;
                    probability += probaConditionelle;
                }
            }

            if (probability>0 || probability > 0) {

                newDistrib.addWeight(newPair, probability);
            }


        }

        timestep++;
        this.distribution = newDistrib;
        this.uncompressedDistrib = this.distribution;

        if ((compress && timestep==POSG.profondeurMax-1)||true) {

            this.distribution = makeCompression();
        }
        try {
            this.sanityCheck();
        }
        catch (Exception e){
            e.printStackTrace();
            this.distribution = uncompressedDistrib;
        }


        this.distribution.sanityCheck();
    }

    private void sanityCheck() {

        try{
            this.distribution.sanityCheck();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("sum : "+this.distribution.getSumOfWeights());

        }
    }

    private boolean extendsHistory(Pair<State, JointHistory<Action, Observation>> newPair, Pair<State, JointHistory<Action, Observation>> pair){
        ArrayList<Pair<Action,Observation>> listJ1 = new ArrayList<>();
        listJ1 = pair.getElement1().getHistJ1().getListeActionObservation();

        ArrayList<Pair<Action,Observation>> newListJ1 = new ArrayList<>();
        newListJ1 = newPair.getElement1().getHistJ1().getListeActionObservation();

        ArrayList<Pair<Action,Observation>> listJ1copie = new ArrayList<>(newListJ1);
        listJ1copie.remove(listJ1copie.size()-1);

        ArrayList<Pair<Action,Observation>> listJ2 = new ArrayList<>();
        listJ2 = pair.getElement1().getHistJ2().getListeActionObservation();

        ArrayList<Pair<Action,Observation>> newListJ2 = new ArrayList<>();
        newListJ2 = newPair.getElement1().getHistJ2().getListeActionObservation();

        ArrayList<Pair<Action,Observation>> listJ2copie = new ArrayList<>(newListJ2);
        listJ2copie.remove(listJ2copie.size()-1);


        if (listJ1copie.equals(listJ1) && listJ2copie.equals(listJ2)){
            return true;
        }

        return false;
    }

    private ArrayList<Pair<State, JointHistory<Action,Observation>>> getListNextStateHistory() throws Exception {
        ArrayList<Pair<State, JointHistory<Action,Observation>>> NextStateHistory = new ArrayList<>();
        for (JointHistory<Action,Observation> pair : this.groupByState(this.uncompressedDistrib.getNonZeroElements())) {
            for (Observation zJ1 : this.POSG.getObservationsJ1()){
                for (Observation zJ2 : this.POSG.getObservationsJ2()) {
                    for (Action aj1 : this.POSG.getActionsJ1()){
                        for (Action aj2 : this.POSG.getActionsJ2()) {
                            for (State s : this.POSG.getStates()){
                                History<Action, Observation> histJ1 = new History<Action, Observation>();
                                History<Action, Observation> histJ2 = new History<Action, Observation>();
                                histJ1.setListeActionObservation(new ArrayList<>(pair.getHistJ1().getListeActionObservation()));
                                histJ2.setListeActionObservation(new ArrayList<>(pair.getHistJ2().getListeActionObservation()));
                                histJ1.addActionObservation(aj1, zJ1);
                                histJ2.addActionObservation(aj2, zJ2);
                                JointHistory<Action, Observation> newJointHistory = new JointHistory<Action, Observation>(histJ1, histJ2);

                                NextStateHistory.add(new Pair<State, JointHistory<Action, Observation>>(s, newJointHistory));
                            }
                        }
                    }
                }
            }
        }
        return NextStateHistory;
    }

    public ArrayList<JointHistory<Action,Observation>> groupByState(Set<Pair<State, JointHistory<Action,Observation>>> nonZeroElements){
        ArrayList<JointHistory<Action,Observation>> list = new ArrayList<>();
        for (Pair<State, JointHistory<Action,Observation>> element : nonZeroElements){
            if (list.contains(element.getElement1()) == false){
                list.add(element.getElement1());
            }
        }
        return list;
    }
    public void setInitialBelief(Distribution<State> initialBelief) {
        this.InitialBelief = initialBelief;
    }

    public void show() throws Exception {
        if (timestep == 0) {
            for (State s : InitialBelief.getNonZeroElements()) {
                System.out.println("b_o(" + s.toString() + ") = " + InitialBelief.getWeight(s));
            }
            System.out.println("Begin sanity check over o_\\0");
            this.InitialBelief.sanityCheck();
            System.out.println("End sanity check over o_\\0");
        }
        if(timestep>0){


            System.out.print("OccupancyState::distribution :" + this.distribution.toString());
            if (this.distribution.size() != (int) (Math.pow((Math.pow(POSG.cardAction,timestep)*Math.pow(POSG.cardObservations,timestep)),1))*POSG.cardStates){


            }
            else{
                System.out.println("----------------------------");
                System.out.println("cardinal of o_\\tau has been tested");
                System.out.println("----------------------------");
            }
        }
    }

    public int getTimestep() {
        return this.timestep;
    }

    public Distribution<Pair<State, JointHistory<Action,Observation>>> getDistrib() {
        return this.distribution;
    }

    public double getProba(Pair<State, JointHistory<Action, Observation>> pair) {
        if (this.distribution.size() == 0){
            return InitialBelief.getWeight(pair.getElement0());
        }
        return this.distribution.getWeight(pair);
    }

    public double getNorme1(OccupancyState<State,Action,Observation> oPrime){
        double norme = 0.0;
        for (Pair<State, JointHistory<Action,Observation>> pair : this.distribution.getNonZeroElements()){

            norme+= Math.abs(oPrime.getDistrib().getWeight(pair)-this.getProba(pair));
        }

        return norme;
    }

    public Distribution<Pair<State, JointHistory<Action, Observation>>> getDistribution() {
        return distribution;
    }

    public void setDistribution(Distribution<Pair<State, JointHistory<Action, Observation>>> distribution) {
        this.distribution = distribution;
    }

    @Override
    public String toString() {


        NumberFormat formatter = new DecimalFormat("###.###");

        String S = "";
	if (timestep == 0) {
            S += "{";
            for (State s : InitialBelief.getNonZeroElements()) {

		Double d=InitialBelief.getWeight(s);
		if (d > 0)
		    S+= "(" + s.toString() + ",((-),(-)))=" + formatter.format(d) + ", ";
            }
            S += "} ";
            S += "\nlist compressed histories : " + this.listCompressedHistoriesP1 + " and " + this.listCompressedHistoriesP2;
            return S;
        }
        if(timestep>0){
            return "AOH belief :" + this.distribution.toString() + "\nlist compressed histories : " + this.listCompressedHistoriesP1 + " and " + this.listCompressedHistoriesP2;
        }
        return "<void>";
    }
    public String sanityCheck(int a) throws Exception {
        if (this.timestep == 0){
            this.InitialBelief.sanityCheck();
        }
        else{
            this.distribution.sanityCheck();
        }
        return "";
    }
    @Override
    public int hashCode() {

        if (this.getDistribution()==null){
            return this.InitialBelief.hashCode();
        }
        else{
            return this.distribution.hashCode();
        }
    }

    @Override
    public boolean equals(Object obj) {

        OccupancyState<State,Action,Observation> oprime = new OccupancyState<State, Action, Observation>(this.POSG);
        try{
            oprime = (OccupancyState<State,Action,Observation>) obj;
        }
        catch(Exception e){
            System.out.println("occupancyState::cannot cast into occupancy state the object : " + obj.toString());
        }
        if (this.getDistribution()==null){
            return (oprime.InitialBelief.equals(this.InitialBelief));
        }
        else{
            return (oprime.getDistribution().equals(this.distribution));
        }
    }

    public HashSet<History<Action,Observation>> getLocalHistory(int player) {
        if (player%2 == 1) {
            HashSet<History<Action, Observation>> hashSet = new HashSet<>();
            for (Pair<State, JointHistory<Action, Observation>> pair : this.distribution.getNonZeroElements()) {
                if (!(hashSet.contains(pair.getElement1().getHistJ1()))) {
                    hashSet.add(pair.getElement1().getHistJ1());
                }
            }
            if (hashSet.size() == 0){
                hashSet.add(new History<>());
            }
            return hashSet;
        }
        if (player%2 == 0){
            HashSet<History<Action, Observation>> hashSet = new HashSet<>();
            for (Pair<State, JointHistory<Action, Observation>> pair : this.distribution.getNonZeroElements()) {
                if (!(hashSet.contains(pair.getElement1().getHistJ2()))) {
                    hashSet.add(pair.getElement1().getHistJ2());
                }
            }
            if (hashSet.size() == 0){
                hashSet.add(new History<>());
            }
            return hashSet;
        }
        return null;
    }

    public HashMap<History<Action, Observation>, History<Action, Observation>>  getListCompressedHistories(int player) {
        if (player%2==1){
            if (listCompressedHistoriesP1!=null) {
                return this.listCompressedHistoriesP1;
            }
            else{
                return new HashMap<>();
            }
        }
        else{
            if (listCompressedHistoriesP2!=null) {
                return this.listCompressedHistoriesP2;
            }
            else{
                return new HashMap<>();
            }
        }
    }

    public ArrayList<History<Action,Observation>> getLocalCompressedHistory(int player) {
        ArrayList<History<Action,Observation>> list = new ArrayList<>();
        for (History<Action,Observation> histNotCompressed : this.getListCompressedHistories(player).keySet()){
            list.add(this.getListCompressedHistories(player).get(histNotCompressed));
        }
        System.out.println("list compressed histories : " + list);
        return list;
    }


    public HashMap<History<Action,Observation>, History<Action,Observation>> getReversedListCompressedHistories(int player){
        HashMap<History<Action,Observation>, History<Action,Observation>> res = new HashMap<>();
        HashMap<History<Action,Observation>, History<Action,Observation>> listCompressed = this.getListCompressedHistories(player);
        for (History<Action,Observation> h : listCompressed.keySet()){
            //if (res.containsKey(listCompressed.get(h))){
                //System.out.println("multiple keys !!");

            //}
            res.put(listCompressed.get(h),h);
        }
        return res;
    }
}
