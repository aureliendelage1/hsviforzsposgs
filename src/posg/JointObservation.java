package posg;

public class JointObservation<Observation> {

    private Pair<Observation,Observation> observations;

    public JointObservation(Observation obsJ1, Observation obsJ2){
        this.observations = new Pair<Observation,Observation>(obsJ1,obsJ2);
    }

    public JointObservation(Observation oPI, Observation oPNegI, int player) {
        if (player%2==1) {
            this.observations = new Pair<Observation, Observation>(oPI, oPNegI);
        }
        else{
            this.observations = new Pair<Observation, Observation>(oPNegI, oPI);

        }
    }

    @Override
    public String toString(){
        return "("+this.observations.getElement0().toString()+","+this.observations.getElement1().toString()+")";
    }
    public Pair<Observation,Observation> getObs(){
        return this.observations;
    }
    public boolean equals(JointObservation z) {
        return (this.observations.equals(z.observations));
    }

    @Override
    public boolean equals(Object obj) {
        JointObservation o;
        try{
            o = (JointObservation) obj;
        }
        catch(Exception e){
            return false;
        }
        if (o.observations.getElement0().equals(this.observations.getElement0()) && o.observations.getElement1().equals(this.observations.getElement1())){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.observations.hashCode();
    }
}
