package util;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class ObjectIntegerConverter<A> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<A,Integer> toIntegers;
    private List<A> toA;
    public ObjectIntegerConverter() {
        toIntegers = new HashMap<>();
        toA = new ArrayList<>();
    }

    public ObjectIntegerConverter(List<A> elemList) {
        toIntegers = new HashMap<>();
        toA = new ArrayList<>(elemList);
        for(int i = 0; i<elemList.size(); i++) {
            toIntegers.put(elemList.get(i),i);
        }
    }
    public A toA(int i) throws IllegalArgumentException {
        if(i < toA.size() && i >= 0) {
            return toA.get(i);
        }
        throw new IllegalArgumentException("No element has index " + i + " in the converter.");
    }
    public int toInteger(A a) throws IllegalArgumentException{
	Integer i = toIntegers.get(a);
        if( i != null ) {
            return i;
        }
        throw new IllegalArgumentException("The element " + a + " was not added in the converter.");
    }

    public void add(A a) {
        toIntegers.put(a,toA.size());
        toA.add(a);
    }

    public int getSize() {
        return toA.size();
    }
}
