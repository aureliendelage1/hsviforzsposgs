package util;

import java.io.*;
import java.util.ArrayList;

public class fixParser {
    String filename;
    public fixParser(String filename) throws IOException {
        this.filename = filename;
        File file = new File(filename);
        BufferedReader br = new BufferedReader(new FileReader(file));
        this.fixFile(br,filename);
    }

    public String avoidCharacter(String st, PrintWriter writer, boolean shouldWrite){
        if (st.contains("\"")) {

            st = st.replaceAll("\"", "");

            if (st.contains("values")){
                writer.println(st);
                shouldWrite = false;
            }
        }
        if (st.contains(" : ")){

            st = st.replaceAll(" : "," ");
        }
        return st;
    }
    public void fixFile(BufferedReader br,String filename) throws IOException {
        PrintWriter writer = new PrintWriter(filename + "Modififed");
        String st;
        boolean shouldWrite = true;
        ArrayList<String> actionsJ1 = new ArrayList<>();
        ArrayList<String> observationsJ1 = new ArrayList<>();
        ArrayList<String> actionsJ2 = new ArrayList<>();
        ArrayList<String> observationsJ2 = new ArrayList<>();
        while ((st = br.readLine()) != null) {
            shouldWrite = true;

            if (st.contains("\"")) {

                st = st.replaceAll("\"", "");

                if (st.contains("values")){
                    writer.println(st);
                    shouldWrite = false;
                }
            }

            if (st.toLowerCase().contains("actions")) {

                String s = st;
                shouldWrite = false;
                st = br.readLine();
                st = this.avoidCharacter(st, writer, shouldWrite);
                writer.println(s+ " "+st);
                String[] a = st.split(" ");
                for (int i = 0; i < a.length; i++) {
                    actionsJ1.add(a[i]);
                }
                st = br.readLine();
                st = this.avoidCharacter(st, writer, shouldWrite);
                writer.println(st);
                String[] pikachu = st.split(" ");
                for (int i = 0; i < pikachu.length; i++) {
                    actionsJ2.add(pikachu[i]);
                }
            }
            if (st.toLowerCase().contains("observations")) {

                String ss = st;
                shouldWrite = false;
                st = br.readLine();
                st = this.avoidCharacter(st, writer, shouldWrite);

                String[] a = st.split(" ");
                String s = "";
                for (int i = 0; i < Integer.parseInt(a[0]); i++) {
                    s += "obs"+i+ " ";
                    observationsJ1.add("obs"+i);
                }

                writer.println(ss + " " +s);
                st = br.readLine();
                st = this.avoidCharacter(st, writer, shouldWrite);

                String[] pikachu = st.split(" ");
                s = "";
                for (int i = 0; i < Integer.parseInt(pikachu[0]); i++) {
                    s += "obs"+i+" ";
                    observationsJ2.add("obs"+i);
                }
                writer.println(s);
            }
            if(st.toLowerCase().contains("t:")){
                for (String salameche : actionsJ1){
                    for (String carapuce : actionsJ2){
                        if (st.contains("T: "+actionsJ1.indexOf(salameche)+" "+actionsJ2.indexOf(carapuce))) {

                            st = this.avoidCharacter(st,writer,shouldWrite);
                            st = st.replace("T: "+actionsJ1.indexOf(salameche)+" "+actionsJ2.indexOf(carapuce),"T: "+salameche+" "+carapuce);
                            writer.println(st);
                            shouldWrite = false;
                        }
                    }
                }
            }

            if(st.toLowerCase().contains("o:")){

                for (String sacha : actionsJ1){
                    for(String ondine : actionsJ2) {
                        if (st.toLowerCase().contains("o: " + actionsJ1.indexOf(sacha) + " " + actionsJ2.indexOf(ondine))){

                            for (String salameche : observationsJ1) {
                                for (String carapuce : observationsJ2) {
                                    if (st.contains(" : " + observationsJ1.indexOf(salameche) + " " + observationsJ2.indexOf(carapuce))) {
                                        st = st.replace("O: " + actionsJ1.indexOf(sacha) + " " + actionsJ2.indexOf(ondine),
                                                "O: " + sacha + " " +ondine);
                                        st = st.replace(" : " + observationsJ1.indexOf(salameche) + " " + observationsJ2.indexOf(carapuce),
                                                " : " + salameche + " " +carapuce);
                                        st = this.avoidCharacter(st, writer, shouldWrite);
                                        writer.println(st);
                                        shouldWrite = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (st.contains("R:")){
                for (String salameche : actionsJ1){
                    for (String carapuce : actionsJ2){
                        if (st.contains("R: "+actionsJ1.indexOf(salameche)+" "+actionsJ2.indexOf(carapuce))) {


                            st = st.replace("R: "+actionsJ1.indexOf(salameche)+" "+actionsJ2.indexOf(carapuce),"R: "+salameche+" "+carapuce);

                            String[] s = st.split(" : ");

                            String recompense = s[s.length-1];

                            String toWrite = "";
                            for (int i = 0;i<s.length-1;i++){
                                toWrite+=s[i]+" ";
                            }
                            toWrite+= "* * " + recompense;
                            toWrite = this.avoidCharacter(toWrite,writer,shouldWrite);
                            writer.println(toWrite);
                            shouldWrite = false;
                        }
                    }
                }
            }

            if (shouldWrite == true)
                writer.println(st);
        }
        writer.close();
    }
}
