package util;

import java.util.Random;
public class RandomGenerator {
    private static Random random = new Random(104);

    public static void regenerateRandomGenerator() {
        random = new Random(43);
	random.nextInt();
    }
    public static void regenerateRandomGenerator(long seed) {
        random = new Random(seed);
	random.nextInt();
    }
    public double getDouble() {
        return random.nextDouble();
    }

    public static int getInt() {
        return random.nextInt();
    }

    public static int getInt(int mini, int maxi) {
        return (random.nextInt(Integer.MAX_VALUE) % (maxi-mini)) + mini;
    }
}
