package util;

import Jama.Matrix;

public class MatrixX extends Matrix {
    public MatrixX(Matrix that) {
	super(that.getArray());
    }

    public MatrixX(double[][] A) {
	super(A);
    }

    public MatrixX(double[][] A, int m, int n) {
	super(A,m,n);
    }

    public MatrixX(double[] vals, int m) {
	super(vals, m);
    }

    public MatrixX(int m, int n) {
	super(m,n);
    }

    public MatrixX(int m, int n, double s) {
	super(m,n,s);
    }
    public boolean equals(Object o) {


        if (this == o) return true;
        if (!(o instanceof MatrixX)) return false;

        MatrixX that = (MatrixX) o;
	Matrix diff = this.minus(that);
	return diff.normInf() < 0.000001;
    }
    public boolean isGE(MatrixX that) {
	if (this == that) return true;
	checkMatrixDimensions(that);

	double[][] A = this.getArray();
	double[][] B = that.getArray();

	for (int i=0; i<this.getRowDimension(); i++) {
	    for (int j=0; j<this.getColumnDimension(); j++) {
		if ( A[i][j] < B[i][j] )
		    return false;
	    }
	}

        return true;
    }
    public boolean isLE(MatrixX that) {
	if (this == that) return true;
	checkMatrixDimensions(that);

	double[][] A = this.getArray();
	double[][] B = that.getArray();

	for (int i=0; i<this.getRowDimension(); i++) {
	    for (int j=0; j<this.getColumnDimension(); j++) {
		if ( A[i][j] > B[i][j] )
		    return false;
	    }
	}

        return true;
    }
    public MatrixX arrayAbsEquals() {
	double[][] A = this.getArray();

	for (int i=0; i<this.getRowDimension(); i++) {
	    for (int j=0; j<this.getColumnDimension(); j++) {
		A[i][j] = Math.abs(A[i][j]);
	    }
	}

	return this;
    }
    public MatrixX arrayAbs() {
	MatrixX M = (MatrixX)(this.copy());

	return M.arrayAbsEquals();
    }
    public MatrixX arrayMaxEquals(MatrixX that) {
	double[][] A = this.getArray();
	double[][] B = that.getArray();

	for (int i=0; i<this.getRowDimension(); i++) {
	    for (int j=0; j<this.getColumnDimension(); j++) {
		A[i][j] = Math.max(A[i][j], B[i][j]);
	    }
	}

	return this;
    }
    public MatrixX arrayMax(MatrixX that) {
	MatrixX M = (MatrixX)(this.copy());

	return M.arrayMaxEquals(that);
    }
    public MatrixX arrayMinEquals(MatrixX that) {
	double[][] A = this.getArray();
	double[][] B = that.getArray();

	for (int i=0; i<this.getRowDimension(); i++) {
	    for (int j=0; j<this.getColumnDimension(); j++) {
		A[i][j] = Math.min(A[i][j], B[i][j]);
	    }
	}

	return this;
    }
    public MatrixX arrayMin(MatrixX that) {
	MatrixX M = (MatrixX)(this.copy());

	return M.arrayMinEquals(that);
    }
    public double normInf(MatrixX that) {
	double max = 0.0;

	double[][] A = this.getArray();
	double[][] B = that.getArray();

	for (int i=0; i<this.getRowDimension(); i++) {
	    for (int j=0; j<this.getColumnDimension(); j++) {
		double tmp = Math.abs(A[i][j]- B[i][j]);
		if (tmp > max)
		    max = tmp;
	    }
	}

	return max;
    }
    public double norm1(MatrixX that) {
	double[][] A = this.getArray();
	double[][] B = that.getArray();

	double tmp=0.0;
	for (int i=0; i<this.getRowDimension(); i++) {
	    for (int j=0; j<this.getColumnDimension(); j++) {
		tmp += Math.abs(A[i][j]- B[i][j]);
	    }
	}

	return tmp;
    }
    public String toString() {


	String str = new String();
	double[][] A = this.getArray();

	str += "[";
	for(int i=0; i<A.length; i++) {
	    str += "[";
	    for(int j=0; j<A[0].length; j++) {
		if (j!=0) str += " ";
		str += String.format("%6.2e",A[i][j]);
	    }
	    str += "]";
	}
	str += "]";

	return str;
    }

   private void checkMatrixDimensions (MatrixX B) {
       if (B.getRowDimension() != this.getRowDimension()
	   ||
	   B.getColumnDimension() != this.getColumnDimension()) {
         throw new IllegalArgumentException("MatrixX dimensions must agree.");
      }
   }
}
