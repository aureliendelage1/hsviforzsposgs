package util;

import posg.Observation;
import posg.Posg;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;


public class Distribution<A> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Double sumOfWeights;

    public Distribution(Distribution<A> actionDistribution) {
        this.weights = new HashMap<>();
        for (A a : actionDistribution.getNonZeroElements()){
            this.addWeight(a,actionDistribution.getWeight(a));
        }
    }


    public void setWeights(Map<A, Double> weights) {
        this.weights = weights;
    }

    public Map<A, Double> getWeights() {
        return weights;
    }
    private Map<A, Double> weights;

    public Distribution() {
        weights = new HashMap<>();
        sumOfWeights = null;
    }

    public Distribution(A element) {
        weights = new HashMap<>();
            weights.put(element,1.0);
        sumOfWeights = null;
    }

    public Distribution(boolean random, Collection<A> set){
        weights = new HashMap<>();
        sumOfWeights = null;
        if (random){
            int i = 0;
            for (A a : set){
                this.setWeight(a,(float) (1.0/set.size())*i);
                i= 1;
            }
        }
        this.normalize();
    }

    public Distribution(Collection<A> set){
        weights = new HashMap<>();
        sumOfWeights = null;
            for (A a : set){
                this.addWeight(a,0.0);
            }
    }

    public void setWeight(A element, double weight) throws IllegalArgumentException{
        if(weight > 0.0) {
            weights.put(element, weight);
        } else if(weight == 0.0) {
            remove(element);
        }
        else if (weight> -0.002) {
        }
        else {
            throw new IllegalArgumentException("A weight is always non-negative");
        }
    }

    public void sanityCheck() throws Exception {
        this.computeSumOfWeights();
        if (this.sumOfWeights>1.02 || this.sumOfWeights <0.98){


        }
        if (this.sumOfWeights.isNaN()){
            System.out.println("NaN distribution");
            System.exit(1);
            throw new Exception("Not a valid distribution !!");
        }


    }


    public void remove(A element) {
        weights.remove(element);
        sumOfWeights = null;
    }

    public Distribution<A> normalize(double normalizationValue) {
        computeSumOfWeights();

        if(normalizationValue == sumOfWeights) {
            return this;
        }

        if(sumOfWeights == 0.0) {
            return this;
        }

        if(normalizationValue < 0) {
            throw new IllegalArgumentException("The normalization value should be non-negative");
        }

        double factor = normalizationValue/sumOfWeights;

        for(Map.Entry<A,Double> entry: weights.entrySet()) {
            entry.setValue(entry.getValue()*factor);
        }

        sumOfWeights = normalizationValue;
        return this;
    }

    public void normalize() {
        normalize(1.0);
    }

    public A sample() {
        RandomGenerator r = new RandomGenerator();
        computeSumOfWeights();
        double randDouble = r.getDouble();
        randDouble *= sumOfWeights;

        if(weights.size() == 0) {
            throw new UnsupportedOperationException("Cannot generate a value on a null distribution");
        }

        A temp = null;

        for(Map.Entry<A,Double> entry: weights.entrySet()) {
            temp = entry.getKey();
            if(randDouble < entry.getValue()) {
                return entry.getKey();
            }
            randDouble -= entry.getValue();
        }

        return temp;
    }

    public double getWeight(A element){

        for (A a : this.getNonZeroElements()){
            if (a.equals(element)){
                return this.weights.get(a);
            }
        }

        return 0.0;
    }
    public double getProbability(A element) {
        computeSumOfWeights();
        if (this.sumOfWeights>1.02 || this.sumOfWeights <0.98){
        }
        Double d = weights.get(element);
        if(d == null) {
            return 0.0;
        }
        return d;
    }

    public Set<A> getNonZeroElements() {
        return weights.keySet();
    }

    private void computeSumOfWeights() {

        if(sumOfWeights != null) {
            return;
        }
        sumOfWeights = 0.0;
        for(Map.Entry<A,Double> entry : weights.entrySet()) {
            sumOfWeights += entry.getValue();
        }
    }

    public double getSumOfWeights() {
	    computeSumOfWeights();
	    return sumOfWeights;
    }
    @Override
    public String toString() {

        NumberFormat formatter = new DecimalFormat("0.##########");
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for(A elem : getNonZeroElements()) {
            sb.append(elem.toString()).append("=");


	    sb.append(formatter.format(this.getWeight(elem)));
            sb.append(", ");
        }
        sb.append("}");
        return sb.toString();
    }

    public void addWeight(A a, double probability) {

        this.weights.put(a,probability);

    }

    public int size() {
        return this.weights.size();
    }

    @Override
    public int hashCode() {

        return this.weights.hashCode();
    }


    @Override
    public boolean equals(Object obj) {

        Distribution<A> distribPrime = new Distribution<>();
        try{
            distribPrime = (Distribution<A>) obj;
        }
        catch(Exception e){
            System.out.println("Distribution::Cannot cast obj : " + obj.toString() + " into Distribution<??>");
            return false;
        }
        return this.weights.equals(distribPrime.weights);
    }

    public Distribution<A> scalarMultiply(Double lambda) {
        Distribution<A> newDistrib = new Distribution<>();
        for (A a : weights.keySet()){

            newDistrib.setWeight(a,weights.get(a) * lambda);
        }
        return newDistrib;
    }

    public Distribution<A> addDistribution(Distribution<A> distributionToAdd) {
        Distribution<A> distrib = new Distribution<>();
        ArrayList<A> list = new ArrayList<>();
        for (A a : weights.keySet()){
            list.add(a);
            distrib.addWeight(a, this.getWeight(a) + distributionToAdd.getWeight(a));
        }
        for (A a : distributionToAdd.getNonZeroElements()){
            if (!(list.contains(a))){
                distrib.addWeight(a, distributionToAdd.getWeight(a));
            }
        }
        return distrib;
    }

    public void sanityCheckForBehavioralStrat() {
        this.computeSumOfWeights();
        if (this.sumOfWeights>1.02 || this.sumOfWeights <0.98){
            System.out.println("sum : "+this.sumOfWeights);
            System.out.println(this.toString());
            System.exit(1);


        }
        if (this.sumOfWeights.isNaN()){
            System.out.println("NaN distribution");
            System.exit(1);
        }


    }


}
