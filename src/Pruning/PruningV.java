package Pruning;

import Approximations.Vector;
import Approximations.VectorApproximation;

import java.util.ArrayList;

public class PruningV<State,Action,Observation> {
    boolean lossless;
    double epsilonLossLess;
    double lambda;

    public PruningV(){

    }

    public PruningV(boolean lossless, double epsilonLossLess, double lambda){
        this.lossless = lossless;
        this.epsilonLossLess = epsilonLossLess;
        this.lambda = lambda;
    }

    public void prune(VectorApproximation<State,Action,Observation> vectorApproximation, int timstep, int player) throws Exception {
        ArrayList<Vector<State,Action,Observation>> listToPrune = new ArrayList<>();
        if (vectorApproximation != null) {
            for (Vector<State,Action,Observation> v : vectorApproximation.vectorList.get(timstep)) {
                for (Vector<State,Action,Observation> vToCheck : vectorApproximation.vectorList.get(timstep)) {
                    if (this.checkDominance(v,vToCheck,player)){
                        listToPrune.add(v);
                        break;
                    }
                }
            }
        }
        for (Vector<State,Action,Observation> object: listToPrune){
            vectorApproximation.vectorList.get(timstep).remove(object);
        }
        if (listToPrune.size() == 0){
        }
    }

    private boolean checkDominance(Vector<State, Action, Observation> v, Vector<State, Action, Observation> vToCheck, int player) throws Exception {
        if (player%2==1) {
            if (v.getValue(player) + epsilonLossLess > vToCheck.getValue(player) +
                    this.lambda * v.s.getNorme1(vToCheck.s)) {
                return true;
            }
        }
        else{
            if (v.getValue(player) - epsilonLossLess < vToCheck.getValue(player) -
                    this.lambda * v.s.getNorme1(vToCheck.s)) {
                return true;
            }
        }
        return false;
    }


}
