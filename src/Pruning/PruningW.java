package Pruning;


import Approximations.ApproximatorW;
import Approximations.Vector;
import posg.*;

import java.util.ArrayList;
import java.util.HashMap;

public class PruningW<State,Action,Observation> {
    boolean lossless;
    double epsilonLossLess;
    public HashMap<Integer,Double> lambda;
    ApproximatorW<State,Action,Observation> W;
    private int timstep;
    Posg<State,Action,Observation> posg;

    public PruningW(){

    }

    public PruningW(boolean lossless, double epsilonLossLess, HashMap<Integer,Double> lambda, Posg<State,Action,Observation> posg){
        this.posg = posg;
        this.lossless = lossless;
        this.epsilonLossLess = epsilonLossLess;
        this.lambda = new HashMap<>() ;
        for (int t = 0; t<=posg.profondeurMax;t++){
            this.lambda.put(t,lambda.get(t));
        }


    }

    public void prune(ApproximatorW<State,Action,Observation> W, int timstep, int player) throws Exception {

        this.timstep = timstep;
        this.W = W;
        ArrayList<Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>> listToSupress = new ArrayList<>();

        for (Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                objet : W.bagW.get(timstep)){
            for (Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                    objetToCheck : W.bagW.get(timstep)){

                if (checkDominance(objet,objetToCheck,player) && !(objet.equals(objetToCheck))){

                    listToSupress.add(objet);
                    break;
                }
            }
        }
        System.out.println("size of bag at timestep " + timstep + " before pruning : "  + " : " + W.bagW.get(timstep).size());
        for (Object o: listToSupress){

            W.bagW.get(timstep).remove(o);
        }
        System.out.println("size of bag at timestep " + timstep + " after pruning : "  + " : " + W.bagW.get(timstep).size());
        return;
    }

    private boolean checkDominance(Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                                           triplet, Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                                                tripletToCheck, int player) throws Exception {

        if (this.timstep > 0) {

            for (History<Action, Observation> history : triplet.getElement0().o.getLocalHistory(player)) {
                for (Action a : triplet.getElement0().o.POSG.getActions(player)) {


                    for (State s : triplet.getElement0().o.POSG.getStates()){
                    for (History<Action,Observation> histNegI : triplet.getElement0().o.getLocalHistory(player+1%2)) {

                        if (!(tripletToCheck.getElement0().o.getLocalHistory(player).contains(history))) {

                            if (!(tripletToCheck.getElement0().o.getListCompressedHistories(player).containsKey(history))) {

                                return false;
                            }
                        } else {

                        }
                        if (player % 2 == 1) {
                            if (W.computeValueForPruning(history, a, triplet, histNegI, player) <=
                                    W.computeValueForPruning(history, a, tripletToCheck, histNegI, player)
                                            + this.lambda.get(timstep) * triplet.getElement0().getNorme1(tripletToCheck.getElement0())) {
                                return false;
                            }
                        } else {
                            if (W.computeValueForPruning(history, a, triplet, histNegI, player) >=
                                    W.computeValueForPruning(history, a, tripletToCheck, histNegI, player)
                                            - this.lambda.get(timstep) * triplet.getElement0().getNorme1(tripletToCheck.getElement0())) {
                                return false;
                            }
                        }
                    }

                    }
                }
            }


            return true;
        }
        else{
            History<Action,Observation> history = new History<>();
            for (Action a : triplet.getElement0().o.POSG.getActions(player)) {

                    if (player % 2 == 1) {
                        if (W.computeValueForPruning(history, a, triplet,new History<>(), player) <
                                W.computeValueForPruning(history, a, tripletToCheck,new History<>(), player)) {
                            return false;
                        }
                    } else {
                        if (W.computeValueForPruning(history, a, triplet,new History<>(), player) >
                                W.computeValueForPruning(history, a, tripletToCheck,new History<>(), player)) {
                            return false;
                        }
                    }

            }
        }
        return true;
    }

}
