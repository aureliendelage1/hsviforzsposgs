package Algorithm;


import ilog.concert.IloException;
import ilog.concert.IloLPMatrix;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import posg.*;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;


public class CplexSolverForPosgDirectV2<State,Action,Observation> {
    private static final long serialVersionUID = 1L;
    Posg<State,Action,Observation> posg;
    OccupancyState<State,Action,Observation> o ;
    public CplexSolverForPosgDirectV2(Posg<State,Action,Observation> posg, OccupancyState<State,Action,Observation> o ) {
        this.posg = posg;
        this.o = o;
    }
    public void printSolution(double[] solution, int size, int sizeHistoryP2) throws Exception {

        if (solution != null) {

            double max = 0.0;
            for (int i = 0;i<sizeHistoryP2;i++) {
                max += solution[size+i]*this.getProbaIndividualHistory(i);


            }

            for (int j = 0; j < solution.length; j++) {

            }

        }

    }

    public Pair<Distribution<Action>,Double> getActionMax(double[][] m) throws Exception {


        int nActionsMax = posg.getActionsJ1().size();
        int nActionsMin = posg.getActionsJ2().size();

        int sizeHistoryP1 = o.getLocalHistoryJ1().size();
        int sizeHistoryP2 = o.getLocalHistoryJ2().size();
        double mamax = Double.NEGATIVE_INFINITY;
        for(int j=0; j<nActionsMin*sizeHistoryP2; j++)
            for(int i=0; i<nActionsMax*sizeHistoryP1; i++)
                mamax = (m[i][j]>mamax) ? m[i][j] : mamax;

        for(int j=0; j<nActionsMin*sizeHistoryP2; j++)
            for(int i=0; i<nActionsMax*sizeHistoryP1; i++)
                m[i][j] = m[i][j];


        try {

            IloCplex cplexModel = new IloCplex();
            IloLPMatrix lp = cplexModel.addLPMatrix();

            double[] lb = new double[nActionsMax*sizeHistoryP1 + sizeHistoryP2];
            double[] ub = new double[nActionsMax*sizeHistoryP1 + sizeHistoryP2];
            String[] varname = new String[nActionsMax*sizeHistoryP1 + sizeHistoryP2];
            for(int i=0; i<nActionsMax*sizeHistoryP1; i++) {
                varname[i] = "x"+i;
                lb[i] = 0.0;
                ub[i] = 1.0;
            }
            for (int i = 0; i< sizeHistoryP2; i++){
                varname[nActionsMax*sizeHistoryP1+i] = "u"+i;
                lb[nActionsMax*sizeHistoryP1+i] = - Double.MAX_VALUE;
                ub[nActionsMax*sizeHistoryP1+i] = +Double.MAX_VALUE;
            }

            IloNumVar[] x = cplexModel.numVarArray(cplexModel.columnArray(lp, nActionsMax*sizeHistoryP1 + sizeHistoryP2), lb, ub, varname);


            double[] objvals = new double[nActionsMax*sizeHistoryP1 + sizeHistoryP2];
            for (int i = 0;i<sizeHistoryP2;i++){
                objvals[nActionsMax*sizeHistoryP1 + i] = this.getProbaIndividualHistory(i);
            }
            cplexModel.addMaximize(cplexModel.scalProd(x, objvals));

            int numberOfConstraints = nActionsMin*sizeHistoryP2;

            double[]   lhs = new double[numberOfConstraints];
            double[]   rhs = new double[numberOfConstraints];
            double[][] val = new double[numberOfConstraints][nActionsMax*sizeHistoryP1 +sizeHistoryP2];
            int[][] ind = new int[numberOfConstraints][nActionsMax*sizeHistoryP1 +sizeHistoryP2];


            for(int i=0; i<sizeHistoryP2; i++) {
                for (int j=0; j<nActionsMin;j++){

                    lhs[i*nActionsMin + j] = 0.0;
                    rhs[i*nActionsMin + j] = Double.MAX_VALUE;
                    for (int k=0; k<nActionsMax*sizeHistoryP1; k++){
                        val[i*nActionsMin+j][k] = m[k][i*nActionsMin + j]/this.getProbaIndividualHistory(i);
                        ind[i*nActionsMin+j][k] = k;
                    }
                    val[i*nActionsMin+j][nActionsMax*sizeHistoryP1 + i] = -1.0;
                    ind[i*nActionsMin+j][nActionsMax*sizeHistoryP1 + i] = nActionsMax*sizeHistoryP1 + i;
                }
            }
            lp.addRows(lhs, rhs, ind, val);

            lhs = new double[nActionsMax*sizeHistoryP1+sizeHistoryP2];
            rhs = new double[nActionsMax*sizeHistoryP1+sizeHistoryP2];
            val = new double[nActionsMax*sizeHistoryP1+sizeHistoryP2][1];
            ind = new int[nActionsMax*sizeHistoryP1+sizeHistoryP2][1];

            for(int i=0; i<(nActionsMax*sizeHistoryP1); i++) {
                lhs[i] = 0.0;
                rhs[i] = Double.MAX_VALUE;
                val[i][0] = 1;
                ind[i][0] = i;
            }
            int N = nActionsMax*sizeHistoryP1;
            for (int i = 0;i<sizeHistoryP2;i++){
                lhs[N+i] = -Double.MAX_VALUE;
                rhs[N+i] = Double.MAX_VALUE;
                val[N+i][0] = 1;
                ind[N+i][0] = N+i;
            }
            lp.addRows(lhs, rhs, ind, val);


            for (int j = 0; j < sizeHistoryP1;j++) {
                lhs = new double[1]; lhs[0] = 1.0;
                rhs = new double[1]; rhs[0] = 1.0;
                val = new double[1][nActionsMax*sizeHistoryP1];
                ind = new int[1][nActionsMax*sizeHistoryP1];
                for (int i = 0; i < nActionsMax; i++) {
                    val[0][j*nActionsMax + i] = 1;
                    ind[0][j*nActionsMax + i] = j*nActionsMax+i;
                }

                lp.addRows(lhs, rhs, ind, val);
            }

            cplexModel.setOut(null);
            System.out.print("CplexSolverForPosgDirectV2::getActionMax()") ;
            System.out.println(cplexModel.toString());
            if ( cplexModel.solve() ) {
                HashMap<History<Action,Observation>, Distribution<Action>> hashMap = new HashMap<>();
                double[] solution = cplexModel.getValues(lp);

                printSolution(solution, nActionsMax*sizeHistoryP1,sizeHistoryP2);

                for (int i = 0; i < o.getLocalHistoryJ1().size(); i++) {
                    Distribution<Action> distirb = new Distribution<>();
                    for (int j = 0; j < this.posg.getActionsJ1().size(); j++) {
                        Action aMax = (Action) this.posg.getActionsJ1().toArray()[j];
                        double tmp = solution[i*this.posg.getActionsJ1().size() + j];

                        if (tmp < 0) {
                            tmp = 0;
                            throw new Exception("A solution value shouldn't be negative");
                        }
                        distirb.addWeight(aMax,tmp);

                    }
                    hashMap.put((History<Action, Observation>) this.o.getLocalHistoryJ1().toArray()[i],distirb);
                }

                cplexModel.end();


                double max = 0.0;
                for (int i = 0;i<sizeHistoryP2;i++){
                     max+= this.getProbaIndividualHistory(i)*solution[nActionsMax*sizeHistoryP1+i];
                }
                return new Pair(hashMap, max);

            } else {
                System.out.println(cplexModel.getAborter());
                throw new Exception("no solution were found.");
            }


        }  catch (IloException e) {
            System.err.println("Concert exception '" + e + "' caught");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String printTableau(double[][] val) {
        String res = "";
        for (int i = 0;i<val.length;i++){
            for (int j = 0;j<val[i].length;j++){
                res += "\ntab[" + i + "][" + j +"]" + " = " + val[i][j];
            }
        }
        return res;
    }

    public HashMap<Integer, ArrayList<Integer>> makeIndexes(int sizeActionsP2, int sizeHistoryP2){
        HashMap<Integer, ArrayList<Integer>> hashMap = new HashMap<>();
        for (int j = 0; j < sizeActionsP2; j++) {
            ArrayList<Integer> list = new ArrayList<>();
            list.add(j);
            hashMap.put(hashMap.size()+1,list);
        }

        hashMap = this.recursiveFunctionMakeIndexes(new HashMap<>(hashMap),sizeActionsP2,sizeHistoryP2,1);
        return hashMap;
    }

    private HashMap<Integer, ArrayList<Integer>> recursiveFunctionMakeIndexes(HashMap<Integer, ArrayList<Integer>> hashmap, int sizeActionsP2, int sizeHistoryP2, int indice){
        if (indice != sizeHistoryP2) {

            int N = hashmap.size();
            int decalage = (int) hashmap.keySet().toArray()[0];

            for (int i = 0; i<N ; i++) {

                ArrayList<Integer> indexes = hashmap.get(i+decalage);

                for (int j = 0; j < sizeActionsP2; j++) {
                    ArrayList<Integer> newArrayList = new ArrayList<>(indexes);
                    newArrayList.add(j);

                    hashmap.put(hashmap.size()+1+decalage,newArrayList);

                }
            }
            for (int k = 0;k<N;k++){
                hashmap.remove(k+decalage);
            }
            hashmap = this.renameHashMapKeys(hashmap);
            hashmap = this.recursiveFunctionMakeIndexes(hashmap,sizeActionsP2,sizeHistoryP2,indice+1);
            return hashmap;
        }
        else{

            return hashmap;
        }
    }

    private HashMap<Integer, ArrayList<Integer>> renameHashMapKeys(HashMap<Integer, ArrayList<Integer>> hashmap) {

        int indice = 0;
        HashMap<Integer, ArrayList<Integer>> hashMapCopie = new HashMap<>();
        for (Integer i : hashmap.keySet()){
            hashMapCopie.put(indice,hashmap.get(i));
            indice++;
        }
        hashmap = new HashMap<>(hashMapCopie);

        return hashmap;
    }

    public double getProbaIndividualHistory(int i) throws Exception {
        double res = 0.0;
        History<Action,Observation> historyP2 = (History<Action, Observation>) this.o.getLocalHistoryJ2().toArray()[i];
        for (State s : this.posg.getStates()){
            for (History<Action,Observation> hP1 : this.o.getLocalHistoryJ1()){
                res += o.getDistribution().getWeight(new Pair<State, JointHistory<Action,Observation>>(s,new JointHistory<Action,Observation>(hP1,historyP2)));
            }
        }
        return res;
    }
}
