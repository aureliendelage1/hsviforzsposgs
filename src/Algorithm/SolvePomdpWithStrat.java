package Algorithm;

import PomdpSolver.Belief;
import posg.*;
import util.Distribution;
import util.RandomGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class SolvePomdpWithStrat<State,Action,Observation> {
    Posg<State,Action,Observation> posg;
    int player;
    public HashMap<Integer,BehavioralStrategy<State,Action,Observation>> strat;

    public SolvePomdpWithStrat(Posg<State,Action,Observation> posg, int player, HashMap<Integer,BehavioralStrategy<State,Action,Observation>> strat) throws Exception {
        this.posg = posg;
        this.player = player;
        this.strat = strat;
    }

    public double solve(Distribution<Pair<State,History<Action,Observation>>> distribution, int t) throws Exception {

        double res;
        if (player%2==0){
            res = Double.POSITIVE_INFINITY;
        }
        else{
            res = -Double.POSITIVE_INFINITY;
        }

        for (Action aPlayerI : this.posg.getActions(player)) {
            double valAPlayerI = 0.0;
            //if (t == 0) {
                //System.out.println("SolvePomdp: timestep : action : " + aPlayerI);
            //}
            double rewardValue = this.computeReward(distribution, aPlayerI, strat, t);
            double valSumNextHistory = 0.0;
            if (t < posg.profondeurMax - 1) {
                for (Observation observation : posg.getObservations(player)) {
                    double probaObservation = this.getConditionalProbabilityOfObservation(distribution, aPlayerI, observation,player,t);
                    if (probaObservation > 0.0) {

                        valSumNextHistory += probaObservation * this.solve(this.getNewDistrib(distribution,aPlayerI,observation, t), t + 1);

                    }
                }
            }
            valAPlayerI = (rewardValue + valSumNextHistory);
            if (player % 2 == 1) {

                if (valAPlayerI > res) {
                    res = valAPlayerI;
                }
            } else {
                if (valAPlayerI < res) {
                    res = valAPlayerI;
                }
            }
        }
        return res;
    }

    private double computeReward(Distribution<Pair<State, History<Action, Observation>>> distribution, Action aPlayerI, HashMap<Integer, BehavioralStrategy<State, Action, Observation>> strat, int timestep) {
        double res = 0.0;
        for (Pair<State, History<Action, Observation>> x : distribution.getNonZeroElements()) {
            if (this.strat.get(timestep).getStrategy().containsKey(x.getElement1())) {
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    res += distribution.getWeight(x)
                            * posg.rewards.getReward(x.getElement0(), new JointAction(aPlayerI, aNegI, player), posg)
                            * strat.get(timestep).getStrategy().get(x.getElement1()).getWeight(aNegI);
                }
            } else {


                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    res += distribution.getWeight(x)
                            * posg.rewards.getReward(x.getElement0(), new JointAction(aPlayerI, aNegI, player), posg)
                            * (float) (1.0 / this.posg.getActions(player + 1 % 2).size());
                }
            }
        }
        return res;
    }

    private Distribution<Pair<State, History<Action, Observation>>> getNewDistrib(Distribution<Pair<State, History<Action, Observation>>> distribution, Action action, Observation o, int timestep) throws Exception {
        double denominator = 0.0;
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()) {
            for (Action aNegI : this.posg.getActions(player+1%2)) {
                for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                    for (State nextx : this.posg.getStates()) {
                        History<Action, Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                        nextHistory.addActionObservation(aNegI, zNegI);
                        Pair<State, History<Action, Observation>> nextStateD = new Pair<>(nextx, nextHistory);
                        if (strat.get(timestep).getStrategy().containsKey(nextStateD.getElement1().getlastHistory())) {
                            denominator += distribution.getWeight(new Pair<>(x.getElement0(), x.getElement1()))
                                    * strat.get(timestep).getStrategy().get(nextStateD.getElement1().getlastHistory()).getWeight(nextStateD.getElement1().getLastAction())
                                    * this.posg.observationsProbabilities.getProba(new JointAction(action, nextStateD.getElement1().getLastAction(), player),
                                    nextStateD.getElement0(), new JointObservation(o, nextStateD.getElement1().getLastObservation(), player))
                                    * this.posg.transitions.getProbability(x.getElement0(), new JointAction(action, aNegI, player), nextStateD.getElement0());
                        }
                        else{
                            denominator += distribution.getWeight(new Pair<>(x.getElement0(), x.getElement1()))
                                    * (float) (1.0/this.posg.getActions(player+1%2).size())
                                    * this.posg.observationsProbabilities.getProba(new JointAction(action, nextStateD.getElement1().getLastAction(), player),
                                    nextStateD.getElement0(), new JointObservation(o, nextStateD.getElement1().getLastObservation(), player))
                                    * this.posg.transitions.getProbability(x.getElement0(), new JointAction(action, aNegI, player), nextStateD.getElement0());
                        }
                    }
                }
            }
        }
        Distribution<Pair<State, History<Action, Observation>>> distrib = new Distribution<>();
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()) {
            for (Action aNegI : this.posg.getActions(player+1%2)) {
                for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                    for (State nextState : this.posg.getStates()) {
                        History<Action,Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                        nextHistory.addActionObservation(aNegI,zNegI);

                        distrib.addWeight(new Pair<>(nextState,nextHistory), this.computeProba(distribution, action, o, new Pair<>(nextState,nextHistory), timestep, denominator));
                    }
                }
            }
        }

        distrib.sanityCheck();
        return distrib;
    }


    private double computeProba(Distribution<Pair<State,History<Action,Observation>>> distribution, Action actionPlayerI, Observation zPlayerI, Pair<State,History<Action,Observation>> nextState, int timestep, double denominator) throws Exception {
        double numerator = 0.0;
        for (State s : this.posg.getStates()){
            if (strat.get(timestep).getStrategy().containsKey(nextState.getElement1().getlastHistory())) {
                numerator += distribution.getWeight(new Pair<>(s, nextState.getElement1().getlastHistory()))
                        * strat.get(timestep).getStrategy().get(nextState.getElement1().getlastHistory()).getWeight(nextState.getElement1().getLastAction())
                        * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI, nextState.getElement1().getLastAction(), player),
                        nextState.getElement0(), new JointObservation(zPlayerI, nextState.getElement1().getLastObservation(), player))
                        * this.posg.transitions.getProbability(s, new JointAction(actionPlayerI, nextState.getElement1().getLastAction(), player), nextState.getElement0());
            }
            else{
                numerator += distribution.getWeight(new Pair<>(s, nextState.getElement1().getlastHistory()))
                        * (float) (1.0/this.posg.getActions(player+1%2).size())
                        * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI, nextState.getElement1().getLastAction(), player),
                        nextState.getElement0(), new JointObservation(zPlayerI, nextState.getElement1().getLastObservation(), player))
                        * this.posg.transitions.getProbability(s, new JointAction(actionPlayerI, nextState.getElement1().getLastAction(), player), nextState.getElement0());
            }
        }
        if (denominator>0.0) {
            return numerator / denominator;
        }
        else{
            return 0.0;
        }
    }

    public double getConditionalProbabilityOfObservation(Distribution<Pair<State, History<Action, Observation>>> distribution, Action maxiAction, Observation observation, int player, int timestep) {

        double res = 0.0;
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()){
            for (State nextState : this.posg.getStates()) {
                    for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                        for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                            History<Action, Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                            nextHistory.addActionObservation(aNegI, zNegI);
                            if (strat.get(timestep).getStrategy().containsKey(x.getElement1())) {
                                res += distribution.getWeight(x)
                                        * strat.get(timestep).getStrategy().get(x.getElement1()).getWeight(aNegI)
                                        * this.posg.observationsProbabilities.getProba(new JointAction(maxiAction, aNegI, player),
                                        nextState, new JointObservation(observation, zNegI, player))
                                        * this.posg.transitions.getProbability(x.getElement0(), new JointAction(maxiAction, aNegI, player), nextState);
                            }
                            else{
                                res += distribution.getWeight(x)
                                        * (float) (1.0/this.posg.getActions(player+1%2).size())
                                        * this.posg.observationsProbabilities.getProba(new JointAction(maxiAction, aNegI, player),
                                        nextState, new JointObservation(observation, zNegI, player))
                                        * this.posg.transitions.getProbability(x.getElement0(), new JointAction(maxiAction, aNegI, player), nextState);
                            }
                        }
                    }
                }
            }
        return res;
    }

    public double getObservationProbability(Action action, State endState, Observation observation, History<Action,Observation> hNegI, int player, int t) {
        double res = 0.0;
        if (this.strat.get(t).getStrategy().containsKey(hNegI)){
            for (Action aNegI : posg.getActions(player+1%2)){
                for (Observation zNegI : posg.getObservations(player+1%2)){
                    res += posg.observationsProbabilities.getProba(new JointAction(action,aNegI,player),endState,new JointObservation(observation,zNegI,player))
                            * strat.get(t).getStrategy().get(hNegI).getProbability(aNegI);
                }
            }
            return res;
        }
        for (Action aNegI : posg.getActions(player+1%2)){
            for (Observation zNegI : posg.getObservations(player+1%2)){
                res += posg.observationsProbabilities.getProba(new JointAction(action,aNegI,player),endState,new JointObservation(observation,zNegI,player))
                        * (float)(1.0/posg.getActions(player+1%2).size());
            }
        }
        return res;
    }

    public double getTransitionProbability(State startState, Action action, State endState, History<Action,Observation> hNegI, int player, int t) {
        double res = 0.0;
        if (this.strat.get(t).getStrategy().containsKey(hNegI)) {
            for (Action aNegI : posg.getActions(player+1%2)){
                res += posg.transitions.getProbability(startState,new JointAction(action,aNegI,player),endState)
                        * strat.get(t).getStrategy().get(hNegI).getProbability(aNegI);
            }
            return res;
        }

        for (Action aNegI : posg.getActions(player+1%2)){
            res += posg.transitions.getProbability(startState,new JointAction(action,aNegI,player),endState)
                    * (float)(1.0/posg.getActions(player+1%2).size());
        }
        return res;
    }
}
