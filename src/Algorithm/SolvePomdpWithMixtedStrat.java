package Algorithm;

import PomdpSolver.Belief;
import posg.*;
import util.Distribution;
import util.RandomGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class SolvePomdpWithMixtedStrat<State,Action,Observation> {
    Posg<State,Action,Observation> posg;
    int player;
    HashMap<DecisionRule<State,Action,Observation>,Double> strat;

    public SolvePomdpWithMixtedStrat(Posg<State,Action,Observation> posg, int player, HashMap<DecisionRule<State,Action,Observation>,Double> strat) throws Exception {
        this.posg = posg;
        this.player = player;
        this.strat = strat;
        System.out.println("strat : " + strat);
    }

    public double solve(Distribution<Pair<State,History<Action,Observation>>> distribution, int t) throws Exception {

        double res;
        if (player%2==0){
            res = Double.POSITIVE_INFINITY;
        }
        else{
            res = -Double.POSITIVE_INFINITY;
        }

        for (Action aPlayerI : this.posg.getActions(player)) {
            double valAPlayerI = 0.0;
            if (t == 0) {

            }
            double rewardValue = this.computeReward(distribution, aPlayerI, strat, t);
            double valSumNextHistory = 0.0;
            if (t < posg.profondeurMax - 1) {
                for (Observation observation : posg.getObservations(player)) {
                    double probaObservation = this.getConditionalProbabilityOfObservation(distribution, aPlayerI, observation,player,t);
                    if (probaObservation > 0.0) {

                        valSumNextHistory += probaObservation * this.solve(this.getNewDistrib(distribution,aPlayerI,observation, t), t + 1);

                    }
                }
            }
            valAPlayerI = (rewardValue + valSumNextHistory);
            if (player % 2 == 1) {

                if (valAPlayerI > res) {
                    res = valAPlayerI;
                }
            } else {
                if (valAPlayerI < res) {
                    res = valAPlayerI;
                }
            }
        }
        return res;
    }

    private double computeReward(Distribution<Pair<State, History<Action, Observation>>> distribution, Action aPlayerI, HashMap<DecisionRule<State,Action,Observation>,Double> strat, int timestep) {
        double res = 0.0;
        for (Pair<State, History<Action, Observation>> x : distribution.getNonZeroElements()) {
            if (this.computeProbaMixtedStrat(strat,x.getElement1())>0.0) {
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    res += distribution.getWeight(x)
                            * posg.rewards.getReward(x.getElement0(), new JointAction(aPlayerI, aNegI, player), posg)
                            * this.computeProbaMixtedStrat(strat,x.getElement1(),aNegI);

                }
            } else {


                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    res += distribution.getWeight(x)
                            * posg.rewards.getReward(x.getElement0(), new JointAction(aPlayerI, aNegI, player), posg)
                            * (float) (1.0 / this.posg.getActions(player + 1 % 2).size());
                }
            }
        }
        return res;
    }

    private double computeProbaMixtedStrat(HashMap<DecisionRule<State,Action,Observation>, Double> strat, History<Action,Observation> element1) {
        double res = 0.0;
        for (Action a : this.posg.getActions(player+1%2)){
            res += this.computeProbaMixtedStrat(strat,element1,a);
        }
        return res;
    }

    private double computeProbaMixtedStrat(HashMap<DecisionRule<State,Action,Observation>, Double> strat, History<Action,Observation> element1, Action aNegI) {
        double res = 0.0;

        double denominator = 0.0;
        double numerator = 0.0;

        HashMap<Pair<History<Action,Observation>,Action>, Observation> mapAlreadyComputed = new HashMap<>();
        for (DecisionRule<State,Action,Observation> decisionRule : strat.keySet()){

            for (History<Action,Observation> h : decisionRule.decisionRule.keySet()){
                if (h.equals(element1) && (
                        !(mapAlreadyComputed.containsKey(new Pair<>(h,decisionRule.decisionRule.get(h))))
                || (mapAlreadyComputed.get(new Pair<>(h, decisionRule.decisionRule.get(h))).equals(decisionRule.getNextObs(h, decisionRule.decisionRule.get(h)))) ) )
                {

                        denominator += strat.get(decisionRule);
                        if (decisionRule.decisionRule.get(h).equals(aNegI)) {
                            numerator += strat.get(decisionRule);
                        }
                        mapAlreadyComputed.put(new Pair<>(h, decisionRule.decisionRule.get(h)), decisionRule.getNextObs(h, decisionRule.decisionRule.get(h)));
                    }
                }
        }
        if (denominator==0.0){
            return 0.0;
        }
        else{


            return numerator/denominator;
        }
    }

    private Distribution<Pair<State, History<Action, Observation>>> getNewDistrib(Distribution<Pair<State, History<Action, Observation>>> distribution, Action action, Observation o, int timestep) throws Exception {

        Distribution<Pair<State, History<Action, Observation>>> distrib = new Distribution<>();
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()) {
            for (Action aNegI : this.posg.getActions(player+1%2)) {
                for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                    for (State nextState : this.posg.getStates()) {
                        History<Action,Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                        nextHistory.addActionObservation(aNegI,zNegI);
                        distrib.addWeight(new Pair<>(nextState,nextHistory), this.computeProba(distribution, action, o, new Pair<>(nextState,nextHistory), timestep));
                    }
                }
            }
        }

        distrib.sanityCheck();
        return distrib;
    }


    private double computeProba(Distribution<Pair<State,History<Action,Observation>>> distribution, Action actionPlayerI, Observation zPlayerI, Pair<State,History<Action,Observation>> nextState, int timestep) throws Exception {
        double numerator = 0.0;
        for (State s : this.posg.getStates()){
            numerator += distribution.getWeight(new Pair<>(s,nextState.getElement1().getlastHistory()))

                    * this.computeProbaMixtedStrat(strat,nextState.getElement1().getlastHistory(),nextState.getElement1().getLastAction())
                    * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI,nextState.getElement1().getLastAction(),player),
                    nextState.getElement0(),new JointObservation(zPlayerI,nextState.getElement1().getLastObservation(),player))
                    * this.posg.transitions.getProbability(s,new JointAction(actionPlayerI,nextState.getElement1().getLastAction(),player),nextState.getElement0());
        }

        double denominator = 0.0;
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()) {
            for (Action aNegI : this.posg.getActions(player+1%2)) {
                for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                    for (State nextx : this.posg.getStates()) {
                        History<Action, Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                        nextHistory.addActionObservation(aNegI, zNegI);
                        Pair<State, History<Action, Observation>> nextStateD = new Pair<>(nextx, nextHistory);
                        denominator += distribution.getWeight(new Pair<>(x.getElement0(), x.getElement1()))

                                * this.computeProbaMixtedStrat(strat,nextStateD.getElement1().getlastHistory(),nextStateD.getElement1().getLastAction())
                                * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI, nextStateD.getElement1().getLastAction(), player),
                                nextStateD.getElement0(), new JointObservation(zPlayerI, nextStateD.getElement1().getLastObservation(), player))
                                * this.posg.transitions.getProbability(x.getElement0(), new JointAction(actionPlayerI, aNegI, player), nextStateD.getElement0());
                    }
                }
            }
        }

        if (denominator>0.0) {
            return numerator / denominator;
        }
        else{
            return 0.0;
        }
    }

    public double getConditionalProbabilityOfObservation(Distribution<Pair<State, History<Action, Observation>>> distribution, Action maxiAction, Observation observation, int player, int timestep) {

        double res = 0.0;
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()){
            for (State nextState : this.posg.getStates()) {
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                        History<Action, Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                        nextHistory.addActionObservation(aNegI, zNegI);
                        res += distribution.getWeight(x)

                                * this.computeProbaMixtedStrat(strat,x.getElement1(),aNegI)
                                * this.posg.observationsProbabilities.getProba(new JointAction(maxiAction,aNegI,player),
                                nextState,new JointObservation(observation,zNegI,player))
                                * this.posg.transitions.getProbability(x.getElement0(),new JointAction(maxiAction,aNegI,player),nextState);
                    }
                }
            }
        }
        return res;
    }

    public double getObservationProbability(Action action, State endState, Observation observation, History<Action,Observation> hNegI, int player, int t) {
        double res = 0.0;
        if (this.computeProbaMixtedStrat(strat,hNegI)>0.0){
            for (Action aNegI : posg.getActions(player+1%2)){
                for (Observation zNegI : posg.getObservations(player+1%2)){
                    res += posg.observationsProbabilities.getProba(new JointAction(action,aNegI,player),endState,new JointObservation(observation,zNegI,player))
                            * this.computeProbaMixtedStrat(strat,hNegI,aNegI);

                }
            }
            return res;
        }


        for (Action aNegI : posg.getActions(player+1%2)){
            for (Observation zNegI : posg.getObservations(player+1%2)){
                res += posg.observationsProbabilities.getProba(new JointAction(action,aNegI,player),endState,new JointObservation(observation,zNegI,player))
                        * (float)(1.0/posg.getActions(player+1%2).size());
            }
        }
        return res;
    }

    public double getTransitionProbability(State startState, Action action, State endState, History<Action,Observation> hNegI, int player, int t) {
        double res = 0.0;
        if (this.computeProbaMixtedStrat(strat,hNegI)>0.0) {
            for (Action aNegI : posg.getActions(player+1%2)){
                res += posg.transitions.getProbability(startState,new JointAction(action,aNegI,player),endState)
                        * this.computeProbaMixtedStrat(strat,hNegI,aNegI);

            }
            return res;
        }


        for (Action aNegI : posg.getActions(player+1%2)){
            res += posg.transitions.getProbability(startState,new JointAction(action,aNegI,player),endState)
                    * (float)(1.0/posg.getActions(player+1%2).size());
        }
        return res;
    }
}
