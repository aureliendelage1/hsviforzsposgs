package Algorithm;

import PomdpSolver.Belief;
import posg.*;
import util.Distribution;
import util.RandomGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class SolvePomdpWithMixtedStratStore<State,Action,Observation> {
    Posg<State,Action,Observation> posg;
    int player;
    HashMap<DecisionRule<State,Action,Observation>, Double> mixtedStrat;
    DecisionRule<State,Action,Observation> currentDecisionRule;

    public SolvePomdpWithMixtedStratStore(Posg<State,Action,Observation> posg, int player, HashMap<DecisionRule<State,Action,Observation>, Double> mixtedStrat) throws Exception {
        this.posg = posg;
        this.player = player;

        this.mixtedStrat = mixtedStrat;
    }

    private HashMap<DecisionRule<State,Action,Observation>, Double> computeMixtedStrat(HashMap<Pair<History<Action,Observation>, History<Action,Observation>>, Double> strat) {
        HashMap<DecisionRule<State,Action,Observation>, Double> res = new HashMap<>();
        for (Pair<History<Action,Observation>,History<Action,Observation>> p : strat.keySet()){
            if (p.getElement0().getListeActionObservation().size()==0){
                DecisionRule<State,Action,Observation> decisionRule = new DecisionRule<>(p.getElement1());
                res.put(decisionRule,strat.get(p));
            }
        }
        return res;
    }

    public double solve(Distribution<Pair<State,History<Action,Observation>>> distribution, int t) throws Exception {

        double sumRes = 0.0;
            System.out.println("current decsision rule : " + currentDecisionRule);
            double res;
            if (player % 2 == 0) {
                res = Double.POSITIVE_INFINITY;
            } else {
                res = -Double.POSITIVE_INFINITY;
            }

            for (Action aPlayerI : this.posg.getActions(player)) {
                double valAPlayerI = 0.0;
                if (t == 0) {

                }
                double rewardValue = this.computeReward(distribution, aPlayerI, mixtedStrat, t);
                double valSumNextHistory = 0.0;
                if (t < posg.profondeurMax - 1) {
                    for (Observation observation : posg.getObservations(player)) {
                        double probaObservation = this.getConditionalProbabilityOfObservation(distribution, aPlayerI, observation, player, t);
                        if (probaObservation > 0.0) {

                            valSumNextHistory += probaObservation * this.solve(this.getNewDistrib(distribution, aPlayerI, observation, t), t + 1);

                        }
                    }
                }
                valAPlayerI = (rewardValue + valSumNextHistory);
                if (player % 2 == 1) {

                    if (valAPlayerI > res) {
                        res = valAPlayerI;
                    }
                } else {
                    if (valAPlayerI < res) {
                        res = valAPlayerI;
                    }
                }
            }
            return res;
    }

    private double computeReward(Distribution<Pair<State, History<Action, Observation>>> distribution, Action aPlayerI, HashMap<DecisionRule<State, Action, Observation>, Double> strat, int timestep) {
        double res = 0.0;
        for (Pair<State, History<Action, Observation>> x : distribution.getNonZeroElements()) {
            if (this.currentDecisionRule.decisionRule.containsKey(x.getElement1())) {
                    res += distribution.getWeight(x)
                            * posg.rewards.getReward(x.getElement0(), new JointAction(aPlayerI, currentDecisionRule.decisionRule.get(x.getElement1()), player), posg);

            } else {
                System.out.println("this shouldn't happen");
                System.exit(1);
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    res += distribution.getWeight(x)
                            * posg.rewards.getReward(x.getElement0(), new JointAction(aPlayerI, aNegI, player), posg)
                            * (float) (1.0 / this.posg.getActions(player + 1 % 2).size());
                }
            }
        }
        return res;
    }

    private Distribution<Pair<State, History<Action, Observation>>> getNewDistrib(Distribution<Pair<State, History<Action, Observation>>> distribution, Action action, Observation o, int timestep) throws Exception {

        Distribution<Pair<State, History<Action, Observation>>> distrib = new Distribution<>();
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()) {
                for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                    for (State nextState : this.posg.getStates()) {
                        History<Action,Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                        nextHistory.addActionObservation(this.currentDecisionRule.decisionRule.get(x.getElement1()),zNegI);
                        distrib.addWeight(new Pair<>(nextState,nextHistory), this.computeProba(distribution, action, o, new Pair<>(nextState,nextHistory), timestep));
                    }
                }
        }

        distrib.sanityCheck();
        return distrib;
    }


    private double computeProba(Distribution<Pair<State,History<Action,Observation>>> distribution, Action actionPlayerI, Observation zPlayerI, Pair<State,History<Action,Observation>> nextState, int timestep) throws Exception {
        double numerator = 0.0;
        for (State s : this.posg.getStates()){
            numerator += distribution.getWeight(new Pair<>(s,nextState.getElement1().getlastHistory()))
                    * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI,nextState.getElement1().getLastAction(),player),
                    nextState.getElement0(),new JointObservation(zPlayerI,nextState.getElement1().getLastObservation(),player))
                    * this.posg.transitions.getProbability(s,new JointAction(actionPlayerI,nextState.getElement1().getLastAction(),player),nextState.getElement0());
        }

        double denominator = 0.0;
        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()) {
            for (Action aNegI : this.posg.getActions(player+1%2)) {
                for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                    for (State nextx : this.posg.getStates()) {
                        History<Action, Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));
                        nextHistory.addActionObservation(aNegI, zNegI);
                        Pair<State, History<Action, Observation>> nextStateD = new Pair<>(nextx, nextHistory);
                        denominator += distribution.getWeight(new Pair<>(x.getElement0(), x.getElement1()))
                                * this.posg.observationsProbabilities.getProba(new JointAction(actionPlayerI, nextStateD.getElement1().getLastAction(), player),
                                nextStateD.getElement0(), new JointObservation(zPlayerI, nextStateD.getElement1().getLastObservation(), player))
                                * this.posg.transitions.getProbability(x.getElement0(), new JointAction(actionPlayerI, aNegI, player), nextStateD.getElement0());
                    }
                }
            }
        }

        if (denominator>0.0) {
            return numerator / denominator;
        }
        else{
            return 0.0;
        }
    }

    public double getConditionalProbabilityOfObservation(Distribution<Pair<State, History<Action, Observation>>> distribution, Action maxiAction, Observation observation, int player, int timestep) {

        double res = 0.0;

        for (Pair<State,History<Action,Observation>> x : distribution.getNonZeroElements()){
            for (State nextState : this.posg.getStates()) {
                    for (Observation zNegI : this.posg.getObservations(player + 1 % 2)) {
                        History<Action, Observation> nextHistory = new History<>(new ArrayList<>(x.getElement1().getListeActionObservation()));

                        Action aNegI = this.currentDecisionRule.decisionRule.get(x.getElement1());

                        nextHistory.addActionObservation(aNegI, zNegI);
                        res += distribution.getWeight(x)
                                * this.posg.observationsProbabilities.getProba(new JointAction(maxiAction,aNegI,player),
                                nextState,new JointObservation(observation,zNegI,player))
                                * this.posg.transitions.getProbability(x.getElement0(),new JointAction(maxiAction,aNegI,player),nextState);
                    }
                }
        }
        return res;
    }

    public double getObservationProbability(Action action, State endState, Observation observation, History<Action,Observation> hNegI, int player, int t) {
        double res = 0.0;
        if (this.currentDecisionRule.decisionRule.containsKey(hNegI)){
            Action aNegI = this.currentDecisionRule.decisionRule.get(hNegI);
                for (Observation zNegI : posg.getObservations(player+1%2)){
                    res += posg.observationsProbabilities.getProba(new JointAction(action,aNegI,player),endState,new JointObservation(observation,zNegI,player));
                }

            return res;
        }
        System.out.println("this shouldn't happen");
        System.exit(1);
        for (Action aNegI : posg.getActions(player+1%2)){
            for (Observation zNegI : posg.getObservations(player+1%2)){
                res += posg.observationsProbabilities.getProba(new JointAction(action,aNegI,player),endState,new JointObservation(observation,zNegI,player))
                        * (float)(1.0/posg.getActions(player+1%2).size());
            }
        }
        return res;
    }

    public double solveMixted(Distribution<Pair<State, History<Action, Observation>>> d, int i) throws Exception {
        System.out.println("mixted strat : " + mixtedStrat);
        double res = 0.0;
        for (DecisionRule<State, Action, Observation> decisionRule : mixtedStrat.keySet()) {
            this.currentDecisionRule = decisionRule;
            res += mixtedStrat.get(decisionRule) * this.solve(d,0);
        }
        return res;
    }
}
