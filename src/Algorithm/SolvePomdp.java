package Algorithm;

import posg.*;
import util.Distribution;
import util.RandomGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class SolvePomdp<State,Action,Observation> {
    Posg<State,Action,Observation> posg;
    int player;
    Distribution<Action> strat;

    public SolvePomdp(Posg<State,Action,Observation> posg, int player) throws Exception {
        this.posg = posg;
        this.player = player;
    }

    public double solve(Distribution<State> distribution, int t, Distribution<Action> strat) throws Exception {
        this.strat = strat;
        double res;
        if (player%2==0){
            res = Double.POSITIVE_INFINITY;
        }
        else{
            res = -Double.POSITIVE_INFINITY;
        }

        for (Action aPlayerI : this.posg.getActions(player)) {
            double valAPlayerI = 0.0;
            if (t == 0) {
                System.out.println("SolvePomdp: action : " + aPlayerI);
            }
            double rewardValue = this.computeReward(distribution, aPlayerI, strat);
            double valSumNextHistory = 0.0;
            if (t < posg.profondeurMax - 1) {
                for (Observation observation : posg.getObservations(player)) {
                    double probaObservation = this.getConditionalProbabilityOfObservation(distribution, aPlayerI, observation, player);
                    if (probaObservation > 0.0) {
                        valSumNextHistory += probaObservation * this.solve(this.getNewDistrib(distribution, aPlayerI, observation), t + 1, strat);
                    }
                }
            }
            valAPlayerI = (rewardValue + valSumNextHistory);
            if (player % 2 == 1) {

                if (valAPlayerI > res) {
                    res = valAPlayerI;
                }
            } else {
                if (valAPlayerI < res) {
                    res = valAPlayerI;
                }
            }
        }
        return res;
    }

    private double computeReward(Distribution<State> distribution, Action aPlayerI, Distribution<Action> strat) {
        double res = 0.0;
        for (State state : this.posg.getStates()) {
            for (Action aNegI : this.posg.getActions(player + 1 % 2)) {


                System.out.println("strat : " + strat);
                res += distribution.getWeight(state)
                        *posg.rewards.getReward(state, new JointAction(aPlayerI, aNegI, player), posg)
                        * strat.getWeight(aNegI);
            }
        }
        return res;
    }

    private Distribution<State> getNewDistrib(Distribution<State> distribution,Action action, Observation o) throws Exception {

        Distribution<State> distrib = new Distribution<>();
        for (State s : this.posg.getStates()) {
            distrib.addWeight(s, this.computeProba(distribution,action,o,s));
        }
        distrib.sanityCheck();
        return distrib;
    }

    private double computeProba(Distribution<State> distribution, Action actionPlayerI, Observation zPlayerI, State nextState) throws Exception {
        double numerator = 0.0;
        for (State s : this.posg.getStates()) {
            for (Action aNegI : this.posg.getActions(player+1%2)) {


                numerator += distribution.getWeight(s)
                                * this.strat.getWeight(aNegI)
                                * this.posg.getObsProbaPomdp(new JointAction(actionPlayerI,aNegI,player),nextState,zPlayerI,player)
                                * this.posg.transitions.getProbability(s,new JointAction(actionPlayerI,aNegI,player),nextState);
            }
        }

        double denominator = 0.0;
        for (Action aNegI : this.posg.getActions(player+1%2)) {
            for (State sprime : this.posg.getStates()) {
                for (State startState : this.posg.getStates()) {


                    denominator += distribution.getProbability(startState)
                                    * this.strat.getWeight(aNegI)
                                    * this.posg.getObsProbaPomdp(new JointAction(actionPlayerI,aNegI,player),sprime,zPlayerI,player)
                                    * this.posg.transitions.getProbability(startState,new JointAction(actionPlayerI,aNegI,player),sprime);
                }
            }
        }
        if (denominator>0.0) {
            return numerator / denominator;
        }
        else{
            return 0.0;
        }
    }

    public double getConditionalProbabilityOfObservation(Distribution<State> distribution, Action maxiAction, Observation observation, int player) {

        double res = 0.0;

        for (State s : distribution.getNonZeroElements()){
            for (State nextState : this.posg.getStates()) {
                res += distribution.getWeight(s)
                        * posg.getObservationProbability(maxiAction,nextState,observation,player)
                        * posg.getTransitionProbability(s,maxiAction,nextState,player);
            }
        }

        return res;
    }
}
