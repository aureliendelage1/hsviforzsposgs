package Algorithm;

import Approximations.ApproximatorPB;
import Approximations.MajorantPointBased;
import Approximations.MinorantPointBased;
import posg.History;
import posg.*;
import util.Distribution;
import util.MatrixX;
import util.RandomGenerator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.ToDoubleBiFunction;

public class DOObackup<State,Action,Observation> {
    private Posg<State,Action,Observation> POSG;
    public double finalValue = 0.0;
    public HashMap<History<Action,Observation>,Distribution<Action>> strategyP1 = new HashMap<>();
    public HashMap<History<Action,Observation>,Distribution<Action>> strategyP2 = new HashMap<>();
    private ApproximatorPB<State,Action,Observation> approximatorPB;
    private double epsilon = 0.2;
    public DOObackup(double epsilon){
        this.epsilon = epsilon;
    }

    public double f(ArrayList<Double> x,ArrayList<Double> y) throws Exception {
        if (x.size() != y.size()){
            throw new Exception("x et y n'ont pas la même dimension!");
        }
        double S = 0;
        for (int i = 0; i<x.size();i++){
            S+= x.get(i) + y.get(i);
        }
        return S;
    }

    public double fonctionRecompenseBis(HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayer1,
                                        HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayer2,
                                        OccupancyState<State,Action,Observation> o) throws Exception {
        double value = 0.0;
        value = POSG.computeReward(distributionStrategyPlayer1, distributionStrategyPlayer2, o);

        return value;
    }

    public double fonctionRecompense(HashMap<History<Action,Observation>,ArrayList<Double>> x, HashMap<History<Action,Observation>,ArrayList<Double>> y,
                                     OccupancyState<State,Action,Observation> o, boolean show) throws Exception {


        double value = 0.0;
        HashMap<History<Action, Observation>, Distribution<Action>> strategyPlayer1 = this.getDistributionFromArrayList(x, 0);
        HashMap<History<Action, Observation>, Distribution<Action>> strategyPlayer2 = this.getDistributionFromArrayList(y, 1);
        value = POSG.computeReward(strategyPlayer1, strategyPlayer2, o);


        OccupancyState<State, Action, Observation> oNext = o.copy();
        if (o.getTimestep() == 0) {
            oNext.initialupdate(strategyPlayer1, strategyPlayer2);
        } else {
            oNext.update(strategyPlayer1, strategyPlayer2);
        }

        if (show){


        }

        value += this.approximatorPB.getValue(oNext,oNext.getTimestep());

        return value;
    }

    public double f(HashMap<History<Action,Observation>,ArrayList<Double>> x, HashMap<History<Action,Observation>,ArrayList<Double>> y,
                    OccupancyState o, boolean show) throws Exception {
        return -fonctionRecompense(x,y,o, show);
    }

    public double fExt(HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> x,
                       HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> subsInt,
                       int dim, int partionnement, double Lambda, double epsilonInt,
                       OccupancyState<State,Action,Observation> o, boolean show) throws Exception {

        return -DOOinterne(getValidProbability(x),subsInt,dim,partionnement,Lambda,epsilonInt,o,show);

    }

    public static ArrayList<Double> linspace(double min, double max, int points) {
        ArrayList<Double> d = new ArrayList<Double>();
        for (int i = 0; i < points; i++){
            d.add(i,min + i * (max - min) / (points - 1));
        }
        return d;
    }

    public boolean IsInSimplexeDimensionNM1(ArrayList<ArrayList<Double>> subdivision){
        double S = 0.0;
        for (ArrayList<Double> subsInDimensions : subdivision){
            if (subsInDimensions.get(0)<0.0 || subsInDimensions.get(1)>1.0){
                return false;
            }
            S += subsInDimensions.get(0);
        }
        if (S>=1){

            return false;
        }

        return true;
    }

    public boolean isProbability(ArrayList<Double> x){
        double S = 0.0;
        for (Double d : x){
            S+= d;
        }
        if (S != 1){

        }
        return (S==1);
    }
    public boolean IsInSimplexeDimensionN(ArrayList<ArrayList<Double>> subdivision){
        double Sinf = 0.0;
        double Ssup = 0.0;
        for (ArrayList<Double> subsInDimensions : subdivision){
            if (subsInDimensions.get(0)<0.0 || subsInDimensions.get(1)>1.0){
                return false;
            }
            Sinf += subsInDimensions.get(0);
            Ssup += subsInDimensions.get(1);
        }
        if (Sinf <1 && Ssup>=1){

            this.isProbability(milieu(subdivision));
            return true;
        }
        if (Ssup<1){

        }


        return false;
    }

    private double valAbs(Double d){
        if (d>0){
            return d;
        }
        return -d;
    }
    private double NormeInf(ArrayList<ArrayList<Double>> subdivision) {
        ArrayList<Double> milieu = this.milieu(subdivision);

        return valAbs(subdivision.get(0).get(0) - milieu.get(0));
    }

    private ArrayList<Double> milieu(ArrayList<ArrayList<Double>> subdivision) {
        ArrayList<Double> milieu = new ArrayList<>();
        for (ArrayList<Double> list : subdivision){
            milieu.add(0.5*(list.get(1)+list.get(0)));
        }

        return milieu;
    }

    public ArrayList<ArrayList<ArrayList<Double>>> Subdiviser(ArrayList<ArrayList<Double>> ToBeSubdivised, int dimension, int partitionnement){
        partitionnement ++;
        ArrayList<Double> a = ToBeSubdivised.get(0);
        ArrayList<Double> domaine = this.linspace(a.get(0),a.get(1),partitionnement);
        ArrayList<ArrayList<ArrayList<Double>>> S = new ArrayList<>();
        for (int k=0;k<domaine.size()-1;k++) {
            ArrayList<ArrayList<Double>> dd = new ArrayList<>();
            ArrayList<Double> ddd = new ArrayList<>();
            ddd.add(domaine.get(k));
            ddd.add(domaine.get(k+1));
            dd.add(ddd);
            S.add(dd);
        }


        int i = 1;
        while (i<dimension) {
            a = ToBeSubdivised.get(i);
            domaine = this.linspace(a.get(0), a.get(1), partitionnement);
            ArrayList<ArrayList<ArrayList<Double>>> SS = new ArrayList<>();
            for (int indice =0;indice<S.size();indice++){
                ArrayList<ArrayList<Double>> temp = new ArrayList<>(S.get(indice));
                for (int elt=0; elt<domaine.size()-1;elt++){
                    ArrayList<ArrayList<Double>> tempBis = new ArrayList<>(temp);

                    ArrayList<Double> toBeAdded = new ArrayList<>();
                    toBeAdded.add(domaine.get(elt));
                    toBeAdded.add(domaine.get(elt+1));
                    tempBis.add(toBeAdded);
                    SS.add(tempBis);
                }
            }
            S = SS;
            i++;
        }
        ArrayList<ArrayList<ArrayList<Double>>> ListSuppSimplexe = new ArrayList<>();
        for (ArrayList<ArrayList<Double>> carapuce : S) {
            if (!this.IsInSimplexeDimensionN(carapuce)) {

                ListSuppSimplexe.add(carapuce);
            }
        }
        for (ArrayList<ArrayList<Double>> salameche : ListSuppSimplexe) {
            S.remove(salameche);
        }


        return S;
    }

    public ArrayList<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>>
    SubdiviserPdtCartesien(HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> toBeSubdivised, int dimension, int partitionnement){
        ArrayList<ArrayList<ArrayList<Double>>> a = Subdiviser(toBeSubdivised.get(toBeSubdivised.keySet().iterator().next()),dimension,partitionnement);
        ArrayList<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>> S = new ArrayList<>();
        History<Action,Observation> o1 = toBeSubdivised.keySet().iterator().next();

        for (ArrayList<ArrayList<Double>> temp : a) {
            HashMap<History<Action,Observation>, ArrayList<ArrayList<Double>>> tempHashmap = new HashMap<>();
            tempHashmap.put(o1,temp);
            S.add(tempHashmap);
        }
        int i = 1;
        HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> b = new HashMap<>();

        while (i<toBeSubdivised.size()){
            History<Action,Observation> o = (History<Action,Observation>) toBeSubdivised.keySet().toArray()[i];
            a = Subdiviser(toBeSubdivised.get(o),dimension,partitionnement);
            ArrayList<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>> SS = new ArrayList<>();
            for (int indice =0;indice<S.size();indice++){
                b = S.get(indice);
                HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> temp = new HashMap<>(b);
                for (ArrayList<ArrayList<Double>> temp1 : a) {
                    HashMap<History<Action,Observation>, ArrayList<ArrayList<Double>>> tempHashmap = new HashMap<>(temp);
                    tempHashmap.put(o,temp1);
                    SS.add(tempHashmap);
                }
            }
            S = SS;
            i++;
        }
        return S;
    }


    public double DOOinterne(HashMap<History<Action,Observation>,ArrayList<Double>> x,
                             HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> subsInt
            ,int dim, int partitionnement, double Lambda, double epsilon, OccupancyState<State,Action,Observation> o, boolean show) throws Exception {


        int N = 0;
        double maj = 100000;
        double minorant = -100000;
        double valYmax = -100000;
        HashMap<History<Action,Observation>,ArrayList<Double>> Ymax = milieu(subsInt.entrySet().iterator().next().getKey());
        HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subYmax = new HashMap<>(subsInt.entrySet().iterator().next().getKey());
        HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> listSubsErased = new HashMap<>();
        subsInt.replace(subsInt.entrySet().iterator().next().getKey(),f(x,Ymax,o,false)+Lambda*NormeInf(subYmax));
        while (valAbs(maj - f(x,Ymax,o,false))> epsilon){


            HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> bestSubsToSubdivise = new HashMap<>();
            int argmax = -1;
            double valueArgmax = -10000000;
            int indice=0;
            ArrayList<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>> toBeSupressed = new ArrayList<>();
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subsDimension : subsInt.keySet()){


                double valueSubsDimension = f(x,milieu(subsDimension),o,false) + Lambda*NormeInf(subsDimension);
                subsInt.replace(subsDimension,valueSubsDimension);

                if (valueSubsDimension+Lambda*NormeInf(subsDimension)>=minorant) {
                    if (valueSubsDimension > valueArgmax) {
                        bestSubsToSubdivise = new HashMap<>(subsDimension);
                        valueArgmax = valueSubsDimension;
                    }
                }
                else{
                    toBeSupressed.add(subsDimension);
                }
                indice ++;
            }
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> tmpSupressed : toBeSupressed){
                subsInt.remove(tmpSupressed);
            }
            ArrayList<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>> listNewSubdivisions = new ArrayList<>();
            listNewSubdivisions = SubdiviserPdtCartesien(bestSubsToSubdivise,dim,partitionnement);
            listSubsErased.put(bestSubsToSubdivise,valueArgmax);
            subsInt.remove(bestSubsToSubdivise);
            double value;
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> newSubToAdd : listNewSubdivisions){
                value = f(x,milieu(newSubToAdd),o,false) + Lambda * NormeInf(newSubToAdd);
                subsInt.put(newSubToAdd,value);
            }
            Ymax = new HashMap<>(milieu(subsInt.entrySet().iterator().next().getKey()));
            valYmax = f(x,Ymax,o,false)-Lambda*NormeInf(subsInt.entrySet().iterator().next().getKey());
            subYmax = new HashMap<>(subsInt.entrySet().iterator().next().getKey());
            double valPourMaj = subsInt.get(subsInt.entrySet().iterator().next().getKey());
            double valPourMin = valPourMaj - 2*Lambda*NormeInf(subsInt.entrySet().iterator().next().getKey());
            double valPourMax = valAbs(valPourMaj - valPourMin)/2;
            double valSub = -100000;

            maj = valPourMaj;
            minorant = valPourMin;
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subsToGetMax : subsInt.keySet()){

                valSub = subsInt.get(subsToGetMax);
                if ((valSub - Lambda*NormeInf(subsToGetMax)) > valYmax){

                    Ymax = new HashMap<>(milieu(subsToGetMax));
                    subYmax = new HashMap<>(subsToGetMax);

                    valYmax = valSub - Lambda*NormeInf(subsToGetMax);
                }
                if (valSub>maj){

                    maj = valSub;
                }
                if (valSub - 2* Lambda*NormeInf(subsToGetMax)<minorant){
                    minorant = valSub - 2* Lambda*NormeInf(subsToGetMax);
                }
            }


            double valueFromErased = -10000;
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subsToGetMax : listSubsErased.keySet()){
                valueFromErased = listSubsErased.get(subsToGetMax);
                if ((valueFromErased-Lambda*NormeInf(subsToGetMax))>valYmax){

                }
            }
            N++;
        }


        this.strategyP2 = getDistributionFromArrayList(getValidProbability(subYmax),1);

        return f(x,getValidProbability(subYmax),o,show);


    }

    public void DOOexterne(HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>, Double> subsExt,
                           HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>, Double> subsInt, int dimJ1, int dimJ2, int partitionnement,
                           double Lambda, double epsilonExt, double epsilonInt, OccupancyState<State,Action,Observation> o) throws Exception {

        int N = 0;
        double maj = 100000;
        double minorant = -100000;
        double valXmax = -100000;
        HashMap<History<Action,Observation>,ArrayList<Double>> xmax = milieu(subsExt.entrySet().iterator().next().getKey());
        HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subMax = new HashMap<>(subsExt.entrySet().iterator().next().getKey());
        HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> listSubsErased = new HashMap<>();
        while (valAbs(maj - fExt(subMax,subsInt,dimJ2,partitionnement,Lambda,epsilonInt,o,false))> epsilonExt){
            HashMap<History<Action,Observation>,ArrayList<Double>> b = new HashMap<>();
            HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> bestSubsToSubdivise = new HashMap();
            int argmax = -1;
            double valueArgmax = -10000000;
            int indice=0;
            ArrayList<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>> toBeSupressed = new ArrayList<>();
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subsDimension : subsExt.keySet()){

                double valueSubsDimension = subsExt.get(subsDimension);

                if (valueSubsDimension+Lambda*NormeInf(subsDimension)>=minorant) {
                    if (valueSubsDimension > valueArgmax) {
                        bestSubsToSubdivise = new HashMap<>(subsDimension);
                        valueArgmax = valueSubsDimension;
                    }
                }
                else{

                    System.exit(1);
                    toBeSupressed.add(subsDimension);
                }
                indice ++;
            }
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> tmpSupressed : toBeSupressed){
                subsExt.remove(tmpSupressed);
            }
            ArrayList<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>> listNewSubdivisions = new ArrayList<>();
            listNewSubdivisions = SubdiviserPdtCartesien(bestSubsToSubdivise,dimJ1,partitionnement);
            listSubsErased.put(bestSubsToSubdivise,valueArgmax);
            subsExt.remove(bestSubsToSubdivise);
            double value;
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> newSubToAdd : listNewSubdivisions){

                value = fExt(newSubToAdd,subsInt,dimJ2,partitionnement,Lambda,epsilonInt,o,false) + Lambda * NormeInf(newSubToAdd);
                subsExt.put(newSubToAdd,value);
            }


            valXmax = subsExt.get(subsExt.entrySet().iterator().next().getKey()) - Lambda*NormeInf(subsExt.entrySet().iterator().next().getKey());
            subMax = new HashMap<>(subsExt.entrySet().iterator().next().getKey());
            double valPourMaj = subsExt.get(subsExt.entrySet().iterator().next().getKey());
            double valPourMin = valPourMaj - 2*Lambda*NormeInf(subsExt.entrySet().iterator().next().getKey());
            double valPourMax = valAbs(valPourMaj - valPourMin)/2;
            double valSub = -100000;

            maj = valPourMaj;
            minorant = valPourMin;
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subsToGetMax : subsExt.keySet()){

                valSub = subsExt.get(subsToGetMax);
                if ((valSub - Lambda*NormeInf(subsToGetMax)) > valXmax){


                    subMax = new HashMap<>(subsToGetMax);

                    valXmax = valSub - Lambda*NormeInf(subsToGetMax);
                }
                if (valSub>maj){

                    maj = valSub;
                }
                if (valSub - 2* Lambda*NormeInf(subsToGetMax)<minorant){
                    minorant = valSub - 2* Lambda*NormeInf(subsToGetMax);
                }
            }


            double valueFromErased = -10000;
            for (HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> subsToGetMax : listSubsErased.keySet()){
                valueFromErased = listSubsErased.get(subsToGetMax);
                if ((valueFromErased-Lambda*NormeInf(subsToGetMax))>valXmax){

                }
            }
            N++;
        }
        this.strategyP1 = getDistributionFromArrayList(getValidProbability(subMax),0);
        this.finalValue = fExt(subMax,subsInt,dimJ2,partitionnement,Lambda,epsilonInt,o,true);
    }
    private HashMap<History<Action,Observation>, Distribution<Action>> getDistributionFromArrayList(HashMap<History<Action,Observation>, ArrayList<Double>> x, int player) {
        HashMap<History<Action,Observation>,Distribution<Action>> res = new HashMap<>();
        if (player == 0) {
            ArrayList<Action> ListActions = (ArrayList<Action>) POSG.getActionsJ1();
            for (History<Action, Observation> h : x.keySet()) {
                Distribution<Action> distrib = new Distribution<>();
                for (int i =0;i<ListActions.size();i++){
                    distrib.addWeight(ListActions.get(i),x.get(h).get(i));
                }
                try {
                    distrib.sanityCheck();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                res.put(h,distrib);
            }
        } else {

            ArrayList<Action> ListActions = (ArrayList<Action>) POSG.getActionsJ2();
            for (History<Action, Observation> h : x.keySet()) {
                Distribution<Action> distrib = new Distribution<>();

                for (int i =0;i<ListActions.size();i++){


                    distrib.addWeight(ListActions.get(i),x.get(h).get(i));
                }
                try {

                    distrib.sanityCheck();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                res.put(h,distrib);
            }
        }

        return res;
    }
    private HashMap<History<Action,Observation>, ArrayList<Double>> getValidProbability(HashMap<History<Action,Observation>, ArrayList<ArrayList<Double>>> subMax) {
        HashMap<History<Action,Observation>,ArrayList<Double>> validProbability = new HashMap<>();
        for (History<Action,Observation> key : subMax.keySet()){
            validProbability.put(key,getValidProbability(subMax.get(key)));
        }

        return validProbability;
    }

    private double NormeInf(HashMap<History<Action,Observation>, ArrayList<ArrayList<Double>>> subs) {
        double val = 0.0;
        for (History<Action,Observation> key : subs.keySet()){
            double temp = this.NormeInf(subs.get(key));
            if (temp>val){
                val = temp;
            }
        }
        return val;
    }

    private HashMap<History<Action,Observation>,ArrayList<Double>> milieu(HashMap<History<Action,Observation>, ArrayList<ArrayList<Double>>> key) {
        HashMap<History<Action,Observation>,ArrayList<Double>> res = new HashMap<>();
        for (History<Action,Observation> h : key.keySet()){
            res.put(h,getValidProbability(key.get(h)));
        }
        return res;
    }

    private ArrayList<Double> getValidProbability(ArrayList<ArrayList<Double>> subMax) {
        ArrayList<Double> validProbability = new ArrayList<>();
        double sumCoord = 0.0;
        for (ArrayList<Double> coordCoins: subMax){
            sumCoord += coordCoins.get(0);
        }
        double t = (1-sumCoord)/(2*subMax.size()*NormeInf((subMax)));

        for (ArrayList<Double> coordFromSubMax : subMax){
            validProbability.add(coordFromSubMax.get(0) + t *(coordFromSubMax.get(1) - coordFromSubMax.get(0)));
        }

        this.isProbability(validProbability);

        return validProbability;
    }

    public void computeStartegy(HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayer1,
                                HashMap<History<Action, Observation>, Distribution<Action>> distributionStrategyPlayer2,
                                MajorantPointBased majorantPointBased, Posg<State,Action,Observation> POSG,
                                OccupancyState<State,Action,Observation> o,
                                History<Action,Observation> h1, History<Action,Observation> h2) throws Exception {

        this.POSG = POSG;

        int dimJ2 = 2;
        int dimJ1 = 2;
        ArrayList<ArrayList<Double>> dJ1 = new ArrayList<>();
        ArrayList<ArrayList<Double>> dJ2 = new ArrayList<>();
        ArrayList<Double> a = new ArrayList<>();
        a.add(0.0);
        a.add(1.0);
        for (int i = 0;i<dimJ1;i++){
            dJ1.add(new ArrayList<>(a));
        }
        for (int i = 0;i<dimJ2;i++){
            dJ2.add(new ArrayList<>(a));
        }

        HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> temp = new HashMap<>();

        if (o.getLocalHistoryJ1().isEmpty()){
            temp.put(h1,new ArrayList<ArrayList<Double>>(dJ1));
            temp.put(h2,new ArrayList<ArrayList<Double>>(dJ1));

        }
        else{
            for (History<Action,Observation> h : o.getLocalHistoryJ1()){
                temp.put(h,new ArrayList<ArrayList<Double>>(dJ1));
            }
        }
        HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> h = new HashMap<>();
        h.put(temp,-1.0);
        HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> arbre = new HashMap();
        arbre.put( new HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>(temp),-1.0);

        this.DOOexterne(h,arbre,dJ1.size(),2,2,38.0,0.2,0.2,o);
    }

    public void computeStartegy(ApproximatorPB approximatorPB, Posg<State,Action,Observation> POSG,
                                OccupancyState<State,Action,Observation> o, double lambda) throws Exception {

        this.POSG = POSG;
        this.approximatorPB = approximatorPB;

        int dimJ1 = POSG.getActionsJ1().size();
        int dimJ2 = POSG.getActionsJ2().size();


        ArrayList<ArrayList<Double>> dJ1 = new ArrayList<>();
        ArrayList<ArrayList<Double>> dJ2 = new ArrayList<>();
        ArrayList<Double> a = new ArrayList<>();
        a.add(0.0);
        a.add(1.0);
        for (int i = 0;i<dimJ1;i++){
            dJ1.add(new ArrayList<>(a));
        }
        for (int i = 0;i<dimJ2;i++){
            dJ2.add(new ArrayList<>(a));
        }

        HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> temp = new HashMap<>();
        HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>> tempJ2 = new HashMap<>();

        if (o.getLocalHistoryJ1().isEmpty()){

            History<Action,Observation> h1 = new History<Action, Observation>();

            temp.put(h1,new ArrayList<ArrayList<Double>>(dJ1));

        }
        else {
            for (History<Action, Observation> h : o.getLocalHistoryJ1()) {
                temp.put(h, new ArrayList<ArrayList<Double>>(dJ1));
            }
        }

        if (o.getLocalHistoryJ2().isEmpty()){

            History<Action,Observation> h1 = new History<Action, Observation>();

            tempJ2.put(h1,new ArrayList<ArrayList<Double>>(dJ2));

        }
        else{
            for (History<Action,Observation> h : o.getLocalHistoryJ2()){
                tempJ2.put(h,new ArrayList<ArrayList<Double>>(dJ2));
            }
        }

        HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> h = new HashMap<>();
        h.put(temp,-1.0);
        HashMap<HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>,Double> arbre = new HashMap();

        arbre.put( new HashMap<History<Action,Observation>,ArrayList<ArrayList<Double>>>(tempJ2),-1.0);

        this.DOOexterne(h,arbre,dJ1.size(),dJ2.size(),2,lambda,this.epsilon,this.epsilon,o);

    }
}
