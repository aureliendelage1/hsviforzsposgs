package PomdpSolver;
import posg.*;

import java.io.Serializable;
import java.util.HashMap;


public class SawtoothBound<State,Action,Observation> extends ValueFunctionBound<State,Action,Observation>
        implements Serializable {
    private SawtoothRepresentation<State,Action,Observation> representation;
    private int depth;


    public SawtoothBound(boolean isUpperBound, Posg<State,Action,Observation> pomdp, double discount, double errorMargin, boolean pruning, int player, int depth) throws Exception {
        super(isUpperBound, pomdp, discount, pruning, player, depth);
        init(errorMargin, pruning);
    }
    @Override
    public void update(Belief<State,Action,Observation> belief, double value){

        SawtoothRepresentation.Tooth tooth = new SawtoothRepresentation.Tooth(belief,value);
        representation.addImprovingTooth(tooth,belief.timestep);
    }
    @Override
    public double getValue(Belief<State,Action,Observation> point) {
        if (point.timestep == depth){
            return 0.0;
        }
        return representation.getValue(point);
    }

    private void init(double errorMargin, boolean pruning) throws Exception {
        if(isUpperBound) {
            initWhenUpperBound(errorMargin, pruning);
        } else {
            initWhenLowerBound(errorMargin, pruning);
        }
    }

    private void initWhenUpperBound(double errorMargin, boolean pruning) throws Exception {
        this.depth = problem.profondeurMax;
        HashMap<Integer, HashMap<Pair<State,History<Action,Observation>>,Double>> cornerValues = new HashMap<>();

        OccupancyState<State,Action,Observation> initialOccupancyState = new OccupancyState<State,Action,Observation>(problem);
        initialOccupancyState.InitialBelief = problem.initialBelief;
        OccupancyState<State,Action,Observation> oprime = initialOccupancyState.copy();

        this.initTimestep0(depth,cornerValues);

        BehavioralStrategy<State,Action,Observation> stratPlayerNegI = new BehavioralStrategy<>(oprime,(player+1)%2,problem);
        stratPlayerNegI.generateRandomDistribution(oprime);
        BehavioralStrategy<State,Action,Observation> stratPlayerI = new BehavioralStrategy<>(oprime,player,problem);
        stratPlayerI.generateRandomDistribution(oprime);

        if (player%2==1) {
            oprime.initialupdate(stratPlayerI.getStrategy(), stratPlayerNegI.getStrategy());
        }
        else{
            oprime.initialupdate(stratPlayerNegI.getStrategy(),stratPlayerI.getStrategy());
        }

        stratPlayerI.generateRandomDistribution(oprime);
        stratPlayerNegI.generateRandomDistribution(oprime);

        for (int timestep = 1;timestep<this.problem.profondeurMax-1;timestep++){
            this.init(oprime,oprime.timestep, depth, player,cornerValues);

            if (player%2==1) {
                oprime.update(stratPlayerI.getStrategy(), stratPlayerNegI.getStrategy());
            }
            else{
                oprime.update(stratPlayerNegI.getStrategy(), stratPlayerI.getStrategy());
            }

            stratPlayerNegI = new BehavioralStrategy<>(oprime,(player+1)%2,problem);
            stratPlayerI = new BehavioralStrategy<>(oprime,player,problem);
            stratPlayerI.generateRandomDistribution(oprime);
            stratPlayerNegI.generateRandomDistribution(oprime);
        }
        this.init(oprime,oprime.timestep, depth, player,cornerValues);


        SawtoothRepresentation sawtoothRepresentation = new SawtoothRepresentation(true,true,problem.profondeurMax,cornerValues,player);
        this.representation = sawtoothRepresentation;

    }

    private void init(OccupancyState<State, Action, Observation> oprime, int timestep, int depth, int player, HashMap<Integer,
            HashMap<Pair<State, History<Action, Observation>>, Double>> cornerValues) {
        HashMap<Pair<State, History<Action, Observation>>, Double> hashMap = new HashMap<>();
        double rMaxMin;
        if (player%2==1){
            rMaxMin = problem.computeMaxReward();
        }
        else{
            rMaxMin = problem.computeMinReward();
        }
        for (Pair<State, History<Action, Observation>> pair : oprime.groupByForPomdp(player)){
            hashMap.put(pair,rMaxMin*(depth - oprime.timestep));
        }
        cornerValues.put(oprime.timestep,hashMap);
    }

    private void initTimestep0(int depth, HashMap<Integer, HashMap<Pair<State, History<Action, Observation>>, Double>> cornerValues) {
        HashMap<Pair<State, History<Action, Observation>>, Double> hashMap = new HashMap<>();
        double rMaxMin;
        if (player%2==1){
            rMaxMin = problem.computeMaxReward();
        }
        else{
            rMaxMin = problem.computeMinReward();
        }
        History<Action,Observation> h = new History<>();
        for (State x : problem.getStates()){
            hashMap.put(new Pair<>(x,h),rMaxMin*depth);
        }


        cornerValues.put(0,hashMap);
    }

    private void initWhenLowerBound(double errorMargin, boolean pruning) {
        System.out.println("method not done : initWhenLowerBound");
        System.exit(1);


    }
    public int getSize() {
        return representation.getSize();
    }

    public double getMaxSlope() {
        return representation.getMaxSlope();
    }

}