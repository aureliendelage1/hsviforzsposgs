package PomdpSolver;

import posg.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LowerBoundHyperplane<State,Action,Observation> extends ValueFunctionBound<State,Action,Observation>{

    private Posg<State,Action,Observation> posg;
    private int player;
    private HashMap<Integer, ArrayList<Hyperplane<State,Action,Observation>>> setOfHyperplanes = new HashMap<>();

    public LowerBoundHyperplane(Posg<State,Action,Observation> posg, int player, double discount, boolean pruning, int depth) throws Exception {
        super(false,posg,discount,pruning,player, depth);
        this.posg = posg;
        this.player = player;
        this.Initialize(posg.profondeurMax);
    }

    public double ComputeMaxValue(Belief<State,Action,Observation> b){
        double res = -10000.0;
        for (Hyperplane<State,Action,Observation> hyperplane : this.setOfHyperplanes.get(b.timestep)){
            double tmp = hyperplane.computeValue(b);
            if (tmp>res){
                res = tmp;
            }
        }
        return res;
    }

    @Override
    public void update(Belief<State, Action, Observation> belief, double value) throws Exception {

        HashMap<Pair<Action,Observation>, Hyperplane<State,Action,Observation>> map = new HashMap<>();
        if (belief.timestep<posg.profondeurMax-1) {
            for (Action action : this.posg.getActions(player)) {
                for (Observation o : this.posg.getPossibleObservations(belief,action,player)) {

                    map.put(new Pair<>(action, o), this.getNextBestVector(belief.getNewBelief(action, o)));

                }
            }
        }

        HashMap<Action,HashMap<Pair<State,History<Action,Observation>>, Double>> betaA = new HashMap<>();
        for (Action a : this.posg.getActions(player)) {
            betaA.put(a,this.computeBetaA(belief, map,a));
        }
        HashMap<Pair<State, History<Action, Observation>>, Double> argmaxHashmap = new HashMap<>();
        if (player%2==1) {
            double max = -Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double sum = 0.0;
                for (Map.Entry mapEntry : betaA.get(a).entrySet()) {
                    Pair<State, History<Action, Observation>> pair = (Pair<State, History<Action, Observation>>) mapEntry.getKey();
                    sum += (double) mapEntry.getValue() * belief.getBelief().getWeight(pair);
                }
                if (sum > max) {
                    max = sum;
                    argmaxHashmap = betaA.get(a);
                }
            }
        }
        else{
            double max = Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)) {
                double sum = 0.0;
                for (Map.Entry mapEntry : betaA.get(a).entrySet()) {
                    Pair<State, History<Action, Observation>> pair = (Pair<State, History<Action, Observation>>) mapEntry.getKey();
                    sum += (double) mapEntry.getValue() * belief.getBelief().getWeight(pair);
                }
                if (sum < max) {
                    max = sum;
                    argmaxHashmap = betaA.get(a);
                }
            }
        }
        Hyperplane<State,Action,Observation> argmaxHyperplane = new Hyperplane<>(posg,player, belief.timestep, argmaxHashmap);

        this.setOfHyperplanes.get(belief.timestep).add(argmaxHyperplane);
    }


    private HashMap<Pair<State, History<Action,Observation>>,Double> computeBetaA(
            Belief<State,Action,Observation> belief, HashMap<Pair<Action,Observation>, Hyperplane<State,Action,Observation>> map, Action action) throws Exception {
        HashMap<Pair<State, History<Action,Observation>>,Double> hashMapRes = new HashMap<>();

        for (Pair<State, History<Action,Observation>> pair : belief.getBelief().getNonZeroElements()){
            double reward = 0.0;
            for (Action aNegI : posg.getActions((player+1)%2)) {
                reward += this.posg.rewards.getReward(pair.getElement0(), new JointAction(action,aNegI,player),posg)
                        *(float)(1.0/this.posg.getActions((player+1)%2).size());
            }

            double valueNextStep = 0.0;
            if (belief.timestep < posg.profondeurMax-1) {
                for (State nextState : this.posg.getStates()) {
                    for (Action aNegI : this.posg.getActions((player + 1) % 2)) {
                        for (Observation zNegI : this.posg.getObservations((player + 1) % 2)) {
                            History<Action, Observation> nextHistory = new History<>();
                            ArrayList<Pair<Action, Observation>> list = new ArrayList<>(pair.getElement1().getListeActionObservation());
                            list.add(new Pair<>(aNegI, zNegI));
                            nextHistory.setListeActionObservation(list);
                            for (Observation zPlayerI : this.posg.getObservations(player)) {
                                if (map.containsKey(new Pair<>(action, zPlayerI)) && map.get(new Pair<>(action, zPlayerI)).values.containsKey(new Pair<>(nextState, nextHistory))) {
                                    valueNextStep += map.get(new Pair<>(action, zPlayerI)).values.get(new Pair<>(nextState, nextHistory))
                                            * posg.getObservationProbability(zPlayerI,nextState,nextHistory,action,player)
                                            * posg.getTransitionProbability(nextState,nextHistory,action,pair.getElement0(),player,new BehavioralStrategy<>(),true);
                                }
                            }
                        }
                    }
                }
            }
            hashMapRes.put(pair,reward+1.0*valueNextStep);
        }
        return hashMapRes;
    }

    @Override
    public double getValue(Belief<State, Action, Observation> belief) {
        if (belief.timestep == depth){
            return 0.0;
        }
        if (player%2==1) {
            double res = -Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> hyperplane : this.setOfHyperplanes.get(belief.timestep)) {
                double val = hyperplane.computeValue(belief);
                if (val > res) {
                    res = val;
                }
            }
            return res;
        }
        else{
            double res = Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> hyperplane : this.setOfHyperplanes.get(belief.timestep)) {
                double val = hyperplane.computeValue(belief);
                if (val < res) {
                    res = val;
                }
            }
            return res;
        }
    }

    @Override
    public int getSize() {

        return this.setOfHyperplanes.get(0).size();
    }

    @Override
    public double getMaxSlope() {

        return 0;
    }

    private Hyperplane<State,Action,Observation> getNextBestVector(Belief<State,Action,Observation> newBelief){
        Hyperplane<State,Action,Observation> hyperplane = new Hyperplane<>(posg,player, newBelief.timestep);
        if (player%2==1) {
            double res = -Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> h : this.setOfHyperplanes.get(newBelief.timestep)) {
                double tmp = h.computeValue(newBelief);
                if (tmp > res) {
                    res = tmp;
                    hyperplane = h;
                }
            }
            return hyperplane;
        }
        else{
            double res = Double.POSITIVE_INFINITY;
            for (Hyperplane<State, Action, Observation> h : this.setOfHyperplanes.get(newBelief.timestep)) {
                double tmp = h.computeValue(newBelief);
                if (tmp < res) {
                    res = tmp;
                    hyperplane = h;
                }
            }
            return hyperplane;
        }
    }

    private void Initialize(int depth) throws Exception {
        OccupancyState<State,Action,Observation> initialOccupancyState = new OccupancyState<State,Action,Observation>(posg);
        initialOccupancyState.InitialBelief = posg.initialBelief;
        OccupancyState<State,Action,Observation> oprime = initialOccupancyState.copy();

        this.initTimestep0(depth);

        BehavioralStrategy<State,Action,Observation> stratPlayerNegI = new BehavioralStrategy<>(oprime,(player+1)%2,posg);
        stratPlayerNegI.generateRandomDistribution(oprime);
        BehavioralStrategy<State,Action,Observation> stratPlayerI = new BehavioralStrategy<>(oprime,player,posg);
        stratPlayerI.generateRandomDistribution(oprime);
        if (player%2==1) {
            oprime.initialupdate(stratPlayerI.getStrategy(), stratPlayerNegI.getStrategy());
        }
        else{
            oprime.initialupdate(stratPlayerNegI.getStrategy(),stratPlayerI.getStrategy());
        }

        stratPlayerI.generateRandomDistribution(oprime);
        stratPlayerNegI.generateRandomDistribution(oprime);

        for (int timestep = 1;timestep<this.posg.profondeurMax-1;timestep++){

            this.init(oprime,oprime.timestep, depth, player);

            if (player%2==1) {
                oprime.update(stratPlayerI.getStrategy(), stratPlayerNegI.getStrategy());
            }
            else{
                oprime.update(stratPlayerNegI.getStrategy(), stratPlayerI.getStrategy());
            }

            stratPlayerNegI = new BehavioralStrategy<>(oprime,(player+1)%2,posg);
            stratPlayerI = new BehavioralStrategy<>(oprime,player,posg);
            stratPlayerI.generateRandomDistribution(oprime);
            stratPlayerNegI.generateRandomDistribution(oprime);
        }
        this.init(oprime,oprime.timestep, depth, player);
    }

    private void init(OccupancyState<State, Action, Observation> oprime, int timestep, int depth, int player) {
        ArrayList<Hyperplane<State,Action,Observation>> listHyperplanes = new ArrayList<>();
        double rMaxMin;
        if (player%2==1){

            rMaxMin = posg.computeMinReward();
        }
        else{
            rMaxMin = posg.computeMaxReward();
        }
        HashMap<Pair<State,History<Action,Observation>>,Double> hashMap = new HashMap();
        for (Pair<State, History<Action, Observation>> pair : oprime.groupByForPomdp(player)){
            hashMap.put(pair,rMaxMin*(depth-oprime.timestep));
        }
        Hyperplane<State,Action,Observation> hyperplaneInit = new Hyperplane<>(posg,player,oprime.timestep,hashMap);
        listHyperplanes.add(hyperplaneInit);
        this.setOfHyperplanes.put(oprime.timestep,listHyperplanes);

    }

    private void initTimestep0(int depth) {
        ArrayList<Hyperplane<State,Action,Observation>> arrayList = new ArrayList<>();
        HashMap<Pair<State,History<Action,Observation>>, Double> hashMap = new HashMap<>();
        double rMaxMin;
        if (player%2==1){
            rMaxMin = this.posg.computeMinReward()*depth;
        }
        else{
            rMaxMin = this.posg.computeMaxReward()*depth;
        }
        for (State s : posg.getStates()){
            hashMap.put(new Pair<>(s,new History<Action,Observation>()),rMaxMin);
        }
        Hyperplane<State,Action,Observation> h = new Hyperplane<>(posg,player,0,hashMap);
        arrayList.add(h);
        this.setOfHyperplanes.put(0,arrayList);
    }
}
