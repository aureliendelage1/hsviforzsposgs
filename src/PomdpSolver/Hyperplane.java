package PomdpSolver;

import posg.History;
import posg.Pair;
import posg.Posg;

import java.util.HashMap;

public class Hyperplane<State,Action,Observation> {

    private Posg<State,Action,Observation> posg;
    private int player;
    private int timestep;
    HashMap<Pair<State, History<Action,Observation>>, Double> values = new HashMap<>();

    public Hyperplane(Posg<State,Action,Observation> posg, int player, int timestep){
        this.posg = posg;
        this.player = player;
        this.timestep = timestep;
    }

    public Hyperplane(Posg<State,Action,Observation> posg, int player, int timestep, HashMap<Pair<State, History<Action,Observation>>,
            Double> argmaxHashmap) {
        this.posg = posg;
        this.player = player;
        this.timestep = timestep;
        values = argmaxHashmap;
    }

    public double computeValue(Belief<State,Action,Observation> b){
        double res = 0.0;

        for (Pair<State,History<Action,Observation>> pair : b.getBelief().getNonZeroElements()){
            if (values.containsKey(pair)) {
                res += b.getBelief().getProbability(pair) * values.get(pair);
            }
        }
        return res;
    }

    @Override
    public String toString(){
        return this.values.toString();
    }

}
