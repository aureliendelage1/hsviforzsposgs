package PomdpSolver;


import posg.*;
import util.Distribution;
import util.MatrixX;
import util.RandomGenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class Belief<State,Action,Observation> {
    private Distribution<Pair<State, History<Action, Observation>>> belief = new Distribution<>();
    private Posg<State, Action, Observation> posg;
    private int player;
    public int timestep;
    private BehavioralStrategy<State,Action,Observation> strat;

    public Belief(Posg<State,Action,Observation> posg, Distribution<Pair<State, History<Action, Observation>>> distribution, int timestep, int player) {
        this.posg = posg;
        this.belief = distribution;
        this.timestep = timestep;
        this.player = player;
    }

    public Distribution<Pair<State,History<Action,Observation>>> getBelief(){
        return belief;
    }
    public Belief(Posg<State,Action,Observation> posg, int player, int timestep) {
        this.posg = posg;
        History<Action,Observation> hPlayerI = new History<>();
        History<Action,Observation> hPlayerNegI = new History<>();
        Distribution<Pair<State, History<Action, Observation>>> distribution = new Distribution<>();
        for (State s : posg.getStates()){
            this.belief.addWeight(new Pair<State,History<Action,Observation>>(s,hPlayerNegI),posg.initialBelief.getWeight(s));
        }
        this.timestep = timestep;
    }

    public MatrixX toMatrixX(Collection<State> collection){
        System.out.println("method not done : toMatrixX");
        System.exit(1);
        double[][] A = new double[0][];
        return new MatrixX(A);
    }

    public Belief<State,Action,Observation> getNewBelief( Action actionPlayerI, Observation zPlayerI, BehavioralStrategy<State,Action,Observation> strat) throws Exception {
        this.strat = strat;
        Distribution<Pair<State,History<Action,Observation>>> distribution = this.getNewDistrib(this.belief,actionPlayerI,zPlayerI);
        try {

        }
        catch(Exception e){

            e.printStackTrace();
        }

        return new Belief<>(posg,distribution,this.timestep+1,player);
    }

    public Distribution<Pair<State,History<Action,Observation>>> getNewDistrib(
            Distribution<Pair<State,History<Action,Observation>>> distrib,Action aPlayerI, Observation zPlayerI){

        if (distrib.getNonZeroElements().size() == 0){
            for (State s : this.posg.getStates()){
                distrib.addWeight(new Pair<State,History<Action,Observation>>(s,new History<>()),this.posg.initialBelief.getProbability(s));
            }
        }
        Distribution<Pair<State,History<Action,Observation>>> newDistrib = this.getDistrib(aPlayerI,zPlayerI,distrib);
        return newDistrib;
    }

    private Distribution<Pair<State, History<Action,Observation>>> getDistrib(Action aPlayerI, Observation zPlayerI,
                                                                              Distribution<Pair<State,History<Action,Observation>>> distribConditionnelle){

        Distribution<Pair<State, History<Action,Observation>>> distrib = new Distribution<>();
        for (Pair<State,History<Action,Observation>> PairStateHistory : distribConditionnelle.getNonZeroElements()) {
                History<Action,Observation> hNegI = PairStateHistory.getElement1();
                for (Action aNegI : this.posg.getActions(this.player + 1 % 2)) {
                    for (Observation zNegI : this.posg.getObservations(this.player + 1 % 2)) {
                        History<Action, Observation> nextHistoryPlayerNegI = new History<>();
                        nextHistoryPlayerNegI.setListeActionObservation(hNegI.getListeActionObservation());
                        nextHistoryPlayerNegI.addActionObservation(aNegI, zNegI);
                        if (this.strat != null) {
                            double denominator = 0.0;
                            for (Pair<State,History<Action,Observation>> pair : distribConditionnelle.getNonZeroElements()){
                                State s = pair.getElement0();
                                History<Action,Observation> hNegIDenominator = pair.getElement1();
                                for (State nextStateDenominator : this.posg.getStates()) {
                                    for (Action aNegIDenominator : this.posg.getActions(player+1%2)){
                                        for (Observation zNegIDenominator : this.posg.getObservations(player+1%2)){
                                            History<Action, Observation> nextHistoryPlayerNegIDenominator = new History<>();
                                            nextHistoryPlayerNegIDenominator.setListeActionObservation(hNegIDenominator.getListeActionObservation());
                                            nextHistoryPlayerNegIDenominator.addActionObservation(aNegIDenominator, zNegIDenominator);
                                            denominator += distribConditionnelle.getProbability(new Pair<State, History<Action, Observation>>(s, hNegIDenominator))
                                                    * this.posg.getObservationProbability(zPlayerI,nextStateDenominator,nextHistoryPlayerNegIDenominator,aPlayerI,player)
                                                    * this.posg.getTransitionProbability(nextStateDenominator,nextHistoryPlayerNegIDenominator,aPlayerI,s,player,strat,false);
                                        }
                                    }

                                }
                            }
                            for (State nextState : this.posg.getStates()) {
                                distrib.addWeight(new Pair<State, History<Action, Observation>>(nextState, new History<Action, Observation>(nextHistoryPlayerNegI.getListeActionObservation())),
                                        this.computeProba(distribConditionnelle, nextState,
                                                new History<>(nextHistoryPlayerNegI.getListeActionObservation()), aPlayerI, zPlayerI, denominator));
                            }
                        }
                        else{
                            for (State nextState : this.posg.getStates()) {
                                distrib.addWeight(new Pair<State, History<Action, Observation>>(nextState, new History<Action, Observation>(nextHistoryPlayerNegI.getListeActionObservation())),
                                        this.computeProba(distribConditionnelle, nextState,
                                                new History<>(nextHistoryPlayerNegI.getListeActionObservation()), aPlayerI, zPlayerI,true));
                            }
                        }
                    }

            }
        }
        try {

        }
        catch (Exception e){

            e.printStackTrace();
        }
        return distrib;
    }

    private double computeProba(Distribution<Pair<State,History<Action,Observation>>> distribConditionnelle, State nextState, History<Action,Observation> nextHistoryPlayerNegI,
                                Action aPlayerI, Observation zPlayerI, double denominator){
        History<Action,Observation> lastHistoryPlayerNegI = nextHistoryPlayerNegI.getlastHistory();
        double numerator = 0.0;
        for (State s : this.posg.getStates()){
                numerator += distribConditionnelle.getProbability(new Pair<State, History<Action, Observation>>(s, lastHistoryPlayerNegI))
                        * this.posg.getObservationProbability(zPlayerI,nextState,nextHistoryPlayerNegI,aPlayerI,player)
                        * this.posg.getTransitionProbability(nextState,nextHistoryPlayerNegI,aPlayerI,s,player,strat,false);
        }


        if (denominator == 0.0){
            return 0.0;
        }
        return (numerator/denominator);
    }


    private double computeProba(Distribution<Pair<State,History<Action,Observation>>> distribConditionnelle, State nextState, History<Action,Observation> nextHistoryPlayerNegI,
                                Action aPlayerI, Observation zPlayerI,boolean unif){
        History<Action,Observation> lastHistoryPlayerNegI = nextHistoryPlayerNegI.getlastHistory();
        Action aNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size()-1).getElement0();
        Observation zNegI = nextHistoryPlayerNegI.getListeActionObservation().get(nextHistoryPlayerNegI.getListeActionObservation().size()-1).getElement1();
        double numerator = 0.0;
        for (State s : this.posg.getStates()){
            numerator += distribConditionnelle.getProbability(new Pair<State, History<Action, Observation>>(s, lastHistoryPlayerNegI))
                    * this.posg.getObservationProbability(zPlayerI,nextState,nextHistoryPlayerNegI,aPlayerI,player)
                    * this.posg.getTransitionProbability(nextState,nextHistoryPlayerNegI,aPlayerI,s,player,strat,true);


        }

        double denominator = 0.0;


        for (Pair<State,History<Action,Observation>> pair : distribConditionnelle.getNonZeroElements()){
            State s = pair.getElement0();
            History<Action,Observation> hNegIDenominator = pair.getElement1();
                for (State nextStateDenominator : this.posg.getStates()) {
                    for (Action aNegIDenominator : this.posg.getActions(player+1%2)){
                        for (Observation zNegIDenominator : this.posg.getObservations(player+1%2)){
                            History<Action, Observation> nextHistoryPlayerNegIDenominator = new History<>();
                            nextHistoryPlayerNegIDenominator.setListeActionObservation(hNegIDenominator.getListeActionObservation());
                            nextHistoryPlayerNegIDenominator.addActionObservation(aNegIDenominator, zNegIDenominator);


                            denominator += distribConditionnelle.getProbability(new Pair<State, History<Action, Observation>>(s, hNegIDenominator))
                                    * this.posg.getObservationProbability(zPlayerI,nextStateDenominator,nextHistoryPlayerNegIDenominator,aPlayerI,player)
                                    * this.posg.getTransitionProbability(nextStateDenominator,nextHistoryPlayerNegIDenominator,aPlayerI,s,player,strat,true);

                        }
                    }

            }
        }
        if (denominator == 0.0){
            return 0.0;
        }
        return (numerator/denominator);
    }
    public ArrayList<History<Action,Observation>> groupByState(Set<Pair<State,History<Action,Observation>>> pairList){
        ArrayList<History<Action,Observation>> res = new ArrayList<>();

        for (Pair<State,History<Action,Observation>> pair : pairList){
            if (!res.contains((pair.getElement1()))){
                res.add(pair.getElement1());
            }
        }
        return res;
    }

    public Belief<State,Action,Observation> getNewBelief( Action actionPlayerI, Observation zPlayerI) throws Exception {
        this.strat = null;
        Distribution<Pair<State,History<Action,Observation>>> distribution = this.getNewDistrib(this.belief,actionPlayerI,zPlayerI);
        try {

        }
        catch(Exception e){


        }

        return new Belief<>(posg,distribution,this.timestep+1,player);
    }

    public PomdpSolverNotDynamic.Belief<State,Action,Observation> Marginalise() throws Exception {
        Distribution<State> distribution = new Distribution<>();
        for (Pair<State, History<Action, Observation>> pair : this.belief.getNonZeroElements()){


            if (distribution.getNonZeroElements().contains(pair.getElement0())){
                double val = distribution.getWeight(pair.getElement0()) + belief.getProbability(pair);

                distribution.addWeight(pair.getElement0(),val);
            }
            else{

                distribution.addWeight(pair.getElement0(),belief.getProbability(pair));
            }
        }


        return new PomdpSolverNotDynamic.Belief<>(posg,distribution,timestep,player);
    }
}
