package PomdpSolver;


import posg.Pair;
import posg.Posg;

import java.io.Serializable;

public abstract class ValueFunctionBound<State,Action,Observation> implements POMDPPolicy<State,Action,Observation>, Serializable {
    protected boolean isUpperBound;


    protected Posg<State,Action,Observation> problem;
    protected double discount;
    protected boolean pruning;

    protected int player;
    protected int depth;
    public ValueFunctionBound(boolean isUpperBound, Posg<State,Action,Observation> problem, double discount, boolean pruning, int player, int depth) {
        this.isUpperBound = isUpperBound;
        this.problem = problem;
        this.discount = discount;
        this.pruning = pruning;
        this.player = player;
        this.depth = depth;
    }
    public abstract void update (Belief<State,Action,Observation> belief, double value) throws Exception;

    public abstract double getValue(Belief<State,Action,Observation> belief);

    @Override
    public Action getBestAction(Belief<State,Action,Observation> belief) throws Exception {
        return getBellman(belief).getElement0();
    }
    public Pair<Action, Double> getBellman(Belief<State,Action,Observation> belief) throws Exception {


        double maxiActionValue;
        Action maxiAction = null;
        if (player%2==1) {
            maxiActionValue = Double.NEGATIVE_INFINITY;
            for (Action action : problem.getActions(player)) {
                double actionValue = getBellmanForAction(belief, action);

                if (maxiActionValue < actionValue) {

                    maxiActionValue = actionValue;
                    maxiAction = action;
                }
            }
        }
        else{
            maxiActionValue = -Double.NEGATIVE_INFINITY;
            for (Action action : problem.getActions(player)) {
                double actionValue = getBellmanForAction(belief, action);

                if (maxiActionValue > actionValue) {

                    maxiActionValue = actionValue;
                    maxiAction = action;
                }
            }
        }
        return new Pair<>(maxiAction,maxiActionValue);
    }

    public double getBellmanForAction(Belief<State,Action,Observation> belief, Action action) throws Exception {
        double res = 0.0;

        if (this instanceof SawtoothBound){

        }
        else{

        }


        double qValue = problem.computeReward(belief,action,player);

        double qValue2 = 0;
        if (belief.timestep < problem.profondeurMax -1) {

            for (Observation observation : problem.getObservations(player)) {

                double probabilityOfObservation = problem.getConditionalProbabilityOfObservation(belief, action, observation, player);
                if (probabilityOfObservation > 0.0) {


                    Belief<State, Action, Observation> newBelief = belief.getNewBelief(action, observation);
                    double Vbaz = getValue(newBelief);

                    qValue2 += probabilityOfObservation * Vbaz;
                }
            }
        }
        return qValue + 1.0*qValue2;
    }

    public abstract int getSize();
    public abstract double getMaxSlope();

}