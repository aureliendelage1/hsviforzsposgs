package Approximations;

import AlgorithmForLpSolving.GameMatrix;
import AlgorithmForLpSolving.GurobiComputeBestStrategy;
import posg.*;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;

public class ApproximatorWMinorant<State,Action,Observation> extends ApproximatorW<State,Action,Observation>{

    public ApproximatorWMinorant(Posg<State,Action,Observation> posg){
        this.posg = posg;
    }
    public Pair<BehavioralStrategy<State,Action,Observation>,ArrayList<Double>> ComputeOptimalStrategy(OccupancyState<State,Action,Observation> o) throws Exception {
        BehavioralStrategy<State,Action,Observation> stratP2 = new BehavioralStrategy<>(o,2,this.posg);
        GurobiComputeBestStrategy<State, Action, Observation> lpComputeBestStrategy = new GurobiComputeBestStrategy<>(this.posg, o, this);
        Pair<HashMap<History<Action, Observation>, Distribution<Action>>, ArrayList<Double>> res = lpComputeBestStrategy.getActionMin();
        stratP2.setStrategy(res.getElement0());
        this.gameMatrix = new GameMatrix<>(lpComputeBestStrategy.matrixP2);

        return new Pair<>(stratP2, res.getElement1());


    }
}
