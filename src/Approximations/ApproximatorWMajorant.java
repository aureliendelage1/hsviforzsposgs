package Approximations;

import AlgorithmForLpSolving.GameMatrix;
import AlgorithmForLpSolving.GurobiComputeBestStrategy;
import posg.*;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;

public class ApproximatorWMajorant<State,Action,Observation> extends ApproximatorW<State,Action,Observation>{

    public ApproximatorWMajorant(Posg<State,Action,Observation> posg){
        this.posg = posg;
    }
    public Pair<BehavioralStrategy<State,Action,Observation>, ArrayList<Double>> ComputeOptimalStrategy(OccupancyState<State,Action,Observation> o) throws Exception {
        BehavioralStrategy<State,Action,Observation> stratP1 = new BehavioralStrategy<>(o,1,this.posg);
        GurobiComputeBestStrategy<State, Action, Observation> lpComputeBestStrategy = new GurobiComputeBestStrategy<>(this.posg, o, this);
        Pair<HashMap<History<Action,Observation>, Distribution<Action>>,ArrayList<Double>> res = lpComputeBestStrategy.getActionMax();
        stratP1.setStrategy(res.getElement0());
        this.gameMatrix = new GameMatrix<>(lpComputeBestStrategy.matrixP1);

        return new Pair<BehavioralStrategy<State,Action,Observation>, ArrayList<Double>>(stratP1,res.getElement1());


    }
}
