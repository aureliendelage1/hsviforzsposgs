package Approximations;

import AlgorithmForLpSolving.GameMatrix;
import posg.*;
import util.Distribution;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class VectorApproximation<State, Action, Observation> {

    HashMap<Integer, Double> lambda;
    int depth;
    double rMaxMin;
    Posg<State, Action, Observation> posg;

    public HashMap<Integer, ArrayList<Vector<State, Action, Observation>>> vectorList = new HashMap<>();

    public ArrayList<Vector<State, Action, Observation>> getListVector(int t) {
        if (this.vectorList.keySet().contains(t)) {
            return this.vectorList.get(t);
        }
        return null;
    }

    private ArrayList<History<Action, Observation>> getPossibleHistory(int t) throws Exception {
        int player = -1;
        if (this instanceof VectorMajorantApproximation) {
            player = 1;
        } else if (this instanceof VectorMinorantApproximation) {
            player = 2;
        } else {
            throw new Exception("VectorApproximation:: cannot recognize type");
        }
        ArrayList<History<Action, Observation>> list = new ArrayList<>();
        for (Action a : this.posg.getActions(player)) {
            for (Observation h : this.posg.getObservations(player)) {
                list.add(new History<Action, Observation>(a, h));
            }
        }
        this.recGenerateHistory(list, player, 1, this.posg.profondeurMax);

        System.exit(1);
        return list;
    }

    private void recGenerateHistory(ArrayList<History<Action, Observation>> list, int player, int t, int profondeurMax) {
        if (t == profondeurMax) {
            return;
        }
        for (History<Action, Observation> history : list) {
            for (Action a : this.posg.getActions(player)) {
                for (Observation z : this.posg.getObservations(player)) {
                    history.addActionObservation(a, z);
                }
            }
        }
        t += 1;
        recGenerateHistory(list, player, t, profondeurMax);
    }

    public Vector<State, Action, Observation>
    updateLastStep(SigmaOccupancyState<State, Action, Observation> lastSigmaOccupancyState,
                   SigmaOccupancyState<State, Action, Observation> s,
                   BehavioralStrategy<State, Action, Observation> strat)
            throws Exception {
        int player = 0;
        Vector<State, Action, Observation> vector = new Vector<>();
        HashMap<History<Action, Observation>, Double> hashMap = new HashMap<>();
        this.makeSureDistributionsAreCalculated(s);
        if (this instanceof VectorMajorantApproximation) {
            player = 1;
            for (History<Action, Observation> h : s.o.getLocalHistory(1)) {
                double value = this.computeValue(s, strat, h, 1);
                hashMap.put(h, value);
            }
        }
        if (this instanceof VectorMinorantApproximation) {
            player = 2;
            for (History<Action, Observation> h : s.o.getLocalHistory(2)) {
                double value = this.computeValue(s, strat, h, 2);
                hashMap.put(h, value);
            }
        }
        vector.MapVectorValues = hashMap;
        vector.s = new SigmaOccupancyState<>(s);
        vector.lastSigmaOccupancyState = lastSigmaOccupancyState;
        vector.realizationWeights = this.computeRealizationWeightsLastStep(strat, player+1%2, vector);

        if (this.vectorList.size() < 0) {
            vector.isStrategyRandom = true;
        }

        if (vector.isStrategyRandom) {


            HashMap<Integer, BehavioralStrategy<State, Action, Observation>> strategyToTheEnd = new HashMap<>();
            strategyToTheEnd.put(s.o.timestep, strat);
            vector.setStrat(strategyToTheEnd);

        } else {
            HashMap<Integer, BehavioralStrategy<State, Action, Observation>> strategyToTheEnd = new HashMap<>();
            strategyToTheEnd.put(s.o.timestep, strat);
            HashMap<Integer, BehavioralStrategy<State, Action, Observation>> strategyToTheEndDecompressed = new HashMap<>();
            strategyToTheEndDecompressed = this.getLastStrategyDecompressed(strategyToTheEnd, s.o, s.o.timestep, player);
            vector.setStrat(strategyToTheEndDecompressed);


        }

        if (this.vectorList.get(s.o.getTimestep()) == null) {
            this.vectorList.put(s.o.getTimestep(), new ArrayList<>());
        }
        if (this instanceof VectorMajorantApproximation) {
            if (s.o.getTimestep() == 0) {
            }
        }
        if (this instanceof VectorMinorantApproximation) {
            if (s.o.getTimestep() == 0) {
            }
        }

        this.vectorList.get(s.o.getTimestep()).add(vector);

        return vector;
    }

    private HashMap<Pair<History<Action, Observation>, History<Action, Observation>>, Double> computeRealizationWeightsLastStep(
            BehavioralStrategy<State,Action,Observation> strat,
            int player,
            Vector<State,Action,Observation> vector) {

        HashMap<Pair<History<Action,Observation>,History<Action,Observation>>, Double> res = new HashMap<>();

        for (History<Action,Observation> h : strat.getStrategy().keySet()){
            for (Action a : posg.getActions(player)){
                ArrayList<Pair<Action,Observation>> list = new ArrayList<>();
                list.add(new Pair<Action,Observation>(a, (Observation) new LocalObservation("-")));
                History<Action,Observation> suffix = new History<>(new ArrayList<>(list));


                if (strat.getStrategy().get(h).getWeight(a)>=0.0) {

                    res.put(new Pair<>(h, suffix), strat.getStrategy().get(h).getWeight(a));
                }
            }
        }

        for (History<Action,Observation> hCompressed : vector.s.o.getListCompressedHistories(player).keySet()){
            for (Action a : posg.getActions(player)) {
                ArrayList<Pair<Action, Observation>> list = new ArrayList<>();
                list.add(new Pair<Action, Observation>(a, (Observation) new LocalObservation("-")));
                History<Action, Observation> suffix = new History<>(new ArrayList<>(list));
                    if (strat.getStrategy().containsKey(vector.s.o.getListCompressedHistories(player).get(hCompressed))
                            && strat.getStrategy().get(vector.s.o.getListCompressedHistories(player).get(hCompressed)).getWeight(a) >= 0.0) {


                        //System.out.println("i'm adding compressed histories");
                        res.put(new Pair<>(hCompressed, suffix), strat.getStrategy().get(vector.s.o.getListCompressedHistories(player).get(hCompressed)).getWeight(a));
                }
            }
        }

        return res;
    }

    private HashMap<DecisionRule<State, Action, Observation>, Double> computeVectorStrategyBisLastStep(
            BehavioralStrategy<State, Action, Observation> strat) {
        HashMap<DecisionRule<State, Action, Observation>, Double> mixedStrategy = strat.toMixedStrategyOneStep();
        return mixedStrategy;
    }

    private HashMap<Integer, BehavioralStrategy<State, Action, Observation>>
    getLastStrategyDecompressed(HashMap<Integer, BehavioralStrategy<State, Action, Observation>> strategyToTheEnd,
                                OccupancyState<State, Action, Observation> o,
                                int timestep,
                                int player) {
        HashMap<Integer, BehavioralStrategy<State, Action, Observation>> hashMap = new HashMap<>();
        BehavioralStrategy<State, Action, Observation> strat = new BehavioralStrategy<>();
        HashMap<History<Action, Observation>, Distribution<Action>> hashMap1 = new HashMap<>();

        for (History<Action, Observation> h : strategyToTheEnd.get(timestep).getStrategy().keySet()) {
            hashMap1.put(h, strategyToTheEnd.get(timestep).getStrategy().get(h));
        }
        for (History<Action, Observation> h : o.getListCompressedHistories(player + 1).keySet()) {
            if (strategyToTheEnd.get(timestep).getStrategy().containsKey(o.getListCompressedHistories(player + 1).get(h)))
                hashMap1.put(h, strategyToTheEnd.get(timestep).getStrategy().get(o.getListCompressedHistories(player + 1).get(h)));
        }
        strat.setStrategy(hashMap1);
        hashMap.put(timestep, strat);

        return hashMap;
    }
    protected double computeValue(
            SigmaOccupancyState<State, Action, Observation> s,
            BehavioralStrategy<State, Action, Observation> strat,
            History<Action, Observation> histPlayerI,
            int player) throws Exception {
        double value;
        if (player % 2 == 1) {
            value = -100000.0;
        } else {
            value = 100000.0;
        }
        for (Action a : this.posg.getActions(player)) {
            double valueForThisAction = 0.0;
            for (History<Action, Observation> histPlayerNegI : s.o.getLocalHistory(player + 1 % 2)) {

                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    double valSumNextHistory = 0.0;
                    if (player % 2 == 1) {
                        valueForThisAction += s.getConditionalValue(histPlayerI, histPlayerNegI, player) * strat.getStrategy().get(histPlayerNegI).getWeight(aNegI) * (valSumNextHistory
                                + this.posg.getSigmaReward(s.o, new JointHistory<Action, Observation>(histPlayerI, histPlayerNegI), new JointAction<Action>(a, aNegI)));

                    } else {
                        valueForThisAction += s.getConditionalValue(histPlayerI, histPlayerNegI, player) * strat.getStrategy().get(histPlayerNegI).getWeight(aNegI) * (valSumNextHistory
                                + this.posg.getSigmaReward(s.o, new JointHistory<Action, Observation>(histPlayerNegI, histPlayerI), new JointAction<Action>(aNegI, a)));
                    }

                }
            }
            if (player % 2 == 1) {
                if (valueForThisAction > value) {
                    value = valueForThisAction;
                }
            } else {
                if (valueForThisAction < value) {
                    value = valueForThisAction;
                }
            }
        }
        return value;
    }

    public Vector<State, Action, Observation> getSupportVector(
            OccupancyState<State, Action, Observation> o) throws Exception {

        int player = -1;
        double val = 0.0;
        if (this instanceof VectorMajorantApproximation) {
            player = 1;
            val = 100000.0;

        }
        if (this instanceof VectorMinorantApproximation) {
            player = 2;
            val = -100000.0;
        }
        Vector<State, Action, Observation> v = new Vector<>();
        SigmaOccupancyState<State, Action, Observation> s = new SigmaOccupancyState<>(o);

        int timestep = s.o.getTimestep();
        ArrayList<Vector<State, Action, Observation>> listOfVectors = vectorList.get(timestep);
        for (Vector<State, Action, Observation> vector : listOfVectors) {
            double tmpVal = 0.0;
            Distribution<History<Action, Observation>> distribMarginale = vector.s.ComputeSigmaMarginal(player);
            HashMap<History<Action, Observation>, Double> hashmap = vector.MapVectorValues;

            for (Map.Entry<History<Action, Observation>, Double> m : hashmap.entrySet()) {
                tmpVal += distribMarginale.getWeight(m.getKey()) * m.getValue();
            }
            if (player % 2 == 1) {
                tmpVal += lambda.get(timestep) * s.getNorme1(vector.s);
                if (tmpVal < val) {
                    val = tmpVal;
                    v = vector;
                }
            } else if (player % 2 == 0) {
                tmpVal -= lambda.get(timestep) * s.getNorme1(vector.s);
                if (tmpVal > val) {
                    val = tmpVal;
                    v = vector;
                }
            }
        }
        return v;
    }

    public Vector<State, Action, Observation>
    update(SigmaOccupancyState<State, Action, Observation> lastSigmaOccupancyState,
           SigmaOccupancyState<State, Action, Observation> s,
           ApproximatorW<State, Action, Observation> W,
           ArrayList<Double> weights,
           GameMatrix<State, Action, Observation> gameMatrix)
            throws Exception {

        Vector<State, Action, Observation> vector = new Vector<>();
        HashMap<History<Action, Observation>, Double> hashMap = new HashMap<>();

        if (this instanceof VectorMajorantApproximation) {
            vector.realizationWeights = this.computeRealizationWeights(s, W, weights, 2);
            for (History<Action, Observation> h : s.o.getLocalHistory(1)) {
                double value;
                if (W.bagW.containsKey(s.o.timestep)) {

                    if (W.bagW.get(s.o.timestep).size() > 1) {
                        value = (float) this.computeValueWithGameMatrix(s,gameMatrix,h,1,weights, W)/((float) s.getMarginalValue(h,1));

                    } else {
                        value = this.computeValue(s, W, weights, h, 1);
                    }
                } else {

                    vector.isStrategyRandom = true;


                    value = this.computeValue(s, h, 1);
                }
                hashMap.put(h, value);
            }
        }
        if (this instanceof VectorMinorantApproximation) {
            vector.realizationWeights = this.computeRealizationWeights(s, W, weights, 1);
            for (History<Action, Observation> h : s.o.getLocalHistory(2)) {
                double value;
                if (W.bagW.containsKey(s.o.timestep)) {

                    if (W.bagW.get(s.o.timestep).size() > 1) {
                        value = (float) this.computeValueWithGameMatrix(s,gameMatrix,h,2,weights, W)/((float) s.getMarginalValue(h,2 ));
                    } else {
                        value = this.computeValue(s, W, weights, h, 2);
                    }
                } else {

                    System.out.println("if this is indeed executed, then it's bad");
                    System.exit(1);
                    vector.isStrategyRandom = true;

                    value = this.computeValue(s, h, 2);
                }

                hashMap.put(h, value);
            }
        }
        vector.MapVectorValues = hashMap;
        vector.s = new SigmaOccupancyState<>(s);
        vector.lastSigmaOccupancyState = lastSigmaOccupancyState;
        if (vector.isStrategyRandom) {
            vector.strat = this.describeRandomStrategy(vector);


        }
        if (this.vectorList.get(s.o.getTimestep()) == null) {
            this.vectorList.put(s.o.getTimestep(), new ArrayList<>());
        }
        if (this instanceof VectorMajorantApproximation) {
        }
        if (this instanceof VectorMinorantApproximation) {
        }
        this.vectorList.get(s.o.getTimestep()).add(vector);
        return vector;
    }

    private HashMap<Pair<History<Action, Observation>, History<Action,Observation>>, Double> computeRealizationWeights(
            SigmaOccupancyState<State, Action, Observation> s,
            ApproximatorW<State, Action, Observation> approximatorW,
            ArrayList<Double> weights, int player) throws Exception {

        HashMap<Pair<History<Action, Observation>, History<Action,Observation>>, Double> res = new HashMap<>();
        int index = 0;
        for (Pair<SigmaOccupancyState<State, Action, Observation>,
                Pair<BehavioralStrategy<State, Action, Observation>, Vector<State, Action, Observation>>
                >
                objet : approximatorW.bagW.get(s.o.getTimestep())) {
            if (player%2==0) {
                //System.out.println("objet : " + objet.getElement1().getElement1());
            }
            HashMap<Pair<History<Action,Observation>,History<Action,Observation>>,Double> resForThisW =  this.computeRealizationWeightForW(objet,player%2,s);
            if (weights.get(index)!=0.0) {
                for (Pair<History<Action, Observation>, History<Action,Observation>> hAndActNext : resForThisW.keySet()) {
                    if (res.containsKey(hAndActNext)) {
                        res.replace(hAndActNext, res.get(hAndActNext) + Math.abs(weights.get(index)) * resForThisW.get(hAndActNext));
                    }
                    else{
                        res.put(hAndActNext, Math.abs(weights.get(index)) * resForThisW.get(hAndActNext));
                    }
                }
            }
            index++;
        }
        HashMap<Pair<History<Action, Observation>, History<Action,Observation>>, Double> resToAdd = new HashMap<>();

        for (History<Action,Observation> histCompressed : s.o.getListCompressedHistories(player).keySet()){
            for (Pair<History<Action, Observation>, History<Action,Observation>> pair : res.keySet()){
                if (pair.getElement0().equals(s.o.getListCompressedHistories(player).get(histCompressed))){
                    resToAdd.put(new Pair<>(histCompressed,pair.getElement1()), res.get(pair));
                }
            }
        }
        for (History<Action,Observation> histKeyCompressed : s.o.getReversedListCompressedHistories(player).keySet()) {
            for (Pair<History<Action, Observation>, History<Action, Observation>> pair : res.keySet()) {
                if (pair.getElement0().equals(histKeyCompressed)){
                    resToAdd.put(new Pair<>(s.o.getReversedListCompressedHistories(player).get(histKeyCompressed),pair.getElement1()),res.get(pair));
                }
            }
        }
        for (Pair<History<Action, Observation>, History<Action,Observation>> tmp : resToAdd.keySet()){
            res.put(tmp,resToAdd.get(tmp));
        }

        /*
        System.out.println("------------------- \nchecking res");
        MixtedStrategy<State,Action,Observation> mixedStrat = new MixtedStrategy<State,Action,Observation>(posg,res,player);
        mixedStrat.behavioralStrat.sanityCheck();
        System.out.println("------------------- \n end checking res, passed");*/
        return res;
    }

    private HashMap<Pair<History<Action, Observation>, History<Action, Observation>>, Double> computeRealizationWeightForW(
            Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>> objet,
            int player,
            SigmaOccupancyState<State,Action,Observation> s) {
        SigmaOccupancyState<State,Action,Observation> lastOccupancyState = objet.getElement0();
        HashMap<Pair<History<Action, Observation>, History<Action,Observation>>, Double> res = new HashMap<>();
        ArrayList<History<Action,Observation>> listHistoriesWithCompressedOnes = new ArrayList<>(objet.getElement1().getElement0().getStrategy().keySet());

        if (!objet.getElement1().getElement0().alreadyDecompressed) {
            objet.getElement1().getElement0().decompress(objet.getElement1().getElement1().lastSigmaOccupancyState, player);
        }
        for (History<Action,Observation> h : objet.getElement1().getElement0().getStrategy().keySet()){
            for (Action a : posg.getActions(player)) {

                for (Pair<History<Action, Observation>, History<Action, Observation>> hAndActNext : objet.getElement1().getElement1().realizationWeights.keySet()) {
                    double proba;
                    History<Action, Observation> prefix;
                    if (objet.getElement1().getElement1().lastSigmaOccupancyState.o.getListCompressedHistories(player).containsKey(h)){
                        prefix = objet.getElement1().getElement1().lastSigmaOccupancyState.o.getListCompressedHistories(player).get(h);
                    }
                    else {
                         prefix = new History<>(new ArrayList<>(h.getListeActionObservation()));
                    }
                    History<Action, Observation> suffix = new History<>(new ArrayList<>(hAndActNext.getElement1().getListeActionObservation()));
                    suffix.addActionObservationFirst(a, hAndActNext.getElement0().getLastObservation());
                    if (hAndActNext.getElement0().getLastAction().equals(a) && prefix.equals(hAndActNext.getElement0().getlastHistory())) {
                        if (objet.getElement1().getElement0().getStrategy().containsKey(h)) {
                            proba = objet.getElement1().getElement0().getStrategy().get(h).getWeight(a)
                                    * objet.getElement1().getElement1().realizationWeights.get(hAndActNext);
                        } else {
                            //System.out.println("adding random");
                            System.exit(1);
                            proba = (float) (1.0 / posg.getActions(player).size())
                                    * objet.getElement1().getElement1().realizationWeights.get(hAndActNext);
                        }

                        prefix = new History<>(new ArrayList<>(h.getListeActionObservation()));
                        if (proba > 0.0) {
                            res.put(new Pair<>(prefix, suffix), proba);
                        }
                    }
                }
            }
        }
        for (Pair<History<Action, Observation>, History<Action,Observation>> hAndActNext : objet.getElement1().getElement1().realizationWeights.keySet()) {
            res.put(hAndActNext,Math.abs(objet.getElement1().getElement1().realizationWeights.get(hAndActNext)));


        }
        /*
        if (player%2==0) {
            for (Pair<History<Action, Observation>, History<Action,Observation>> h : res.keySet()){
                if (h.getElement0().toString().equals("none")){
                    System.out.println("none, " + h.getElement1() + " proba : " + res.get(h));
                }
            }
            System.out.println("res : " + res + " for w : " + objet);
        }*/
        return res;
    }

    private HashMap<DecisionRule<State,Action,Observation>, Double>
    computeVectorStrategyBis(
            SigmaOccupancyState<State,Action,Observation> s,
            ApproximatorW<State,Action,Observation> approximatorW,
            ArrayList<Double> weights,
            int player) throws Exception {
        HashMap<DecisionRule<State,Action,Observation>, Double> res = new HashMap<>();


        DecisionRuleGenerator<State, Action, Observation> decisionRuleGenerator = new DecisionRuleGenerator<>(s);
        ArrayList<History<Action,Observation>> listHistories = new ArrayList<>();
        listHistories.addAll(s.getDistribMarginale(player+1).getNonZeroElements());
        for (DecisionRule<State, Action, Observation> decisionRule : decisionRuleGenerator.generateDecisionRule(player,
                listHistories)) {
            int index = 0;
            for (Pair< SigmaOccupancyState<State,Action,Observation>,
                    Pair< BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation> >
                    >
                    objet : approximatorW.bagW.get(s.o.getTimestep())) {
                double weight = Math.abs(weights.get(index));

                for (DecisionRule<State, Action, Observation> nextDecisionRule : objet.getElement1().getElement1().mixtedStrategy.keySet()) {


                    double probaForBeta = objet.getElement1().getElement0().getProbaOfDecisionRule(decisionRule,player+1);
                    double proba = weight*objet.getElement1().getElement1().mixtedStrategy.get(nextDecisionRule)*probaForBeta;
                    if (nextDecisionRule.isAnExtensionOf(decisionRule)){


                        DecisionRule<State,Action,Observation> concatenationOfDecisionRules = decisionRule.append(nextDecisionRule);
                        if (res.containsKey(concatenationOfDecisionRules)){
                            res.replace(concatenationOfDecisionRules,proba+res.get(concatenationOfDecisionRules));
                        }
                        else {
                            res.put(concatenationOfDecisionRules, proba);
                        }
                    }
                }


                index++;
            }
        }

        return res;
    }

    private double computeValueWithGameMatrix(
            SigmaOccupancyState<State, Action, Observation> s,
            GameMatrix<State, Action, Observation> gameMatrix,
            History<Action, Observation> h,
            int player,
            ArrayList<Double> weights,
            ApproximatorW<State, Action, Observation> W) {
        double res = 0.0;
        if (player%2==1){
            double max = -Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)){
                int index = 0;
                double valForAction =  0.0;
                for (Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                        objet : W.bagW.get(s.o.getTimestep())) {
                    double w = Math.abs(weights.get(index));
                    valForAction += w*gameMatrix.matrix.get(new Pair<>(new Pair(h,a),objet));
                    index++;
                }
                if (valForAction>max){
                    max = valForAction;
                }
            }
            res = max;
        }
        if (player%2==0){
            double min = Double.POSITIVE_INFINITY;
            for (Action a : this.posg.getActions(player)){
                int index = 0;
                double valForAction =  0.0;
                for (Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                        objet : W.bagW.get(s.o.getTimestep())) {
                    double w = Math.abs(weights.get(index));
                    valForAction += w*gameMatrix.matrix.get(new Pair<>(new Pair(h,a),objet));
                    index++;
                }
                if (valForAction<min){
                    min = valForAction;
                }
            }
            res = min;
        }

        return res;
    }

    private HashMap<Integer, BehavioralStrategy<State,Action,Observation>> describeRandomStrategy(
            Vector<State, Action, Observation> vector) throws Exception {
        int player;
        if (this instanceof VectorMajorantApproximation){
            player = 1;
        }
        else{
            player = 2;
        }
        vector.s.makeSureDistributionsAreCalculated(player);
        HashMap<Integer, BehavioralStrategy<State,Action,Observation>> res = new HashMap<>();
        BehavioralStrategy<State,Action,Observation> behavioralStrategy = new BehavioralStrategy<>();
        behavioralStrategy.setStrategy(new HashMap<>());
        for (History<Action,Observation> history : vector.s.getDistribMarginale(player+1).getNonZeroElements()){

            behavioralStrategy.getStrategy().put(history, new Distribution<Action>(true, this.posg.getActions(player+1)));
        }
        res.put(vector.s.o.timestep,behavioralStrategy);
        for (int t = vector.s.o.timestep+1;t<posg.profondeurMax;t++) {
            BehavioralStrategy<State,Action,Observation> strategy = new BehavioralStrategy<>();
            strategy.setStrategy(new HashMap<>());
            for (History<Action,Observation> history : res.get(t-1).getStrategy().keySet()){
                for (Action aNegI : this.posg.getActions(player+1)){
                    for (Observation zNegI : this.posg.getObservations(player+1)){
                        History<Action, Observation> nextHistory = new History<>();
                        nextHistory.setListeActionObservation(new ArrayList<>(history.getListeActionObservation()));
                        nextHistory.addActionObservation(aNegI, zNegI);
                        strategy.getStrategy().put(nextHistory, new Distribution<Action>(true, this.posg.getActions(player+1)));
                    }
                }
            }
            res.put(t,strategy);
        }

        return res;
    }

    public HashMap<Integer, BehavioralStrategy<State,Action,Observation>> computeVectorStrategy(
            SigmaOccupancyState<State,Action,Observation> s,
            ApproximatorW<State,Action,Observation> W,
            ArrayList<Double> weights,
            int i) {

        HashMap<Integer,BehavioralStrategy<State,Action,Observation>> strategyToTheEnd = new HashMap<>();

        int player;
        if (this instanceof VectorMajorantApproximation){
            player=1;
        }
        else{
            player=2;
        }

        int index = 0;
        for (Pair< SigmaOccupancyState<State,Action,Observation>,
                Pair< BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation> >
                >
                objet : W.bagW.get(s.o.getTimestep())) {
            double w = Math.abs(weights.get(index));
            HashMap<History<Action,Observation>, Distribution<Action>> strategyHashMap;
            if (strategyToTheEnd.containsKey(s.o.timestep)) {
                strategyHashMap = strategyToTheEnd.get(s.o.timestep).copy().getStrategy();
            }
            else{
                strategyHashMap = new HashMap<>();
            }

            if (weights.get(index) != 0.0){


                for (History<Action,Observation> history : objet.getElement1().getElement0().getStrategy().keySet()){
                    if (strategyHashMap.containsKey(history)){
                        strategyHashMap.replace(history,strategyHashMap.get(history).addDistribution(objet.getElement1().getElement0().getStrategy().get(history).scalarMultiply(w)));

                    }
                    else{

                        strategyHashMap.put(history,objet.getElement1().getElement0().getStrategy().get(history).scalarMultiply(w));


                    }

                }

                for (History<Action,Observation> history : objet.getElement0().o.getListCompressedHistories(player+1).keySet()){
                    History<Action,Observation> historyCompressed = objet.getElement1().getElement1().s.o.getListCompressedHistories(player+1).get(history);
                    if (strategyHashMap.containsKey(history)){
                        strategyHashMap.replace(history,strategyHashMap.get(historyCompressed).addDistribution(objet.getElement1().getElement0().getStrategy().get(historyCompressed).scalarMultiply(w)));

                    }
                    else{

                        strategyHashMap.put(history,objet.getElement1().getElement0().getStrategy().get(historyCompressed).scalarMultiply(w));


                    }
                }
                strategyToTheEnd.put(s.o.timestep,new BehavioralStrategy<State,Action,Observation>(strategyHashMap));
                for (int t = s.o.timestep+1;t<s.o.POSG.profondeurMax;t++){
                    if (strategyToTheEnd.containsKey(t)) {
                        strategyHashMap = strategyToTheEnd.get(t).copy().getStrategy();
                    }
                    else{
                        strategyHashMap = new HashMap<>();
                    }


                    for (History<Action, Observation> history : objet.getElement1().getElement1().getStrat().get(t).getStrategy().keySet()) {
                        if (!strategyHashMap.containsKey(history)) {
                            strategyHashMap.put(history, new Distribution<Action>(posg.getActions(player+1)));
                        }
                        strategyHashMap.replace(history,strategyHashMap.get(history).addDistribution(objet.getElement1().getElement1().getStrat().get(t).getStrategy().get(history).scalarMultiply( Math.abs(weights.get(index)))));
                    }
                    if (t == s.o.timestep+1) {


                    }
                    strategyToTheEnd.put(t,new BehavioralStrategy<>(strategyHashMap));


                }


            }
            index ++;
        }

        for (int t = s.o.timestep;t<posg.profondeurMax;t++) {

            for (History<Action, Observation> hist : strategyToTheEnd.get(t).getStrategy().keySet()) {
                strategyToTheEnd.get(t).getStrategy().get(hist).normalize();
            }

        }
        return strategyToTheEnd;
    }

    private double computeValue(SigmaOccupancyState<State,Action,Observation> sigmaOccupancyState, History<Action,Observation> h, int player) throws Exception {
        sigmaOccupancyState.makeSureDistributionsAreCalculated(player);

        if (player%2==1){
            return this.posg.maxReward*(posg.profondeurMax - sigmaOccupancyState.o.timestep);
        }
        if (player%2==0){
            return this.posg.minReward*(posg.profondeurMax - sigmaOccupancyState.o.timestep);
        }

        return this.posg.computeRMaxMin(player,sigmaOccupancyState.getStateDistrib(posg,player,h,sigmaOccupancyState.o.timestep));
    }

    protected double computeValue(SigmaOccupancyState<State,Action,Observation> s, ApproximatorW<State,Action,Observation> W, ArrayList<Double> weights
            , History<Action,Observation> histPlayerI,int player) throws Exception {
        double finalRes;
        if (player%2==1){
            finalRes = -100000.0;
        }
        else{
            finalRes = 100000.0;
        }
        s.makeSureDistributionsAreCalculated(player);

        for (Action a : this.posg.getActions(player)) {
            double valueForThisAction = 0.0;
            int index = 0;
            for (Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                    objet : W.bagW.get(s.o.getTimestep())){
                Vector<State,Action,Observation> vNextStep = objet.getElement1().getElement1();
                double valueForThisElementOfBag = 0.0;
                if (weights.get(index) != 0.0){
                    HashMap<History<Action,Observation>, Double> mapAlreadyComptedValues = new HashMap<>();
                    weights.set(index, Math.abs(weights.get(index)));
                    double rewardValue = this.computeReward(a,histPlayerI,s,objet.getElement1().getElement0(),player);
                    double valueNextStep = 0.0;
                    if (objet.getElement0().getDistribMarginale(player).getNonZeroElements().contains(histPlayerI)){
                        valueNextStep = this.computeForNextStep(histPlayerI,a,objet,player);
                        mapAlreadyComptedValues.put(histPlayerI,valueNextStep);
                    }
                    else {
                        if (player%2==1) {
                            if (objet.getElement0().o.listCompressedHistoriesP1.containsKey(histPlayerI)){
                                if (mapAlreadyComptedValues.containsKey(objet.getElement0().o.listCompressedHistoriesP1.get(histPlayerI))) {
                                    valueNextStep = mapAlreadyComptedValues.get(objet.getElement0().o.listCompressedHistoriesP1.get(histPlayerI));
                                }
                                else {
                                    valueNextStep = this.computeForNextStep(objet.getElement0().o.listCompressedHistoriesP1.get(histPlayerI), a, objet, player);
                                }
                            }
                            else {
                            }
                        }
                        else{
                            if (objet.getElement0().o.listCompressedHistoriesP2.containsKey(histPlayerI)){
                                if (mapAlreadyComptedValues.containsKey(objet.getElement0().o.listCompressedHistoriesP2.get(histPlayerI))) {
                                    valueNextStep = mapAlreadyComptedValues.get(objet.getElement0().o.listCompressedHistoriesP2.get(histPlayerI));
                                }
                                else {
                                    valueNextStep = this.computeForNextStep(objet.getElement0().o.listCompressedHistoriesP2.get(histPlayerI), a, objet, player);
                                }

                            }
                            else {
                            }
                        }
                    }
                    if (player%2==1){

                    }
                    valueForThisElementOfBag = rewardValue + valueNextStep;
                }
                if (player%2==1) {
                    valueForThisAction += weights.get(index) * (valueForThisElementOfBag + this.posg.lambda.get(s.o.timestep) * s.getNorme1(objet.getElement0()));
                }
                else{
                    valueForThisAction += weights.get(index) * (valueForThisElementOfBag - this.posg.lambda.get(s.o.timestep) * s.getNorme1(objet.getElement0()));
                }
                index++;
            }
            if (player % 2 == 1) {

                if (valueForThisAction > finalRes) {
                    finalRes = valueForThisAction;
                }


            } else {
                if (valueForThisAction < finalRes) {
                    finalRes = valueForThisAction;
                }
            }
        }
        return finalRes;
    }

    private double computeForNextStep(History<Action,Observation> histPlayerI, Action a, Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>> objet, int player) throws Exception {


        double res = 0.0;

        Vector<State,Action,Observation> vNextStep = objet.getElement1().getElement1();
        for (Observation oPI : this.posg.getObservations(player)){
            History<Action, Observation> nextHistory = new History<>();
            nextHistory.setListeActionObservation(histPlayerI.getListeActionObservation());
            nextHistory.addActionObservation(a, oPI);
            for (State state : this.posg.getStates()) {
                for (History<Action, Observation> histPlayerNegI : objet.getElement0().o.getLocalHistory(player + 1 % 2)) {
                    for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                        for (Observation oPNegI : this.posg.getObservations(player + 1 % 2)) {
                            for (State nextState : this.posg.getStates()) {
                                if (objet.getElement0().o.getProba(new Pair<>(state, new JointHistory<>(histPlayerI, histPlayerNegI, player))) / objet.getElement0().getMarginalValue(histPlayerI, player)
                                        * objet.getElement1().getElement0().getStrategy().get(histPlayerNegI).getWeight(aNegI)
                                        * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                        * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState) > 0.0) {

                                    res += objet.getElement0().o.getProba(new Pair<>(state, new JointHistory<>(histPlayerI, histPlayerNegI, player))) / objet.getElement0().getMarginalValue(histPlayerI, player)
                                            * objet.getElement1().getElement0().getStrategy().get(histPlayerNegI).getWeight(aNegI)
                                            * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                            * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState)
                                            * vNextStep.get(nextHistory, objet.getElement0().o.timestep + 1, player, posg, objet.getElement0(), vNextStep.s,
                                            objet.getElement1().getElement0());

                                }
                            }
                        }
                    }
                }
            }
        }
        return res;
    }

    private double getProbaObservation(History<Action,Observation> histPlayerI, Action a, Pair<SigmaOccupancyState<State,Action,Observation>,
            Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>> objet, int player, Observation oPI) throws Exception {
        double res = 0.0;

        for (State state : this.posg.getStates()) {
            for (History<Action, Observation> histPlayerNegI : objet.getElement0().o.getLocalHistory(player + 1 % 2)) {
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    for (Observation oPNegI : this.posg.getObservations(player + 1 % 2)) {
                        for (State nextState : this.posg.getStates()) {
                            res += objet.getElement0().o.getProba(new Pair<>(state, new JointHistory<>(histPlayerI, histPlayerNegI, player)))
                                    / objet.getElement0().getMarginalValue(histPlayerI, player)
                                    * objet.getElement1().getElement0().getStrategy().get(histPlayerNegI).getWeight(aNegI)
                                    * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                    * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState);
                        }
                    }
                }
            }
        }
        return res;
    }

    private double computeReward(Action a, History<Action,Observation> histPlayerI, SigmaOccupancyState<State,Action,Observation> s, BehavioralStrategy<State,Action,Observation> element0,int player) throws Exception {

        double res = 0.0;
        if (histPlayerI.toString().equals("(,)")){
            for (State state : this.posg.getStates()) {
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    res += s.o.InitialBelief.getWeight(state)
                            * this.posg.rewards.getReward(state, new JointAction(a, aNegI, player), posg)
                            * element0.getStrategy().get(histPlayerI).getWeight(aNegI);
                }
            }
        }
        else {
            HashMap<History<Action,Observation>,Distribution<Pair<State,History<Action,Observation>>>> hashMap =  s.computeConditionalStateHistoryDistrib(player);
            for (State state : this.posg.getStates()) {
                for (History<Action, Observation> histPlayerNegI : s.o.getLocalHistory(player + 1 % 2)) {

                    double tmp = 0.0;
                    for (History<Action, Observation> histPlayerNegITemp : s.o.getLocalHistory(player + 1%2)){

                        tmp += s.o.getProba(new Pair<State,JointHistory<Action,Observation>>(state,new JointHistory<>(histPlayerI,histPlayerNegITemp,player)));
                    }


                    if (element0.getStrategy().keySet().contains(histPlayerNegI)) {
                        for (Action aNegI : this.posg.getActions(player + 1 % 2)) {

                            res += this.posg.rewards.getReward(state, new JointAction<Action>(a, aNegI, player), posg)
                                    * hashMap.get(histPlayerI).getWeight(new Pair<>(state,histPlayerNegI))
                                    * element0.getStrategy().get(histPlayerNegI).getWeight(aNegI);
                        }
                    } else {

                        for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                            res += this.posg.rewards.getReward(state, new JointAction<Action>(a, aNegI, player), posg)
                                    * hashMap.get(histPlayerI).getWeight(new Pair<>(state,histPlayerNegI))
                                    * (float) (1.0 / this.posg.getActions(player + 1 % 2).size());
                        }
                    }
                }
            }
        }
        return res;
    }

    private void makeSureDistributionsAreCalculated(SigmaOccupancyState<State,Action,Observation> s) throws Exception {
        if (s.distribMarginaleP1 == null) {
            s.ComputeSigmaMarginal(1);
        }
        if (s.distribMarginaleP2 == null) {
            s.ComputeSigmaMarginal(2);
        }
        if (s.distribConditionnelleP1 == null) {
            s.ComputeSigmaCondition(1);
        }
        if (s.distribConditionnelleP2 == null) {
            s.ComputeSigmaCondition(2);
        }
    }

    public double getNorm(HashMap<History<Action,Observation>, Distribution<History<Action,Observation>>> distrib,
                          HashMap<History<Action,Observation>, Distribution<History<Action,Observation>>> distribPrime,
                          History<Action,Observation> history, int timestep) {
        if (timestep == 0){
            return 0.0;
        }
        double res = 0.0;
        Distribution<History<Action, Observation>> distribCond = distrib.get(history);
        Distribution<History<Action, Observation>> distribCondPrime = distribPrime.get(history);


        if (distribCond != null && distribCondPrime != null) {
            for (History<Action, Observation> h : distribCond.getNonZeroElements()) {
                res += Math.abs(distribCond.getWeight(h) - distribCondPrime.getWeight(h));
            }
            for (History<Action, Observation> h : distribCondPrime.getNonZeroElements()) {
                if (!distribCond.getNonZeroElements().contains(h)) {
                    res += distribCondPrime.getWeight(h);
                }
            }
            return res;
        }
        else{
            return 1.0;
        }
    }


    public Vector<State,Action,Observation>
    update(SigmaOccupancyState<State, Action, Observation> lastSigmaOccupancyState,
           SigmaOccupancyState<State, Action, Observation> s,
           Vector<State, Action, Observation> vMajorant)
            throws Exception {

        Vector<State,Action,Observation> vector = new Vector<>();
        HashMap<History<Action,Observation>, Double> hashMap = new HashMap<>();

        if (this instanceof VectorMajorantApproximation) {
            for (History<Action, Observation> h : s.o.getLocalHistory(1)) {
                double value;
                value = vMajorant.MapVectorValues.get(h);
                hashMap.put(h, value);
            }
        }
        else{
            for (History<Action, Observation> h : s.o.getLocalHistory(2)) {
                double value;
                value = vMajorant.MapVectorValues.get(h);
                hashMap.put(h, value);
            }
        }
        vector.MapVectorValues = hashMap;
        vector.s = new SigmaOccupancyState<>(s);
        vector.lastSigmaOccupancyState = lastSigmaOccupancyState;
        if (vector.isStrategyRandom) {
            vector.strat = this.describeRandomStrategy(vector);

        }
        if (this.vectorList.get(s.o.getTimestep()) == null){
            this.vectorList.put(s.o.getTimestep(), new ArrayList<>());
        }
        if (this instanceof  VectorMajorantApproximation) {
        }
        if (this instanceof  VectorMinorantApproximation) {
        }
        this.vectorList.get(s.o.getTimestep()).add(vector);
        return vector;
    }
}