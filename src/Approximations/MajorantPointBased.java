package Approximations;

import posg.OccupancyState;

import java.util.HashMap;
import java.util.Map;

public class MajorantPointBased<State,Action,Observation> extends ApproximatorPB<State,Action,Observation>{

    public double rMax;
    int depth;

    public MajorantPointBased(double lambda,double gamma,double rmax){
        this.lambda = lambda;
        this.rMax = rmax;
    }

    public void initializeB0(OccupancyState<State,Action,Observation> o, int depth){
        HashMap<OccupancyState<State,Action,Observation>,Double> h = new HashMap<>();
        h.put(o, rMax*depth);

        this.setOfValues.put(0,h);
    }


    @Override
    public double getLipschitzApproximationValue(double lambda, OccupancyState<State,Action,Observation> o, int timestep) {
        double min = 10000;
        double val = 0.0;
        if (!(this.setOfValues.get(timestep) == null)) {

            for (Map.Entry mapentry : this.setOfValues.get(timestep).entrySet()) {


                val = (double) mapentry.getValue() + lambda * o.getNorme1((OccupancyState<State, Action, Observation>) mapentry.getKey());

                if (val < min) {
                    min = val;
                }
            }


            if (min > (depth-timestep)*rMax){
                return (depth-timestep)*rMax;
            }
            return min;
        }
        else{
            if (timestep == this.depth){

                return 0.0;
            }
            else {


                return (depth-timestep)*rMax;
            }
        }
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public double getMaxValue(int t) {
            double max = -100000.0;
            for (OccupancyState<State,Action,Observation> o : this.setOfValues.get(t).keySet()){
                if (this.setOfValues.get(t).get(o)>max){
                    max = this.setOfValues.get(t).get(o);
                }
            }
            return max;
    }
}
