package Approximations;

import posg.OccupancyState;
import java.util.HashMap;

public abstract class ApproximatorPB<State,Action,Observation> {

    public HashMap<Integer,HashMap<OccupancyState<State,Action,Observation>, Double>> setOfValues = new HashMap<>();
    public double lambda;
    public double gamma;

    public void initialize(OccupancyState<State,Action,Observation> initialOccupancyState, double value){

        HashMap<OccupancyState<State,Action,Observation>,Double> hashMap = new HashMap<>();
        hashMap.put(initialOccupancyState,value);
        this.setOfValues.put(0, new HashMap<>(hashMap));
    }


    public void update(OccupancyState<State,Action,Observation> o, double d, int timestep) throws Exception {

        if (this.setOfValues.get(timestep)==null){

            HashMap<OccupancyState<State,Action,Observation>,Double> h = new HashMap<>();
            h.put(o.copy(),d);
            this.setOfValues.put(timestep,new HashMap<>(h));
        }
        else {
            this.setOfValues.get(timestep).put(o.copy(), d);

        }

    }


    public double getExactValue(OccupancyState<State,Action,Observation> o, int timestep) throws Exception {
        return this.setOfValues.get(timestep).get(o);
    }

    public double getLipschitzApproximationValue(double lambda, OccupancyState<State,Action,Observation> o, int timestep){

        return 0.0;
    }

    public double getValue(OccupancyState<State,Action,Observation> o, int timestep) throws Exception {

        if (!(this.setOfValues.get(timestep) == null)) {
            if (this.setOfValues.get(timestep).containsKey(o)) {


                return this.getExactValue(o, timestep);
            } else {
                return getLipschitzApproximationValue(this.lambda, o, timestep);
            }
        }
        else{

            return this.getLipschitzApproximationValue(lambda,o,timestep);
        }
    }

}
