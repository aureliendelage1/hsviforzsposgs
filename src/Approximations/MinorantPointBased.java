package Approximations;

import posg.OccupancyState;

import java.util.HashMap;
import java.util.Map;

public class MinorantPointBased<State,Action,Observation> extends ApproximatorPB<State,Action,Observation>{

    public double rMin;
    private int depth;

    public MinorantPointBased(double lambda,double gamma,double rmin){
        this.lambda = lambda;
        this.rMin = rmin;
    }

    public void initializeB0(OccupancyState<State,Action,Observation> o,int depth){
        HashMap<OccupancyState<State,Action,Observation>,Double> h = new HashMap<>();
        h.put(o, rMin*depth);

        this.setOfValues.put(0,h);
    }

    @Override
    public double getLipschitzApproximationValue(double lambda, OccupancyState<State,Action,Observation> o, int timestep) {
        double min = -10000;
        double val = 10000;
        if (!(this.setOfValues.get(timestep) == null)) {

            for (Map.Entry mapentry : this.setOfValues.get(timestep).entrySet()) {
                val = (double) mapentry.getValue() - lambda* o.getNorme1((OccupancyState<State, Action, Observation>) mapentry.getKey());


                if (val > min) {
                    min = val;
                }
            }
            if (min > (depth-timestep)*rMin){

                return min;
            }

            return (depth-timestep)*rMin;
        }
        else{
            if (timestep == this.depth){

                return 0.0;
            }
            else {

                return (depth-timestep)*rMin;

            }
        }
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public double getMinValue(int t) {
        double min = 100000.0;
        for (OccupancyState<State,Action,Observation> o : this.setOfValues.get(t).keySet()){
            if (this.setOfValues.get(t).get(o)<min){
                min = this.setOfValues.get(t).get(o);
            }
        }
        return min;
    }
}
