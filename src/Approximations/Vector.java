package Approximations;

import Mdp.MdpHSVISolver;
import Mdp.MdpHSVISolverMixtedStrat;
import posg.*;
import util.Distribution;

import java.util.*;

public class Vector<State,Action,Observation> {
    public SigmaOccupancyState<State, Action, Observation> s;
    public HashMap<History<Action, Observation>, Double> MapVectorValues;
    private boolean voidVector;
    private Posg<State,Action,Observation> posg;

    public HashMap<Pair<History<Action,Observation>,History<Action,Observation>>, Double> realizationWeights = new HashMap<>();
    public HashMap<DecisionRule<State,Action,Observation>, Double> mixtedStrategy = new HashMap<>();
    public SigmaOccupancyState<State,Action,Observation> lastSigmaOccupancyState;

    public HashMap<Integer,BehavioralStrategy<State,Action,Observation>> strat;

    public boolean isStrategyRandom = false;

    public Vector(SigmaOccupancyState<State, Action, Observation> lastSigmaOccupancyState, SigmaOccupancyState<State, Action, Observation> s, HashMap<History<Action, Observation>, Double> MapVectorValues, Posg<State,Action,Observation> posg ){
        this.s = s;
        this.MapVectorValues = MapVectorValues;
        this.voidVector = false;
        this.posg = posg;
        this.lastSigmaOccupancyState = lastSigmaOccupancyState;
    }

    public Vector(SigmaOccupancyState<State, Action, Observation> s, int a, int player, Posg<State,Action,Observation> posg) throws Exception {
        this.posg = posg;
        HashMap<History<Action, Observation>, Double> hashMap = new HashMap<>();
        for (History<Action, Observation> h : s.o.getLocalHistory(player)) {
            for (Action aNext: this.posg.getActionsJ1()){
                for (Observation oNext : this.posg.getObservations(player)){
                    History<Action,Observation> hprime = new History<>();
                    hprime.setListeActionObservation(h.getListeActionObservation());
                    hprime.addActionObservation(aNext,oNext);
                    hashMap.put(hprime, 0.0);
                }
            }
        }
        this.MapVectorValues = hashMap;

        this.lastSigmaOccupancyState = s;
    }

    public Vector(){
        this.voidVector = true;
    }

    public double getValue(int player) throws Exception {
        double res = 0.0;
        Distribution<History<Action,Observation>> distribMarginale = s.getDistribMarginale(player);
        HashMap<History<Action, Observation>, Double> hashmap = this.MapVectorValues;

        for (Map.Entry<History<Action,Observation>, Double> m: hashmap.entrySet()){
            res += distribMarginale.getWeight(m.getKey()) * m.getValue();
        }

        return res;
    }

    @Override
    public String toString() {
        return "Vector: " + this.MapVectorValues.toString() + " -- strat: " + this.strat + " realization weights : " + this.realizationWeights + " s : " + s.o;

    }

    @Override
    public boolean equals(Object v){


        Vector<State,Action,Observation> vprime = new Vector<>();
        try{
            vprime = (Vector<State, Action, Observation>) v;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        if (!this.MapVectorValues.equals(vprime.MapVectorValues)){

            return false;
        }
        if (!this.s.equals(vprime.s)){

            return false;
        }

        if (!this.lastSigmaOccupancyState.equals(vprime.lastSigmaOccupancyState)){

            return false;
        }


        if (!(this.isStrategyRandom == vprime.isStrategyRandom)){

            return false;
        }
        return true;
    }

    public double get(History<Action, Observation> nextHistory, int timestep, int player, Posg<State,Action,Observation> posg,
                      SigmaOccupancyState<State,Action,Observation> s, SigmaOccupancyState<State,Action,Observation> nextSigma,
                      BehavioralStrategy<State,Action,Observation> strategy) throws Exception {

        if (this.MapVectorValues.containsKey(nextHistory)){

            return this.MapVectorValues.get(nextHistory);
        }
        if (nextSigma.o.getListCompressedHistories(player).containsKey(nextHistory)){
            return this.MapVectorValues.get(nextSigma.o.getListCompressedHistories(player).get(nextHistory));
        }
        if (!posg.mdpHeuristic || true){

            if (player % 2 == 1) {
                return posg.maxReward*(posg.profondeurMax - timestep);

            }
            if (player%2==0) {


                return posg.minReward*(posg.profondeurMax - timestep);

            }
            return -Double.POSITIVE_INFINITY;
        }
        else {

            double res = 0.0;
            Action aPlayerI = nextHistory.getLastAction();
            Observation zPlayerI = nextHistory.getLastObservation();
            PomdpSolver.Belief<State, Action, Observation> belief = s.getDistribCondWithState(posg, player, nextHistory.getlastHistory(), s.o.timestep);
            PomdpSolver.Belief<State, Action, Observation> newBelief = belief.getNewBelief(aPlayerI, zPlayerI, strategy);


            for (Pair<State, History<Action, Observation>> initialMdpState : newBelief.getBelief().getNonZeroElements()) {
                if (timestep != posg.profondeurMax-1) {
                    MixtedStrategy<State, Action, Observation> mixedStrat = new MixtedStrategy<State, Action, Observation>(posg, this.realizationWeights, initialMdpState.getElement1(), player + 1 % 2);

                    System.out.println("strategy for mdp b4 : " + mixedStrat.behavioralStrat);
                    mixedStrat.behavioralStrat.posg = posg;
                    Mdp.MdpHSVISolver<State, Action, Observation> mdpHSVISolver = new MdpHSVISolver<State, Action, Observation>(mixedStrat.behavioralStrat.toIntBehavioral(), (posg.profondeurMax - timestep + 1), posg, posg.epsilon, player%2, initialMdpState, timestep);


                    res += newBelief.getBelief().getWeight(initialMdpState) * mdpHSVISolver.solve();
                }
                else{
                    Mdp.MdpHSVISolver<State, Action, Observation> mdpHSVISolver = new MdpHSVISolver<State, Action, Observation>(strat, (posg.profondeurMax - timestep + 1), posg, posg.epsilon, player%2, initialMdpState, timestep);
                    res += newBelief.getBelief().getWeight(initialMdpState) * mdpHSVISolver.solve();
                }
            }
            this.MapVectorValues.put(nextHistory,res);
            return res;
        }
    }

    public HashMap<Integer,BehavioralStrategy<State, Action, Observation>> getStrat() {
        return strat;
    }

    public void setStrat(HashMap<Integer,BehavioralStrategy<State, Action, Observation>> strat) {
        this.strat = strat;
    }

    public HashMap<DecisionRule<State,Action,Observation>,Double> cleanMixtedStrategies(
            HashMap<DecisionRule<State, Action, Observation>, Double> mixtedStrategy)
    {
        HashMap<DecisionRule<State,Action,Observation>,Double> res = new HashMap<>();

        for (DecisionRule<State,Action,Observation> d : this.mixtedStrategy.keySet()){
            if (this.mixtedStrategy.get(d)>0.0){
                res.put(d,this.mixtedStrategy.get(d));
            }
        }
        return res;
    }

    public void decompressRW(int player) {


        HashMap<Pair<History<Action,Observation>,History<Action,Observation>>,Double> listToAdd = new HashMap<>();
        for (History<Action,Observation> history : this.s.o.getListCompressedHistories(player).keySet()){
            for (Pair<History<Action,Observation>,History<Action,Observation>> p : this.realizationWeights.keySet()) {
                if (p.getElement0().equals(this.s.o.getListCompressedHistories(player).get(history))) {
                    History<Action,Observation> suffix = p.getElement1();
                    listToAdd.put(new Pair<>(history, suffix), this.realizationWeights.get(new Pair<>(this.s.o.getListCompressedHistories(player).get(history), suffix)));
                }
            }
        }
        for (History<Action,Observation> history : this.s.o.getReversedListCompressedHistories(player).keySet()){
            for (Pair<History<Action,Observation>,History<Action,Observation>> p : this.realizationWeights.keySet()) {
                if (p.getElement0().equals(history)) {
                    History<Action,Observation> suffix = p.getElement1();
                    listToAdd.put(new Pair<>(this.s.o.getReversedListCompressedHistories(player).get(history), suffix),
                            this.realizationWeights.get(p));
                }
            }
        }

        this.realizationWeights.putAll(listToAdd);
    }
}
