package Approximations;

import AlgorithmForLpSolving.GameMatrix;
import posg.*;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class ApproximatorW<State,Action,Observation> {
    Posg<State,Action,Observation> posg;
    GameMatrix<State,Action,Observation> gameMatrix;

    public HashMap<Integer, ArrayList<Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>>>
            bagW = new HashMap<>();

    public GameMatrix<State,Action,Observation> getGameMatrix(){
        return this.gameMatrix;
    }

    public void update(int timestep,
		       SigmaOccupancyState<State,Action,Observation> s,
		       BehavioralStrategy<State,Action,Observation> strat,
		       Vector<State, Action, Observation> v){


        if (this.bagW.get(timestep) != null) {
            this.bagW.get(timestep).add(new Pair< SigmaOccupancyState<State, Action, Observation>,
					Pair< BehavioralStrategy<State, Action, Observation>,
					Vector<State, Action, Observation> > >(s,
									       new Pair<BehavioralStrategy<State, Action, Observation>, Vector<State, Action, Observation>>(strat, v)
									       )
					);
        }
        else{
            ArrayList<Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>> list = new ArrayList<>();
            this.bagW.put(timestep, list);
            this.bagW.get(timestep).add(new Pair< SigmaOccupancyState<State, Action, Observation>,
					Pair< BehavioralStrategy<State, Action, Observation>,
					Vector<State, Action, Observation> > >(s,
									       new Pair<BehavioralStrategy<State, Action, Observation>, Vector<State, Action, Observation>>(strat, v)));
        }

    }

    @Override
    public String toString() {
        return this.bagW.toString();
    }

    public void initialize(VectorApproximation<State,Action,Observation> vectorApproximation, OccupancyState<State,Action,Observation> initialOccupancyState) throws Exception {
        int player = -1;
        if (this instanceof ApproximatorWMajorant){
            player = 1;
        }
        else if (this instanceof ApproximatorWMinorant){
            player = 2;
        }
        else{
            throw new Exception("cannot determine type");
        }
        OccupancyState<State,Action,Observation> oprime = initialOccupancyState.copy();
        BehavioralStrategy<State,Action,Observation> stratPlayerNegI = new BehavioralStrategy<>(oprime,(player+1)%2,posg);
        stratPlayerNegI.generateRandomDistribution(oprime);
        BehavioralStrategy<State,Action,Observation> stratPlayerI = new BehavioralStrategy<>(oprime,player,posg);
        stratPlayerI.generateRandomDistribution(oprime);

        for (int timestep = 0;timestep<this.posg.profondeurMax-1;timestep++){
            this.update(timestep,new SigmaOccupancyState<>(oprime.copy()),stratPlayerNegI,vectorApproximation.getListVector(timestep+1).get(0));
            if (player%2==1) {
                if (timestep == 0) {
                    oprime.initialupdate(stratPlayerI.getStrategy(), stratPlayerNegI.getStrategy());
                }
                else{
                    oprime.update(stratPlayerI.getStrategy(), stratPlayerNegI.getStrategy());
                }
            }
            else{
                if (timestep == 0) {
                    oprime.initialupdate(stratPlayerNegI.getStrategy(), stratPlayerI.getStrategy());
                }
                else{
                    oprime.update(stratPlayerNegI.getStrategy(), stratPlayerI.getStrategy());
                }
            }
            stratPlayerNegI = new BehavioralStrategy<>(oprime,(player+1)%2,posg);
            stratPlayerI = new BehavioralStrategy<>(oprime,player,posg);
            stratPlayerI.generateRandomDistribution(oprime);
            stratPlayerNegI.generateRandomDistribution(oprime);
        }

    }

    public double computeValueForLP(History<Action,Observation> historyPlayerI, Action actionPlayerI, int player, Posg<State,Action,Observation> posg,
                                    Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>,
                                            Vector<State,Action,Observation>>> triplet, SigmaOccupancyState<State,Action,Observation> sCurrent) throws Exception {
        double res = 0.0;
        sCurrent.makeSureDistributionsAreCalculated(player);
        HashMap<History<Action,Observation>, Double> mapAlreadyComptedValues = new HashMap<>();
        double rewardValue = this.computeReward(actionPlayerI,historyPlayerI,sCurrent,triplet.getElement1().getElement0(),player);
        double valueNextStep = 0.0;

        if (triplet.getElement0().getDistribMarginale(player).getNonZeroElements().contains(historyPlayerI)){
            valueNextStep = this.computeForNextStep(historyPlayerI,actionPlayerI,sCurrent,triplet.getElement1().getElement1(),
                    triplet.getElement1().getElement0(),player);
            mapAlreadyComptedValues.put(historyPlayerI,valueNextStep);
        }
        else {
            if (player % 2 == 1) {
                if (triplet.getElement0().o.listCompressedHistoriesP1.containsKey(historyPlayerI)) {


                    if (mapAlreadyComptedValues.containsKey(triplet.getElement0().o.listCompressedHistoriesP1.get(historyPlayerI))){
                        valueNextStep = mapAlreadyComptedValues.get(triplet.getElement0().o.listCompressedHistoriesP1.get(historyPlayerI));
                    }
                    else {
                        valueNextStep = this.computeForNextStep(triplet.getElement0().o.listCompressedHistoriesP1.get(historyPlayerI), actionPlayerI, sCurrent,triplet.getElement1().getElement1(), triplet.getElement1().getElement0(), player);
                    }

                }
                else {
                }
            }
            else {
                if (triplet.getElement0().o.listCompressedHistoriesP2.containsKey(historyPlayerI)) {


                    if (mapAlreadyComptedValues.containsKey(triplet.getElement0().o.listCompressedHistoriesP1.get(historyPlayerI))){
                        valueNextStep = mapAlreadyComptedValues.get(triplet.getElement0().o.listCompressedHistoriesP2.get(historyPlayerI));
                    }
                    else {
                        valueNextStep = this.computeForNextStep(triplet.getElement0().o.listCompressedHistoriesP2.get(historyPlayerI), actionPlayerI, sCurrent,
                                triplet.getElement1().getElement1(),triplet.getElement1().getElement0(), player);
                    }

                } else {
                }
            }
        }
        res = rewardValue + valueNextStep;


        if (player%2==1) {
            return res + this.posg.lambda.get(sCurrent.o.timestep)
                    * sCurrent.getConditionalNorm(triplet.getElement0(),triplet.getElement1().getElement0(), player)
                    * sCurrent.getMarginalValue(historyPlayerI,player);
        }
        else{
            return res - this.posg.lambda.get(sCurrent.o.timestep)
                    * sCurrent.getConditionalNorm(triplet.getElement0(), triplet.getElement1().getElement0(), player)
                    * sCurrent.getMarginalValue(historyPlayerI,player);
        }
    }

    public double computeValueForPruning(History<Action,Observation> h, Action a, Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>,
            Vector<State,Action,Observation>>> triplet, History<Action,Observation> hNegI, int player) throws Exception {
        double res;
        double reward = 0.0;
        Distribution<State> distrib = new Distribution<>();
        distrib = triplet.getElement0().getStateDistrib(posg,new JointHistory<>(h,hNegI,player));
        if (triplet.getElement1().getElement0().getStrategy().containsKey(hNegI)) {
            for (State s : this.posg.getStates()) {
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    reward += this.posg.rewards.getReward(s, new JointAction(a, aNegI, player), this.posg)
                            * triplet.getElement1().getElement0().getStrategy().get(hNegI).getWeight(aNegI)
                            * distrib.getWeight(s);
                }
            }
        }
        double valueNextStep = this.computeForNextStep(h,a,distrib,hNegI,triplet,player);

        res = reward + valueNextStep;
        return res;
    }

    private double computeForNextStep(History<Action,Observation> histPlayerI, Action a, Distribution<State> d , History<Action,Observation> histPlayerNegI , Pair<SigmaOccupancyState<State,Action,Observation>, Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>> objet, int player) throws Exception {
        double res = 0.0;

        Vector<State,Action,Observation> vNextStep = objet.getElement1().getElement1();
        for (Observation oPI : this.posg.getObservations(player)){
            History<Action, Observation> nextHistory = new History<>();
            nextHistory.setListeActionObservation(histPlayerI.getListeActionObservation());
            nextHistory.addActionObservation(a, oPI);
            for (State state : this.posg.getStates()) {

                    for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                        for (Observation oPNegI : this.posg.getObservations(player + 1 % 2)) {
                            for (State nextState : this.posg.getStates()) {
                                if (objet.getElement1().getElement0().getStrategy().containsKey(histPlayerNegI)) {
                                    if (d.getWeight(state)
                                            * objet.getElement1().getElement0().getStrategy().get(histPlayerNegI).getWeight(aNegI)
                                            * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                            * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState) > 0.0) {

                                        res += d.getWeight(state)
                                                * objet.getElement1().getElement0().getStrategy().get(histPlayerNegI).getWeight(aNegI)
                                                * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                                * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState)
                                                * vNextStep.get(nextHistory, objet.getElement0().o.timestep + 1, player, posg, objet.getElement0(), vNextStep.s,
                                                objet.getElement1().getElement0());

                                    }
                                }
                                else{
                                    res += d.getWeight(state)
                                            * (float)(1.0/this.posg.getActions(player+1%2).size())
                                            * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                            * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState)
                                            * vNextStep.get(nextHistory, objet.getElement0().o.timestep + 1, player, posg, objet.getElement0(), vNextStep.s,
                                            objet.getElement1().getElement0());
                                }
                            }
                        }
                    }
                }
            }

        return res;
    }


    private double computeForNextStep(History<Action,Observation> histPlayerI, Action a, SigmaOccupancyState<State,Action,Observation> sCurrent, Vector<State,Action,Observation> vNextStep,
            BehavioralStrategy<State,Action,Observation> strat, int player) throws Exception {
        double res = 0.0;


        for (Observation oPI : this.posg.getObservations(player)){
            History<Action, Observation> nextHistory = new History<>();
            nextHistory.setListeActionObservation(histPlayerI.getListeActionObservation());
            nextHistory.addActionObservation(a, oPI);
            for (State state : this.posg.getStates()) {
                for (History<Action, Observation> histPlayerNegI : sCurrent.o.getLocalHistory(player + 1 % 2)) {
                    for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                        for (Observation oPNegI : this.posg.getObservations(player + 1 % 2)) {
                            for (State nextState : this.posg.getStates()) {
                                if (strat.getStrategy().containsKey(histPlayerNegI) && sCurrent.o.getProba(new Pair<>(state, new JointHistory<>(histPlayerI, histPlayerNegI, player)))
                                        * strat.getStrategy().get(histPlayerNegI).getWeight(aNegI)
                                        * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                        * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState) > 0.0) {

                                    res += sCurrent.o.getProba(new Pair<>(state, new JointHistory<>(histPlayerI, histPlayerNegI, player)))
                                            * strat.getStrategy().get(histPlayerNegI).getWeight(aNegI)
                                            * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                            * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState)
                                            * vNextStep.get(nextHistory, sCurrent.o.timestep + 1, player, posg, sCurrent,vNextStep.s,strat
                                            );

                                }
                                else
                                {
                                    if (!strat.getStrategy().containsKey(histPlayerNegI)){
                                        if (strat.getStrategy().containsKey(histPlayerNegI) && sCurrent.o.getProba(new Pair<>(state, new JointHistory<>(histPlayerI, histPlayerNegI, player)))
                                                * (float) (1.0 / this.posg.getActions(player + 1 % 2).size())
                                                * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                                * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState) > 0.0) {
                                            res += sCurrent.o.getProba(new Pair<>(state, new JointHistory<>(histPlayerI, histPlayerNegI, player)))
                                                    * (float) (1.0 / this.posg.getActions(player + 1 % 2).size())
                                                    * posg.observationsProbabilities.getProba(new JointAction(a, aNegI, player), nextState, new JointObservation(oPI, oPNegI, player))
                                                    * posg.transitions.getProbability(state, new JointAction(a, aNegI, player), nextState)
                                                    * vNextStep.get(nextHistory, sCurrent.o.timestep + 1, player, posg, sCurrent, vNextStep.s, strat);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return res;
    }


    private double computeReward(Action a, History<Action,Observation> histPlayerI, SigmaOccupancyState<State,Action,Observation> s, BehavioralStrategy<State,Action,Observation> element0,int player) throws Exception {
        double res = 0.0;
        if (histPlayerI.toString().equals("(,)")){
            for (State state : this.posg.getStates()) {
                for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                    res += s.o.InitialBelief.getWeight(state)
                            * this.posg.rewards.getReward(state, new JointAction(a, aNegI, player), posg)
                            * element0.getStrategy().get(histPlayerI).getWeight(aNegI);
                }
            }
        }
        else {
            for (State state : this.posg.getStates()) {
                for (History<Action, Observation> histPlayerNegI : s.o.getLocalHistory(player + 1 % 2)) {
                    if (element0.getStrategy().containsKey(histPlayerNegI)) {
                        for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                            res += this.posg.rewards.getReward(state, new JointAction<Action>(a, aNegI, player), posg)
                                    * s.o.getProba(new Pair<>(state,new JointHistory<>(histPlayerI,histPlayerNegI,player)))
                                    * element0.getStrategy().get(histPlayerNegI).getWeight(aNegI);
                        }
                    } else {
                        for (Action aNegI : this.posg.getActions(player + 1 % 2)) {
                            res += this.posg.rewards.getReward(state, new JointAction<Action>(a, aNegI, player), posg)
                                    * s.o.getProba(new Pair<>(state,new JointHistory<>(histPlayerI,histPlayerNegI,player)))
                                    * (float) (1.0 / this.posg.getActions(player + 1 % 2).size());
                        }
                    }
                }
            }
        }
        return res;
    }

    public double computeValueForLP(History<Action, Observation> h, Action a, int player, Posg<State, Action, Observation> posg, SigmaOccupancyState<State, Action, Observation> sigmaOccupancyState) throws Exception {

        if (player%2==1){

            return posg.maxReward*(posg.profondeurMax - sigmaOccupancyState.o.timestep);
        }
        if (player%2==0){
            return posg.minReward*(posg.profondeurMax - sigmaOccupancyState.o.timestep);
        }

        sigmaOccupancyState.makeSureDistributionsAreCalculated(player);
        PomdpSolverNotDynamic.Belief<State,Action,Observation> b = sigmaOccupancyState.getStateDistrib(posg,player,h,sigmaOccupancyState.o.timestep);
        try {
            sigmaOccupancyState.o.sanityCheck(1);
        }
        catch(Exception e ){
            e.printStackTrace();
        }
        return this.posg.computeRMaxMin(player,sigmaOccupancyState.getStateDistrib(posg,player,h,sigmaOccupancyState.o.timestep));
    }

}
