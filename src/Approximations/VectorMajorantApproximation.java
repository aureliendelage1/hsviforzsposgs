package Approximations;


import posg.History;
import posg.Posg;
import posg.SigmaOccupancyState;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VectorMajorantApproximation<State,Action,Observation> extends VectorApproximation<State,Action,Observation>{

    private SigmaOccupancyState<State,Action,Observation> lastSigmaOccupancyState;

    public VectorMajorantApproximation(int depth, HashMap<Integer,Double> lambda, SigmaOccupancyState<State,Action,Observation> s, Posg<State,Action,Observation> posg){
        this.depth = depth;
        this.lambda = lambda;
        this.lastSigmaOccupancyState = s;
        this.posg = posg;
        this.rMaxMin = this.posg.computeMaxReward();
    }


    public void setrMax(double rMax) {
        this.rMaxMin = rMax;
    }

    public double getValue(SigmaOccupancyState<State,Action,Observation> s) throws Exception {
        Vector<State,Action,Observation> v = new Vector<>();
        s.makeSureDistributionsAreCalculated(1);
        double val = 100000.0;
        int timestep = s.o.getTimestep();
        if (vectorList.get(timestep) == null){


            return posg.maxReward*(depth - timestep);
        }
        else {
            ArrayList<Vector<State,Action,Observation>> listOfVectors = vectorList.get(timestep);
            for (Vector<State,Action,Observation> vector : listOfVectors){

                double tmpVal = 0.0;
                Distribution<History<Action,Observation>> distribMarginaleP1 = vector.s.getDistribMarginale(1);
                HashMap<History<Action, Observation>, Double> hashmap = vector.MapVectorValues;

                for (Map.Entry<History<Action,Observation>, Double> m: hashmap.entrySet()){


                    tmpVal += distribMarginaleP1.getWeight(m.getKey()) * m.getValue();
                }
                tmpVal += lambda.get(timestep) * s.getNorme1(vector.s);

                if (tmpVal<val){
                    v = vector;
                    val = tmpVal;
                }

            }

            return val;
        }
    }

}
