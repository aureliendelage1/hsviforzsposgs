package Approximations;


import posg.History;
import posg.Posg;
import posg.SigmaOccupancyState;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VectorMinorantApproximation<State,Action,Observation> extends VectorApproximation<State,Action,Observation>{

    private SigmaOccupancyState<State,Action,Observation> lastSigmaOccupancyState;

    public void setrMin(double rMin) {
        this.rMaxMin = rMin;
    }

    public VectorMinorantApproximation(int depth, HashMap<Integer,Double> lambda, SigmaOccupancyState<State,Action,Observation> s, Posg<State,Action,Observation> posg){
        this.depth = depth;
        this.lambda = lambda;
        this.lastSigmaOccupancyState = s;
        this.posg = posg;
        this.rMaxMin = this.posg.computeMaxReward();
    }

    public double getValue(SigmaOccupancyState<State,Action,Observation> s) throws Exception {
        Vector<State,Action,Observation> v = new Vector<>();
        s.makeSureDistributionsAreCalculated(2);

        double val = -100000.0;
        int timestep = s.o.getTimestep();
        if (vectorList.get(timestep) == null){


            return posg.minReward*(depth - timestep);
        }
        else {
            ArrayList<Vector<State,Action,Observation>> listOfVectors = vectorList.get(timestep);
            for (Vector<State,Action,Observation> vector : listOfVectors){
                double tmpVal = 0.0;
                Distribution<History<Action,Observation>> distribMarginaleP2 = vector.s.getDistribMarginale(2);
                HashMap<History<Action, Observation>, Double> hashmap = vector.MapVectorValues;
                for (Map.Entry<History<Action,Observation>, Double> m: hashmap.entrySet()){
                    tmpVal += distribMarginaleP2.getWeight(m.getKey()) * m.getValue();
                }
                tmpVal -= lambda.get(timestep) * s.getNorme1(vector.s);
                if (tmpVal>val){
                    v = vector;
                    val = tmpVal;
                }
            }

            return val;
        }
    }

}
