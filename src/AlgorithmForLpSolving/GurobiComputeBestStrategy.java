package AlgorithmForLpSolving;


import Approximations.ApproximatorW;
import Approximations.Vector;
import gurobi.*;
import posg.*;
import util.Distribution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import java.text.DecimalFormat;
import java.text.NumberFormat;


public class    GurobiComputeBestStrategy<State,Action,Observation> {
    private static final long serialVersionUID = 1L;
    Posg<State,Action,Observation> posg;
    OccupancyState<State,Action,Observation> o ;
    ApproximatorW<State,Action,Observation> W;

    public HashMap<Pair<Pair<History<Action,Observation>,Action>,Pair<SigmaOccupancyState<State,Action,Observation>,
            Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>>,Double> matrixP1 = new HashMap<>();
    public HashMap<Pair<Pair<History<Action,Observation>,Action>,Pair<SigmaOccupancyState<State,Action,Observation>,
            Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>>,Double> matrixP2 = new HashMap<>();
    public GurobiComputeBestStrategy(Posg<State,Action,Observation> posg, OccupancyState<State,Action,Observation> o, ApproximatorW<State,Action,Observation> W) {
        this.posg = posg;
        this.o = o;
        this.W = W;
    }
    public void printSolution(double[] solution, int size, int sizeHistoryP2) throws Exception {
        System.out.println("--------CplexSolverForPosgDirect::Printing solutions");
        if (solution != null) {

            double max = 0.0;
            for (int i = 0;i<sizeHistoryP2;i++) {
                max += solution[size+i]*this.getProbaIndividualHistory(i);


            }
            System.out.println("CplexSolverForPosgDirect::Opt: " + max);
            for (int j = 0; j < solution.length; j++) {
                System.out.print(solution[j] + " ");
            }
            System.out.println("");
        }
        System.out.println("--------CplexSolverForPosgDirect::End printing solutions");
    }

    public Pair<HashMap<History<Action,Observation>,Distribution<Action>>,ArrayList<Double>> getActionMax() throws Exception {

        int nActionsMax = posg.getActionsJ1().size();


        int sizeHistoryP1 = o.getLocalHistoryJ1().size();

        HashSet<History<Action,Observation>> listlocalHistory = o.getLocalHistory(1);
        if (sizeHistoryP1 == 0){
            sizeHistoryP1 = 1;
            listlocalHistory.add(new History<>());

        }
        try {
            GRBEnv env = new GRBEnv();
            env.set("OutputFlag", "0");
            GRBModel gurobiModel = new GRBModel(env);
            GRBVar[] var = new GRBVar[nActionsMax*sizeHistoryP1 + 1];

            for(int i=0; i<nActionsMax*sizeHistoryP1; i++) {
                var[i] = gurobiModel.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "beta"+i);
            }


            var[nActionsMax*sizeHistoryP1] = gurobiModel.addVar(-GRB.INFINITY,GRB.INFINITY,0.0,GRB.CONTINUOUS,"v");


            GRBLinExpr obj = new GRBLinExpr();
            obj.addTerm(1.0, var[nActionsMax*sizeHistoryP1]);
            gurobiModel.setObjective(obj,GRB.MAXIMIZE);
            for (int j = 0; j < sizeHistoryP1;j++) {


                GRBLinExpr qexpr = new GRBLinExpr();
                for (int i = 0; i < nActionsMax; i++) {
                    qexpr.addTerm(1.0,var[j*nActionsMax+i]);


                }
                gurobiModel.addConstr(qexpr,GRB.LESS_EQUAL,1.0,"inf," + j);
                gurobiModel.addConstr(qexpr,GRB.GREATER_EQUAL,1.0,"sup," + j);
            }


	    int numberOfconstraint = 0;

            SigmaOccupancyState<State,Action,Observation> sigmaOccupancyState = new SigmaOccupancyState<>(o);
            if (this.W.bagW.get(o.getTimestep()) != null){

                for (Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                        triplet : this.W.bagW.get(o.getTimestep())){


                    GRBLinExpr expr = new GRBLinExpr();


                    int actionCounter = 0;
                    int HistoryCounter = 0;

                    for (History<Action,Observation> h : listlocalHistory){

                        actionCounter = 0;
                        for (Action a : this.posg.getActions(1)){


                            double valueForLp = W.computeValueForLP(h,a,1, posg, triplet, sigmaOccupancyState);
                            expr.addTerm(valueForLp,var[HistoryCounter*nActionsMax + actionCounter]);


                            matrixP1.put(new Pair<>(new Pair<>(h,a),triplet),valueForLp);
                            actionCounter++;
                        }
                        HistoryCounter++;
                    }


                    expr.addTerm(-1.0,var[var.length-1]);


                    gurobiModel.addConstr(expr,GRB.GREATER_EQUAL,0.0,"D" +numberOfconstraint);
                    numberOfconstraint++;
                }
            } else {


                    GRBLinExpr expr = new GRBLinExpr();


                    int actionCounter = 0;
                    int HistoryCounter = 0;

                    for (History<Action,Observation> h : listlocalHistory){

                        actionCounter = 0;
                        for (Action a : this.posg.getActions(1)){


                            double valueForLp = W.computeValueForLP(h,a,1, posg, sigmaOccupancyState);
                            expr.addTerm(valueForLp,var[HistoryCounter*nActionsMax + actionCounter]);
                            actionCounter++;
                        }
                        HistoryCounter++;
                    }


                    expr.addTerm(-1.0,var[var.length-1]);


                    gurobiModel.addConstr(expr,GRB.GREATER_EQUAL,0.0,"D" +numberOfconstraint);
                    numberOfconstraint++;
            }


            gurobiModel.optimize();
	    gurobiModel.write("lpMax.lp");

            if ( true ) {
                HashMap<History<Action,Observation>, Distribution<Action>> hashMap = new HashMap<>();
                double[] solution = new double[nActionsMax*sizeHistoryP1+1];

                for (int i = 0; i < o.getLocalHistory(1).size(); i++) {
                    Distribution<Action> distirb = new Distribution<>();
                    for (int j = 0; j < this.posg.getActionsJ1().size(); j++) {
                        Action aMax = (Action) this.posg.getActionsJ1().toArray()[j];

                        double tmp = var[i*this.posg.getActionsJ1().size() + j].get(GRB.DoubleAttr.X);
                        distirb.addWeight(aMax,tmp);

                    }
                    hashMap.put((History<Action, Observation>) this.o.getLocalHistory(1).toArray()[i],distirb);
                }


                ArrayList<Double> listForDual = new ArrayList<>();
                GRBConstr[] constrForDual =  gurobiModel.getConstrs();

                for (int i = 0; i<constrForDual.length; i++) {
                    if (i>=2*sizeHistoryP1) {


                        listForDual.add(constrForDual[i].get(GRB.DoubleAttr.Pi));
                    }
                }
                gurobiModel.getEnv().dispose();

		return new Pair(hashMap, listForDual);

            } else {

                throw new Exception("no solution were found.");
            }


        }  catch (Exception e) {
            e.printStackTrace();
            System.err.println("Concert exception '" + e + "' caught");
        }

        return null;
    }

    public Pair<HashMap<History<Action,Observation>,Distribution<Action>>,ArrayList<Double>> getActionMin() throws Exception {

        int nActionsMax = posg.getActionsJ2().size();


        int sizeHistoryP1 = o.getLocalHistoryJ2().size();

        HashSet<History<Action,Observation>> listlocalHistory = o.getLocalHistory(2);
        if (sizeHistoryP1 == 0){
            sizeHistoryP1 = 1;
            listlocalHistory.add(new History<>());

        }


        try {

            GRBEnv env = new GRBEnv();
            env.set("OutputFlag", "0");
            GRBModel gurobiModel = new GRBModel(env);


            GRBVar[] var = new GRBVar[nActionsMax*sizeHistoryP1 + 1];

            for(int i=0; i<nActionsMax*sizeHistoryP1; i++) {
                var[i] = gurobiModel.addVar(0.0,1.0 ,0.0,GRB.CONTINUOUS,"beta"+i);
            }


            var[nActionsMax*sizeHistoryP1] = gurobiModel.addVar(-GRB.INFINITY,GRB.INFINITY,0.0,GRB.CONTINUOUS,"v");


            GRBLinExpr obj = new GRBLinExpr();
            obj.addTerm(1.0, var[nActionsMax*sizeHistoryP1]);
            gurobiModel.setObjective(obj,GRB.MINIMIZE);
            double[] lhs = new double[nActionsMax*sizeHistoryP1+1];
            double[] rhs = new double[nActionsMax*sizeHistoryP1+1];
            double[][] val = new double[nActionsMax*sizeHistoryP1+1][1];

            int[][] ind = new int[nActionsMax*sizeHistoryP1+1][1];

            for (int j = 0; j < sizeHistoryP1;j++) {


                GRBLinExpr qexpr = new GRBLinExpr();
                for (int i = 0; i < nActionsMax; i++) {
                    qexpr.addTerm(1.0,var[j*nActionsMax+i]);


                }


                gurobiModel.addConstr(qexpr,GRB.LESS_EQUAL,1.0,"inf," + j);
                gurobiModel.addConstr(qexpr,GRB.GREATER_EQUAL,1.0,"sup," + j);

            }

            int numberOfconstraint = 0;
            SigmaOccupancyState<State,Action,Observation> sigmaOccupancyState = new SigmaOccupancyState<>(o);
            if (this.W.bagW.get(o.getTimestep()) != null){
                for (Pair<SigmaOccupancyState<State,Action,Observation>,Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>
                        triplet : this.W.bagW.get(o.getTimestep())){
                    GRBLinExpr expr = new GRBLinExpr();


                    int actionCounter = 0;
                    int HistoryCounter = 0;

                    for (History<Action,Observation> h : listlocalHistory){

                        actionCounter = 0;
                        for (Action a : this.posg.getActions(2)){


                            double valueForLp = -W.computeValueForLP(h,a,2, posg, triplet, sigmaOccupancyState);
                            expr.addTerm(valueForLp,var[HistoryCounter*nActionsMax + actionCounter]);
                            this.matrixP2.put(new Pair<>(new Pair<>(h,a),triplet),-valueForLp);


                            actionCounter++;
                        }
                        HistoryCounter++;
                    }


                    expr.addTerm(1.0,var[var.length-1]);


                    gurobiModel.addConstr(expr,GRB.GREATER_EQUAL,0.0,"D" +numberOfconstraint);
                    numberOfconstraint++;
                }
            }
            else{
                GRBLinExpr expr = new GRBLinExpr();


                int actionCounter = 0;
                int HistoryCounter = 0;

                for (History<Action,Observation> h : listlocalHistory){

                    actionCounter = 0;
                    for (Action a : this.posg.getActions(2)){


                        expr.addTerm(-W.computeValueForLP(h,a,2, posg, sigmaOccupancyState),var[HistoryCounter*nActionsMax + actionCounter]);


                        actionCounter++;
                    }
                    HistoryCounter++;
                }


                expr.addTerm(1.0,var[var.length-1]);


                gurobiModel.addConstr(expr,GRB.GREATER_EQUAL,0.0,"D" +numberOfconstraint);
                numberOfconstraint++;
            }
            gurobiModel.optimize();
	    gurobiModel.write("lpMin.lp");


            if ( true ) {
                HashMap<History<Action,Observation>, Distribution<Action>> hashMap = new HashMap<>();
                double[] solution = new double[nActionsMax*sizeHistoryP1+1];

                for (int i = 0; i < o.getLocalHistory(2).size(); i++) {
                    Distribution<Action> distirb = new Distribution<>();
                    for (int j = 0; j < this.posg.getActionsJ2().size(); j++) {
                        Action aMax = (Action) this.posg.getActionsJ2().toArray()[j];

                        double tmp = var[i*this.posg.getActionsJ2().size() + j].get(GRB.DoubleAttr.X);
                        distirb.addWeight(aMax,tmp);

                    }
                    hashMap.put((History<Action, Observation>) this.o.getLocalHistory(2).toArray()[i],distirb);
                }


                ArrayList<Double> listForDual = new ArrayList<>();
                GRBConstr[] constrForDual =  gurobiModel.getConstrs();

                for (int i = 0; i<constrForDual.length; i++) {
                    if (i>=2*sizeHistoryP1) {


                        listForDual.add(constrForDual[i].get(GRB.DoubleAttr.Pi));
                    }
                }
                gurobiModel.getEnv().dispose();

		return new Pair(hashMap, listForDual);

            } else {

                throw new Exception("no solution were found.");
            }


        }  catch (Exception e) {
            e.printStackTrace();
            System.err.println("Concert exception '" + e + "' caught");
        }

        return null;
    }

    private String printTableau(double[][] val) {
        String res = "";
        for (int i = 0;i<val.length;i++){
            for (int j = 0;j<val[i].length;j++){
                res += "\ntab[" + i + "][" + j +"]" + " = " + val[i][j];
            }
        }
        return res;
    }


    public double getProbaIndividualHistory(int i) throws Exception {
        double res = 0.0;
        History<Action,Observation> historyP2 = (History<Action, Observation>) this.o.getLocalHistoryJ2().toArray()[i];
        for (State s : this.posg.getStates()){
            for (History<Action,Observation> hP1 : this.o.getLocalHistoryJ1()){
                res += o.getDistribution().getWeight(new Pair<State, JointHistory<Action,Observation>>(s,new JointHistory<Action,Observation>(hP1,historyP2)));
            }
        }
        return res;
    }

    public double getProbaIndividualHistoryMin(int i) throws Exception {
        double res = 0.0;
        History<Action,Observation> historyP1 = (History<Action, Observation>) this.o.getLocalHistoryJ1().toArray()[i];
        for (State s : this.posg.getStates()){
            for (History<Action,Observation> hP2 : this.o.getLocalHistoryJ2()){
                res += o.getDistribution().getWeight(new Pair<State, JointHistory<Action,Observation>>(s,new JointHistory<Action,Observation>(historyP1,hP2)));
            }
        }
        return res;
    }
}
