package AlgorithmForLpSolving;

import Approximations.Vector;
import posg.BehavioralStrategy;
import posg.History;
import posg.Pair;
import posg.SigmaOccupancyState;

import java.util.HashMap;

public class GameMatrix<State,Action,Observation> {
    public HashMap<Pair<Pair<History<Action,Observation>,Action>,Pair<SigmaOccupancyState<State,Action,Observation>,
            Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>>,Double> matrix = new HashMap<>();
    public GameMatrix(HashMap<Pair<Pair<History<Action,Observation>,Action>,Pair<SigmaOccupancyState<State,Action,Observation>,
            Pair<BehavioralStrategy<State,Action,Observation>, Vector<State,Action,Observation>>>>,Double> matrix){
        this.matrix = matrix;
    }
}
