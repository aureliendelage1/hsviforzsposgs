HSVI for zs-POSGs
=============================================
# Requirements :
* OS : linux
* install Gurobi
* install Cplex
* Jama

Note: In the short term CPlex will be completely replaced by Gurobi.

# You need to add to the lib directory : 
* gurobi.jar
* libGurobiJni91.so (a priori not libgurobi91.so )
* cplex.jar
* libcplex1280.so
* Jama-1.0.3.jar
Note: More recent versions are likely to work as well.

# GUROBI configuration :
(see also, e.g., https://www.gurobi.com/documentation/quickstart.html)

You need to set the environment variables for GUROBI as follows, where <installdir> is GUROBI's install directory:
- GUROBI_HOME should point to your <installdir>.
- PATH should be extended to include <installdir>/bin.
- LD_LIBRARY_PATH should be extended to include <installdir>/lib. 

For instance:
```shell
$ export GUROBI_HOME="/opt/gurobi911/linux64"
$ export PATH="${PATH}:${GUROBI_HOME}/bin"
$ export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
```

# compile with:

```shell
$ cd src/
$ javac -classpath .:../lib/cplex.jar:../lib/gurobi.jar:../lib/Jama-1.0.3.jar */*.java
```

# run with:
(from within src/)

```shell
$ java -Djava.library.path=.:../lib: -classpath .:../lib/cplex.jar:.:../lib/gurobi.jar  posg/Main -m="[method: lp || lc]" -p="[problem]" -h=[>=2]
```
where :
* "lp" is for the Convex-Concave and Lipschitz resolution using normal form games and linear programs.
* "lc" is for the only Lipschitz resolution using DOO.

Avaible problems are :
* "compTiger" for the Competitive Tiger problem,
* "advTiger" for the Adversarial Tiger problem,
* "recycling" for the Recycling problem, and
* "mabc" for the mabc problem.

For example:

```shell
$ java -Djava.library.path=.:../lib: -classpath .:../lib/cplex.jar:.:../lib/gurobi.jar  posg/Main -m="lp" -p="recycling" -h="3"
```

# additional options

To change the epsilon that was used in the experiments of our submission, an additional option -e="0.xxx" can be added. 
